<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],


    'facebook' => [
        'client_id' => '377405749439677',         
        'client_secret' => '6013e761dccfd0e9414df7f5561ca475', 
        'redirect' => env('APP_URL').'/login/facebook/callback',
    ],

    'google' => [
        'client_id' => '595567958878-986hj0elegfsrl8457l5v956ddol7hpq.apps.googleusercontent.com',         
        'client_secret' => 'IJKIw2TftUM5P4f4ambgat1g', 
        'redirect' => env('APP_URL').'/login/google/callback',
    ],

];
