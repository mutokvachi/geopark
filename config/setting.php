<?php return array (
  'social' => 
  array (
    'fb' => 'https://www.facebook.com/nationalparksofgeorgia/',
    'twitter' => 'https://google.ge',
    'vk' => 'https://google.ge',
    'instagram' => 'https://www.instagram.com/nationalparksofgeorgia/',
    'youtube' => 'https://www.youtube.com/channel/UCE4xj7oqWl4z6eHpsHqoNuw',
    'pinterest' => 'https://www.pinterest.com/nationalparksofgeorgia/',
  ),
  'footer_en' => 
  array (
    'ticket' => 
    array (
      'title' => 'Attraction Passes',
      'description' => 'You are able to  practice payable eco-tourist services at the National Parks of Georgia.',
      'link' => NULL,
    ),
    'bus' => 
    array (
      'title' => 'Transportation',
      'description' => 'Learn how to get to the National Parks of Georgia.',
      'link' => NULL,
    ),
    'guide' => 
    array (
      'title' => 'Visitor Guide',
      'description' => 'Plan your trip in National Parks of Georgia.',
      'link' => NULL,
    ),
    'map' => 
    array (
      'title' => 'MAPS',
      'description' => 'Plan your trip in National Parks of Georgia.',
      'link' => NULL,
    ),
    'info' => 
    array (
      'title' => 'ABOUT US',
      'description' => 'Dear travelers, we are glad to invite you to the National Parks of Georgia.',
      'link' => NULL,
    ),
  ),
  'footer_ka' => 
  array (
    'ticket' => 
    array (
      'title' => 'ფასიანი სერვისები',
      'description' => 'საქართველოს ეროვნულ პარკებში თქვენ შეგიძლიათ ისარგებლოთ ფასიანი ეკოტურისტული სერვისებით.',
      'link' => NULL,
    ),
    'bus' => 
    array (
      'title' => 'ტრანსპორტი',
      'description' => 'გაიგეთ როგორ შეგიძლიათ მოხვდეთ საქართველოს ეროვნულ პარკებში.',
      'link' => NULL,
    ),
    'guide' => 
    array (
      'title' => 'გზამკვლევი',
      'description' => 'დაგეგმეთ მოგზაურობა საქართველოს ეროვნულ პარკებში.',
      'link' => NULL,
    ),
    'map' => 
    array (
      'title' => 'რუკები',
      'description' => 'დაგეგმეთ მოგზაურობა საქართველოს ეროვნულ პარკებში.',
      'link' => NULL,
    ),
    'info' => 
    array (
      'title' => 'ჩვენს შესახებ',
      'description' => 'ძვირფასო მოგზაურებო, გიწვევთ საქართველოს ეროვნულ პარკებში სამოგზაუროდ.',
      'link' => NULL,
    ),
  ),
  'footer_ru' => 
  array (
    'ticket' => 
    array (
      'title' => 'attraction passes',
      'description' => 'ru Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis',
      'link' => NULL,
    ),
    'bus' => 
    array (
      'title' => 'transportation',
      'description' => 'ru Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis',
      'link' => NULL,
    ),
    'guide' => 
    array (
      'title' => 'VISITOR GUIDE',
      'description' => 'ru Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis',
      'link' => NULL,
    ),
    'map' => 
    array (
      'title' => 'MAPS',
      'description' => 'ru Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis',
      'link' => NULL,
    ),
    'info' => 
    array (
      'title' => 'BASIC INFO',
      'description' => 'ru Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis',
      'link' => NULL,
    ),
  ),
  'mainpage_ka' => 
  array (
    'title' => 'საქართველოს ეროვნული პარკები',
    'description' => 'ძვირფასო მოგზაურებო, გიწვევთ საქართველოს ეროვნულ პარკებში სამოგზაუროდ. საქართველოს გამორჩეული ბუნებრივი მემკვიდრეობის დაცულ ტერიტორიებზე თქვენ გელით უამრავი თავგადასავალი და დაუვიწყარი მოგონებები.',
  ),
  'mainpage_en' => 
  array (
    'title' => 'National Parks of Georgia',
    'description' => 'Dear travelers, You are invited to visit national parks of Georgia. You will find lots of adventure and unforgettable memories on Georgia\'s unique natural heritage National Parks.',
  ),
  'mainpage_ru' => 
  array (
    'title' => 'National Parks of Geoxxrgia',
    'description' => 'RU RasdasdasasdU Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  ),
);