<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripAdventure extends Model
{
    public function trip(){
    	return $this->belongsTo(TripStep::class, 'trip_id', 'id')->with('myTrip');
    }

    public function adventure(){
    	return $this->belongsTo(Adventure::class, 'adventure_id', 'id')->with(['park', 'imgs']);
    }
}
