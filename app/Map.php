<?php

namespace App;

use App\Helpers\Stuff;
use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    public static function getCoors(){
    	$coors = [];
        $map = Map::first();
        $center = ['lat'=> (float)explode(',', $map->center)[0], 'lng'=>(float)explode(',', $map->center)[1] ];
        $coors['center'] = $center;

        $lat = explode(',', $map->lat);
        $lng = explode(',', $map->lng);
        $info = explode(',', Stuff::trans($map, 'name'));

        $pins = [];
        foreach($lng as $key => $coor){
            if($key+1 > count($lat))
                break;
            if($key+1 > count($info))
                break;
            
            $pins[] = ['lat'=> (float)$lng[$key], 'lng'=> (float)$lat[$key], 'info'=> $info[$key] ];
        }
        $coors['pins'] = $pins;

        return $coors;
    }
}
