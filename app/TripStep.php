<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripStep extends Model
{
    public function adventures(){
    	return $this->hasMany(TripAdventure::class, 'trip_id', 'id')->with('adventure');
    }

    public function myTrip(){
    	return $this->belongsTo(Trip::class, 'trip_id', 'id');
    }
}
