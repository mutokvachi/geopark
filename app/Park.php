<?php

namespace App;

use DB;
use App\Helpers\Stuff;
use Illuminate\Database\Eloquent\Model;

class Park extends Model
{
    public static function getParkNames(&$parkNames){
    	$parks_id = DB::select('SELECT name, GROUP_CONCAT(id) as ids FROM parks GROUP BY name');
		$parkNames = [];
		foreach ($parks_id as $key => $park) {
			$parkNames[$park->ids] = $park->name;
		}
    }

    public static function getParkAliases(){
        $parks_id = DB::select('SELECT name, GROUP_CONCAT(alias) as ids FROM parks GROUP BY name');
        $parkNames = [];
        foreach ($parks_id as $key => $park) {
            $parkNames[$park->ids] = $park->name;
        }

        return $parkNames;
    }

    public function memories(){
        return $this->hasMany(Memory::class);
    }

    public function alerts(){
        return $this->hasMany(Alert::class);
    }

    public function arounds(){
        return $this->hasMany(AroundPark::class);
    }

    public function things(){
        return $this->hasMany(ToBuy::class)->orderBy('id', 'desc');
    }

    public function adventures(){
        return $this->hasMany(Adventure::class);
    }

    public function myAdventures(){
        return $this->hasMany(Adventure::class, 'park_id', 'id');
    }


    public function natures(){
        return $this->hasMany(Nature::class);
    }

    public function events(){
        return $this->hasMany(Event::class);
    }

    public function images(){
        return $this->hasMany(Image::class);
    }

    public function videos(){
        return $this->hasMany(Video::class);
    }

    public function toSees(){
        return $this->hasMany(ToSee::class)->limit(18);
    }

    public function getHere(){
        return $this->hasMany(GetHere::class); //removed ->limit(4)
    }

    public static function getContacts($data){
        $info = $data;

        $name = explode(',', Stuff::trans($info, 'specialist_name'));
        $number = explode(',', $info->specialist_mobile);
        $email = explode(',', $info->specialist_email);

        $results = [];
        foreach($name as $key => $result){
            if($key+1 > count($number))
                break;
            if($key+1 > count($email))
                break;
            
            $results[] = ['name'=> $name[$key], 'number'=> $number[$key], 'email'=> $email[$key] ];
        }

        return $results;
    }

}
