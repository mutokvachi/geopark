<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public static function getParkNames(){
    	$parks_id = DB::select('SELECT name, GROUP_CONCAT(id) as ids FROM regions GROUP BY name');
		$regionNames = [];
		foreach ($parks_id as $key => $park) {
			$regionNames[$park->ids] = $park->name;
		}
		return $regionNames;
    }
}
