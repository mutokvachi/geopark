<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AroundPark extends Model
{
	protected $table = 'aroundParks';

    public function place(){
    	return $this->belongsTo(Place::class);
    }

    public function park(){
    	return $this->belongsTo(Park::class);
    }
}
