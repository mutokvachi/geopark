<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToBuy extends Model
{
    protected $table = 'toBuy';

    public function park(){
    	return $this->belongsTo(Park::class);
    }

    public function shops(){
    	return $this->hasMany(Shop::class, 'thing_id', 'id');
    }
}
