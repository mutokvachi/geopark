<?php

namespace App\Helpers;

use App;
use Auth;
use Cookie;
use Session;
use DateTime;

class Stuff {
	public static function saveUser(){
		return Session::put('user',Auth::user());
	}

	public static function getUser(){
		return Session::get('user');
	}

	public static function uploader($file, &$name){
        $name = time().rand(0,100).$file->getClientOriginalName();
        $file->move(public_path('/main/img'), $name);
	}	

	public static function lang(){
		$lang = Cookie::get('locale');
		
		if(empty($lang))
			$lang = 'ka';

		return $lang;
	}

	public static function trans($obj, $column){
		$lang = request()->segment(1);
		if($lang == 'ka')
			return $obj->{$column};
		else if($lang == 'en' || $lang == 'ru'){
			$col = $column.'_'.$lang;
			return $obj->{$col};
		}else{
			return;
		}
	}

	public static function ogShare($title, $img, $description){
		return [
			'image' => $img,
			'title' => $title,
			'description' => $description
		];
	}

	public static function imgUrl($img, $defualt = false){
		if($defualt == true)
			return '/files/'.$img;

		return '/main/img/'.$img;
	}

	public static function drivingTime($km){
		$currency = 0.9;
		$minutes = $km*$currency;
		$hours = $minutes / 60;

		return $hours;
	}


	public static function getDate(){
		$today = new DateTime();

		$spring = new DateTime('March 20');
		$summer = new DateTime('June 20');
		$fall = new DateTime('September 22');
		$winter = new DateTime('December 21');

		switch(true) {
		    case $today >= $spring && $today < $summer:
		        $season = 2; // spring
		        break;

		    case $today >= $summer && $today < $fall:
		        $season = 3; // summer
		        break;

		    case $today >= $fall && $today < $winter:
		        $season = 4; // fall
		        break;

		    default:
		        $season = 1; //winter
		}

		return $season;
	}
}