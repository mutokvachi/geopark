<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    public static function getNames(){
    	$item_id = DB::select('SELECT name, GROUP_CONCAT(id) as ids FROM trips GROUP BY name');
		$regionNames = [];
		foreach ($item_id as $key => $item) {
			$regionNames[$item->ids] = $item->name;
		}
		return $regionNames;
    }

    public function park(){
    	return $this->belongsTo(Park::class);
    }

    public function steps(){
        return $this->hasMany(TripStep::class)->with('adventures');
    }
}
