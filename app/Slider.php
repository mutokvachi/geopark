<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    public static function getData($park = false){ 
    	if(isset($park) && !empty($park))
    		$data = Slider::where('park_id', $park)->orderBy('sort_by','asc')->get();
    	else
    		$data = Slider::orderBy('sort_by','asc')->limit(5)->get(); 

    	return $data;
    }
}
