<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Adventure extends Model
{
    public static function getTableNames(){
    	$parks_id = DB::select('SELECT name, GROUP_CONCAT(id) as ids FROM regions GROUP BY name');
		$tableNames = [];
		foreach ($parks_id as $key => $park) {
			$tableNames[$park->ids] = $park->name;
		}
		return $tableNames;
    }

     public static function getAdventureNames(){
        $parks_id = DB::select('SELECT name, GROUP_CONCAT(id) as ids FROM adventures GROUP BY name');
        $parkNames = [];
        foreach ($parks_id as $key => $park) {
            $parkNames[$park->ids] = $park->name;
        }

        return $parkNames;
    }

    public function park(){
    	return $this->belongsTo(Park::class);
    }

    public function imgs(){
        return $this->hasMany(AdventureImage::class);
    }

}
