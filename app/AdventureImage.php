<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdventureImage extends Model
{
    public function adventure(){
    	return $this->belongsTo(Adventure::class);
    } 
}
