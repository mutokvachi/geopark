<?php

namespace App;

use Session;
use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    public static function check($type, $type_id){
    	if(!Session::has('user'))
    		return false;

    	$user_id = Session::get('user')->id;
    	$favour = Favourite::where('user_id', $user_id)
	                ->where('type', $type)
	                ->where('type_id', $type_id)
	                ->first();

	    if($favour === null)
	    	return 0;
	    else
	    	return 1;
    }

    public function memory(){
    	return $this->hasOne(Memory::class, 'id', 'type_id')->with('park');
    }

    public function aroundPark(){
    	return $this->hasOne(AroundPark::class, 'id', 'type_id')->with('park');
    }

    public function trip(){
        return $this->hasOne(Trip::class, 'id', 'type_id')->with('park');
    }

    public function toSee(){
        return $this->hasOne(ToSee::class, 'id', 'type_id')->with('park');
    }
}
