<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class RedirectIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->type != 'admin') {
            return redirect()->route('adminLogout');
        }

        return $next($request);
    }
}
