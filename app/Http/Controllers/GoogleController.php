<?php

namespace App\Http\Controllers;

use App;
use Auth;
use Cookie;
use Session;
use App\User;
use Socialite;
use Illuminate\Http\Request;

class GoogleController extends Controller
{
    /**
     * Redirect the user to the google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
	    return Socialite::driver('google')->scopes(['profile','email'])->redirect();

        // return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request)
    {
    	if (!$request->has('code') || $request->has('denied')) {
            return redirect()->route('login');
        }

        $user = Socialite::driver('google')->stateless()->user();

        $checkUser = User::where('email', $user->email )->orWhere('email', $user->id)->first();
        
        $lang = Cookie::get('locale');
        if($checkUser !== null ){
            Auth::login($checkUser);
            $user = Auth::user();
            Session::put('user', $user);
     		return redirect()->route('profileView', ['lang' => $lang]);       
        }
        else{
            $newUser = User::create([
                "email"    => $user->email,
                "name"     => $user->name,
                "img"      => 'avatar.jpg',
                'birthday' => '0-0-0',
            ]);
            Auth::login($newUser);
            $user = Auth::user();
            Session::put('user', $user);
     		return redirect()->route('profile', ['lang' => $lang]);       
        }


        // $user->token;
    } 
}
