<?php

namespace App\Http\Controllers;

use App;
use Auth;
use Cookie;
use Session;
use App\User;
use Socialite;
use Illuminate\Http\Request;

class FacebookController extends Controller
{
	/**
     * Redirect the user to the facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request)
    {
        if (!$request->has('code') || $request->has('denied')) {
            return redirect()->route('login');
        }

        $user = Socialite::driver('facebook')->user();

        $checkUser = User::where('email', $user->email )->orWhere('email', $user->id)->first();
        
        $lang = Cookie::get('locale');
        if($checkUser !== null ){
            Auth::login($checkUser);
            $user = Auth::user();
            Session::put('user', $user);
            return redirect()->route('profileView', ['lang' => $lang]);       
        }
        else{
            $newUser = User::create([
                "email"    => $user->email,
                "name"     => $user->name,
                "img"      => 'avatar.jpg',
                'birthday' => '0-0-0',
            ]);
            Auth::login($newUser);
            $user = Auth::user();
            Session::put('user', $user);
            return redirect()->route('profile', ['lang' => $lang]);       
        }
        // $user->token;
    } 
}
