<?php

namespace App\Http\Controllers;

use DB;
use App;
use URL;
use Config;
use App\Map;
use App\Park;
use App\Shop;
use App\Trip;
use App\Place;
use App\Alert;
use App\ToBuy;
use App\Memory;
use App\Slider;
use App\Region;
use App\Comment;
use App\GetHere;
use App\Program;
use App\Favourite;
use App\BasicInfo;
use App\Recommend;
use App\Adventure;
use App\AroundPark;
use App\Downloadable;
use App\TripAdventure;
use App\Helpers\Stuff;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function home(Request $request){
        // dd(URL::to('/'), Config::get('app.url'));
        $domains = [
            'adventure2019.com' => 'https://nationalparks.ge',
            'adventure2021.com' => 'https://nationalparks.ge',
            'adventure2022.com' => 'https://nationalparks.ge',
            'adventure2023.com' => 'https://nationalparks.ge',
            'adventure2024.com' => 'https://nationalparks.ge',
            'adventure2025.com' => 'https://nationalparks.ge',
            'adventures2019.com' => 'https://nationalparks.ge',
            'adventures2020.com' => 'https://nationalparks.ge',
            'adventures2021.com' => 'https://nationalparks.ge',
            'adventures2022.com' => 'https://nationalparks.ge',
            'adventures2023.com' => 'https://nationalparks.ge',
            'adventures2024.com' => 'https://nationalparks.ge',
            'adventures2025.com' => 'https://nationalparks.ge',
            'algetinationalpark.com' => 'https://nationalparks.ge/ka/site/algetinp',
            'borjomi-kharagaulinationalpark.com' => 'https://nationalparks.ge/ka/site/borjomi-kharagaulinp',
            'destinations2018.com' => 'https://nationalparks.ge',
            'destinations2019.com' => 'https://nationalparks.ge',
            'destinations2020.com' => 'https://nationalparks.ge',
            'destinations2021.com' => 'https://nationalparks.ge',
            'destinations2022.com' => 'https://nationalparks.ge',
            'destinations2023.com' => 'https://nationalparks.ge',
            'destinations2024.com' => 'https://nationalparks.ge',
            'destinations2025.com' => 'https://nationalparks.ge',
            'georgiannature.com' => 'https://nationalparks.ge',
            'javakhetinationalpark.com' => 'https://nationalparks.ge/ka/site/javakhetinp',
            'kazbeginationalpark.com' => 'https://nationalparks.ge/ka/site/kazbeginp',
            'kinchkhanationalpark.com' => 'https://nationalparks.ge/ka/site/OkatseWaterfall',
            'kintrishinationalpark.com' => 'https://nationalparks.ge/ka/site/kintrishinp',
            'kobuletinationalpark.com' => 'https://nationalparks.ge/ka/site/kobuletinp',
            'kolkhetinationalpark.com' => 'https://nationalparks.ge/ka/site/kolxetinp',
            'lagodekhinationalpark.com' => 'https://nationalparks.ge/ka/site/lagodekhinp',
            'machakhelanationalpark.com' => 'https://nationalparks.ge/ka/site/machakhelanp',
            'martvilicanyon.com' => 'https://nationalparks.ge/ka/site/martvilicanyon',
            'martvilinationalpark.com' => 'https://nationalparks.ge/ka/site/martvilicanyon',
            'mtiralanationalpark.com' => 'https://nationalparks.ge/ka/site/mtiralanp',
            'nationalparksofgeorgia.com' => 'https://nationalparks.ge',
            'naturalmonument.com' => 'https://nationalparks.ge',
            'navenakhevicave.com' => 'https://nationalparks.ge/ka/site/NavenakheviCave',
            'okatsecanyon.com' => 'https://nationalparks.ge/ka/site/okatsecanyon',
            'okatsenationalpark.com' => 'https://nationalparks.ge/ka/site/okatsecanyon',
            'placemustsee.com' => 'https://nationalparks.ge',
            'placemustvisit.com' => 'https://nationalparks.ge',
            'satapliacave.com' => 'https://nationalparks.ge/ka/site/sataplia',
            'sataplianationalpark.com' => 'https://nationalparks.ge/ka/site/sataplia',
            'sights2018.com' => 'https://nationalparks.ge',
            'sights2019.com' => 'https://nationalparks.ge',
            'sights2020.com' => 'https://nationalparks.ge',
            'sights2021.com' => 'https://nationalparks.ge',
            'sights2022.com' => 'https://nationalparks.ge',
            'sights2023.com' => 'https://nationalparks.ge',
            'sights2024.com' => 'https://nationalparks.ge',
            'sights2025.com' => 'https://nationalparks.ge',
            'tbilisinationalpark.com' => 'https://nationalparks.ge/ka/site/tbilisinp',
            'traveldestination2018.com' => 'https://nationalparks.ge',
            'tripguide2018.com' => 'https://nationalparks.ge',
            'tushetinationalpark.com' => 'https://nationalparks.ge/ka/site/tushetinp',
            'vashlovaninationalpark.com' => 'https://nationalparks.ge/ka/site/vashlovaninp',
            'visitnationalpark.com' => 'https://nationalparks.ge',
            'wecreatememories.asia' => 'https://nationalparks.ge',
            'nationalpark.ge' => 'https://nationalparks.ge',
            'georgiannature.ge' => 'https://nationalparks.ge',
            'algetinationalpark.ge' => 'https://nationalparks.ge/ka/site/algetinp',
            'algetipark.ge' => 'https://nationalparks.ge/ka/site/algetinp',
            'borjomi-kharagaulinationalpark.ge' => 'https://nationalparks.ge/ka/site/borjomi-kharagaulinp',
            'borjominationalpark.ge' => 'https://nationalparks.ge/ka/site/borjomi-kharagaulinp',
            'vashlovaninationalpark.ge' => 'https://nationalparks.ge/ka/site/vashlovaninp',
            'tbilisinationalpark.ge' => 'https://nationalparks.ge/ka/site/tbilisinp',
            'tushetinationalpark.ge' => 'https://nationalparks.ge/ka/site/tushetinp',
            'kintrishinationalpark.ge' => 'https://nationalparks.ge/ka/site/kintrishinp',
            'kolkhetinationalpark.ge' => 'https://nationalparks.ge/ka/site/kolxetinp',
            'lagodekhinationalpark.ge' => 'https://nationalparks.ge/ka/site/lagodekhinp',
            'machakhelanationalpark.ge' => 'https://nationalparks.ge/ka/site/machakhelanp',
            'kobuletinationalpark.ge' => 'https://nationalparks.ge/ka/site/kobuletinp',
            'kazbeginationalpark.ge' => 'https://nationalparks.ge/ka/site/kazbeginp',
            'javakhetinationalpark.ge' => 'https://nationalparks.ge/ka/site/javakhetinp',
            'martvilinationalpark.ge' => 'https://nationalparks.ge/ka/site/martvilicanyon',
            'okatsenationalpark.ge' => 'https://nationalparks.ge/ka/site/okatsecanyon',
            'prometheuscave.ge' => 'https://nationalparks.ge/ka/site/prometheuscave',
            'prometheusnationalpark.ge' => 'https://nationalparks.ge/ka/site/prometheuscave',
            'sataplianationalpark.ge' => 'https://nationalparks.ge/ka/site/sataplia',
            'sataplia.ge' => 'https://nationalparks.ge/ka/site/sataplia',
            'satapliacave.ge' => 'https://nationalparks.ge/ka/site/sataplia',
            'kinckkha.ge' => 'https://nationalparks.ge/ka/site/OkatseWaterfall',
            'kinchkhawaterfall.ge' => 'https://nationalparks.ge/ka/site/OkatseWaterfall',
            'kinchkhanationalpark.ge' => 'https://nationalparks.ge/ka/site/OkatseWaterfall',
            'navenakhevi.ge' => 'https://nationalparks.ge/ka/site/navenakhevicave',
            'navenakhevicave.ge' => 'https://nationalparks.ge/ka/site/navenakhevicave',
            'mtirala.ge' => 'https://nationalparks.ge/ka/site/mtiralanp',
            'machakhela.ge' => 'https://nationalparks.ge/ka/site/machakhelanp',
            'okatse.ge' => 'https://nationalparks.ge/ka/site/okatsecanyon',
            'canyons.ge' => 'https://nationalparks.ge',
            'parks.ge' => 'https://nationalparks.ge',
            'caves.ge' => 'https://nationalparks.ge',
            'apa.ge' => 'https://nationalparks.ge',
            'okatsecanyon.ge' => 'https://nationalparks.ge/ka/site/okatsecanyon',
            'mtiralanationalpark.ge' => 'https://nationalparks.ge/ka/site/mtiralanp',
            'okatsewaterfall.ge' => 'https://nationalparks.ge/ka/site/OkatseWaterfall',
            'wecreatememories.ge' => 'https://nationalparks.ge',
            'martvilicanyons.ge' => 'https://nationalparks.ge/ka/site/martvilicanyon',
            'naturemonument.ge' => 'https://nationalparks.ge',
            'naturalmonument.ge' => 'https://nationalparks.ge',
            'naturalmonuments.ge' => 'https://nationalparks.ge',
            '100years.ge' => 'https://nationalparks.ge',
        ];

        // dd($request->redirect);

        if(isset($domains[$request->redirect]))
            return redirect($domains[$request->redirect]);

        // if(URL::to('/') != Config::get('app.url'))
        //     return redirect('https://nationalparks.ge');

        // return view('user.layouts.construction'); // remove for production

        return redirect()->route('index', ['lang'=> Stuff::lang()]);
    }

    public function geoAdventures(Request $request, $lang){
    	$slides = Slider::where('park_id', 'geoAdventures')->orderBy('sort_by','asc')->get();
    	$adventures = Adventure::where('park_id', 0)->with('imgs')->orderBy('name', 'asc')->get();

        $filter = [];
        if(empty($request->all()))
            $all_adventures = Adventure::where('park_id', '!=', 0)->with('park')->get()->groupBy('name');
        else{
            $query = [];
            foreach($request->all() as $key => $current){
                if(empty($current))
                    continue;

                if($key == 'park_id')
                    $query[] = [$key, '=', $current];
                else
                    $query[] = [$key, 'like', '%'.$current.'%'];
            }
            $all_adventures = Adventure::where('park_id', '!=', 0)
                ->where($query)
                ->with('park')
                ->get()
                ->groupBy('name');

            $filter['name'] = $request->name;
            $filter['park_id'] = $request->park_id;
            $filter['season'] = $request->seasons;
        }

        $grouped_adventures = Adventure::where('park_id', '!=', 0)->with('park')->get()->groupBy('name')->sortBy('name');
        $grouped_destinations = Park::all();


        $json = [];
        foreach($all_adventures as $key => $groupAdventure){
            foreach($groupAdventure as $adventure){
                $icon = $adventure->map_icon;
                $json[] = [
                    'activity' => $adventure->name,
                    'destination' => Stuff::trans($adventure->park, 'name'),
                    'ico' =>$icon,
                    'lat' => (float)json_decode($adventure->coordinates)[0]->lat,
                    'lng' => (float)json_decode($adventure->coordinates)[0]->lng,
                    'season' => $adventure->seasons,
                    'title'  => Stuff::trans($adventure, 'name').' - '.Stuff::trans($adventure->park, 'name'),
                    'url'   => route('siteAdventuresInner', ['lang'=>App::getLocale(), 'park'=>$adventure->park->alias, 'id' => $adventure->id])
                ];                 
            }
        }

    	return view('user.main.adventures.geoAdventure', compact('slides', 'adventures', 'json', 'grouped_adventures', 'grouped_destinations', 'filter'));
    }

    public function parks(Request $request, $lang){
        $slides = Slider::where('park_id', 'parkList')->orderBy('sort_by','asc')->get();
        $page = 'parks';

        if(isset($request->type)){
            if($request->type == 'region')
                $my_parks = Park::where('region_id', $request->target)->with('adventures')->orderBy('name', 'asc')->get();
            elseif($request->type == 'adventure'){
                $park_ids = Adventure::select('park_id')->where('name', $request->target)->get()->groupBy('park_id');
                $ids = [];
                foreach ($park_ids as $key => $value) {
                    $ids[] = $key;
                }
                $my_parks = Park::whereIn('id', $ids)->with('adventures')->orderBy('name', 'asc')->get();
            }else{
                $my_parks = Park::where('new_dest', 1)->with('adventures')->orderBy('name', 'asc')->get();
                $page = 'new_dest';
            }
        }
        else
            $my_parks = Park::with('adventures')->orderBy('name', 'asc')->get();

        $all_parks = Park::all();
        $all_adventures = Adventure::where('park_id', '!=', 0)->with('park')->get()->groupBy('name');

        $json = [];
        $categoryJson = [];
        foreach($all_adventures as $key => $groupAdventure){
            foreach($groupAdventure as $adventure){
                $json[] = ['height'=> floatval($adventure->sea_level),'position'=>$adventure->coordinates, 'name'=>Stuff::trans($adventure->park, 'name').' - '.Stuff::trans($adventure, 'name'), 'adventure_id' => $adventure->name, 'seasons' => $adventure->seasons];
                $categoryJson[$key][] = ['height'=> floatval($adventure->sea_level),'position'=>$adventure->coordinates, 'name'=>Stuff::trans($adventure->park, 'name').' - '.Stuff::trans($adventure, 'name'), 'adventure_id' => $adventure->name, 'seasons' => $adventure->seasons, 'park_alias'=>$adventure->park->alias, 'redirect_id'=> $adventure->id];                 
            }
        }

        $advJson = [];
        foreach($all_parks as $key => $current_park){
            $adventureString = '';
            foreach($current_park->myAdventures as $key => $my_adv){
                $adventureString .= Stuff::trans($my_adv, 'name').',';
            }
            $advJson[] = ['position'=>['lat'=>floatval($current_park->coordinate_lat), 'lng'=>floatval($current_park->coordinate_long)], 'title'=> Stuff::trans($current_park, 'name'), 'adventures' => $adventureString, 'regionID'=>$current_park->region_id, 'park_alias'=>$current_park->alias, 'occupation'=>$current_park->occupation];
        }
        
        $regions = Region::orderBy('name', 'asc')->get();
        $adventures = Adventure::where('park_id', '!=', 0)->groupBy('name')->get();

        // dd($my_parks);

        return view('user.main.park.list', compact('slides', 'my_parks', 'regions', 'adventures', 'page', 'all_adventures', 'json', 'categoryJson', 'all_parks', 'advJson'));
    }

    public function trips($lang, $filter = false){
        $slides = Slider::where('park_id', 'tripsPage')->orderBy('sort_by','asc')->get();

        if($filter){
            $filter = explode('=', $filter);
            if(isset($filter[1])){
                $trips = Trip::with('park')->where('park_id', $filter[1])->orderBy('id', 'desc')->get();
            }else{
                $tripsAvailable = TripAdventure::where('adventure_id', $filter)->with('trip')->orderBy('id', 'desc')->get();
                $trips = [];
                foreach($tripsAvailable as $available){
                    $trips[] = $available->trip->myTrip;
                }
            }
        }
        else    
            $trips = Trip::with('park')->orderBy('sort_by', 'asc')->limit(4)->get();

        // dd($trips);
        $sort_parks = Park::all();
        return view('user.main.trip.list', compact('slides', 'trips', 'sort_parks', 'filter'));
    }

    public function tourFilter(Request $request){
        $request = $request->all();
        unset($request['_token']);
        $query = [];
        foreach($request as $key => $req){
            // if(strpos($key, 'season') !== false){
            //     $query[] = [$key, 'like' ,'%'.$req.'%']; 
            //     continue;
            // }
            $query[] = [$key, '=' ,$req]; 
        }
        $events = Trip::where($query)->with('park')->get();

        return $events;
    }

    public function tourShowMore(Request $request){
        $skip = $request->page*4;
        $trips = Trip::with('park')->orderBy('sort_by', 'asc')->skip($skip)->limit(4)->get();

        return $trips;
    }

    public function index($lang){
        $slides = Slider::where('park_id', 'mainPage')->orderBy('sort_by','asc')->get();
        $adventures = Adventure::where('park_id', 0)->with('imgs')->get();

        $memories = Memory::where('status', 1)
            ->where('lang', App::getLocale())
            ->orderBy('id', 'desc')
            ->with('park')
            ->limit(4)
            ->get();
        $all_parks = Park::orderBy('name', 'asc')->with('myAdventures')->get();

        $all_adventures = Adventure::where('park_id', '!=', 0)->orderBy('name', 'asc')->with('park')->get()->groupBy('name');

        $json = [];
        $categoryJson = [];
        foreach($all_adventures as $key => $groupAdventure){
            foreach($groupAdventure as $adventure){
                $json[] = ['height'=> floatval($adventure->sea_level),'position'=>$adventure->coordinates, 'name'=>Stuff::trans($adventure->park, 'name').' - '.Stuff::trans($adventure, 'name'), 'adventure_id' => $adventure->name, 'seasons' => $adventure->seasons];
                $categoryJson[$key][] = ['height'=> floatval($adventure->sea_level),'position'=>$adventure->coordinates, 'name'=>Stuff::trans($adventure->park, 'name').' - '.Stuff::trans($adventure, 'name'), 'adventure_id' => $adventure->name, 'seasons' => $adventure->seasons, 'park_alias'=>$adventure->park->alias, 'redirect_id'=> $adventure->id];                 
            }
        }

        $advJson = [];
        foreach($all_parks as $key => $current_park){
            $adventureString = '';
            foreach($current_park->myAdventures as $key => $my_adv){
                $adventureString .= Stuff::trans($my_adv, 'name').',';
            }
            $advJson[] = ['position'=>['lat'=>floatval($current_park->coordinate_lat), 'lng'=>floatval($current_park->coordinate_long)], 'title'=> Stuff::trans($current_park, 'name'), 'adventures' => $adventureString, 'regionID'=>$current_park->region_id, 'park_alias'=>$current_park->alias, 'occupation'=>$current_park->occupation];
        }
        $regions = Region::orderBy('name', 'asc')->get();
        $recommends = Recommend::orderBy('id', 'desc')->with('park')->limit(4)->get();

        $coors = Map::getCoors();

        return view('user.main.index.index', compact('slides', 'adventures', 'memories', 'all_parks', 'recommends', 'all_adventures', 'json', 'categoryJson', 'all_adventures', 'regions', 'coors', 'advJson'));
    }

    private function aroundParks(){
        $ip = $_SERVER['REMOTE_ADDR'];
        $content = @file_get_contents("http://ip-api.com/json/$ip");
        $data = [];
        if ($content === false || !isset(json_decode($content)->lat)) {
            $my_parks = Park::all();
            $coordinates = null;

        }else{
            $location = json_decode($content);
            $user_lat = $location->lat;
            $user_lon = $location->lon;

            $sql = "SELECT *, ( 6371 * acos( cos( radians(" . $user_lat . ") ) * cos( radians( coordinate_lat ) ) * cos( radians( coordinate_long ) - radians(" . $user_lon . ") ) + sin( radians(" . $user_lat . ") ) * sin( radians( coordinate_lat ) ) ) ) AS distance FROM parks HAVING distance < 55555555 ORDER BY distance asc ";

            $my_parks = DB::select($sql);
            $coordinates = [$user_lat, $user_lon];
        }
        
        $data['coors'] = $coordinates;
        $data['parks'] = $my_parks;
        
        return $data;
    } 


    public function aroundMe($lang){
        $slides = Slider::getData();
        $all_parks = Park::select('alias','name', 'name_en', 'name_ru', 'coordinate_lat', 'coordinate_long')->get()->toArray();

        $data = $this->aroundParks();
        $my_parks = $data['parks'];
        $coordinates = $data['coors'];
        $year_season = Stuff::getDate();

        return view('user.main.aroundMe.list', compact('slides','my_parks', 'coordinates', 'year_season', 'all_parks'));
    }

    public function search(Request $request){
        $keyword = $request->keyword;
        $lang = $request->lang;


        if($lang == 'ka'){
            $name = 'name';
            $description = 'description';
        }
        else{
            $name = 'name_'.$lang;
            $description = 'description_'.$lang;
        }

        $parks = Park::where($name, 'like', '%'.$keyword.'%')
            ->orWhere($description, 'like', '%'.$keyword.'%')
            ->with('adventures')
            ->limit(20)
            ->get();

        $trips = Trip::where($name, 'like', '%'.$keyword.'%')
            ->with('park')
            ->limit(20)
            ->get();

        $adventures = Adventure::where($name, 'like', '%'.$keyword.'%')
            ->orWhere($description, 'like', '%'.$keyword.'%')
            // ->where('park_id','>', 0)
            ->with('park')
            ->limit(20)
            ->get();

        $arounds = AroundPark::where($name, 'like', '%'.$keyword.'%')
            ->orWhere($description, 'like', '%'.$keyword.'%')
            ->with('park')
            ->limit(20)
            ->get();


        $result = [];
        
        $result['parks'] = $parks;
        $result['trips'] = $trips;
        $result['adventures'] = $adventures;
        $result['arounds'] = $arounds;
        $result['keyword'] = null;


        if(isset($parks[0]) && empty($result['keyword']) ){
            if(strpos($parks[0][$name], $keyword) !== false) {
                $word = mb_substr($parks[0][$name], strpos($parks[0][$name], $keyword));
                $result['keyword'] = $word;                 
            }
        }

        if(isset($adventures[0]) && empty($result['keyword']) ){
            if(strpos($adventures[0][$name], $keyword) !== false) {
                $word = mb_substr($adventures[0][$name], strpos($adventures[0][$name], $keyword));
                $result['keyword'] = $word;                 
            }
        }


        if(isset($arounds[0]) && empty($result['keyword']) ){
            if(strpos($arounds[0][$name], $keyword) !== false) {
                $word = mb_substr($arounds[0][$name], strpos($arounds[0][$name], $keyword));
                $result['keyword'] = $word;                 
            }
        }

        // echo 'keyword - '.$result['keyword'];
        // return $result['keyword']; 

        return $result;
    }


    public function adventureSortPark(Request $request){
        $adventures = Adventure::where('name', $request->name)
            ->where('park_id', '>', 0)
            ->select('park_id')
            ->pluck('park_id');

        $parks = Park::findMany($adventures);

        return $parks;
    }

    public function services(){
        $buyTickets = [
            '20' => 'https://saleframe.guru.ge/?sid=24616&_ftm=HFYT34HGJHG&lang=',
            '13' => 'https://saleframe.guru.ge/?sid=21339&_ftm=HFYT34HGJHG&lang=',
            '9'  => 'https://saleframe.guru.ge/?sid=3157&_ftm=HFYT34HGJHG&lang=',
            '12' => 'https://saleframe.guru.ge/?sid=15820&_ftm=HFYT34HGJHG&lang=',
            // '21' => 'https://saleframe.guru.ge/?sid=16067&_ftm=HFYT34HGJHG&lang=',
            '14' => 'https://saleframe.guru.ge/?sid=24979&_ftm=HFYT34HGJHG&lang=',
        ];

        $slides = Slider::where('park_id', 'footerServices')->orderBy('sort_by','asc')->get(); 
        
        

        $data = $this->aroundParks();
        $my_parks = $data['parks'];
        $ordered_parks = [];
        foreach($my_parks as $my_park){
            $ordered_parks[$my_park->popularity] = $my_park;
        }
        krsort($ordered_parks);
        $my_parks = $ordered_parks;
        $year_season = Stuff::getDate();

        $lng = Stuff::lang();
        if(isset($slides[0]->img))
            $meta_img = Stuff::imgUrl(json_decode($slides[0]->img)[0], true);
        else
            $meta_img = null;
        $meta = Stuff::ogShare(Config::get("setting.footer_$lng.ticket.title"), $meta_img, Config::get("setting.footer_$lng.ticket.description"));

        return view('user.footer.services', compact('slides', 'my_parks', 'meta', 'year_season', 'buyTickets'));
    }

    

    public function guide(){
        $slides = Slider::where('park_id', 'footerGuide')->orderBy('sort_by','asc')->get(); 
        
        $data = $this->aroundParks();
        $my_parks = $data['parks'];
        $downloadables = Downloadable::get()->keyBy('park_id');

        $year_season = Stuff::getDate();

        $lng = Stuff::lang();
        if(isset($slides[0]->img))
            $meta_img = Stuff::imgUrl(json_decode($slides[0]->img)[0], true);
        else
            $meta_img = null;
        $meta = Stuff::ogShare(Config::get("setting.footer_$lng.guide.title"), $meta_img, Config::get("setting.footer_$lng.guide.description"));

        return view('user.footer.guide', compact('slides', 'my_parks', 'meta', 'year_season', 'downloadables'));
    }

    public function basicInfo(){
        $slides = Slider::where('park_id', 'footerInfo')->orderBy('sort_by','asc')->get(); 
        $infos = BasicInfo::all();

        $lng = Stuff::lang();
        if(isset($slides[0]->img))
            $meta_img = Stuff::imgUrl(json_decode($slides[0]->img)[0], true);
        else
            $meta_img = null;
        $meta = Stuff::ogShare(Config::get("setting.footer_$lng.info.title"), $meta_img, Config::get("setting.footer_$lng.info.description"));

        return view('user.footer.basicInfo', compact('slides', 'meta', 'infos'));
    }

    public function transportation($lang, $parkAlias = null){
        $slides = Slider::where('park_id', 'footerTransportation')->orderBy('sort_by','asc')->get(); 
        $data = $this->aroundParks();
        $my_parks = $data['parks'];
        $year_season = Stuff::getDate();

        if(isset($parkAlias)){
            $optional = true;
            $park = Park::where('alias', $parkAlias)->first();
            $heres = GetHere::where('park_id', $park->id)->get();
        }
        else
            $optional = false;


        $lng = Stuff::lang();
        if(isset($slides[0]->img))
            $meta_img = Stuff::imgUrl(json_decode($slides[0]->img)[0], true);
        else
            $meta_img = null;

        $meta = Stuff::ogShare(Config::get("setting.footer_$lng.bus.title"), $meta_img, Config::get("setting.footer_$lng.bus.description"));

        return view('user.footer.transportation', compact('slides', 'my_parks', 'meta', 'optional', 'heres', 'park', 'year_season'));
    }


    public function programs($lang, $id){
        $slides = Slider::getData(); 
        $program = Program::where('program', $id)->first();

        return view('user.footer.programs', compact('slides', 'program'));
    }

    public function notFound(){
        
        return view('user.main.404.index');
    }

}
