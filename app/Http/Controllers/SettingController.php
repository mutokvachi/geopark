<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function settings(){
    	$settings = Config::get('setting');
    	
    	// $result = '<?php return '.var_export($settings, true).';';
    	// $path = config_path().'/setting.php';
    	// file_put_contents($path, $result);

    	
    	return view('admin.settings.index', compact('settings'));
    }

    public function settingsEdit(Request $request){
        $settings = Config::get('setting');
    	
        $settings['social']['fb'] = $request->social_fb;
        $settings['social']['twitter'] = $request->social_twitter;
        $settings['social']['vk'] = $request->social_vk;
        $settings['social']['instagram'] = $request->social_instagram;
        $settings['social']['youtube'] = $request->social_youtube;
        $settings['social']['pinterest'] = $request->social_pinterest;
        $settings['footer_ka']['ticket']['title']  = $request->footer_ka_ticket_title;
        $settings['footer_ka']['ticket']['description']  = $request->footer_ka_ticket_description;
        $settings['footer_ka']['ticket']['link']  = $request->footer_ka_ticket_link;
        $settings['footer_ka']['bus']['title']  = $request->footer_ka_bus_title;
        $settings['footer_ka']['bus']['description']  = $request->footer_ka_bus_description;
        $settings['footer_ka']['bus']['link']  = $request->footer_ka_bus_link;
        $settings['footer_ka']['guide']['title']  = $request->footer_ka_guide_title;
        $settings['footer_ka']['guide']['description']  = $request->footer_ka_guide_description;
        $settings['footer_ka']['guide']['link']  = $request->footer_ka_guide_link;
        $settings['footer_ka']['map']['title']  = $request->footer_ka_map_title;
        $settings['footer_ka']['map']['description']  = $request->footer_ka_map_description;
        $settings['footer_ka']['map']['link']  = $request->footer_ka_map_link;
        $settings['footer_ka']['info']['title']  = $request->footer_ka_info_title;
        $settings['footer_ka']['info']['description']  = $request->footer_ka_info_description;
        $settings['footer_ka']['info']['link']  = $request->footer_ka_info_link;
        $settings['footer_en']['ticket']['title']  = $request->footer_en_ticket_title;
        $settings['footer_en']['ticket']['description']  = $request->footer_en_ticket_description;
        $settings['footer_en']['ticket']['link']  = $request->footer_en_ticket_link;
        $settings['footer_en']['bus']['title']  = $request->footer_en_bus_title;
        $settings['footer_en']['bus']['description']  = $request->footer_en_bus_description;
        $settings['footer_en']['bus']['link']  = $request->footer_en_bus_link;
        $settings['footer_en']['guide']['title']  = $request->footer_en_guide_title;
        $settings['footer_en']['guide']['description']  = $request->footer_en_guide_description;
        $settings['footer_en']['guide']['link']  = $request->footer_en_guide_link;
        $settings['footer_en']['map']['title']  = $request->footer_en_map_title;
        $settings['footer_en']['map']['description']  = $request->footer_en_map_description;
        $settings['footer_en']['map']['link']  = $request->footer_en_map_link;
        $settings['footer_en']['info']['title']  = $request->footer_en_info_title;
        $settings['footer_en']['info']['description']  = $request->footer_en_info_description;
        $settings['footer_en']['info']['link']  = $request->footer_en_info_link;
        $settings['footer_ru']['ticket']['title'] = $request->footer_ru_ticket_title;
        $settings['footer_ru']['ticket']['description'] = $request->footer_ru_ticket_description;
        $settings['footer_ru']['ticket']['link'] = $request->footer_ru_ticket_link;
        $settings['footer_ru']['bus']['title'] = $request->footer_ru_bus_title;
        $settings['footer_ru']['bus']['description'] = $request->footer_ru_bus_description;
        $settings['footer_ru']['bus']['link'] = $request->footer_ru_bus_link;
        $settings['footer_ru']['guide']['title'] = $request->footer_ru_guide_title;
        $settings['footer_ru']['guide']['description'] = $request->footer_ru_guide_description;
        $settings['footer_ru']['guide']['link'] = $request->footer_ru_guide_link;
        $settings['footer_ru']['map']['title'] = $request->footer_ru_map_title;
        $settings['footer_ru']['map']['description'] = $request->footer_ru_map_description;
        $settings['footer_ru']['map']['link'] = $request->footer_ru_map_link;
        $settings['footer_ru']['info']['title'] = $request->footer_ru_info_title;
        $settings['footer_ru']['info']['description'] = $request->footer_ru_info_description;
        $settings['footer_ru']['info']['link'] = $request->footer_ru_info_link;
        $settings['mainpage_ka']['title'] = $request->mainpage_ka_title;
        $settings['mainpage_ka']['description'] = $request->mainpage_ka_description;
        $settings['mainpage_en']['title'] = $request->mainpage_en_title;
        $settings['mainpage_en']['description'] = $request->mainpage_en_description;
        $settings['mainpage_ru']['title'] = $request->mainpage_ru_title;
        $settings['mainpage_ru']['description'] = $request->mainpage_ru_description;

        $result = '<?php return '.var_export($settings, true).';';
        $path = config_path().'/setting.php';
        file_put_contents($path, $result);

    	return redirect()->back()->with('success', 'წარმატებით რედაქტირდა !');
    }
}
