<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InstagramController extends Controller
{

    public function index($hashtag){
		$hashtag = $hashtag;
		$url = "https://www.instagram.com/explore/tags/".$hashtag."/";
		$max = 100; // Number of images to show - 20 is max
		$count = 0;
		$instagram_content = file_get_contents($url);
		preg_match_all('/window._sharedData = (.*)\;\<\/script\>/', $instagram_content, $matches);
		$txt  = implode('', $matches[1]);
		$json = json_decode($txt);
		$arr = [];

		foreach ($json->entry_data->TagPage{0}->graphql->hashtag->edge_hashtag_to_media->edges AS $item) {
			if($count >= $max)
			break;
			
			$arr[] = $item->node->display_url; 
			$count++;
		}
		return $arr;
    }
}

