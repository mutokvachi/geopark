<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Hash;
use App\Map;
use App\Park;
use App\Trip;
use App\Shop;
use Validator;
use App\Place;
use App\Event;
use App\Alert;
use App\ToBuy;
use App\ToSee;
use App\Image;
use App\Video;
use App\Region;
use App\Nature;
use App\Slider;
use App\Memory;
use App\Program;
use App\GetHere;
use App\TripStep;
use App\Instagram;
use App\BasicInfo;
use App\Recommend;
use App\Favourite;
use App\Adventure;
use App\Subscriber;
use App\AroundPark;
use App\Downloadable;
use App\TripAdventure;
use App\AdventureImage;
use Illuminate\Http\Request;

class AdminController extends Controller
{
	public function adminLogin(){
		return view('admin.login');
	}

	public function index(){
		return view('admin.index');
	}

	public function adminAuth(Request $request){
		if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('adminIndex');
        }

		return redirect()->back();
	}

	public function adminLogout(){
		$user = Auth::user();
        Auth::logout($user);

        return redirect()->route('adminLogin');
	}


	public function parksView(){
		$items = Park::orderBy('id', 'desc')->get();
		$regionNames = Region::getParkNames();

		return view('admin.park.index', compact('items', 'regionNames'));
	}

	public function parkAddView(){
		$regions = DB::table('regions')->get();
		$adventures = Adventure::all();
		
		return view('admin.park.add', compact('regions', 'adventures'));
	}


	private function uploader($file, &$name, $optional = false){
        $name = time().rand(0,100).$file->getClientOriginalName();
        if($optional == true){
            $file->move(public_path('/img'), $name);
        }else{
            $file->move(public_path('/files'), $name);
        }
    }

    private function dataInsertor($request, $modelName){
    	$items = $request->all();
		unset($items['_token']);

		if(!empty($request->file())){
			$files = $request->file();
			foreach($files as $key => $file){
				unset($items[$key]);
				foreach($file as $current_file){
					$this->uploader($current_file, $name);
					$items[$key][] = $name;
				}
			}
		}

		foreach($items as $key => $item){
			if(is_array($item)){
				$items[$key] = json_encode($item);
			}
		}

		DB::table($modelName)->insert($items);
    }

    private function dataUpdater($request, $modelName, $id, $imageEditable = false){
    	$items = $request->all();
		unset($items['_token']);

		if(!empty($request->file())){
			$files = $request->file();
			if($imageEditable == false){
				foreach($files as $key => $file){
					unset($items[$key]);
					foreach($file as $current_file){
						$this->uploader($current_file, $name);
						$items[$key][] = $name;
					}
				}
			}else{
				$images = DB::table($modelName)->where('id', $id)->pluck($imageEditable);
				foreach($files as $key => $file){
					unset($items[$key]);
					if(!empty($images[0])){
						// $this->uploader($file, $name);
						// $items[$imageEditable][] = $name;
						foreach(json_decode($images[0]) as $image){
							$items[$key][] = $image;
						}
					}
					foreach($file as $current_file){
						$this->uploader($current_file, $name);
						$items[$key][] = $name;
					}
				}
				
			}
		}

		foreach($items as $key => $item){
			if(is_array($item)){
				$items[$key] = json_encode($item);
			}
		}

		DB::table($modelName)->where('id', $id)->update($items);
    }



    private function validateRequired($request,  &$status,$dontValidate = ['']){
		$rules = [];
		$validates = $request->all(); 

		foreach($validates as $key => $validate){
			if(in_array($key, $dontValidate)){
				$rules[$key] = '';
				continue;
			}
			$rules[$key] = 'required';
		}
		$validator = Validator::make($request->all(), $rules);
		
		$status = [];
		if($validator->fails()){
			$status['status'] = 'fail';
			$status['message'] = $validator->errors();
		}
		else
			$status['status'] = 'success';

    }


	public function parkAdd(Request $request){
		$dontValidate = ['name_en',
			'description_en',
			'operating_hours_1_en',
			'operating_hours_2_en',
			'operating_hours_3_en',
			'operating_hours_4_en',
			'fees_passes_1_en',
			'fees_passes_2_en',
			'fees_passes_3_en',
			'fees_passes_4_en',
			'guide_en',
			'guideline_en',
			'name_ru',
			'description_ru',
			'operating_hours_1_ru',
			'operating_hours_2_ru',
			'operating_hours_3_ru',
			'operating_hours_4_ru',
			'fees_passes_1_ru',
			'fees_passes_2_ru',
			'fees_passes_3_ru',
			'fees_passes_4_ru',
			'guide_ru',
			'guideline_ru',
			'sea_level_en',
			'sea_level_ru',
			'administration_address_en',
			'administration_address_ru',

			'operating_hours_1',
			'operating_hours_2',
			'operating_hours_3',
			'operating_hours_4',
			'fees_passes_1',
			'fees_passes_2',
			'fees_passes_3',
			'fees_passes_4',
			'specialist_name_ru',
			'specialist_name_en',
			'attachments',
		];

		$this->validateRequired($request, $status, $dontValidate);
		
		if($status['status'] == 'fail') 
			dd($status['message']->all());
		
		// if($status['status'] == 'fail'){
		// 	return redirect()->back()->with('error', 'დაფიქსირდა შეცდომა')
		// 							 ->with('errorBag', $status['message']->keys());
		// }


		$this->dataInsertor($request, 'parks');

		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}


	public function parkEditView($id){
		$item = Park::where('id', $id)->first();
		$regions = Region::all();
		$adventures = Adventure::all();


		return view('admin.park.edit', compact('item', 'regions', 'adventures'));
	}

	public function parksFileDel($id, $name){
		$this->multipleImageDelete('parks', $id, 'attachments', $name);

		return redirect()->back();
	}

	public function parkEdit(Request $request, $id){
		$dontValidate = ['name_en',
			'description_en',
			'operating_hours_1_en',
			'operating_hours_2_en',
			'operating_hours_3_en',
			'operating_hours_4_en',
			'fees_passes_1_en',
			'fees_passes_2_en',
			'fees_passes_3_en',
			'fees_passes_4_en',
			'guide_en',
			'guideline_en',
			'name_ru',
			'description_ru',
			'operating_hours_1_ru',
			'operating_hours_2_ru',
			'operating_hours_3_ru',
			'operating_hours_4_ru',
			'fees_passes_1_ru',
			'fees_passes_2_ru',
			'fees_passes_3_ru',
			'fees_passes_4_ru',
			'guide_ru',
			'guideline_ru',
			'sea_level_en',
			'sea_level_ru',
			'administration_address_en',
			'administration_address_ru',

			'operating_hours_1',
			'operating_hours_2',
			'operating_hours_3',
			'operating_hours_4',
			'fees_passes_1',
			'fees_passes_2',
			'fees_passes_3',
			'fees_passes_4',
			'specialist_name_ru',
			'specialist_name_en',
			'attachments',
		];


		$this->validateRequired($request, $status, $dontValidate);
		
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		if($request->has('attachments'))
			$this->dataUpdater($request, 'parks', $id, 'attachments');
		else
			$this->dataUpdater($request, 'parks', $id);
		
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function parkDelete($id){

		$park = Park::where('id', $id)->first();

		Park::where('id', $id)->delete();
		Adventure::where('park_id', $id)->delete();
		DB::table('things')->where('park_id', $id)->delete();
		Image::where('park_id', $id)->delete();
		Video::where('park_id', $id)->delete();
		

		$favAroundParks = AroundPark::where('park_id', $id)->pluck('id');
		if(!empty($favAroundParks)){
			Favourite::where('type', 'aroundInner')->whereIn('type_id', $favAroundParks)->delete();
			AroundPark::where('park_id', $id)->delete();
		}
		
		Event::where('park_id', $id)->delete();
		Alert::where('park_id', $id)->delete();


		$tobuy = DB::table('toBuy')->where('park_id', $id)->pluck('id');
		if(!empty($tobuy)){
			Shop::whereIn('thing_id', $tobuy)->delete();
			Shop::where('park_id', $id)->delete();
			DB::table('toBuy')->where('park_id', $id)->delete();
		}
		
		$favMemories = Memory::where('park_id', $id)->pluck('id');
		if(!empty($favMemories)){
			Favourite::where('type', 'memoriesInner')->whereIn('type_id', $favMemories)->delete();
			Memory::where('park_id', $id)->delete();
		}

		Slider::where('park_id', $park->alias)->delete();
		Nature::where('park_id', $id)->delete();



		$trip = Trip::where('park_id', $park->id)->pluck('id');
		if(!empty($trip)){
			$tripStep = TripStep::whereIn('trip_id', $trip)->pluck('id');
			TripStep::whereIn('trip_id', $trip)->delete();
			Favourite::where('type', 'tripsInner')->whereIn('type_id', $trip)->delete();
			if(!empty($tripStep)){
				TripAdventure::whereIn('trip_id', $tripStep)->delete();
			}
			Trip::where('park_id', $id)->delete();
		}


		Recommend::where('park_id', $id)->delete();

		$favToSee = ToSee::where('park_id', $id)->pluck('id');
		if(!empty($favToSee)){
			Favourite::where('type', 'toSeeInner')->whereIn('type_id', $favToSee)->delete();
			ToSee::where('park_id', $id)->delete();
		}

		GetHere::where('park_id', $id)->delete();
		Downloadable::where('park_id', $id)->delete();
		Instagram::where('park_id', $id)->delete();

		return redirect()->route('parksView');
	}

	public function test(){
		
		dd('test');
	}

	public function parkTranslateView($id){
		$item = Park::where('id', $id)->first();
		return view('admin.park.translate', compact('item'));
	}

	public function regionsView(){
		$items = DB::table('regions')->get();

		return view('admin.region.index', compact('items'));
	}

	public function regionAddView(){
		return view('admin.region.add');
	}

	public function regionAdd(Request $request){
		$dontValidate = ['name_ru', 'name_en'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'regions');

		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function regionEditView($id){
		$item = DB::table('regions')->where('id', $id)->first();

		return view('admin.region.edit', compact('item'));
	}

	public function regionEdit(Request $request, $id){
		$dontValidate = ['name_ru', 'name_en'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'regions', $id);
		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}


	public function adventuresView(){
		$items = DB::table('adventures')->get();
		Park::getParkNames($parkNames);

		return view('admin.adventure.index', compact('items', 'parkNames'));
	}

	public function adventuresAddView(){
		$parks = Park::all();

		return view('admin.adventure.add', compact('parks'));
	}

	public function adventuresAdd(Request $request){
		$dontValidate = ['name_ru', 'name_en', 'description_ru', 'description_en'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'adventures');

		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function adventuresEditView($id){
		$item = DB::table('adventures')->where('id', $id)->first();
		$parks = Park::all();

		return view('admin.adventure.edit', compact('item', 'parks'));
	}

	public function adventuresEdit(Request $request,$id){
		
		$dontValidate = ['name_ru', 'name_en', 'description_ru', 'description_en'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		if($request->has('images')){
			$this->dataUpdater($request, 'adventures', $id, 'images');
		}else{
			$this->dataUpdater($request, 'adventures', $id);
		}

		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}

	public function adventuresDelete($id){
		DB::table('adventures')->where('id', $id)->delete();
		TripAdventure::where('adventure_id', $id)->delete();
		AdventureImage::where('adventure_id', $id)->delete();

		return redirect()->route('adventuresView');
	}

	private function multipleImageDelete($table, $id, $column, $name){
		$images = DB::table($table)->where('id', $id)->pluck($column)->first();

		$images = json_decode($images);
		unset($images[array_search($name,$images)]);
		$query = [];
		foreach($images as $image){
			$query[] = $image;
		}
		$images = json_encode($query);
		DB::table($table)->where('id', $id)->update([$column => $images]);
	}

	public function adventuresImageDel($id, $name){
		$this->multipleImageDelete('adventures', $id, 'images', $name);

		return redirect()->back();
	}

	public function thingsToSeeView(){
		$items = DB::table('things')->get();
		Park::getParkNames($parkNames);

		return view('admin.thingsToSee.index', compact('items', 'parkNames'));
	}

	public function thingsToSeeDelete($id){
		DB::table('things')->where('id', $id)->delete();
		Shop::where('thing_id', $id)->delete();

		return redirect()->route('thingsToSeeView');

	}

	public function thingsToSeeAddView(){
		$parks = Park::all();
		return view('admin.thingsToSee.add', compact('parks'));
	}

	public function thingsToSeeAdd(Request $request){
		$dontValidate = ['name_ru', 'name_en', 'description_ru', 'description_en', 'location_en', 'location_ru', 'road_to_en', 'road_to_ru', 'overnight_link'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'things');

		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function thingsToSeeEditView($id){
		$item = DB::table('things')->where('id', $id)->first();
		$parks = Park::all();

		return view('admin.thingsToSee.edit', compact('item', 'parks'));
	}

	public function thingsToSeeEdit(Request $request, $id){
		$dontValidate = ['name_ru', 'name_en', 'description_ru', 'description_en', 'location_en', 'location_ru', 'road_to_en', 'road_to_ru', 'overnight_link'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'things', $id);
		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}

	public function imageGalleryView(){
		$items = DB::table('images')->get();

		Park::getParkNames($parkNames);

		return view('admin.imageGallery.index', compact('items', 'parkNames'));
	}

	public function imageGalleryAddView(){
		$parks = Park::all();
		return view('admin.imageGallery.add', compact('parks'));
	}

	public function imageGalleryAdd(Request $request){
		$dontValidate = ['name_ru', 'name_en'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'images');

		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function imageGalleryEditView($id){
		$item = DB::table('images')->where('id', $id)->first();
		$parks = Park::all();

		return view('admin.imageGallery.edit', compact('item', 'parks'));
	}

	public function imageGalleryEdit(Request $request, $id){
		$dontValidate = ['name_ru', 'name_en'];
		
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'images', $id);
		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}

	public function imageGalleryDelete($id){
		DB::table('images')->where('id', $id)->delete();

		return redirect()->route('imageGalleryView');
	}

	public function videoGalleryView(){
		$items = DB::table('videos')->get();

		Park::getParkNames($parkNames);
		return view('admin.videoGallery.index', compact('items', 'parkNames'));
	}

	public function videoGalleryAddView(){
		$parks = Park::all();
		return view('admin.videoGallery.add', compact('parks'));
	}

	public function videoGalleryAdd(Request $request){
		$dontValidate = ['name_ru', 'name_en', 'youtube_link'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'videos');

		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function videoGalleryEditView($id){
		$item = DB::table('videos')->where('id', $id)->first();
		$parks = Park::all();

		return view('admin.videoGallery.edit', compact('item', 'parks'));
	}

	public function videoGalleryEdit(Request $request, $id){
		$dontValidate = ['name_ru', 'name_en', 'youtube_link'];

		$this->validateRequired($request, $status,$dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'videos', $id);
		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}

	public function videoGalleryDelete($id){
		DB::table('videos')->where('id', $id)->delete();

		return redirect()->route('videoGalleryView');
	}

	public function aroundParkView(){
		$items = AroundPark::with('place')->get();
		
		Park::getParkNames($parkNames);
		return view('admin.aroundPark.index', compact('items', 'parkNames'));
	}	

	public function aroundParkAddView(){
		$parks = Park::all();
		$places = Place::all();
		return view('admin.aroundPark.add', compact('parks', 'places'));
	}

	public function aroundParkAdd(Request $request){
		$not_required = [
			'name_en',
			'name_ru',
			'price',
			'working_hours',
			'mobile',
			'email',
			'website',
			'description_en',
			'description_ru',
			'rating',
			'address_en',
			'address_ru',
			'contact_person_en',
			'contact_person_ru',
			'fb_page',
			'payment',
			'parking',
			'music',
			'kitchen',
			'wifi',
		];

		$this->validateRequired($request, $status, $not_required);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'aroundParks', 'image');

		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function aroundParkDelete($id){
		DB::table('aroundParks')->where('id', $id)->delete();
		
		Favourite::where('type', 'aroundInner')
			->where('type_id', $id)
			->delete();

		return redirect()->route('aroundParkView');
	}

	public function aroundParkEditView($id){
		$item = AroundPark::with('place')->where('id', $id)->first();
		$parks = Park::all();
		$places = Place::all();

		return view('admin.aroundPark.edit', compact('item', 'parks', 'places'));
	}

	public function aroundParkEdit(Request $request, $id){
		$not_required = ['name_en',
			'name_ru',
			'price',
			'working_hours',
			'mobile',
			'email',
			'website',
			'description_en',
			'description_ru',
			'rating',
			'address_en',
			'address_ru',
			'contact_person_en',
			'contact_person_ru',
			'fb_page',
			'payment',
			'parking',
			'music',
			'kitchen',
			'wifi',
		];

		// checkboxes system
		$checkers = [
			"payment" => null,
			"parking" => null,
			"music"   => null,
			"kitchen" => null,
			"wifi"    => null,
		];
		foreach($checkers as $key => $value){
			if(!$request->has($key))	
				$request->request->add([$key => null]);
		}
		// checkbox system end
			
		$this->validateRequired($request, $status, $not_required);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'aroundParks', $id, 'image');
		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}

	public function aroundParkImageDel($id, $name){
		$this->multipleImageDelete('aroundParks', $id, 'image', $name);

		return redirect()->back();
	}

	public function eventsView(){
		$items = DB::table('events')->get();

		Park::getParkNames($parkNames);
		return view('admin.events.index', compact('items', 'parkNames'));
	}

	public function eventsDelete($id){
		DB::table('events')->where('id', $id)->delete();

		return redirect()->route('eventsView');
	}

	public function eventsAddView(){
		$parks = Park::all();
		return view('admin.events.add', compact('parks'));
	}

	public function eventsAdd(Request $request){
		$dontValidate = ['name_en',
			'name_ru',
			'location_en',
			'location_ru',
			'description_en',
			'description_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'events');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function eventsEditView($id){
		$item = DB::table('events')->where('id', $id)->first();
		$parks = Park::all();

		return view('admin.events.edit', compact('item', 'parks'));
	}

	public function eventsEdit(Request $request, $id){
		$dontValidate = ['name_en',
			'name_ru',
			'location_en',
			'location_ru',
			'description_en',
			'description_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'events', $id);
		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}

	public function alertsView(){
		$items = DB::table('alerts')->get();

		Park::getParkNames($parkNames);
		return view('admin.alerts.index', compact('items', 'parkNames'));
	}

	public function alertsDelete($id){
		DB::table('alerts')->where('id', $id)->delete();

		return redirect()->route('alertsView');
	}

	public function alertsAddView(){
		$parks = Park::all();
		return view('admin.alerts.add', compact('parks'));
	}

	public function alertsAdd(Request $request){
		$dontValidate = [
			'description_en',
			'description_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'alerts');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function alertsEditView($id){
		$item = DB::table('alerts')->where('id', $id)->first();
		$parks = Park::all();

		return view('admin.alerts.edit', compact('item', 'parks'));
	}

	public function alertsEdit(Request $request, $id){
		$dontValidate = [
			'description_en',
			'description_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'alerts', $id);
		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}

	public function toBuyView(){
		$items = DB::table('toBuy')->get();

		Park::getParkNames($parkNames);
		return view('admin.toBuy.things.index', compact('items', 'parkNames'));
	}

	public function toBuyAddView(){
		$parks = Park::all();
		return view('admin.toBuy.things.add', compact('parks'));
	}

	public function toBuyAdd(Request $request){
		$dontValidate = ['name_ru', 'name_en', 'description_ru', 'description_en', 'type_en', 'type_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'toBuy');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function toBuyEditView($id){
		$item = DB::table('toBuy')->where('id', $id)->first();
		$parks = Park::all();

		return view('admin.toBuy.things.edit', compact('item', 'parks'));
	}

	public function toBuyEdit(Request $request,$id){
		$dontValidate = ['name_ru', 'name_en', 'description_ru', 'description_en', 'type_en', 'type_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'toBuy', $id, 'images');
		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}

	public function toBuyImageDel($id, $name){
		$this->multipleImageDelete('toBuy', $id, 'images', $name);

		return redirect()->back();
	}

	public function toBuyDelete($id){
		DB::table('toBuy')->where('id', $id)->delete();
		DB::table('shops')->where('thing_id', $id)->delete();

		return redirect()->route('toBuyView');
	}

	public function toBuyShopsView(){
		$items = DB::table('shops')->get();
		Park::getParkNames($parks);

		$things_id = DB::select('SELECT name, GROUP_CONCAT(id) as ids FROM toBuy GROUP BY name');
		$thingsName = [];
		foreach ($things_id as $key => $thing) {
			$thingsName[$thing->ids] = $thing->name;
		}

		return view('admin.toBuy.shops.index', compact('items', 'thingsName', 'parks'));
	}

	public function toBuyShopsAddView(){
		$things = DB::table('toBuy')->get();
		$parks = Park::all();
		return view('admin.toBuy.shops.add', compact('things', 'parks'));
	}

	public function toBuyShopsAdd(Request $request){
		$dontValidate = ['name_en',
			'name_ru',
			'address_en',
			'address_ru',
			'region_en',
			'region_ru',
			'phone',
			'email',
			'website',
			'rating',
			'type_en',
			'type_ru',
			'park_id',
			'thing_id',
		];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'shops');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function toBuyShopsEditView($id){
		$item = DB::table('shops')->where('id', $id)->first();
		$things = DB::table('toBuy')->get();
		$parks = Park::all();

		return view('admin.toBuy.shops.edit', compact('item', 'things', 'parks'));

	}

	public function toBuyShopsEdit(Request $request,$id){
		$dontValidate = ['name_en',
			'name_ru',
			'address_en',
			'address_ru',
			'region_en',
			'region_ru',
			'phone',
			'email',
			'website',
			'rating',
			'type_en',
			'type_ru',
			'park_id',
			'thing_id',
		];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'shops', $id, 'images');
		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}

	public function toBuyShopsDelete($id){
		DB::table('shops')->where('id', $id)->delete();

		return redirect()->route('toBuyShopsView');

	}

	public function markerDeletor($table,$column, $id){
		DB::table($table)->where('id', $id)->update([$column => '[]']);

		return redirect()->back();
	}


	// Place

	public function placeView(){
		$info = [
			'title'      => 'კატეგორიების სია',
			'add_region' => 'კატეგორიის დამატება',
			'edit_view'  => route('placeEditView', ['id' => 1]),
			'add_view'   => route('placeAddView')
		];
		$items = Place::all();

		return view('admin.place.index', compact('info', 'items'));
	}

	public function placeAddView(){
		$info = [
			'title'         => 'კატეგორიის დამატება',
			'redirect_back' => route('placeView'),
			'form_action'   => route('placeAdd'),
		];

		return view('admin.place.add', compact('info'));
	}

	public function placeAdd(Request $request){
		$dontValidate = ['type_en','type_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'places');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function placeEditView($id){
		$info = [
			'title'         => 'კატეგორიის რედაქტირება',
			'redirect_back' => route('placeView'),
			'form_action'   => route('placeEdit', ['id' => $id]),
			'delete_route'  => route('placeDelete', ['id' => $id]),
		];
		$item = Place::where('id', $id)->first();

		return view('admin.place.edit', compact('info', 'item'));
	}

	public function placeEdit(Request $request, $id){
		$dontValidate = ['type_en','type_ru'];
		
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'places', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function placeDelete(Request $request, $id){
		Place::where('id', $id)->delete();
		AroundPark::where('place_id', $id)->delete();
		return redirect()->route('placeView');
	}

	// Memory
	public function memoryView(){
		$items = Memory::with('user')->get();

		return view('admin.memory.index', compact('items'));
	}

	public function memoryAddView(){
		return view('admin.memory.add');
	}

	public function memoryAdd(Request $request){
		$dontValidate = ['name_ru', 'name_en'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'memories');

		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function memoryEditView($id){
		$item = Memory::where('id', $id)->with('park')->first();

		return view('admin.memory.edit', compact('item'));
	}

	public function memoryEdit(Request $request, $id){
		$dontValidate = ['name_ru', 'name_en'];
		$this->validateRequired($request, $status, $dontValidate);
	
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'memories', $id);
		return redirect()->back()->with('success', 'წარმატებით რედაქტირდა');
	}

	// ---- Gallery

	public function sliderView(){
		$info = [
			'title'      => 'სლაიდების სია',
			'add_region' => 'სლაიდების დამატება',
			'edit_view'  => route('sliderEditView', ['id' => 1]),
			'add_view'   => route('sliderAddView')
		];
		$items = Slider::all();
		$parkAliases = Park::getParkAliases();

		return view('admin.slider.index', compact('info', 'items', 'parkAliases'));
	}

	public function sliderAddView(){
		$info = [
			'title'         => 'სლაიდების დამატება',
			'redirect_back' => route('sliderView'),
			'form_action'   => route('sliderAdd'),
		];
		$parks = Park::all();
		return view('admin.slider.add', compact('info', 'parks'));
	}

	public function sliderAdd(Request $request){
		$dontValidate = ['name_en','name_ru', 'youtube_link', 'img', 'btn_link', 'sort_by'];

		if(empty($request->img) && empty($request->youtube_link))
			dd('აირჩიეთ სურათი ან შეიყვანეთ იუთუბის ლინკი !!');

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'sliders');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function sliderEditView($id){
		$info = [
			'title'         => 'სლაიდების რედაქტირება',
			'redirect_back' => route('sliderView'),
			'form_action'   => route('sliderEdit', ['id' => $id]),
			'delete_route'  => route('sliderDelete', ['id' => $id]),
		];
		$item = Slider::where('id', $id)->first();
		$parks = Park::all();

		return view('admin.slider.edit', compact('info', 'item', 'parks'));
	}

	public function sliderEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru', 'youtube_link', 'img', 'btn_link', 'sort_by'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'sliders', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function sliderDelete(Request $request, $id){
		Slider::where('id', $id)->delete();
		return redirect()->route('sliderView');
	}

	// ---- nature

	public function natureView(){
		$info = [
			'title'      => 'Nature & Culture სია',
			'add_region' => 'Nature & Culture დამატება',
			'edit_view'  => route('natureEditView', ['id' => 1]),
			'add_view'   => route('natureAddView')
		];
		$items = Nature::all();
		$my_parks = Park::getParkNames($parks);
		return view('admin.nature.index', compact('info', 'items', 'parks'));
	}

	public function natureAddView(){
		$info = [
			'title'         => 'Nature & Culture დამატება',
			'redirect_back' => route('natureView'),
			'form_action'   => route('natureAdd'),
		];
		$parks = Park::all();
		return view('admin.nature.add', compact('info', 'parks'));
	}

	public function natureAdd(Request $request){
		$dontValidate = ['name_en','name_ru', 'upper_description_en', 'upper_description_ru', 'lower_description','lower_description_ru', 'lower_description_en'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'natures');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function natureEditView($id){
		$info = [
			'title'         => 'Nature & Culture რედაქტირება',
			'redirect_back' => route('natureView'),
			'form_action'   => route('natureEdit', ['id' => $id]),
			'delete_route'  => route('natureDelete', ['id' => $id]),
		];
		$item = Nature::where('id', $id)->first();
		$parks = Park::all();

		return view('admin.nature.edit', compact('info', 'item', 'parks'));
	}

	public function natureEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru', 'upper_description_en', 'upper_description_ru', 'lower_description','lower_description_ru', 'lower_description_en'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'natures', $id, 'images');
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function natureDelete(Request $request, $id){
		Nature::where('id', $id)->delete();
		return redirect()->route('natureView');
	}

	public function natureImageDel($id, $name){
		$this->multipleImageDelete('natures', $id, 'images', $name);

		return redirect()->back();
	}


	// ---- trip

	public function tripView(){
		$info = [
			'title'      => 'ტურების სია',
			'add_region' => 'ტურის დამატება',
			'edit_view'  => route('tripEditView', ['id' => 1]),
			'add_view'   => route('tripAddView')
		];
		$items = Trip::all();
		$my_parks = Park::getParkNames($parks);
		return view('admin.trip.index', compact('info', 'items', 'parks'));
	}

	public function tripAddView(){
		$info = [
			'title'         => 'ტურის დამატება',
			'redirect_back' => route('tripView'),
			'form_action'   => route('tripAdd'),
		];
		$parks = Park::all();
		return view('admin.trip.add', compact('info', 'parks'));
	}

	public function tripAdd(Request $request){
		$dontValidate = ['name_en','name_ru', 'route_en', 'route_ru', 'highlights_en', 'highlights_ru', 'sort_by'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'trips');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function tripEditView($id){
		$info = [
			'title'         => 'ტურის რედაქტირება',
			'redirect_back' => route('tripView'),
			'form_action'   => route('tripEdit', ['id' => $id]),
			'delete_route'  => route('tripDelete', ['id' => $id]),
		];
		$item = Trip::where('id', $id)->first();
		$parks = Park::all();

		return view('admin.trip.edit', compact('info', 'item', 'parks'));
	}

	public function tripEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru', 'route_en', 'route_ru', 'highlights_en', 'highlights_ru', 'sort_by'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'trips', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function tripDelete(Request $request, $id){
		Trip::where('id', $id)->delete();
		$tripStep = TripStep::where('trip_id', $id)->pluck('id'); 
		if(!empty($tripStep)){
			TripStep::where('trip_id', $id)->delete();
			TripAdventure::whereIn('trip_id', $tripStep)->delete();
		}

		Favourite::where('type', 'tripsInner')
			->where('type_id', $id)
			->delete();

		return redirect()->route('tripView');
	}

	// ---- tripStep

	public function tripStepView(){
		$info = [
			'title'      => 'ტურების დღეების სია',
			'add_region' => 'ტურის დღეების დამატება',
			'edit_view'  => route('tripStepEditView', ['id' => 1]),
			'add_view'   => route('tripStepAddView')
		];
		$items = TripStep::all();
		$my_trips = Trip::getNames();
		return view('admin.tripSteps.index', compact('info', 'items', 'my_trips'));
	}

	public function tripStepAddView(){
		$info = [
			'title'         => 'ტურის დღეების დამატება',
			'redirect_back' => route('tripStepView'),
			'form_action'   => route('tripStepAdd'),
		];
		$trips = Trip::all();
		return view('admin.tripSteps.add', compact('info', 'trips'));
	}

	public function tripStepAdd(Request $request){
		$dontValidate = ['name_en','name_ru', 'route_en', 'route_ru', 'highlights_en', 'highlights_ru','description_en', 'description_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'trip_steps');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function tripStepEditView($id){
		$info = [
			'title'         => 'ტურის დღეების რედაქტირება',
			'redirect_back' => route('tripStepView'),
			'form_action'   => route('tripStepEdit', ['id' => $id]),
			'delete_route'  => route('tripStepDelete', ['id' => $id]),
		];
		$item = TripStep::where('id', $id)->first();
		$trips = Trip::all();

		return view('admin.tripSteps.edit', compact('info', 'item', 'trips'));
	}

	public function tripStepEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru', 'route_en', 'route_ru', 'highlights_en', 'highlights_ru','description_en', 'description_ru'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'trip_steps', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function tripStepDelete(Request $request, $id){
		TripStep::where('id', $id)->delete();
		TripAdventure::where('trip_id', $id)->delete();

		return redirect()->route('tripStepView');
	}

	// subscribeControllView

	public function subscribeControllView(){
		$info = [
			'title'      => 'subscribe',
			'add_region' => 'subscribe',
			'edit_view'  => route('tripStepEditView', ['id' => 1]),
			'add_view'   => route('tripStepAddView')
		];
		$items = Subscriber::all();
		
		return view('admin.subscribe.index', compact('info', 'items'));
	}

	// ---- TripAdventure

	public function tripAdventureView(){
		$info = [
			'title'      => 'ტურების და თავგადასავლების კავშირის სია',
			'add_region' => 'კავშირის დამატება',
			'edit_view'  => route('tripAdventureEditView', ['id' => 1]),
			'add_view'   => route('tripAdventureAddView')
		];
		$items = TripAdventure::with(['trip','adventure'])->get();
		$my_parks = Park::getParkNames($parks);

		return view('admin.tripAdventure.index', compact('info', 'items', 'parks'));
	}

	public function tripAdventureAddView(){
		$info = [
			'title'         => 'კავშირის დამატება',
			'redirect_back' => route('tripAdventureView'),
			'form_action'   => route('tripAdventureAdd'),
		];
		$steps = TripStep::all();
		$adventures = Adventure::where('park_id', '>', 0)->with('park')->get();

		return view('admin.tripAdventure.add', compact('info', 'steps', 'adventures'));
	}

	public function tripAdventureAdd(Request $request){
		$this->validateRequired($request, $status);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'trip_adventures');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function tripAdventureEditView($id){
		$info = [
			'title'         => 'კავშირის რედაქტირება',
			'redirect_back' => route('tripAdventureView'),
			'form_action'   => route('tripAdventureEdit', ['id' => $id]),
			'delete_route'  => route('tripAdventureDelete', ['id' => $id]),
		];
		$item = TripAdventure::where('id', $id)->first();
		$steps = TripStep::all();
		$adventures = Adventure::where('park_id', '>', 0)->with('park')->get();
		
		return view('admin.tripAdventure.edit', compact('info', 'item', 'steps', 'adventures'));
	}

	public function tripAdventureEdit(Request $request, $id){
		$this->validateRequired($request, $status);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'trip_adventures', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function tripAdventureDelete(Request $request, $id){
		TripAdventure::where('id', $id)->delete();

		return redirect()->route('tripAdventureView');
	}


	// ---- Recommend

	public function recommendView(){
		$info = [
			'title'      => 'რეკომენდაციები სია',
			'add_region' => 'რეკომენდაციები დამატება',
			'edit_view'  => route('recommendEditView', ['id' => 1]),
			'add_view'   => route('recommendAddView')
		];
		$items = Recommend::all();
		$my_parks = Park::getParkNames($parks);
		return view('admin.recommend.index', compact('info', 'items', 'parks'));
	}

	public function recommendAddView(){
		$info = [
			'title'         => 'რეკომენდაციები დამატება',
			'redirect_back' => route('recommendView'),
			'form_action'   => route('recommendAdd'),
		];
		$parks = Park::all();
		return view('admin.recommend.add', compact('info', 'parks'));
	}

	public function recommendAdd(Request $request){
		$dontValidate = ['name_en','name_ru', 'description_en', 'description_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'recommends');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function recommendEditView($id){
		$info = [
			'title'         => 'რეკომენდაციები რედაქტირება',
			'redirect_back' => route('recommendView'),
			'form_action'   => route('recommendEdit', ['id' => $id]),
			'delete_route'  => route('recommendDelete', ['id' => $id]),
		];
		$item = Recommend::where('id', $id)->first();
		$parks = Park::all();

		return view('admin.recommend.edit', compact('info', 'item', 'parks'));
	}

	public function recommendEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru', 'description_en', 'description_ru'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'recommends', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function recommendDelete(Request $request, $id){
		Recommend::where('id', $id)->delete();
		return redirect()->route('recommendView');
	}

	public function recommendImageUpload(Request $request){
		$file = $request->file;
		$this->uploader($file, $name);
		$link = asset('/files/'.$name);
		return $link;
	}


	// ---- getHere

	public function getHereView(){
		$info = [
			'title'      => 'მისასვლელი ადგილების სია',
			'add_region' => 'მისასვლელი ადგილების დამატება',
			'edit_view'  => route('getHereEditView', ['id' => 1]),
			'add_view'   => route('getHereAddView')
		];
		$items = GetHere::all();
		$my_parks = Park::getParkNames($parks);
		return view('admin.getHere.index', compact('info', 'items', 'parks'));
	}

	public function getHereAddView(){
		$info = [
			'title'         => 'მისასვლელი ადგილების დამატება',
			'redirect_back' => route('getHereView'),
			'form_action'   => route('getHereAdd'),
		];
		$parks = Park::all();
		return view('admin.getHere.add', compact('info', 'parks'));
	}

	public function getHereAdd(Request $request){
		$dontValidate = ['name_en','name_ru', 'description_en', 'description_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'get_heres');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function getHereEditView($id){
		$info = [
			'title'         => 'მისასვლელი ადგილების რედაქტირება',
			'redirect_back' => route('getHereView'),
			'form_action'   => route('getHereEdit', ['id' => $id]),
			'delete_route'  => route('getHereDelete', ['id' => $id]),
		];
		$item = GetHere::where('id', $id)->first();
		$parks = Park::all();

		return view('admin.getHere.edit', compact('info', 'item', 'parks'));
	}

	public function getHereEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru', 'description_en', 'description_ru'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'get_heres', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function getHereDelete(Request $request, $id){
		GetHere::where('id', $id)->delete();
		return redirect()->route('getHereView');
	}


	// ---- ToSee
	public function toSeeView(){
		$info = [
			'title'      => 'სანახავი ადგილების სია',
			'add_region' => 'სანახავი ადგილების დამატება',
			'edit_view'  => route('toSeeEditView', ['id' => 1]),
			'add_view'   => route('toSeeAddView')
			];
		$items = ToSee::all();
		$my_parks = Park::getParkNames($parks);
		return view('admin.toSee.index', compact('info', 'items', 'parks'));
	}

	public function toSeeAddView(){
		$info = [
			'title'         => 'სანახავი ადგილების დამატება',
			'redirect_back' => route('toSeeView'),
			'form_action'   => route('toSeeAdd'),
		];
		$parks = Park::all();
		return view('admin.toSee.add', compact('info', 'parks'));
	}

	

	public function toSeeAdd(Request $request){
		$dontValidate = ['name_en','name_ru', 'description_en', 'description_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'to_sees');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function toSeeEditView($id){
		$info = [
			'title'         => 'სანახავი ადგილების რედაქტირება',
			'redirect_back' => route('toSeeView'),
			'form_action'   => route('toSeeEdit', ['id' => $id]),
			'delete_route'  => route('toSeeDelete', ['id' => $id]),
		];
		$item = ToSee::where('id', $id)->first();
		$parks = Park::all();
		return view('admin.toSee.edit', compact('info', 'item', 'parks'));
	}


	public function toSeeEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru', 'description_en', 'description_ru'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'to_sees', $id, 'images');
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function toSeeDelete(Request $request, $id){
		ToSee::where('id', $id)->delete();
		
		Favourite::where('type', 'toSeeInner')
			->where('type_id', $id)
			->delete();

		return redirect()->route('toSeeView');
	}

	public function toSeeImageDel($id, $name){
		$this->multipleImageDelete('to_sees', $id, 'images', $name);

		return redirect()->back();
	}

	// map

	public function mapEditView($id){
		$info = [
			'title'         => 'რუქის რედაქტირება',
			'form_action'   => route('mapEdit', ['id' => $id]),
		];
		$item = Map::where('id', $id)->first();
		$parks = Park::all();

		return view('admin.map.edit', compact('info', 'item', 'parks'));
	}

	public function mapEdit(Request $request, $id){
		$dontValidate = ['name_en', 'name_ru'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'maps', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	// Downloadable
	public function downloadableView(){
		$info = [
			'title'      => 'გადმოსაწეი ფაილების სია',
			'add_region' => 'გადმოსაწეი ფაილების დამატება',
			'edit_view'  => route('downloadableEditView', ['id' => 1]),
			'add_view'   => route('downloadableAddView')
			];
		$items = Downloadable::all();
		$my_parks = Park::getParkNames($parks);

		return view('admin.downloadable.index', compact('info', 'items', 'parks'));
	}

	public function downloadableAddView(){
		$info = [
			'title'         => 'გადმოსაწეი ფაილების დამატება',
			'redirect_back' => route('downloadableView'),
			'form_action'   => route('downloadableAdd'),
		];
		$parks = Park::all();
		return view('admin.downloadable.add', compact('info', 'parks'));
	}

	public function downloadableAdd(Request $request){
		$dontValidate = ['name_en','name_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'downloadables');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function downloadableEditView($id){
		$info = [
			'title'         => 'გადმოსაწეი ფაილების რედაქტირება',
			'redirect_back' => route('downloadableView'),
			'form_action'   => route('downloadableEdit', ['id' => $id]),
			'delete_route'  => route('downloadableDelete', ['id' => $id]),
		];
		$item = Downloadable::where('id', $id)->first();
		$parks = Park::all();
		return view('admin.downloadable.edit', compact('info', 'item', 'parks'));
	}


	public function downloadableEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'downloadables', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function downloadableDelete(Request $request, $id){
		Downloadable::where('id', $id)->delete();
		return redirect()->route('downloadableView');
	}


	// ---- Instagram
	public function instagramView(){
		$info = [
			'title'      => 'Instagram ჰეშტეგების სია',
			'add_region' => 'Instagram ჰეშტეგების დამატება',
			'edit_view'  => route('instagramEditView', ['id' => 1]),
			'add_view'   => route('instagramAddView')
			];
		$items = Instagram::all();
		$my_parks = Park::getParkNames($parks);
		return view('admin.instagram.index', compact('info', 'items', 'parks'));
	}

	public function instagramAddView(){
		$info = [
			'title'         => 'Instagram ჰეშტეგების დამატება',
			'redirect_back' => route('instagramView'),
			'form_action'   => route('instagramAdd'),
		];
		$parks = Park::all();
		return view('admin.instagram.add', compact('info', 'parks'));
	}

	

	public function instagramAdd(Request $request){

		$this->validateRequired($request, $status);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'instagrams');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function instagramEditView($id){
		$info = [
			'title'         => 'Instagram ჰეშტეგების რედაქტირება',
			'redirect_back' => route('instagramView'),
			'form_action'   => route('instagramEdit', ['id' => $id]),
			'delete_route'  => route('instagramDelete', ['id' => $id]),
		];
		$item = Instagram::where('id', $id)->first();
		$parks = Park::all();
		return view('admin.instagram.edit', compact('info', 'item', 'parks'));
	}


	public function instagramEdit(Request $request, $id){
		$this->validateRequired($request, $status);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'instagrams', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function instagramDelete(Request $request, $id){
		Instagram::where('id', $id)->delete();
		return redirect()->route('instagramView');
	}


	// ---- BasicInfo
	public function basicInfoView(){
		$info = [
			'title'      => 'Basic Info სია',
			'add_region' => 'Basic Info დამატება',
			'edit_view'  => route('basicInfoEditView', ['id' => 1]),
			'add_view'   => route('basicInfoAddView')
			];
		$items = BasicInfo::all();
		return view('admin.basicInfo.index', compact('info', 'items'));
	}

	public function basicInfoAddView(){
		$info = [
			'title'         => 'Basic Info დამატება',
			'redirect_back' => route('basicInfoView'),
			'form_action'   => route('basicInfoAdd'),
		];
		return view('admin.basicInfo.add', compact('info'));
	}


	public function basicInfoAdd(Request $request){
		$dontValidate = ['name_en','name_ru', 'description_en', 'description_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'basic_infos');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function basicInfoEditView($id){
		$info = [
			'title'         => 'Basic Info რედაქტირება',
			'redirect_back' => route('basicInfoView'),
			'form_action'   => route('basicInfoEdit', ['id' => $id]),
			'delete_route'  => route('basicInfoDelete', ['id' => $id]),
		];
		$item = BasicInfo::where('id', $id)->first();
		return view('admin.basicInfo.edit', compact('info', 'item'));
	}


	public function basicInfoEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru', 'description_en', 'description_ru'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'basic_infos', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function basicInfoDelete(Request $request, $id){
		BasicInfo::where('id', $id)->delete();
		return redirect()->route('basicInfoView');
	}


	// ---- programs
	public function programView(){
		$info = [
			'title'      => 'რეინჯერების პროგრამების სია',
			'add_region' => 'რეინჯერების პროგრამის დამატება',
			'edit_view'  => route('programEditView', ['id' => 1]),
			'add_view'   => route('programAddView')
			];
		$items = Program::all();
		return view('admin.program.index', compact('info', 'items'));
	}

	public function programAddView(){
		$info = [
			'title'         => 'რეინჯერების პროგრამის დამატება',
			'redirect_back' => route('programView'),
			'form_action'   => route('programAdd'),
		];
		return view('admin.program.add', compact('info'));
	}


	public function programAdd(Request $request){
		$dontValidate = ['name_en','name_ru', 'description_en', 'description_ru'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'programs');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function programEditView($id){
		$info = [
			'title'         => 'რეინჯერების პროგრამის რედაქტირება',
			'redirect_back' => route('programView'),
			'form_action'   => route('programEdit', ['id' => $id]),
			'delete_route'  => route('programDelete', ['id' => $id]),
		];
		$item = Program::where('id', $id)->first();
		return view('admin.program.edit', compact('info', 'item'));
	}


	public function programEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru', 'description_en', 'description_ru'];
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'programs', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function programDelete(Request $request, $id){
		Program::where('id', $id)->delete();
		return redirect()->route('programView');
	}


	// ---- adventureImage
	public function adventureImageView(){
		$info = [
			'title'      => 'თავგადასავლების სურათების სია',
			'add_region' => 'თავგადასავლების სურათის დამატება',
			'edit_view'  => route('adventureImageEditView', ['id' => 1]),
			'add_view'   => route('adventureImageAddView')
			];
		$items = AdventureImage::with('adventure')->get();
		$parksnames = Park::getParkNames($parks);

		return view('admin.adventureImage.index', compact('info', 'items', 'adventures', 'parks'));
	}

	public function adventureImageAddView(){
		$info = [
			'title'         => 'თავგადასავლების სურათის დამატება',
			'redirect_back' => route('adventureImageView'),
			'form_action'   => route('adventureImageAdd'),
		];

		$adventures = Adventure::with('park')->get();

		return view('admin.adventureImage.add', compact('info', 'adventures'));
	}


	public function adventureImageAdd(Request $request){
		$dontValidate = ['name_en','name_ru', 'link'];

		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataInsertor($request, 'adventure_images');
		return redirect()->back()->with("success", 'წარმატებით დაემატა !');
	}

	public function adventureImageEditView($id){
		$info = [
			'title'         => 'თავგადასავლების სურათის რედაქტირება',
			'redirect_back' => route('adventureImageView'),
			'form_action'   => route('adventureImageEdit', ['id' => $id]),
			'delete_route'  => route('adventureImageDelete', ['id' => $id]),
		];
		$item = AdventureImage::where('id', $id)->first();
		$adventures = Adventure::with('park')->get();

		return view('admin.adventureImage.edit', compact('info', 'item', 'adventures'));
	}


	public function adventureImageEdit(Request $request, $id){
		$dontValidate = ['name_en','name_ru', 'link'];
		
		$this->validateRequired($request, $status, $dontValidate);
		if($status['status'] == 'fail') 
			dd($status['message']->all());

		$this->dataUpdater($request, 'adventure_images', $id);
		return redirect()->back()->with("success", 'წარმატებით რედაქტირდა !');
	}

	public function adventureImageDelete(Request $request, $id){
		AdventureImage::where('id', $id)->delete();
		return redirect()->route('adventureImageView');
	}

}
