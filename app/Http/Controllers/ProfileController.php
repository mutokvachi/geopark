<?php

namespace App\Http\Controllers;

use App;
use Auth;
use Hash;
use Cookie;
use Session;
use App\User;
use App\Park;
use Validator;
use App\Memory;
use App\Comment;
use App\Favourite;
use App\Subscriber;
use App\AroundPark;
use App\Helpers\Stuff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;

class ProfileController extends Controller
{
	public function register(Request $request){
		$rules = [
			"name"                  => "required|min:5|max:30|regex:/^[\pL\s\-]+$/u",
			"email"                 => "required|min:5|email|unique:users,email|max:70",
			"password"              => "required|min:5|max:20|confirmed",
			"password_confirmation" => "required|max:20",
			// "terms"                 => "required",
			"lang"                  => "required",
		];

		App::setLocale($request->lang);

		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			return Redirect::to(URL::previous()."#register")
				->with('errors',$validator->messages()->all())
				->withInput();
		}

		$newuser = User::create([
			"name"     => $request->name,
			"email"    => $request->email,
			"password" => Hash::make($request->password),
			"type"     => "user",
			"img"      => "avatar.jpg",
			"birthday" => "0-0-0",       
		]);

		Auth::login($newuser);
		Session::put('user', $newuser);

		return redirect()->route('profile', ['lang' => $request->lang]);
	}

	public function authenticate(Request $request, $lang){
		App::setLocale($lang);
		if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'type' => 'user'])) {
			Stuff::saveUser();
            return redirect()->route('profileView', ['lang' => $lang]);
        }
        else{
        	return redirect()->back()->with('error', __('auth.failed'));
        }

	}

    public function loginView(){
    	return view('user.profile.login');
    }

    public function profile(){
    	$user = Stuff::getUser();
    	return view('user.profile.profile', compact('user'));
    }

    public function profileView(){
    	$user = Stuff::getUser();
    	return view('user.profile.profileView', compact('user'));
    }

    public function profileEdit(Request $request){
    	App::setLocale(Stuff::lang());
    	$rules = [
			"name"     => "required|min:5|max:30|regex:/^[\pL\s\-]+$/u",
			"idnumber" => "nullable|numeric|max:99999999999999999999",
			"birthday" => "nullable|max:35",
			"mobile"   => "nullable|max:35",
			"img"      => "nullable|mimes:jpeg,jpg,png,gif|max:20000"
		];
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			return redirect()->back()->with('errors',$validator->messages()->all());
		}

		$query = [
			"name"     => $request->name,
			"IDnumber" => $request->idnumber,
			"birthday" => $request->birthday,
			"mobile"   => $request->mobile,
		];
		if($request->has('img')){
			Stuff::uploader($request->file('img'), $name);
			$query["img"] = $name;
		}

		$user = Stuff::getUser();
		User::where('id', $user->id)->update($query);
		$newUser = User::where('id', $user->id)->first();
		Session::put('user', $newUser);

    	return redirect()->back()->with('success','successfull');
    }

    public function logout($lang){
    	Auth::logout(Auth::user());
    	Session::flush('user');

    	return redirect()->route('loginView', ['lang' => $lang]);
    }

    public function changePassView($lang){
    	return view('user.profile.changePass');
    }

    public function changePass(Request $request, $lang){
    	App::setLocale($lang);
    	$rules = [
    		"password"                  => "required|min:5|max:20",
			"new_password"              => "required|min:5|max:20|confirmed",
			"new_password_confirmation" => "required|max:20",
		];
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			return redirect()->back()->with('errors',$validator->messages()->all());
		}
		$user = Stuff::getUser();
		$pass = Hash::make($request->password);

		if(!Hash::check($request->password, $user->password))
    		return redirect()->back()->with('fail','unsuccessfull');
			
		$update = User::where('id', $user->id)->update([
			'password' => Hash::make($request->new_password)
		]);
    	
    	$newUser = User::where('id', $user->id)->first();
		Session::put('user', $newUser);

    	return redirect()->back()->with('success','successfull');

    }

    public function memories($lang){
    	$id = Stuff::getUser()->id;
    	$memories = Memory::where('user_id', $id)->orderBy('id', 'desc')->get();

    	return view('user.profile.memories', compact('memories'));
    }

    public function memoriesAdd($lang){
    	$parks = Park::all();
    	
    	return view('user.profile.memoriesAdd', compact('parks'));
    }

    public function memoriesAdder(Request $request, $lang){
    	$current_date = date('d-M-Y');

    	// dd($current_date);
    	App::setLocale(Stuff::lang());
    	$rules = [
    		"park_id"        => "required|numeric",
			"title"          => "required|min:5|max:35|regex:/^[\pL\s\-]+$/u",
     	];

     	$park = Park::where('id', $request->park_id)->first();
     	
     	if(!$park)
     		return redirect()->back();
     		
			
     	$query = [];
		if($request->has('video_submitter')){
			$rules['video_links'] = "required|max:15";
			$rules['video_links.*'] = "required";
			$rules['video_tags']  = "required|regex:/(^[A-Za-z0-9, ]+$)+/";
			$rules['video_photos'] = "required|max:15";
			$rules['video_photos.*'] = "required|mimes:jpeg,jpg,png,gif|max:20000";
			$files = $request->video_photos;
			$query['tags'] = $request->video_tags;
			$query['extra'] = json_encode($request->video_links); 
			$query['type'] = 'video';
		}elseif($request->has('gallery_submitter')){
			$rules["gallery_tags"] = 'required|regex:/(^[A-Za-z0-9, ]+$)+/';
			$rules["gallery_photos"] = 'required|max:15';
			$rules["gallery_photos.*"] = 'required|mimes:jpeg,jpg,png,gif|max:20000';
			$files = $request->gallery_photos;
			$query['tags'] = $request->gallery_tags;
			$query['type'] = 'image'; 
		}elseif($request->has('blog_submitter')){
	 		$rules['blog_tags']  = 'required|regex:/(^[A-Za-z0-9, ]+$)+/';
	 		$rules['blog']       = 'required';
			$rules['blog_photos']  = 'required|max:15';
			$rules['blog_photos.*']  = 'required|mimes:jpeg,jpg,png,gif|max:20000';
			$files = $request->blog_photos;
			$query['tags'] = $request->blog_tags;
			$query['extra'] = $request->blog;
			$query['type'] = 'blog'; 
		}else{
			return redirect()->back();
		}

		$validator = Validator::make($request->all(), $rules);
		if($validator->fails())
			return Redirect::to(URL::previous()."#$request->redirect_hash")
				->with('errors',$validator->messages()->all())
				->withInput();

		foreach($files as $file){
			Stuff::uploader($file, $name);
			$query['images'][] = $name;
		}
		$query['images'] = json_encode($query['images']);
		$query['title'] = $request->title;
		$query['park_id'] = $request->park_id;
		$query['lang'] = Stuff::lang();
		$query['user_id'] = Stuff::getUser()->id;
		$query['status'] = 0;
		$query['date'] = $current_date;

		Memory::insert($query);

		return Redirect::to(URL::previous()."#$request->redirect_hash")
				->with('success', __('website.memories_success'));
    }

    public function memoriesEditView($lang, $id){
    	$user = Stuff::getUser();
    	$memory = Memory::where('id', $id)->where('user_id', $user->id)->first();
    	$parks = Park::all();

    	return view('user.profile.memoriesEdit', compact('memory', 'parks'));


    }

    public function memoriesEdit(Request $request,$lang, $id){
    	// dd($request->all());
    	
    	App::setLocale(Stuff::lang());
    	$rules = [
    		"park_id"        => "required|numeric",
			"title"          => "required|min:5|max:35|regex:/^[\pL\s\-]+$/u",
     	];

     	$park = Park::where('id', $request->park_id)->first();
     	if(!$park)
     		return redirect()->back();
     		
			
     	$query = [];
		if($request->has('video_submitter')){
			$rules['video_links'] = "required|max:15";
			$rules['video_links.*'] = "required";
			$rules['video_tags']  = "required|regex:/(^[A-Za-z0-9, ]+$)+/";
			$query['tags'] = $request->video_tags;
			$query['extra'] = json_encode($request->video_links); 
		}elseif($request->has('gallery_submitter')){
			$rules["gallery_tags"] = 'required|regex:/(^[A-Za-z0-9, ]+$)+/';
			$query['tags'] = $request->gallery_tags;
		}elseif($request->has('blog_submitter')){
	 		$rules['blog_tags']  = 'required|regex:/(^[A-Za-z0-9, ]+$)+/';
	 		$rules['blog']       = 'required';
			$query['tags'] = $request->blog_tags;
			$query['extra'] = $request->blog;
		}else{
			return redirect()->back();
		}

		$validator = Validator::make($request->all(), $rules);
		if($validator->fails())
			return redirect()->back()
				->with('errors',$validator->messages()->all());

		$query['title'] = $request->title;
		$query['park_id'] = $request->park_id;

		Memory::where('id', $id)->update($query);

		return redirect()->back()
				->with('success', __('website.memories_success_change'));

    }

    public function memoryDeletor($lang, $id){
    	$user = Stuff::getUser();
    	Memory::where('id', $id)->where('user_id', $user->id)->delete();

    	Favourite::where('type', 'memoriesInner')
			->where('type_id', $id)
			->delete();

    	return redirect()->route('memories', ['lang'=>$lang]);
    }

    public function commentAction(Request $request,$lang){
    	App::setLocale(Stuff::lang());
    	$time = time();
    	$timeValidate = $time-60*60; //$time-60*60  1hours
		$user = Stuff::getUser();
    	$request->merge(['rating' => intval($request->rating)]);

    	$comment = Comment::where('type_id', $request->type_id)
    		->where('type', $request->type)
    		->where('user_id', $user->id)
    		->where('time', '>', $timeValidate)
    		->first();

    	if(!empty($comment))
    		return 'spam'; 
    	
    	$rules = [
    		'rating'      => 'required|numeric|min:1|max:5',
    		'type'        => 'required',
    		'type_id'     => 'required',
    		'description' => 'required|max:600',
    	]; 
    	$validator = Validator::make($request->all(), $rules);
    	if($validator->fails())
			return $validator->messages()->all();

		$query = [
			'rating'      => $request->rating,
    		'type'        => $request->type,
    		'type_id'     => $request->type_id,
    		'description' => $request->description,
    		'user_id'     => $user->id,
    		'time'        => $time,
		];

		Comment::insert($query);

		// count aroundpark rating
		if($request->type == 'aroundInner'){
			$all_comments = Comment::where('type_id', $request->type_id)
				->where('type', $request->type)
				->select('rating')
				->get();
			$length = count($all_comments);
			$rating = 0;
			foreach ($all_comments as $key => $com) {
				$rating += $com->rating;
			}
			$result = $rating/$length;
			AroundPark::where('id', $request->type_id)->update(['rating'=>$result]);
		}
    	// end of count aroundpark rating


    	return 'success';
    }

    public function favourites(){
    	$user = Stuff::getUser();
    	$my_favours = Favourite::where('user_id', $user->id)
    		->with(['memory', 'aroundPark', 'trip', 'toSee'])
    		->get()
    		->groupBy('type');

    	return view('user.profile.favourites', compact('my_favours'));
    }


    public function subscribeView($lang, $email = false){
    	if(isset($email))
    		$my_mail = $email;
    	else
    		$my_mail = null;

    	return view('user.profile.subscribe', compact('my_mail'));
    }

    public function subscribe(Request $request, $lang){
		App::setLocale($lang);

		$rules = [
			'email' => 'required|min:5|email|unique:subscribers,email|max:70',
			// 'agreement' => 'required',
			"events" => 'max:5|required_without_all:trips,alerts,services',
			"trips" => 'max:5|required_without_all:events,alerts,services',
			"alerts" => 'max:5|required_without_all:trips,events,services',
			"services" => 'max:5|required_without_all:trips,alerts,events',
		];
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			return redirect()->back()->with('errors',$validator->messages()->all());
		}    	

		Subscriber::insert([
			"email" => $request->email,
			"events" => $request->events,
			"trips" => $request->trips,
			"alerts" => $request->alerts,
			"services" => $request->services,
		]);
    	
    	return redirect()->back()->with('success', 'subscribed');
    }

    public function unSubscribeView(){
    	return view('user.profile.unSubscribe');
    }


    public function unSubscribe(Request $request, $lang){
		App::setLocale($lang);

		$rules = [
			'email' => 'required|min:5|email|max:70',
			// "events" => 'max:5|required_without_all:trips,alerts,services',
			// "trips" => 'max:5|required_without_all:events,alerts,services',
			// "alerts" => 'max:5|required_without_all:trips,events,services',
			// "services" => 'max:5|required_without_all:trips,alerts,events',
		];
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			return redirect()->back()->with('errors',$validator->messages()->all());
		}    	

		Subscriber::where('email', $request->email)->delete();
    	
    	return redirect()->back()->with('success', 'unSubscribed');
    }

}

