<?php

namespace App\Http\Controllers;

use DB;
use App;
use App\Map;
use App\Park;
use App\Shop;
use App\Trip;
use App\Place;
use App\Alert;
use App\ToBuy;
use App\Event;
use App\ToSee;
use App\Nature;
use App\Memory;
use App\Slider;
use App\Comment;
use App\GetHere;
use App\Instagram;
use App\Favourite;
use App\Recommend;
use App\Adventure;
use App\AroundPark;
use App\Downloadable;
use App\TripAdventure;
use App\Helpers\Stuff;
use Illuminate\Http\Request;

class MainController extends Controller
{

    // optimization 'alerts'
    // main slider cache

    public function test(){
    	// App::setLocale('ka');
        dd('success');
    }

    public function changeLanguage(Request $request){
    	App::setLocale($request->lang);
		$path = explode('/', $request->path);
		array_shift($path);
		$path[0] = $request->lang;
		$path = implode('/', $path);
		
		return $path;
    }


    public function memoriesView($lang, $parkName){
        $slides = Slider::getData($parkName);
        // $favourite = Favourite::check('park', $parkName);
        
        $park = Park::where('alias', $parkName)->with(['memories', 'alerts'])->first();
        $alerts = $park->alerts->where('status', 1);

        $memories = Memory::where('park_id', $park->id)
            ->where('status', 1)
            ->where('lang', App::getLocale())
            ->orderBy('id', 'desc')
            ->get();

        // dd(count($memories));

        $instagrams = Instagram::where('park_id', $park->id)->get();

        return view('user.main.memories.index', compact('memories', 'slides', 'alerts', 'instagrams'));
    }

    public function favouritesControll($type, $type_id, $controll){
        $user_id = Stuff::getUser()->id;

        if($controll == 0){
            Favourite::where('user_id', $user_id)
                ->where('type', $type)
                ->where('type_id', $type_id)
                ->delete();
            return 0;
        }
        elseif($controll == 1){
            Favourite::insert([
                'user_id' => $user_id,
                'type'    => $type,
                'type_id' => $type_id,
            ]);
            return 1;
        }
        else
            return 'error';
    }

    public function memoriesInnerView($lang, $parkAlias, $memoryId){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with('alerts')->first();
        $alerts = $park->alerts->where('status', 1);
        $favourite = Favourite::check('memoriesInner', $memoryId);

        $memory = Memory::where('id', $memoryId)->with('user')->first();
        $recommends = Memory::inRandomOrder()->limit(3)->with('user')->get();

        $meta_img = Stuff::imgUrl(json_decode($memory->images)[0]);
        $meta = Stuff::ogShare($memory->title, $meta_img, $memory->user->name);

        return view('user.main.memories.memoriesInner', compact('favourite','slides', 'memory', 'alerts','recommends', 'meta'));
    }


    public function aroundPark(Request $request, $lang, $parkAlias){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts', 'arounds'])->first();
        $alerts = $park->alerts->where('status', 1);
        $types = Place::all();
        
        $priceFrom = $request['price-from'];
        $priceTo = $request['price-to'];
        $starsFrom = $request['stars-from'];
        $starsTo = $request['stars-to'];
        $filterTypes = $request['types'];
        $checkers = '';

        if(isset($priceFrom)){
            $arounds = $park->arounds
                ->where('price','>=',intval($priceFrom))
                ->where('price','<=',intval($priceTo))
                ->where('rating','>=',intval($starsFrom))
                ->where('rating','<=',intval($starsTo));

            if($request->has('types')){
                $newArounds = [];    
                foreach ($arounds as $key => $around) {
                    foreach($filterTypes as $filter){
                        if($filter == $around->place_id){
                            $newArounds[] = $around;
                        }
                    }
                }
                $arounds = $newArounds;
            }
            $checkers = $request->types;
        }else{
            $arounds = $park->arounds;
        }

        return view('user.main.around.list', compact('slides', 'alerts', 'arounds', 'types', 'checkers'));
    }

    public function aroundInner($lang,$parkAlias, $id){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with('alerts')->first();
        $favourite = Favourite::check('aroundInner', $id);
        $alerts = $park->alerts->where('status', 1);
        $around = AroundPark::where('id', $id)->with('place')->first();
        $user = Stuff::getUser();
        $comments = Comment::where('type', 'aroundInner')
            ->where('type_id', $id)
            ->with('user')
            ->orderBy('id', 'desc')
            ->get();

        $meta_img = Stuff::imgUrl(json_decode($around->image)[0], true);
        $meta = Stuff::ogShare(Stuff::trans($around, 'name'), $meta_img, Stuff::trans($around, 'description'));


        return view('user.main.around.inner', compact('slides','alerts', 'around', 'favourite', 'user', 'comments', 'meta'));
    }

    public function tobuy($lang, $parkAlias){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts', 'things'])->first();
        $alerts = $park->alerts->where('status', 1);
        $things = $park->things;

        return view('user.main.tobuy.list', compact('slides', 'alerts', 'things'));
    }


    public function tobuyInner($lang, $parkAlias, $id){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with('alerts')->first();
        $alerts = $park->alerts->where('status', 1);
        // $favourite = Favourite::check('aroundInner', $id);
        $thing = ToBuy::where('id', $id)->with('shops')->first();

        $meta_img = Stuff::imgUrl(json_decode($thing->images)[0], true);
        $meta = Stuff::ogShare(Stuff::trans($thing, 'name'), $meta_img, Stuff::trans($thing, 'description'));

        return view('user.main.tobuy.inner', compact('slides', 'alerts', 'thing','meta'));
    }

    public function adventures($lang, $parkAlias){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts', 'memories', 'adventures'])->first();
        $alerts = $park->alerts->where('status', 1);
        $memories = $park->memories()
            ->where('status', 1)
            ->where('lang', $lang)
            ->orderBy('id','desc')
            ->limit(4)
            ->get();

        $adventures = Adventure::where('park_id', $park->id)->with('imgs')->get();
        $instagrams = Instagram::where('park_id', $park->id)->get();

        return view('user.main.adventures.parkAdventure',compact('slides', 'alerts','memories', 'adventures', 'instagrams'));
    }

    public function adventuresInner($lang, $parkAlias, $id){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts', 'memories'])->first();
        $alerts = $park->alerts->where('status', 1);
        $memories = $park->memories()
            ->where('status', 1)
            ->where('lang', $lang)
            ->orderBy('id','desc')
            ->limit(4)
            ->get();

        $adventure = Adventure::where('id', $id)->first();

        $meta_image = json_decode($adventure->images);
        if(!empty($meta_image)){
            $meta_img = Stuff::imgUrl(json_decode($adventure->images)[0], true);
        }else{
            $meta_img = '/main/img/logo-brand.png';
        }
        
        $meta = Stuff::ogShare(Stuff::trans($adventure, 'name'), $meta_img, Stuff::trans($adventure, 'description'));

        $tripsAvailable = TripAdventure::where('adventure_id', $id)->with('trip')->orderBy('id', 'desc')->limit(4)->get();
        $trips = [];
        foreach($tripsAvailable as $available){
            $trips[] = $available->trip->myTrip;
        }


        return view('user.main.adventures.inner', compact('slides', 'alerts','memories', 'adventure', 'meta', 'trips'));
    }

    public function natureCulture($lang, $parkAlias, $optional = null){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts', 'memories', 'natures'])->first();
        $alerts = $park->alerts->where('status', 1);

        if(isset($optional)){
            $natures = $park->natures->where('type', 'culture');
            $nature_type = 'culture';
        }
        else{
            $natures = $park->natures->where('type', 'nature');
            $nature_type = 'nature';
        }        


        return view('user.main.natureCulture.index', compact('slides', 'alerts', 'natures', 'nature_type'));
    }

    public function guide($lang, $parkAlias){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts'])->first();
        $alerts = $park->alerts->where('status', 1);

        $recommends = Recommend::where('park_id', $park->id)->with('park')->get();
        $downloadables = Downloadable::where('park_id', $park->id)->get();

        return view('user.main.guide.index', compact('slides', 'alerts', 'park', 'recommends', 'downloadables'));
    }


    public function events(Request $request, $lang, $parkAlias){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts', 'events'])->first();
        $alerts = $park->alerts->where('status', 1);
        
        if($request->has('date'))
            $events = $park->events->where('date', $request->date);
        else
            $events = $park->events;

        return view('user.main.events.index', compact('slides', 'alerts', 'park', 'events'));
    }

    public function eventsInner($lang, $parkAlias, $id){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts'])->first();
        $alerts = $park->alerts->where('status', 1);

        $event = Event::where('id', $id)->first();

        $meta_img = Stuff::imgUrl(json_decode($event->image)[0], true);
        $meta = Stuff::ogShare(Stuff::trans($event, 'name'), $meta_img, Stuff::trans($event, 'description'));

        return view('user.main.events.inner', compact('slides', 'alerts', 'park', 'event', 'meta'));
    }

    public function getHere($lang, $parkAlias){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts', 'getHere'])->first();
        $alerts = $park->alerts->where('status', 1);

        $heres = $park->getHere;

        $coors = Map::getCoors();
        
        return view('user.main.getHere.index', compact('slides', 'alerts', 'heres', 'park','coors'));
    }


    public function parksInner($lang, $parkAlias){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts', 'adventures', 'images', 'videos'])->first();
        $alerts = $park->alerts->where('status', 1);
        $adventures = Adventure::where('park_id', $park->id)->with('imgs')->get();
        $photos = $park->images;
        $my_videos = $park->videos;
        // $favourite = Favourite::check('parks', $id);
        $comments = Comment::where('type', 'park')
            ->where('type_id', $parkAlias)
            ->with('user')
            ->orderBy('id', 'desc')
            ->get();
        $user = Stuff::getUser();


        $sql = "SELECT *, ( 6371 * acos( cos( radians(" . floatval($park->coordinate_lat) . ") ) * cos( radians( coordinate_lat ) ) * cos( radians( coordinate_long ) - radians(" . floatval($park->coordinate_long) . ") ) + sin( radians(" . floatval($park->coordinate_lat) . ") ) * sin( radians( coordinate_lat ) ) ) ) AS distance FROM parks HAVING distance < 99";

        $nearbyParks = DB::select($sql);
        $toSeeObjects = $park->toSees()->get()->groupBy('type');

        $all_adventures = Adventure::where('park_id', $park->id)->get()->groupBy('name');

        $json = [];
        foreach($all_adventures as $key => $groupAdventure){
            foreach($groupAdventure as $adventure){
                $icon = $adventure->map_icon;
                $json[] = ['id'=>$adventure->id,'height'=> $adventure->sea_level,'position'=>$adventure->coordinates,'ico'=>$icon, 'name' => Stuff::trans($adventure, 'name')];                 
            }
        }

        $contacts = Park::getContacts($park);
        // dd($contacts);

        return view('user.main.park.inner', compact('slides', 'alerts', 'adventures', 'park', 'photos', 'my_videos', 'comments', 'user', 'nearbyParks', 'toSeeObjects', 'json', 'contacts'));
    }


    public function tripsInner($lang, $parkAlias, $id){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with(['alerts', 'memories'])->first();
        $alerts = $park->alerts->where('status', 1);
        $favourite = Favourite::check('tripsInner', $id);

        $comments = Comment::where('type', 'tripsInner')
            ->where('type_id', $id)
            ->with('user')
            ->orderBy('id', 'desc')
            ->get();
        $user = Stuff::getUser();
        $memories = $park->memories()
            ->where('status', 1)
            ->where('lang', App::getLocale())
            ->orderBy('id', 'desc')
            ->limit(4)
            ->get();
        $trip = Trip::where('id', $id)->with('steps')->first();
        $steps = $trip->steps()->orderBy('trip_days','asc')->get();

        // dd($steps);

        $similar_trips = Trip::where('park_id', $park->id)->where('id','!=', $id)->limit(2)->get();


        return view('user.main.trip.inner', compact('slides', 'alerts', 'comments','user', 'memories', 'favourite', 'trip', 'steps', 'similar_trips'));
    }   

    
    public function recommendInnerView($lang, $parkAlias, $id){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with('alerts')->first();
        $alerts = $park->alerts->where('status', 1);

        $recommendation = Recommend::where('id', $id)->first();
        $recommends = Recommend::where('park_id', $park->id)->where('id','!=', $park->id)->with('park')->inRandomOrder()->limit(8)->get();

        $meta_img = Stuff::imgUrl(json_decode($recommendation->img)[0]);
        $meta = Stuff::ogShare($recommendation->name, $meta_img, $recommendation->description);

        return view('user.main.recommend.inner', compact('favourite','slides', 'recommendation', 'alerts','recommends', 'meta'));
    }


    public function toSeeInner($lang,$parkAlias, $id){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with('alerts')->first();
        $favourite = Favourite::check('toSeeInner', $id);
        $alerts = $park->alerts->where('status', 1);

        $around = ToSee::where('id', $id)->with('park')->first();
        $user = Stuff::getUser();
        $comments = Comment::where('type', 'toSeeInner')
            ->where('type_id', $id)
            ->with('user')
            ->orderBy('id', 'desc')
            ->get();

        $meta_img = Stuff::imgUrl(json_decode($around->images)[0], true);
        $meta = Stuff::ogShare(Stuff::trans($around, 'name'), $meta_img, Stuff::trans($around, 'description'));


        return view('user.main.toSee.inner', compact('slides','alerts', 'around', 'favourite', 'user', 'comments', 'meta'));
    }

    public function attraction($lang,$parkAlias){
        $slides = Slider::getData($parkAlias);
        $park = Park::where('alias', $parkAlias)->with('alerts')->first();
        $alerts = $park->alerts->where('status', 1);

        return view('user.main.park.attraction', compact('slides', 'alerts', 'park'));
    }

}
