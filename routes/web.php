<?php

Route::get('/', 'MasterController@home')->name('home');
Route::get('/changeLanguage', 'MainController@changeLanguage')->name('changeLanguage');
Route::get('/favouritesControll/{type}/{type_id}/{controll}', 'MainController@favouritesControll')->name('favouritesControll');
Route::post('/tourFilter', 'MasterController@tourFilter')->name('tourFilter');
Route::post('/tourShowMore', 'MasterController@tourShowMore')->name('tourShowMore');
Route::post('/search', 'MasterController@search')->name('search');
Route::post('/adventureSortPark', 'MasterController@adventureSortPark')->name('adventureSortPark');

Route::prefix('{lang?}')->middleware('localization')->group(function() {
	Route::get('/main', 'MasterController@index')->name('index');
	Route::get('/404', 'MasterController@notFound')->name('notFound');


	Route::group(['prefix' => 'user'], function(){
		Route::get('/login', 'ProfileController@loginView')->name('loginView');
		Route::post('/register', 'ProfileController@register')->name('register');
		Route::post('/login', 'ProfileController@authenticate')->name('authenticate');

		Route::get('/subscribe/{email?}', 'ProfileController@subscribeView')->name('subscribeView');
		Route::post('/subscribe', 'ProfileController@subscribe')->name('subscribe');

		Route::get('/unSubscribe', 'ProfileController@unSubscribeView')->name('unSubscribeView');
		Route::post('/unSubscribe', 'ProfileController@unSubscribe')->name('unSubscribe');

		
		Route::group(['middleware' => 'auth'], function(){
			Route::get('/test', 'MainController@test')->name('test');
			Route::get('/profile', 'ProfileController@profile')->name('profile');
			Route::get('/profileView', 'ProfileController@profileView')->name('profileView');
			Route::post('/profile', 'ProfileController@profileEdit')->name('profileEdit');
			Route::get('/logout', 'ProfileController@logout')->name('logout');
			Route::get('/favourites', 'ProfileController@favourites')->name('favourites');
			Route::get('/changePass', 'ProfileController@changePassView')->name('changePassView');
			Route::post('/changePass', 'ProfileController@changePass')->name('changePass');
			Route::get('/memories', 'ProfileController@memories')->name('memories');
			Route::get('/memories/add', 'ProfileController@memoriesAdd')->name('memoriesAdd');
			Route::get('/memories/delete/{id}', 'ProfileController@memoryDeletor')->name('memoryDeletor');
			Route::post('/memories/add', 'ProfileController@memoriesAdder')->name('memoriesAdder');
			Route::get('/memories/edit/{id}', 'ProfileController@memoriesEditView')->name('memoriesEditView');
			Route::post('/memories/edit/{id}', 'ProfileController@memoriesEdit')->name('memoriesEdit');
			Route::post('/commentAction', 'ProfileController@commentAction')->name('commentAction');
		});
	});

	Route::group(['prefix' => 'site/{park}'], function(){
		Route::get('/memories', 'MainController@memoriesView')->name('siteMemoriesView');
		Route::get('/memoriesInner/{id}', 'MainController@memoriesInnerView')->name('siteMemoriesInnerView');
		Route::get('/around', 'MainController@aroundPark')->name('siteAroundPark');
		Route::get('/aroundInner/{id}', 'MainController@aroundInner')->name('siteAroundInner');
		Route::get('/tobuy', 'MainController@tobuy')->name('siteTobuy');
		Route::get('/tobuyInner/{id}', 'MainController@tobuyInner')->name('siteTobuyInner');
		Route::get('/adventures', 'MainController@adventures')->name('siteAdventures');
		Route::get('/adventuresInner/{id}', 'MainController@adventuresInner')->name('siteAdventuresInner');
		Route::get('/natureCulture/{type?}', 'MainController@natureCulture')->name('siteNatureCulture');
		Route::get('/guide', 'MainController@guide')->name('siteGuide');
		Route::get('/events', 'MainController@events')->name('siteEvents');
		Route::get('/eventsInner/{id}', 'MainController@eventsInner')->name('siteEventsInner');
		Route::get('/getHere', 'MainController@getHere')->name('siteGetHere');
		Route::get('/tripsInner/{id}', 'MainController@tripsInner')->name('siteTripsInner');
		Route::get('/recommendInner/{id}', 'MainController@recommendInnerView')->name('siteRecommendInnerView');
		Route::get('/toSeeInner/{id}', 'MainController@toSeeInner')->name('siteToSeeInner');
		Route::get('/attraction', 'MainController@attraction')->name('attraction');


		Route::get('/', 'MainController@parksInner')->name('siteParksInner');
	});
	
	Route::group(['prefix' => 'master'], function(){
		Route::get('/geoAdventures/{current?}', 'MasterController@geoAdventures')->name('masterGeoAdventures');
		Route::get('/parks', 'MasterController@parks')->name('masterParks');
		Route::get('/trips/{filter?}', 'MasterController@trips')->name('masterTrips');
		Route::get('/aroundMe', 'MasterController@aroundMe')->name('masterAroundMe');
		
		Route::get('/services', 'MasterController@services')->name('services');
		Route::get('/guide', 'MasterController@guide')->name('guide');
		Route::get('/transportation/{park?}', 'MasterController@transportation')->name('transportation');
		Route::get('/basicInfo', 'MasterController@basicInfo')->name('basicInfo');

		Route::get('/programs/{id}', 'MasterController@programs')->name('programs');
	});


});

// Facebook
Route::get('/login/facebook', 'FacebookController@redirectToProvider')->name('facebookAuth');
Route::get('/login/facebook/callback', 'FacebookController@handleProviderCallback');
// Google
Route::get('/login/google', 'GoogleController@redirectToProvider')->name('googleAuth');
Route::get('/login/google/callback', 'GoogleController@handleProviderCallback');
// Instagram
Route::get('/instagram/photos/{hashtag}', 'InstagramController@index')->name('instagram');

















// AdminPanel
Route::get('/admin', 'AdminController@adminLogin')->name('adminLogin');
Route::post('/admin', 'AdminController@adminAuth')->name('adminAuth');
Route::get('/adminLogout', 'AdminController@adminLogout')->name('adminLogout');

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function()  
{
	Route::group(['middleware' => 'admin'], function(){
		Route::get('/index', 'AdminController@index')->name('adminIndex');
		Route::get('/parks', 'AdminController@parksView')->name('parksView');
		Route::get('/parks/add', 'AdminController@parkAddView')->name('parkAddView');
		Route::get('/parks/edit/{id}', 'AdminController@parkEditView')->name('parkEditView');
		Route::post('/parks/edit/{id}', 'AdminController@parkEdit')->name('parkEdit');
		Route::post('/parks/add', 'AdminController@parkAdd')->name('parkAdd');
		Route::get('/parks/delete/{id}', 'AdminController@parkDelete')->name('parkDelete');
		Route::get('/parks/translate/{id}', 'AdminController@parkTranslateView')->name('parkTranslateView');
		Route::get('/parks/file/delete/{id}/{name}', 'AdminController@parksFileDel')->name('parksFileDel');

		Route::get('/regions', 'AdminController@regionsView')->name('regionsView');
		Route::get('/regions/add', 'AdminController@regionAddView')->name('regionAddView');
		Route::get('/regions/edit/{id}', 'AdminController@regionEditView')->name('regionEditView');
		Route::post('/regions/edit/{id}', 'AdminController@regionEdit')->name('regionEdit');
		Route::post('/regions/add', 'AdminController@regionAdd')->name('regionAdd');

		Route::get('/adventures', 'AdminController@adventuresView')->name('adventuresView');
		Route::get('/adventures/add', 'AdminController@adventuresAddView')->name('adventuresAddView');
		Route::get('/adventures/edit/{id}', 'AdminController@adventuresEditView')->name('adventuresEditView');
		Route::post('/adventures/edit/{id}', 'AdminController@adventuresEdit')->name('adventuresEdit');
		Route::post('/adventures/add', 'AdminController@adventuresAdd')->name('adventuresAdd');
		Route::get('/adventures/image/delete/{id}/{name}', 'AdminController@adventuresImageDel')->name('adventuresImageDel');
		Route::get('/adventures/delete/{id}', 'AdminController@adventuresDelete')->name('adventuresDelete');

		Route::get('/thingsToSee', 'AdminController@thingsToSeeView')->name('thingsToSeeView');
		Route::get('/thingsToSee/add', 'AdminController@thingsToSeeAddView')->name('thingsToSeeAddView');
		Route::post('/thingsToSee/add', 'AdminController@thingsToSeeAdd')->name('thingsToSeeAdd');
		Route::get('/thingsToSee/edit/{id}', 'AdminController@thingsToSeeEditView')->name('thingsToSeeEditView');
		Route::post('/thingsToSee/edit/{id}', 'AdminController@thingsToSeeEdit')->name('thingsToSeeEdit');
		Route::get('/thingsToSee/delete/{id}', 'AdminController@thingsToSeeDelete')->name('thingsToSeeDelete');


		Route::get('/imageGallery', 'AdminController@imageGalleryView')->name('imageGalleryView');
		Route::get('/imageGallery/add', 'AdminController@imageGalleryAddView')->name('imageGalleryAddView');
		Route::post('/imageGallery/add', 'AdminController@imageGalleryAdd')->name('imageGalleryAdd');
		Route::get('/imageGallery/edit/{id}', 'AdminController@imageGalleryEditView')->name('imageGalleryEditView');
		Route::get('/imageGallery/delete/{id}', 'AdminController@imageGalleryDelete')->name('imageGalleryDelete');
		Route::post('/imageGallery/edit/{id}', 'AdminController@imageGalleryEdit')->name('imageGalleryEdit');


		Route::get('/downloadable', 'AdminController@downloadableView')->name('downloadableView');
		Route::get('/downloadable/add', 'AdminController@downloadableAddView')->name('downloadableAddView');
		Route::post('/downloadable/add', 'AdminController@downloadableAdd')->name('downloadableAdd');
		Route::get('/downloadable/edit/{id}', 'AdminController@downloadableEditView')->name('downloadableEditView');
		Route::get('/downloadable/delete/{id}', 'AdminController@downloadableDelete')->name('downloadableDelete');
		Route::post('/downloadable/edit/{id}', 'AdminController@downloadableEdit')->name('downloadableEdit');

		Route::get('/videoGallery', 'AdminController@videoGalleryView')->name('videoGalleryView');
		Route::get('/videoGallery/add', 'AdminController@videoGalleryAddView')->name('videoGalleryAddView');
		Route::post('/videoGallery/add', 'AdminController@videoGalleryAdd')->name('videoGalleryAdd');
		Route::get('/videoGallery/edit/{id}', 'AdminController@videoGalleryEditView')->name('videoGalleryEditView');
		Route::post('/videoGallery/edit/{id}', 'AdminController@videoGalleryEdit')->name('videoGalleryEdit');
		Route::get('/videoGallery/delete/{id}', 'AdminController@videoGalleryDelete')->name('videoGalleryDelete');

		Route::get('/aroundPark', 'AdminController@aroundParkView')->name('aroundParkView');
		Route::get('/aroundPark/add', 'AdminController@aroundParkAddView')->name('aroundParkAddView');
		Route::get('/aroundPark/edit/{id}', 'AdminController@aroundParkEditView')->name('aroundParkEditView');
		Route::post('/aroundPark/edit/{id}', 'AdminController@aroundParkEdit')->name('aroundParkEdit');
		Route::post('/aroundPark/add', 'AdminController@aroundParkAdd')->name('aroundParkAdd');
		Route::get('/aroundPark/delete/{id}', 'AdminController@aroundParkDelete')->name('aroundParkDelete');
		Route::get('/aroundPark/image/delete/{id}/{name}', 'AdminController@aroundParkImageDel')->name('aroundParkImageDel');


		Route::get('/events', 'AdminController@eventsView')->name('eventsView');
		Route::get('/events/add', 'AdminController@eventsAddView')->name('eventsAddView');
		Route::post('/events/add', 'AdminController@eventsAdd')->name('eventsAdd');
		Route::get('/events/edit/{id}', 'AdminController@eventsEditView')->name('eventsEditView');
		Route::post('/events/edit/{id}', 'AdminController@eventsEdit')->name('eventsEdit');
		Route::get('/events/delete/{id}', 'AdminController@eventsDelete')->name('eventsDelete');

		Route::get('/alerts', 'AdminController@alertsView')->name('alertsView');
		Route::get('/alerts/add', 'AdminController@alertsAddView')->name('alertsAddView');
		Route::post('/alerts/add', 'AdminController@alertsAdd')->name('alertsAdd');
		Route::get('/alerts/edit/{id}', 'AdminController@alertsEditView')->name('alertsEditView');
		Route::post('/alerts/edit/{id}', 'AdminController@alertsEdit')->name('alertsEdit');
		Route::get('/alerts/delete/{id}', 'AdminController@alertsDelete')->name('alertsDelete');

		Route::get('/toBuy', 'AdminController@toBuyView')->name('toBuyView');
		Route::get('/toBuy/add', 'AdminController@toBuyAddView')->name('toBuyAddView');
		Route::post('/toBuy/add', 'AdminController@toBuyAdd')->name('toBuyAdd');
		Route::get('/toBuy/edit/{id}', 'AdminController@toBuyEditView')->name('toBuyEditView');
		Route::post('/toBuy/edit/{id}', 'AdminController@toBuyEdit')->name('toBuyEdit');
		Route::get('/toBuy/image/delete/{id}/{name}', 'AdminController@toBuyImageDel')->name('toBuyImageDel');
		Route::get('/toBuy/delete/{id}', 'AdminController@toBuyDelete')->name('toBuyDelete');

		Route::get('/toBuyShops', 'AdminController@toBuyShopsView')->name('toBuyShopsView');
		Route::get('/toBuyShops/add', 'AdminController@toBuyShopsAddView')->name('toBuyShopsAddView');
		Route::post('/toBuyShops/add', 'AdminController@toBuyShopsAdd')->name('toBuyShopsAdd');
		Route::get('/toBuyShops/edit/{id}', 'AdminController@toBuyShopsEditView')->name('toBuyShopsEditView');
		Route::post('/toBuyShops/edit/{id}', 'AdminController@toBuyShopsEdit')->name('toBuyShopsEdit');
		Route::get('/toBuyShops/delete/{id}', 'AdminController@toBuyShopsDelete')->name('toBuyShopsDelete');

		Route::get('/markerDeletor/{table}/{column}/{id}', 'AdminController@markerDeletor')->name('markerDeletor');


		Route::get('/place', 'AdminController@placeView')->name('placeView');
		Route::get('/place/add', 'AdminController@placeAddView')->name('placeAddView');
		Route::get('/place/edit/{id}', 'AdminController@placeEditView')->name('placeEditView');
		Route::get('/place/delete/{id}', 'AdminController@placeDelete')->name('placeDelete');
		Route::post('/place/add', 'AdminController@placeAdd')->name('placeAdd');
		Route::post('/place/edit/{id}', 'AdminController@placeEdit')->name('placeEdit');


		Route::get('/memory', 'AdminController@memoryView')->name('memoryView');
		Route::get('/memory/add', 'AdminController@memoryAddView')->name('memoryAddView');
		Route::get('/memory/edit/{id}', 'AdminController@memoryEditView')->name('memoryEditView');
		Route::get('/memory/delete/{id}', 'AdminController@memoryDelete')->name('memoryDelete');
		Route::post('/memory/add', 'AdminController@memoryAdd')->name('memoryAdd');
		Route::post('/memory/edit/{id}', 'AdminController@memoryEdit')->name('memoryEdit');


		Route::get('/settings', 'SettingController@settings')->name('settings');
		Route::post('/settings', 'SettingController@settingsEdit')->name('settingsEdit');


		Route::get('/slider', 'AdminController@sliderView')->name('sliderView');
		Route::get('/slider/add', 'AdminController@sliderAddView')->name('sliderAddView');
		Route::get('/slider/edit/{id}', 'AdminController@sliderEditView')->name('sliderEditView');
		Route::get('/slider/delete/{id}', 'AdminController@sliderDelete')->name('sliderDelete');
		Route::post('/slider/add', 'AdminController@sliderAdd')->name('sliderAdd');
		Route::post('/slider/edit/{id}', 'AdminController@sliderEdit')->name('sliderEdit');

		


		Route::get('/nature', 'AdminController@natureView')->name('natureView');
		Route::get('/nature/add', 'AdminController@natureAddView')->name('natureAddView');
		Route::get('/nature/edit/{id}', 'AdminController@natureEditView')->name('natureEditView');
		Route::get('/nature/delete/{id}', 'AdminController@natureDelete')->name('natureDelete');
		Route::post('/nature/add', 'AdminController@natureAdd')->name('natureAdd');
		Route::post('/nature/edit/{id}', 'AdminController@natureEdit')->name('natureEdit');
		Route::get('/nature/image/delete/{id}/{name}', 'AdminController@natureImageDel')->name('natureImageDel');

		Route::get('/trip', 'AdminController@tripView')->name('tripView');
		Route::get('/trip/add', 'AdminController@tripAddView')->name('tripAddView');
		Route::get('/trip/edit/{id}', 'AdminController@tripEditView')->name('tripEditView');
		Route::get('/trip/delete/{id}', 'AdminController@tripDelete')->name('tripDelete');
		Route::post('/trip/add', 'AdminController@tripAdd')->name('tripAdd');
		Route::post('/trip/edit/{id}', 'AdminController@tripEdit')->name('tripEdit');

		Route::get('/tripStep', 'AdminController@tripStepView')->name('tripStepView');
		Route::get('/tripStep/add', 'AdminController@tripStepAddView')->name('tripStepAddView');
		Route::get('/tripStep/edit/{id}', 'AdminController@tripStepEditView')->name('tripStepEditView');
		Route::get('/tripStep/delete/{id}', 'AdminController@tripStepDelete')->name('tripStepDelete');
		Route::post('/tripStep/add', 'AdminController@tripStepAdd')->name('tripStepAdd');
		Route::post('/tripStep/edit/{id}', 'AdminController@tripStepEdit')->name('tripStepEdit');


		Route::get('/subscribeControll', 'AdminController@subscribeControllView')->name('subscribeControllView');

		Route::get('/tripAdventure', 'AdminController@tripAdventureView')->name('tripAdventureView');
		Route::get('/tripAdventure/add', 'AdminController@tripAdventureAddView')->name('tripAdventureAddView');
		Route::get('/tripAdventure/edit/{id}', 'AdminController@tripAdventureEditView')->name('tripAdventureEditView');
		Route::get('/tripAdventure/delete/{id}', 'AdminController@tripAdventureDelete')->name('tripAdventureDelete');
		Route::post('/tripAdventure/add', 'AdminController@tripAdventureAdd')->name('tripAdventureAdd');
		Route::post('/tripAdventure/edit/{id}', 'AdminController@tripAdventureEdit')->name('tripAdventureEdit');


		Route::get('/recommend', 'AdminController@recommendView')->name('recommendView');
		Route::get('/recommend/add', 'AdminController@recommendAddView')->name('recommendAddView');
		Route::get('/recommend/edit/{id}', 'AdminController@recommendEditView')->name('recommendEditView');
		Route::get('/recommend/delete/{id}', 'AdminController@recommendDelete')->name('recommendDelete');
		Route::post('/recommend/add', 'AdminController@recommendAdd')->name('recommendAdd');
		Route::post('/recommend/edit/{id}', 'AdminController@recommendEdit')->name('recommendEdit');
		Route::post('/recommend/image/upload', 'AdminController@recommendImageUpload')->name('recommendImageUpload');


		Route::get('/toSee', 'AdminController@toSeeView')->name('toSeeView');
		Route::get('/toSee/add', 'AdminController@toSeeAddView')->name('toSeeAddView');
		Route::get('/toSee/edit/{id}', 'AdminController@toSeeEditView')->name('toSeeEditView');
		Route::get('/toSee/delete/{id}', 'AdminController@toSeeDelete')->name('toSeeDelete');
		Route::post('/toSee/add', 'AdminController@toSeeAdd')->name('toSeeAdd');
		Route::post('/toSee/edit/{id}', 'AdminController@toSeeEdit')->name('toSeeEdit');
		Route::get('/toSee/image/delete/{id}/{name}', 'AdminController@toSeeImageDel')->name('toSeeImageDel');


		Route::get('/getHere', 'AdminController@getHereView')->name('getHereView');
		Route::get('/getHere/add', 'AdminController@getHereAddView')->name('getHereAddView');
		Route::get('/getHere/edit/{id}', 'AdminController@getHereEditView')->name('getHereEditView');
		Route::get('/getHere/delete/{id}', 'AdminController@getHereDelete')->name('getHereDelete');
		Route::post('/getHere/add', 'AdminController@getHereAdd')->name('getHereAdd');
		Route::post('/getHere/edit/{id}', 'AdminController@getHereEdit')->name('getHereEdit');


		Route::get('/map/edit/{id}', 'AdminController@mapEditView')->name('mapEditView');
		Route::post('/map/edit/{id}', 'AdminController@mapEdit')->name('mapEdit');


		Route::get('/instagram', 'AdminController@instagramView')->name('instagramView');
		Route::get('/instagram/add', 'AdminController@instagramAddView')->name('instagramAddView');
		Route::get('/instagram/edit/{id}', 'AdminController@instagramEditView')->name('instagramEditView');
		Route::get('/instagram/delete/{id}', 'AdminController@instagramDelete')->name('instagramDelete');
		Route::post('/instagram/add', 'AdminController@instagramAdd')->name('instagramAdd');
		Route::post('/instagram/edit/{id}', 'AdminController@instagramEdit')->name('instagramEdit');

		Route::get('/basicInfo', 'AdminController@basicInfoView')->name('basicInfoView');
		Route::get('/basicInfo/add', 'AdminController@basicInfoAddView')->name('basicInfoAddView');
		Route::get('/basicInfo/edit/{id}', 'AdminController@basicInfoEditView')->name('basicInfoEditView');
		Route::get('/basicInfo/delete/{id}', 'AdminController@basicInfoDelete')->name('basicInfoDelete');
		Route::post('/basicInfo/add', 'AdminController@basicInfoAdd')->name('basicInfoAdd');
		Route::post('/basicInfo/edit/{id}', 'AdminController@basicInfoEdit')->name('basicInfoEdit');


		Route::get('/program', 'AdminController@programView')->name('programView');
		Route::get('/program/add', 'AdminController@programAddView')->name('programAddView');
		Route::get('/program/edit/{id}', 'AdminController@programEditView')->name('programEditView');
		Route::get('/program/delete/{id}', 'AdminController@programDelete')->name('programDelete');
		Route::post('/program/add', 'AdminController@programAdd')->name('programAdd');
		Route::post('/program/edit/{id}', 'AdminController@programEdit')->name('programEdit');

		Route::get('/adventureImage', 'AdminController@adventureImageView')->name('adventureImageView');
		Route::get('/adventureImage/add', 'AdminController@adventureImageAddView')->name('adventureImageAddView');
		Route::get('/adventureImage/edit/{id}', 'AdminController@adventureImageEditView')->name('adventureImageEditView');
		Route::get('/adventureImage/delete/{id}', 'AdminController@adventureImageDelete')->name('adventureImageDelete');
		Route::post('/adventureImage/add', 'AdminController@adventureImageAdd')->name('adventureImageAdd');
		Route::post('/adventureImage/edit/{id}', 'AdminController@adventureImageEdit')->name('adventureImageEdit');


		Route::get('/test', 'AdminController@test')->name('test');
		
	});
});