<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_steps', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('name_en')->nullable();
            $table->string('name_ru')->nullable();

            $table->string('route');
            $table->string('route_en')->nullable();
            $table->string('route_ru')->nullable();

            $table->integer('minutes');
            $table->integer('trip_days');
            $table->float('trip_distance');
            
            $table->text('description');
            $table->text('description_en')->nullable();
            $table->text('description_ru')->nullable();

            $table->string('transport');
            $table->string('trip_id');
            $table->text('image');
            $table->string('attachment')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_steps');
    }
}
