<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAroundParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aroundParks', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('name_ru')->nullable();

            $table->string('price')->nullable();
            $table->string('working_hours')->nullable();
            $table->string('place_id')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();

            $table->string('address')->nullable();
            $table->string('address_en')->nullable();
            $table->string('address_ru')->nullable();

            $table->string('contact_person')->nullable();
            $table->string('contact_person_en')->nullable();
            $table->string('contact_person_ru')->nullable();

            $table->string('fb_page')->nullable();


            $table->text('description')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_ru')->nullable(); 

            $table->string('park_id')->nullable();
            $table->string('rating')->nullable();
            $table->string('coordinate_long')->nullable();
            $table->string('coordinate_lat')->nullable();
            $table->text('image')->nullable();


            $table->string('payment')->nullable();
            $table->string('parking')->nullable();
            $table->string('music')->nullable();
            $table->string('kitchen')->nullable();
            $table->string('wifi')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aroundParks');
    }
}
