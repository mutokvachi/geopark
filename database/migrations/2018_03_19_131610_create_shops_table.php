<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('name_ru')->nullable();

            $table->string('address')->nullable();
            $table->string('address_en')->nullable();
            $table->string('address_ru')->nullable();

            $table->string('region')->nullable();
            $table->string('region_en')->nullable();
            $table->string('region_ru')->nullable();

            $table->string('working_hours')->nullable();


            $table->string('type')->nullable();
            $table->string('type_en')->nullable();
            $table->string('type_ru')->nullable();
            

            $table->string('thing_id')->nullable();
            $table->string('park_id')->nullable();
            
            $table->text('image')->nullable();
            
            $table->integer('rating')->default(0);
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();

            $table->string('coordinate_lat')->nullable();
            $table->string('coordinate_long')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
