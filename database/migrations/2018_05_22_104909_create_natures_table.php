<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('natures', function (Blueprint $table) {
            $table->increments('id');

            $table->string('park_id');
            $table->string('type');
            
            $table->string('name');
            $table->string('name_en')->nullable();
            $table->string('name_ru')->nullable();

            $table->text('upper_description');
            $table->text('upper_description_en')->nullable();
            $table->text('upper_description_ru')->nullable();

            $table->text('lower_description')->nullable();
            $table->text('lower_description_en')->nullable();
            $table->text('lower_description_ru')->nullable();
            
            $table->text('images')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('natures');
    }
}
