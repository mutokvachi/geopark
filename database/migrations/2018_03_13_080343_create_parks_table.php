<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('alias');
            
            $table->string('new_dest')->nullable();
            
            $table->string('show_video_gallery')->nullable();

            $table->integer('popularity')->nullable();
            
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('name_ru')->nullable();


            $table->text('description')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_ru')->nullable();


            $table->string('region_id')->nullable();
            $table->string('type_id')->nullable();
            // $table->string('adventure_id')->nullable();
            $table->string('seasons')->nullable();
            $table->string('banner')->nullable();
            
            $table->string('occupation')->nullable();



            $table->string('establish_date')->nullable();

            $table->string('specialist_name')->nullable();
            $table->string('specialist_name_en')->nullable();
            $table->string('specialist_name_ru')->nullable();
            
            $table->string('specialist_mobile')->nullable();
            $table->string('specialist_email')->nullable();
            $table->string('dist_from_tbilisi')->nullable();
            $table->string('park_paths')->nullable();

            $table->string('sea_level')->nullable();
            $table->string('sea_level_en')->nullable();
            $table->string('sea_level_ru')->nullable();
            

            $table->string('administration_address')->nullable();
            $table->string('administration_address_en')->nullable();
            $table->string('administration_address_ru')->nullable();

            $table->text('fees_passes_4')->nullable();
            $table->text('fees_passes_4_en')->nullable();
            $table->text('fees_passes_4_ru')->nullable();




            $table->text('operating_hours_1')->nullable();
            $table->text('operating_hours_1_en')->nullable();
            $table->text('operating_hours_1_ru')->nullable();
            $table->text('operating_hours_2')->nullable();
            $table->text('operating_hours_2_en')->nullable();
            $table->text('operating_hours_2_ru')->nullable();
            $table->text('operating_hours_3')->nullable();
            $table->text('operating_hours_3_en')->nullable();
            $table->text('operating_hours_3_ru')->nullable();
            $table->text('operating_hours_4')->nullable();
            $table->text('operating_hours_4_en')->nullable();
            $table->text('operating_hours_4_ru')->nullable();

            $table->text('fees_passes_1')->nullable();
            $table->text('fees_passes_1_en')->nullable();
            $table->text('fees_passes_1_ru')->nullable();
            $table->text('fees_passes_2')->nullable();
            $table->text('fees_passes_2_en')->nullable();
            $table->text('fees_passes_2_ru')->nullable();
            $table->text('fees_passes_3')->nullable();
            $table->text('fees_passes_3_en')->nullable();
            $table->text('fees_passes_3_ru')->nullable();

            $table->text('guide')->nullable();
            $table->text('guide_en')->nullable();
            $table->text('guide_ru')->nullable();

            $table->text('guideline')->nullable();
            $table->text('guideline_en')->nullable();
            $table->text('guideline_ru')->nullable();

            $table->text('attachments')->nullable();


            $table->string('coordinate_lat')->nullable();
            $table->string('coordinate_long')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parks');
    }
}
