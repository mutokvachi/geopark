<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('things', function (Blueprint $table) {
            $table->increments('id');

            $table->string('height')->nullable();
            
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('name_ru')->nullable();


            $table->string('location')->nullable();
            $table->string('location_en')->nullable();
            $table->string('location_ru')->nullable();

            $table->text('road_to')->nullable();
            $table->text('road_to_en')->nullable();
            $table->text('road_to_ru')->nullable();

            $table->string('coordinate_lat')->nullable();
            $table->string('coordinate_long')->nullable();

            $table->string('overnight_link')->nullable();

            $table->string('specialist_mobile')->nullable();
            $table->string('specialist_email')->nullable();

            $table->text('description')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_ru')->nullable();

            $table->text('image')->nullable();
            $table->string('park_id')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('things');
    }
}
