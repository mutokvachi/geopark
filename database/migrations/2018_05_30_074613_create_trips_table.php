<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('name_en')->nullable();
            $table->string('name_ru')->nullable();

            $table->integer('trip_days');
            $table->float('trip_distance');
            
            $table->text('route');
            $table->text('route_en')->nullable();
            $table->text('route_ru')->nullable();

            $table->string('highlights');
            $table->string('highlights_en')->nullable();
            $table->string('highlights_ru')->nullable();
            
            $table->string('seasons')->nullable();
            $table->string('level')->nullable();

            $table->string('park_id');
            $table->text('image');
            $table->string('coordinate_lat');
            $table->string('coordinate_lng');

            $table->string('sort_by')->default(999);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
