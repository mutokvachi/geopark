<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToSeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('to_sees', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('park_id');
            $table->string('type')->nullable();

            $table->string('name');
            $table->string('name_en')->nullable();
            $table->string('name_ru')->nullable();

            $table->text('description');
            $table->text('description_en')->nullable();
            $table->text('description_ru')->nullable();

            $table->string('coordinate_lat');
            $table->string('coordinate_long');

            $table->text('images');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('to_sees');
    }
}
