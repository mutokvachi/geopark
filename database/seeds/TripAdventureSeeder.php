<?php

use App\TripAdventure;
use Illuminate\Database\Seeder;

class TripAdventureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TripAdventure::select('*')->delete();
		TripAdventure::insert([
			[
				'trip_id' => '1',
	            'adventure_id'  => '2',
        	],

        	[
				'trip_id' => '1',
	            'adventure_id'  => '1',
        	],
		]); 
    }
}
