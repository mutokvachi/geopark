<?php

use App\GetHere;
use Illuminate\Database\Seeder;

class GetHereTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GetHere::select('*')->delete();
		GetHere::insert([
			[
				'park_id' => 1,
				'name' => 'name 1',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru'
        	],

        	[
				'park_id' => 1,
				'name' => 'name 2',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru'
        	],

        	[
				'park_id' => 1,
				'name' => 'name 3',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru'
        	],

        	[
				'park_id' => 1,
				'name' => 'name 4',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru'
        	],
		]); 
    }
}
