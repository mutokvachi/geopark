<?php

use App\Place;
use Illuminate\Database\Seeder;

class PlacesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Place::select('*')->delete();
		Place::insert([
			[
				'name'  => 'name hotel',
	            'type'  => 'ჰოტელი',
	            'type_en'  => 'hotel',
	            'type_ru'  => 'type_ru',

        	],

        	[
				'name'  => 'name apartments',
	            'type'  => 'აპარტამენტები',
	            'type_en'  => 'apartments',
	            'type_ru'  => 'type_ru',

        	],

        	[
				'name'  => 'name hostel',
	            'type'  => 'ჰოსტელი',
	            'type_en'  => 'hostel',
	            'type_ru'  => 'type_ru',

        	],
		]);   
    }
}
