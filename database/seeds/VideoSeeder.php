<?php

use App\Video;
use Illuminate\Database\Seeder;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Video::select('*')->delete();
		Video::insert([
			[
				'park_id' => '1',
				'name' => 'name KA',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'video'  => '["152750155724file_example_MP4_480_1_5MG.mp4"]',
	            'type' => 'video',
        	],

        	[
				'park_id' => '1',
				'name' => 'name KA',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'video'  => '["1527504288100SampleVideo_1280x720_1mb.mp4"]',
	            'type' => 'video',
        	],

        	[
				'park_id' => '1',
				'name' => 'name KA',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'video'  => '["152750155724file_example_MP4_480_1_5MG.mp4"]',
	            'type' => 'video',
        	],
        	
		]); 
    }
}
