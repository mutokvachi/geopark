<?php

use App\AroundPark;
use Illuminate\Database\Seeder;

class AroundParkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AroundPark::select('*')->delete();
		AroundPark::insert([
			[
				'name' => 'name 2',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '35',
	            'working_hours' => '24/7',
	            'place_id' => '1',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '3',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => 'on',
	            'music'  => '',
	            'kitchen'  => '',
	            'wifi'  => '',
	        ],

	        [
				'name' => 'name 5',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '22',
	            'working_hours' => '24/7',
	            'place_id' => '1',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '4',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => '',
	            'music'  => '',
	            'kitchen'  => 'on',
	            'wifi'  => 'on',
	        ],

	        [
				'name' => 'name 7',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '11',
	            'working_hours' => '24/7',
	            'place_id' => '2',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '0',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => 'on',
	            'music'  => '',
	            'kitchen'  => 'on',
	            'wifi'  => '',
	        ],

	        [
				'name' => 'name 8',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '37',
	            'working_hours' => '24/7',
	            'place_id' => '2',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '0',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => 'on',
	            'music'  => 'on',
	            'kitchen'  => '',
	            'wifi'  => 'on',
	        ],

	        [
				'name' => 'name 9',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '130',
	            'working_hours' => '24/7',
	            'place_id' => '2',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '2',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => 'on',
	            'music'  => '',
	            'kitchen'  => '',
	            'wifi'  => '',
	        ],

	        [
				'name' => 'name 17',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '35',
	            'working_hours' => '24/7',
	            'place_id' => '3',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '0',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => 'on',
	            'music'  => '',
	            'kitchen'  => '',
	            'wifi'  => '',
	        ],

	        [
				'name' => 'name 17',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '55',
	            'working_hours' => '24/7',
	            'place_id' => '3',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '1',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => 'on',
	            'music'  => '',
	            'kitchen'  => '',
	            'wifi'  => '',
	        ],

	        [
				'name' => 'name 17',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '7',
	            'working_hours' => '24/7',
	            'place_id' => '3',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '0',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => 'on',
	            'music'  => '',
	            'kitchen'  => '',
	            'wifi'  => '',
	        ],

	        [
				'name' => 'name 17',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '80',
	            'working_hours' => '24/7',
	            'place_id' => '3',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '0',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => 'on',
	            'music'  => '',
	            'kitchen'  => '',
	            'wifi'  => '',
	        ],

	        [
				'name' => 'name 17',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '75',
	            'working_hours' => '24/7',
	            'place_id' => '3',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '0',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => 'on',
	            'music'  => '',
	            'kitchen'  => '',
	            'wifi'  => '',
	        ],

	        [
				'name' => 'name 17',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'price' => '78',
	            'working_hours' => '24/7',
	            'place_id' => '3',
	            'mobile' => '555 12 3311',
	            'email' => 'email',
	            'website' => 'website',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'contact_person' => 'contact_person',
	            'contact_person_en' => 'contact_person_en',
	            'contact_person_ru' => 'contact_person_ru',
	            'fb_page' => 'fb_page',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'park_id' => '1',
	            'rating' => '0',
	            'coordinate_long' => '43.82742997131345',
	            'coordinate_lat' => '41.903443533015256',
	            'image' => '["15266389052211178633dp_12_n_f.jpg"]',
	            'payment'  => 'on',
	            'parking'  => 'on',
	            'music'  => '',
	            'kitchen'  => '',
	            'wifi'  => '',
	        ],
		
		]);  
    }
}
