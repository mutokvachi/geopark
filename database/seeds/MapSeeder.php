<?php

use App\Map;
use Illuminate\Database\Seeder;

class MapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Map::select('*')->delete();
		Map::insert([
        	[
				'center' => '41.7688,44.7688',
				'lat'  => '52.506,55.58, 47.6549',
				'lng' => '13.284,36.8251, 57.9399',
                'name' => 'სამსუნგი, პიეიჩპი',
                'name_en' => 'samsung, php',
                'name_ru' => 'samsung_ru, php_ru',
        	],
        	
		]); 
    }
}
