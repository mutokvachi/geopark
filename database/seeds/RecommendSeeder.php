<?php

use App\Recommend;
use Illuminate\Database\Seeder;

class RecommendSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Recommend::select('*')->delete();
		Recommend::insert([
			[
				'park_id' => '1',
				'name' => 'name 1',
				'name_en' => 'name_en',
				'name_ru' => 'name_ru',
				'description' => 'description',
				'description_en' => 'description_en',
				'description_ru' => 'description_ru',
				'date' => '2018-06-29',
				'img' => '["152838152164banner2.jpg"]',
        	],
        	[
				'park_id' => '1',
				'name' => 'name 2',
				'name_en' => 'name_en',
				'name_ru' => 'name_ru',
				'description' => 'description 2',
				'description_en' => 'description_en',
				'description_ru' => 'description_ru',
				'date' => '2018-06-24',
				'img' => '["152838152164banner2.jpg"]',
        	],
		]);
    }
}
