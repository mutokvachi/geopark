<?php

use App\Nature;
use Illuminate\Database\Seeder;

class NatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Nature::select('*')->delete();
		Nature::insert([
			[
				'park_id' => '1',
				'type' => 'nature',
				'name' => 'name',
				'name_en' => 'name_en',
				'name_ru' => 'name_ru',
				'upper_description' => 'upper_description',
				'upper_description_en' => 'upper_description_en',
				'upper_description_ru' => 'upper_description_ru',
				'lower_description' => 'lower_description',
				'lower_description_en' => 'lower_description_en',
				'lower_description_ru' => 'lower_description_ru',
				'images' => '["152698725022img1.jpg","152698725066img2.jpg"]',
        	],

        	[
				'park_id' => '1',
				'type' => 'culture',
				'name' => 'name 3',
				'name_en' => 'name_en',
				'name_ru' => 'name_ru',
				'upper_description' => 'upper_description',
				'upper_description_en' => 'upper_description_en',
				'upper_description_ru' => 'upper_description_ru',
				'lower_description' => 'lower_description',
				'lower_description_en' => 'lower_description_en',
				'lower_description_ru' => 'lower_description_ru',
				'images' => '["152698725022img1.jpg","152698725066img2.jpg"]',
        	],

        	[
				'park_id' => '1',
				'type' => 'nature',
				'name' => 'name 2',
				'name_en' => 'name_en',
				'name_ru' => 'name_ru',
				'upper_description' => 'upper_description',
				'upper_description_en' => 'upper_description_en',
				'upper_description_ru' => 'upper_description_ru',
				'lower_description' => 'lower_description',
				'lower_description_en' => 'lower_description_en',
				'lower_description_ru' => 'lower_description_ru',
				'images' => '["152698721193banner2.jpg","152698721743nature-banner.jpg"]',
        	],

        	[
				'park_id' => '1',
				'type' => 'culture',
				'name' => 'name 3 3',
				'name_en' => 'name_en',
				'name_ru' => 'name_ru',
				'upper_description' => 'upper_description',
				'upper_description_en' => 'upper_description_en',
				'upper_description_ru' => 'upper_description_ru',
				'lower_description' => 'lower_description',
				'lower_description_en' => 'lower_description_en',
				'lower_description_ru' => 'lower_description_ru',
				'images' => '["152698725022img1.jpg","152698725066img2.jpg"]',
        	],
		]);
    }
}
