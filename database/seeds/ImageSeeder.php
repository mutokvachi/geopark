<?php

use App\Image;
use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Image::select('*')->delete();
		Image::insert([
			[
				'park_id' => '1',
				'name' => 'name KA',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'image'  => '["152690407814nature-banner.jpg"]',
        	],

        	[
				'park_id' => '1',
				'name' => 'name KA',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'image'  => '["152750166262img3.jpg"]',
        	],

        	[
				'park_id' => '1',
				'name' => 'name KA',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'image'  => '["152699341734img1.jpg"]',
        	],

        	[
				'park_id' => '1',
				'name' => 'name KA',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'image'  => '["152690407814nature-banner.jpg"]',
        	],

		]);    
    }
}
