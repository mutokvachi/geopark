<?php

use App\Adventure;
use Illuminate\Database\Seeder;

class AdventuresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Adventure::select('*')->delete();
		Adventure::insert([
			[
				'park_id' => '1',
				'name' => 'adventure 1',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'images' => '["15245624114311178633dp_12_n_f.jpg", "152690407814nature-banner.jpg"]',
	            'icon' => '["15245624114311178633dp_12_n_f.jpg"]',
	            'seasons' => '["winter","spring","summer"]',
	            'coordinates' => '[{"lat":41.41095127829677,"lng":44.55252762756345}]',
	            'map_icon' => '/main/img/adventure-icons/10.png',
	            'sea_level' => '300'
	        ],

	        [
				'park_id' => '1',
				'name' => 'adventure 2',
	            'name_en' => 'name_en 2',
	            'name_ru' => 'name_ru 2',
	            'description' => 'description 2',
	            'description_en' => 'description_en 2',
	            'description_ru' => 'description_ru 2',
	            'images' => '["152690407814nature-banner.jpg","15245624114311178633dp_12_n_f.jpg","152690407814nature-banner.jpg"]',
	            'icon' => '["15245624114311178633dp_12_n_f.jpg"]',
	            'seasons' => '["winter","spring","summer", "autumn"]',
	            'coordinates' => '[{"lat":41.41095127829677,"lng":44.55252762756345}]',
	            'map_icon' => '/main/img/adventure-icons/10.png',
	            'sea_level' => '1500'
	        ],

	        [
				'park_id' => '0',
				'name' => 'name 2',
	            'name_en' => 'name_en 2',
	            'name_ru' => 'name_ru 2',
	            'description' => 'description 2',
	            'description_en' => 'description_en 2',
	            'description_ru' => 'description_ru 2',
	            'images' => '["152690407814nature-banner.jpg","15245624114311178633dp_12_n_f.jpg","152690407814nature-banner.jpg"]',
	            'icon' => '["15245624114311178633dp_12_n_f.jpg"]',
	            'seasons' => '["winter","spring","summer", "autumn"]',
	            'coordinates' => '[{"lat":41.41095127829677,"lng":44.55252762756345}]',
	            'map_icon' => '/main/img/adventure-icons/10.png',
	            'sea_level' => '1100'
	        ],
		]);  
    }
}
