<?php

use App\TripStep;
use Illuminate\Database\Seeder;

class TripStepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TripStep::select('*')->delete();
		TripStep::insert([
        	[
				'name' => 'tripstep 1',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'minutes' => '105',
	            'trip_days' => '1',
	            'trip_distance' => '14',
	            'transport' => '["car","bus"]',
	            'attachment' => '["152766936241521218322221516361892Cruscotto Finanziario OK (6).xls"]',

	            'route' => 'თბილისი-ბათუმი',
	            'route_en' => 'route_en',
	            'route_ru' => 'route_ru',
	            
	            'description' => 'გამარჯობა ეს არის დესქრიფშენი',
	            'description_en' => 'route_en',
	            'description_ru' => 'route_ru',

	            'trip_id' => '1',
	            'image' => '["152766867594img3.jpg"]',
        	],

        	[
				'name' => 'TripStep 2',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'minutes' => '105',
	            'trip_days' => '2',
	            'trip_distance' => '14',
	            'transport' => '["car","bus", "horse"]',
	            'attachment' => '["152766936241521218322221516361892Cruscotto Finanziario OK (6).xls"]',
	            
	            'route' => 'თბილისი-ბათუმი',
	            'route_en' => 'route_en',
	            'route_ru' => 'route_ru',

	            'description' => 'გამარჯობა ეს არის დესქრიფშენი',
	            'description_en' => 'route_en',
	            'description_ru' => 'route_ru',

	            'trip_id' => '1',
	            'image' => '["152766867594img3.jpg"]',
        	],
		]); 
    }
}
