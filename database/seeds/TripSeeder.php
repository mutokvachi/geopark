<?php

use App\Trip;
use Illuminate\Database\Seeder;

class TripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Trip::select('*')->delete();
		Trip::insert([
        	[
				'name' => 'trip 1',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',

	            'trip_days' => '3',
	            'trip_distance' => '52',
	            
	            'route' => 'კახეთი-მცხეთა',
	            'route_en' => 'route_en',
	            'route_ru' => 'route_ru',

	            'highlights' => 'ჩურჩხელა, კამფეტები, შოკოლადები',
	            'highlights_en' => 'highlights_en',
	            'highlights_ru' => 'highlights_ru',

	            'park_id' => '1',
	            'image' => '["152766884883img3.jpg"]',
	            'coordinate_lat' => '42.27033925772377',
	            'coordinate_lng' => '42.67386551818845',
        	],

        	[
				'name' => 'trip 2',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',

	            'trip_days' => '4',
	            'trip_distance' => '2',
	            
	            'route' => 'კახეთი-მცხეთა',
	            'route_en' => 'route_en',
	            'route_ru' => 'route_ru',

	            'highlights' => 'ჩურჩხელა, კამფეტები, შოკოლადები',
	            'highlights_en' => 'highlights_en',
	            'highlights_ru' => 'highlights_ru',

	            'park_id' => '1',
	            'image' => '["152766884883img3.jpg"]',
	            'coordinate_lat' => '42.27033925772377',
	            'coordinate_lng' => '42.67386551818845',
        	],
		]); 
    }
}
