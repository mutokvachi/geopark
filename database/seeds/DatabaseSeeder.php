<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(RegionsSeeder::class);
        $this->call(AdventuresSeeder::class);
        $this->call(ParksSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(MemorySeeder::class);
        $this->call(PlacesSeeder::class);
        $this->call(AroundParkSeeder::class);
        $this->call(ThingsSeeder::class);
        $this->call(ShopsSeeder::class);
        $this->call(NatureSeeder::class);
        $this->call(EventSeeder::class);
        $this->call(AlertSeeder::class);
        $this->call(VideoSeeder::class);
        $this->call(ImageSeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(TripSeeder::class);
        $this->call(TripStepSeeder::class);
        $this->call(TripAdventureSeeder::class);
        $this->call(RecommendSeeder::class);
        $this->call(GetHereTableSeeder::class);
        $this->call(MapSeeder::class);
    }
}
