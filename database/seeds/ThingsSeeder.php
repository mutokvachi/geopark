<?php

use App\ToBuy;
use Illuminate\Database\Seeder;

class ThingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ToBuy::select('*')->delete();
		ToBuy::insert([
			[
	        	'name' => 'name',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'type' => 'წიგნები, ტანსაცმელი, აქსესუარები',
	            'type_en' => 'books, clothes, accessories',
	            'type_ru' => 'type_ru',
	            'images' => '["15268985187523768601_1712662908768275_1029861000_o.jpg","15268985181523768709_1712662918768274_1538206450_o.jpg"]',
	            'park_id' => '1',
        	],

        	[
	        	'name' => 'name 2 ',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'type' => 'წიგნები, ტანსაცმელი, აქსესუარები',
	            'type_en' => 'books, clothes, accessories',
	            'type_ru' => 'type_ru',
	            'images' => '["15268985187523768601_1712662908768275_1029861000_o.jpg","15268985181523768709_1712662918768274_1538206450_o.jpg"]',
	            'park_id' => '1',
        	],

        	[
	        	'name' => 'name 3',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'type' => 'წიგნები, ტანსაცმელი, აქსესუარები',
	            'type_en' => 'books, clothes, accessories',
	            'type_ru' => 'type_ru',
	            'images' => '["15268985187523768601_1712662908768275_1029861000_o.jpg","15268985181523768709_1712662918768274_1538206450_o.jpg"]',
	            'park_id' => '1',
        	],

        	[
	        	'name' => 'name 4',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'description' => 'description',
	            'description_en' => 'description_en',
	            'description_ru' => 'description_ru',
	            'type' => 'წიგნები, ტანსაცმელი, აქსესუარები',
	            'type_en' => 'books, clothes, accessories',
	            'type_ru' => 'type_ru',
	            'images' => '["15268985187523768601_1712662908768275_1029861000_o.jpg","15268985181523768709_1712662918768274_1538206450_o.jpg"]',
	            'park_id' => '1',
        	],

		]); 
    }
}
