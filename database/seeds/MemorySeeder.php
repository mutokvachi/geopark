<?php

use App\Memory;
use Illuminate\Database\Seeder;

class MemorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Memory::select('*')->delete();
		Memory::insert([
			[
				'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 1',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '0',
                'extra' => '<p>testttt</p>',
        	],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 2',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '0',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 3',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 4',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 5',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

             [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 6',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

             [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 7',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

             [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 8',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

             [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 9',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg","15265568044423730945_1712662905434942_1730267349_o.jpg","15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 10',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],
            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 11',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 12',
                'lang' => 'ka',
                'type' => 'blog',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '0',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 13',
                'lang' => 'ka',
                'type' => 'video',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 14',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '2',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 15',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 16',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 17',
                'lang' => 'ka',
                'type' => 'video',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 18',
                'lang' => 'ka',
                'type' => 'blog',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt this is blog type</p>',
            ],

            [
                'user_id' => '2',
                'park_id' => '1',
                'title' => 'this is memory ttitle 19',
                'lang' => 'ka',
                'type' => 'image',
                'date' => '17-May-2018',
                'images' => '["15265568044423730945_1712662905434942_1730267349_o.jpg"]',
                'tags' => 'asd asd, asdas d,as',
                'status' => '1',
                'extra' => '<p>testttt</p>',
            ],
            
		]); 
    }
}
