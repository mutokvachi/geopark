<?php

use App\Region;
use Illuminate\Database\Seeder;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Region::select('*')->delete();
		Region::insert([
			[
				'name' => 'name',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru'
        	],

        	[
				'name' => 'name 2',
	            'name_en' => 'name_en 2',
	            'name_ru' => 'name_ru 2'
        	],

        	[
				'name' => 'name 3',
	            'name_en' => 'name_en 3',
	            'name_ru' => 'name_ru 3'
        	],
		]);        
    }
}
