<?php

use App\Alert;
use Illuminate\Database\Seeder;

class AlertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Alert::select('*')->delete();
		Alert::insert([
			[
				'date' => '2018-04-15',
		        'park_id' => '1',
		        'status' => '1',

		        'description' => 'here goes description',
		        'description_en' => 'description_en',
		        'description_ru' => 'description_ru',
        	],

        	[
				'date' => '2018-04-12',
		        'park_id' => '1',
		        'status' => '1',

		        'description' => 'here goes description',
		        'description_en' => 'description_en',
		        'description_ru' => 'description_ru',
        	],

        	[
				'date' => '2018-04-10',
		        'park_id' => '1',
		        'status' => '1',

		        'description' => 'here goes description',
		        'description_en' => 'description_en',
		        'description_ru' => 'description_ru',
        	],

        	[
				'date' => '2018-04-11',
		        'park_id' => '1',
		        'status' => '0',

		        'description' => 'here goes description',
		        'description_en' => 'description_en',
		        'description_ru' => 'description_ru',
        	],
		]); 
    }
}
