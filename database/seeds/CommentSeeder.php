<?php

use App\Comment;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comment::select('*')->delete();
		Comment::insert([
			[
				'user_id' => '2',
				'type' => 'park',
	            'type_id' => 'testPark',
	            'rating' => '2',
	            'description' => 'this is test  1',
	            'time' => '1527503127',
        	],

        	[
				'user_id' => '2',
				'type' => 'park',
	            'type_id' => 'testPark',
	            'rating' => '1',
	            'description' => 'this is test 2',
	            'time' => '1527503127',
        	],

        	[
				'user_id' => '2',
				'type' => 'park',
	            'type_id' => 'testPark',
	            'rating' => '5',
	            'description' => 'this is test 3',
	            'time' => '1527503127',
        	],

        	
		]);    
    }
}
