<?php

use App\Shop;
use Illuminate\Database\Seeder;

class ShopsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Shop::select('*')->delete();
		Shop::insert([
			[
				'name' => 'name',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'region' => 'region',
	            'region_en' => 'region_en',
	            'region_ru' => 'region_ru',
	            'working_hours' => '24/7',
	            'type' => 'shopify',
	            'type_en' => 'type_en',
	            'type_ru' => 'type_ru',
	            'thing_id' => '1',
	            'park_id' => '1',
	            'image' => '["15269009293823768601_1712662908768275_1029861000_o.jpg"]',
	            'rating' => '3',
	            'phone' => '555 55-55-11',
	            'email' => 'shop@example.com',
	            'website' => 'www.shops.ge',
	            'coordinate_lat' => '41.87890839127881',
	            'coordinate_long' => '42.37723465881345',
        	],

        	[
				'name' => 'name 2',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'region' => 'region',
	            'region_en' => 'region_en',
	            'region_ru' => 'region_ru',
	            'working_hours' => '24/7',
	            'type' => 'shopify',
	            'type_en' => 'type_en',
	            'type_ru' => 'type_ru',
	            'thing_id' => '1',
	            'park_id' => '1',
	            'image' => '["15269009293823768601_1712662908768275_1029861000_o.jpg"]',
	            'rating' => '5',
	            'phone' => '',
	            'email' => 'shop@example.com',
	            'website' => '',
	            'coordinate_lat' => '41.87890839127881',
	            'coordinate_long' => '42.37723465881345',
        	],

        	[
				'name' => 'name 3',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'address' => 'address',
	            'address_en' => 'address_en',
	            'address_ru' => 'address_ru',
	            'region' => 'region',
	            'region_en' => 'region_en',
	            'region_ru' => 'region_ru',
	            'working_hours' => '24/7',
	            'type' => 'shopify',
	            'type_en' => 'type_en',
	            'type_ru' => 'type_ru',
	            'thing_id' => '1',
	            'park_id' => '1',
	            'image' => '["15269009293823768601_1712662908768275_1029861000_o.jpg"]',
	            'rating' => '1',
	            'phone' => '555 55-55-11',
	            'email' => '',
	            'website' => 'www.shops.ge',
	            'coordinate_lat' => '41.87890839127881',
	            'coordinate_long' => '42.37723465881345',
        	],
		]);  
    }
}
