<?php

use App\Slider;
use Illuminate\Database\Seeder;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slider::select('*')->delete();
		Slider::insert([
			[
				'park_id' => 'testPark',
				'name' => 'name KA',
	            'name_en' => 'name_en',
	            'name_ru' => 'name_ru',
	            'img'  => '["152690406118banner2.jpg"]',
        	],

        	[
        		'park_id' => 'testPark',
				'name' => 'name 2 ka',
	            'name_en' => 'name_en 2',
	            'name_ru' => 'name_ru 2',
	            'img'  => '["152690407814nature-banner.jpg"]',
        	],

        	[
        		'park_id' => 'testPark',
				'name' => 'name 3 ka',
	            'name_en' => 'name_en 3',
	            'name_ru' => 'name_ru 3',
	            'img'  => '["152690406118banner2.jpg"]',
        	],
		]);    
    }
}
