<?php

use App\Park;
use Illuminate\Database\Seeder;

class ParksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Park::select('*')->delete();
        Park::create([
        	'alias' => 'testPark',
            'name' => 'ალგეთის ეროვნული პარკი 2',             
            'name_en' => '',              
            'name_ru' => '',              
            'description' => 'თბილისიდან 1 საათის დაშორებით მდებარე ალგეთის ეროვნული პარკი საუკეთესო ტერიტორიაა მეგობრებთან ერთად დროის გასატარებლად. მდინარე ალგეთის ხეობისა და შერეული ტყით დაფარული მთების ხედების ნახვასთან ერთად მოინახულებთ დაშბაშის, ბირთვისისა და სამშვილდის კანიონების ბუნების ძეგლებს, ესტუმრებით კლდეკარის ციხე-სიმაგრეს. ალგეთის სახელმწიფო ნაკრძალი 1965 წელს აღმოსავლეთის ნაძვისა და კავკასიური სოჭის გავრცელების სამხრეთ-აღმოსავლეთით არეალის საზღვრის დაცვის მიზნით შეიქმნა. 2007 წელს ალგეთის სახელმწიფო ნაკრძალს მიენიჭა ეროვნული პარკის სტატუსი. ეროვნული პარკის ტერიტორია მდებარეობს ზღვის დონიდან 1100-1950 მ-ზე და გამოირჩევა ლანდშაფტური და ბიოლოგიური მრავალფეროვნებით. ასევე აღსანიშნავია რელიქტური ტყეები, რაც მას განსაკუთრებულ მიმზიდველობას ანიჭებს. ეროვნული პარკის ტერიტორიაზე თავმოყრილია სხვადასხვა ბოტანიკურ-გეოგრაფიული ოლქების: კოლხეთის, ჰირკანის, იბერიის, კავკასიურის, წინააზიურის და სხვა ფლორის წარმომადგენლები. ალგეთის ეროვნულ პარკში გვხვდება 900 წლოვანი მაღალი მთის მუხა.',              
            'description_en' => '',              
            'description_ru' => '',              
            'region_id' => '1',              
            'type_id' => '["1"]',              
            'seasons' => '["winter","spring","summer", "autumn"]',              
            'sea_level' => '1100-დან 1950-მდე',
            'banner' => '["152690406118banner2.jpg"]',               

            'operating_hours_1' => '',              
            'operating_hours_1_en' => '',              
            'operating_hours_1_ru' => '',              
            'operating_hours_2' => '10:00-დან 17:30-მდე',              
            'operating_hours_2_en' => '',              
            'operating_hours_2_ru' => '',              
            'operating_hours_3' => '10:00-დან 18:00-მდე',              
            'operating_hours_3_en' => '',              
            'operating_hours_3_ru' => '',              
            'operating_hours_4' => '10:00-დან 17:00-მდე',              
            'operating_hours_4_en' => '',              
            'operating_hours_4_ru' => '',              

            'fees_passes_1' => '12.50 GEL <br> per person',              
            'fees_passes_1_en' => '',              
            'fees_passes_1_ru' => '',              
            'fees_passes_2' => '12.50 GEL <br> per person',              
            'fees_passes_2_en' => '',              
            'fees_passes_2_ru' => '',              
            'fees_passes_3' => '12.50 GEL <br> per person',              
            'fees_passes_3_en' => '',              
            'fees_passes_3_ru' => '',              

            'guide' => 'this is my guidethis is my guidethis is my guidethis is my guidethis is my guide',              
            'guide_en' => '',              
            'guide_ru' => '',              

            'guideline' => 'this is my guideline this is my guidelinethis is my guidelinethis is my guidelinethis is my guidelinethis is my guideline',              
            'guideline_en' => '',              
            'guideline_ru' => '',              

            'attachments' => '["15215398032261725.doc", "15215409234461725.doc"]',              


            'coordinate_lat' => '41.72092522856933',              
            'coordinate_long' => '44.35221325856037',              
        ]);
    }
}
