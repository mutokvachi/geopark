<?php

use App\Event;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::select('*')->delete();
		Event::insert([
			[
				'price' => '35',
				'name' => 'name 1',
				'name_en' => 'name_en',
				'name_ru' => 'name_ru',
				'start_time' => '00:59',
				'finish_time' => '12:59',
				'location' => 'location',
				'location_en' => 'location_en',
				'location_ru' => 'location_ru',
				'description' => 'description',
				'description_en' => 'description_en',
				'description_ru' => 'description_ru',
				'date' => '2018-05-23',
				'park_id' => '1',
				'image' => '["152699341734img1.jpg"]',
        	],

        	[
				'price' => '25',
				'name' => 'name 2',
				'name_en' => 'name_en',
				'name_ru' => 'name_ru',
				'start_time' => '00:59',
				'finish_time' => '12:59',
				'location' => 'location',
				'location_en' => 'location_en',
				'location_ru' => 'location_ru',
				'description' => 'description',
				'description_en' => 'description_en',
				'description_ru' => 'description_ru',
				'date' => '2018-05-22',
				'park_id' => '1',
				'image' => '["152699341734img1.jpg"]',
        	],

        	[
				'price' => '35',
				'name' => 'name 3',
				'name_en' => 'name_en',
				'name_ru' => 'name_ru',
				'start_time' => '00:59',
				'finish_time' => '12:59',
				'location' => 'location',
				'location_en' => 'location_en',
				'location_ru' => 'location_ru',
				'description' => 'description',
				'description_en' => 'description_en',
				'description_ru' => 'description_ru',
				'date' => '2018-05-25',
				'park_id' => '1',
				'image' => '["152699341734img1.jpg"]',
        	],

        	[
				'price' => '15',
				'name' => 'name 3',
				'name_en' => 'name_en',
				'name_ru' => 'name_ru',
				'start_time' => '00:59',
				'finish_time' => '12:59',
				'location' => 'location',
				'location_en' => 'location_en',
				'location_ru' => 'location_ru',
				'description' => 'description',
				'description_en' => 'description_en',
				'description_ru' => 'description_ru',
				'date' => '2018-05-25',
				'park_id' => '1',
				'image' => '["152699344220img3.jpg"]',
        	],
		]);
    }
}
