<?php 

return [
	'test' => 'ტესტი',

	'page_404_not_found' => 'გვერდი ვერ მოიძებნა',
	'page_404_back' => 'მთავარ გვერდზე დაბრუნება',
	'default_button' => 'განახლება',
	'main_attraction_passes' => 'ფასიანი სერვისები',
	'main_start_exploring' => 'აღმოაჩინე',
	'main_search' => 'ძიება',
	'main_add_favourites' => 'რჩეულებში დამატება',  
	'main_comment_spam' => 'თქვენ უნდა დაიცადოთ 1 საათი, შემდეგი შეფასების მისაცემად !', 
	'main_hour_h' => 'სთ',
	'main_hour_m' => 'წთ',
	'main_search_result' => 'მონაცემი ვერ მოიძებნა !',

	'main_adventures_title' => 'ჩვენი პარკების თავგადასავლები',
	'main_mainpage_seasons' => 'სეზონები',
	'main_sort_region' => 'რეგიონები',
	'main_sort_park' => 'პარკები',
	'main_sort_activity' => 'თავგადასავლები',
	
	'main_back_to' => 'უკან დაბრუნება',
	'main_empty_section' => 'ცარიელია',

	'main_sign_up_newsletter' => 'გამოიწერეთ ახალი ამბები',
	'main_accept_terms_sub' => 'გამოწერის ღილაკზე დაჭერით თქვენ ეთანხმებით ',
	'main_terms_conditions' => 'ჩვენს წესებს',
	'main_copyright' => 'ყველა წესი დაცულია "საქართველოს ეროვნული პარკები" 2018',

	'master_read_more' => 'მეტის ნახვა',
	'master_see_more' => 'ყველას ნახვა',

	'master_nature_title' => 'ბუნება',
	'master_culture_title' => 'კულტურა',

	'master_geolocation_validation' => 'გთხოვთ ჩართეთ ლოკაციის მაჩვენებელი',

	'new_destinations' => 'ახალი მიმართულებები',
	'national_parks' => 'ეროვნული პარკები',
	'adventure' => 'თავგადასავლები',
	'plan_your_trip' => 'დაგეგმეთ მოგზაურობა',
	'around_me' => 'ჩემს გარშემო',
	'follow_us' => 'გამოგვყევი',
	'english' => 'en',
	'russian' => 'русский',
	'georgian' => 'ქა',

	'login_fb_onnect' => 'Facebook ავტორიზაცია',
	'login_google_connect' => 'Google ავტორიზაცია',
	'login_forgot' => 'დაგავიწყდა პაროლი?',
	'login_recover' => 'პაროლის აღდგენა',
	'login_login' => 'შესვლა',
	'login_log_in' => 'შესვლა',
	'login_register' => 'რეგისტრაცია',
	'login_agree' => 'მე ვეთანხმები',
	'login_terms' => 'წესებს და პირობებს',
	'login_email' => 'ელ-ფოსტა',
	'login_password' => 'პაროლი',
	'login_name' => 'თქვენი სახელი',
	'login_confirm' => 'დაადასტურეთ პაროლი',


	'profile_favorities' => 'ფავორიტები',
	'profile_tickets' => 'ჩემი ბილეთები',
	'profile_memories' => 'ჩემი მოგონებები',
	'profile_places' => 'ჩემი ადგილები',
	'profile_profile' => 'ჩემი პროფილი',
	'profile_password' => 'პაროლის შეცვლა',
	'profile_january' => 'იანვარი',
	'profile_february' => 'თებერვალი',
	'profile_march' => 'მარტი',
	'profile_april' => 'აპრილი',
	'profile_may' => 'მაისი',
	'profile_june' => 'ივნისი',
	'profile_jun' => 'ივნისი',
	'profile_july' => 'ივლისი',
	'profile_jul' => 'ივლისი',
	'profile_august' => 'აგვისტო',
	'profile_september' => 'სექტემბერი',
	'profile_oct' => 'ოქტომბერი',
	'profile_nov' => 'ნოემბერი',
	'profile_dec' => 'დეკემბერი',

	'profile_Name' => 'სახელი',
	'profile_Birth' => 'დაბადების თარიღი',
	'profile_Day' => 'დღე',
	'profile_Month' => 'თვე',
	'profile_Year' => 'წელი',
	'profile_id_Number' => 'პირადი ნომერი',
	'profile_Mail' => 'ელ-ფოსტა',
	'profile_number' => 'ტელეფონის ნომერი',
	'profile_Photo' => 'პროფილის სურათი',
	'profile_photo' => 'სურათის შეცვლა',
	'profile_save' => 'დამახსოვრება',
	'profile_success' => 'პროფილის ინფორმაცია წარმატებით განახლდა',
	
	'subscribe_success' => 'ელ-ფოსტა წარმატებით იქნდა გამოწერილი',
	'subscribe_newsletter' => 'გამოიწერეთ სიახლეები',
    'subscribe_email' => 'ელ-ფოსტა',
    'subscribe_events' => 'ივენთები',
    'subscribe_trips' => 'ტურები',
    'subscribe_alerts' => 'შეტყიბინებები',
    'subscribe_services' => 'სერვისები',
    'subscribe_agree' => 'მე ვეთანხმები ',
    'subscribe_terms' => 'წესებს და პირობებს',
    'subscribe_btn_subscribe' => 'გამოწერა',
    'subscribe_fb_join' => 'შემოგვიერთდით facebook ზე',
    'subscribe_twitter_join' => 'შემოგვიერთდით twitter ზე',
    'subscribe_unsubscribe' => 'გამოწერის გაუქმება',
    'subscribe_unnewsletter' => 'გააუქმეთ გამოწერილი სიახლეები',
    'subscribe_unsuccess' => 'გამოწერა წარმატებით გაუქმდა',
    'subscribe_social_connects' => 'გამოიწერე სოციალური ქსელები',
    
    'subscribe_instagram_join' => 'შემოგვიერთდით instagram ზე',
    'subscribe_youtube_join' => 'შემოგვიერთდით youtube ზე',
    'subscribe_pinterest_join' => 'შემოგვიერთდით pinterest ზე',


	'header_favorities' => 'ფავორიტები',
	'header_tickets' => 'ჩემი ბილეთები',
	'header_memories' => 'ჩემი მოგონებები',
	'header_places' => 'ჩემი ადგილები',
	'header_profile' => 'ჩემი პროფილი',
	'header_change_password' => 'პაროლის შეცვლა',
	'header_log_out' => 'გამოსვლა',
	'header_hello' => 'მოგესალმებით',
	'header_login' => 'შესვლა',

	
	
	'change_pass_title' => 'პაროლის შეცვლა',
	'change_pass_current' => 'ახლანდელი პაროლი',
	'change_pass_new' => 'ახალი პაროლი',
	'change_pass_confirm' => 'დაადასტურეთ ახალი პაროლი',
	'change_pass_save' => 'შენახვა',
	'change_pass_success' => 'პაროლი წარმატებით განახლდა ',
	'change_pass_fail' => 'დაფიქსირდა შეცდომა, გთხოვთ ცადოთ ხელალხა ',

	'profile_view_profile' => 'ჩემი პროფილი',
    'profile_view_name' => 'სახელი',
    'profile_view_email' => 'ელ-ფოსტა',
    'profile_view_number' => 'ტელეფონის ნომერი',
    'profile_view_date' => 'დაბადების თარიღი',
    'profile_view_IDnumber' => 'პირადი ნომერი',
    'profile_view_edit_profile' => 'პროფილის რედაქტირება',
    'profile_view_empty' => 'შეყვანილი არ არის',


    'memories_memories' => 'ჩემი მოგონებები',
    'memories_Park' => 'აირჩიეთ პარკი',
    'memories_title' => 'სათაური',
    'memories_Type' => 'ტიპი',
    'memories_blog_post' => 'ბლოგ პოსტი',
    'memories_photo_gallery' => 'ფოტო გალერეა',
    'memories_video_gallery' => 'ვიდეო გალერეა',
    'memories_Memory' => 'აღწერა',
    'memories_Photo' => 'ატვირთეთ ფოტო',
    'memories_multiple_photos' => 'ატვირთეთ რამდენიმე სურათი',
    'memories_choose' => 'არჩევა',
    'memories_Tags' => 'დაამატეთ ტეგები',
    'memories_post' => 'გამოქვეყნება',
    'memories_Link' => 'ჩასვით YOUTUBE ლინკი აქ',
    'memories_more' => 'მეტის დამატება',
    'memories_success' => 'წარმატებით დაემატა',
    'memories_new_memory' => 'დაამატეთ თქვენი მოგონება',
    'memories_my_memory' => 'ჩემი მოგონებები',
    'memories_my_status_' => 'სტატუსი',
    'memories_rejected' => 'უარყოფილია',
    'memories_active' => 'აქტიურია',
    'memories_pending' => 'მიმდინარეობს განხილვა',
    'memories_publish' => 'დამატების თარიღი',
	'memories_success_change' => 'წარმატებით რედაქტირდა !',
	'memories_Photo_view' => 'ატვირთული სურათი',
	'memories_multiple_photo_view' => 'ატვირთული სურათები',
	'memories_delete' => 'მოგონების წაშლა',
	'memories_edit' => 'რედაქტირება',
	'memories_empty' => 'მოგონებების სექცია ცარიელია !',
	'memories_reviewed' => 'თქვენი მოგონება განხილულია ადმინისტრაციის მიერ და ეხლა ვეღარ შეძლებთ რედაქტირებას !',
	'memories_memoriesInner' => 'მოგონებები',
	'memories_other_memories' => 'სხვა მოგონებები',


	'alertMenu_overview' => 'პარკის შესახებ',
	'alertMenu_adventures' => 'თავგადასავლები',
	'alertMenu_getThere' => 'როგორ მოვხვდეთ აქ',
	'alertMenu_nature' => 'ბუნება და კულტურა',
	'alertMenu_memories' => 'მოგონებები',
	'alertMenu_events' => 'ივენთები',
	'alertMenu_eatSleep' => 'განთავსება და კვება',
	'alertMenu_things' => 'საყიდელი ნივთები',
	'alertMenu_guide' => 'გზამკვლევი',
	'alertMenu_contact' => 'კონტაქტი',
	'alertMenu_alert' => 'შეტყიბინებები',


	'siteMemories_memories' => 'მოგონებები',
	'siteMemories_featured_memories' => 'მოგონებები',
	'siteMemories_view_all' => 'ყველას ნახვა',
	'siteMemories_instagram_feed' => 'ინსტაგრამის მოგონებები',

	'around_places_around' => 'განთავსება და კვება პარკის გარშემო',
	'around_toSee' => 'ადგილები რომლებიც უნდა ნახო',
	'around_price_range' => 'ფასი',
	'around_rating' => 'რეიტინგი',
	'around_stars' => 'ვარსკვლავი',
	'around_type' => 'ტიპი',
	'around_to_stay' => 'დასარჩენი ადგილები',
	'around_to_eat' => 'საკვები ობიექტები',
	'around_tourist_companies' => 'ტურისტული კომპანიები',
	'around_price' => 'ფასი',

	'around_working_hours' => 'სამუშაო საათები',
	'around_my_phone' => 'ტელეფონი',
	'around_my_email' => 'ელ-ფოსტა',
	'around_my_website' => 'ვებ-გვერდი',
	'around_my_comments' => 'კომენტარები',
	'around_my_rate' => 'შეფასება',
	'around_filter_btn' => 'გაფილტვრა',
	

	'around_payment' => 'ბარათით გადახდა',
	'around_parking' => 'პარკინგი',
	'around_music' => 'მუსიკა',
	'around_kitchen' => 'სამზარეულო',
	'around_wifi' => 'WIFI',


	

	'favourite_favorities' => 'ფავორიტები',
	'favourite_memories' => 'მოგონებები',
	'favourite_around' => 'განთავსება და კვება',
	'favourite_toSee' => 'სანახავი ადგილები',
	'favourite_empty' => 'ცარიელია',
	'favourite_trips' => 'ტურები',
	'favourite_all' => 'ყველა',

	'thing_thigs_to_buy' => 'საყიდელი ნივთები',
	'thing_where_buy' => 'სად შევიძინოთ',
	'thing_hours' => 'სამუშაო საათები',
	'thing_type' => 'ტიპი',
	'thing_phone' => 'ტელეფონი',
	'thing_email' => 'ელ-ფოსტა',
	'thing_website' => 'ვებ-გვერდი',

	'adventures_summer' => 'ზაფხული',
	'adventures_winter' => 'ზამთარი',
	'adventures_autumn' => 'შემოდგომა',
	'adventures_spring' => 'გაზაფხული',
	'adventures_available_in' => 'ხელმისაწვდომია',
	'adventures_banner_adventure' => 'თავგადასავალი',
	'adventures_available_trips' => 'შესაბამისი ტურები',

	'guide_guide' => 'გაითვალისწინეთ',
	'guide_guideline' => 'ჩვენ გირჩევთ',
	'guide_download' => 'მასალები',
	'download_guide' => 'ჩამოტვირთე',
	'guide_emergency' => 'გადაუდებელი <br> დახმარების ნომერი',
	'guide_recommend' => 'მედია',
	'guide_download_link' => 'გადმოწერა',
	'guide_empty' => 'ცარიელია',

	'events_events' => 'ივენთები',
	'events_date' => 'ივენთის თარიღი',
	'events_location' => 'მდებარეობა',
	'events_price' => 'ფასი',
	'events_from' => 'საწყისი',

	'park_sor_alphabet' => 'პოპულარობის მიხედვით დალაგება',
	'park_sort_region' => 'რეგიონის მიხედვით დალაგება',
	'park_sort_adventure' => 'თავგადასავლის მიხედვით დალაგება',
	'park_adventures' => 'თავგადასავლები პარკში',
	'park_seasons' => 'სეზონები',
	'park_discover' => 'აღმოაჩინე პარკი',
	'park_buy' => 'იყიდეთ ბილეთი',
	'park_view_map' => 'იხილეთ რუკაზე',
	'park_all_regions' => 'ყველა რეგიონი',
	'park_all_adventures' => 'ყველა თავგადასავალი',

	'park_nature_monuments' => 'ბუნების ძეგლები',
	'park_around_park_objects' => 'განთავსება და კვება პარკის გარშემო',
	
	'park_toSee_type_2' => 'პარკში სანახავი ადგილები',
	'park_toSee_type_1' => 'ბუნების ძეგლები',
	'park_toSee_type_0' => 'პარკის გარშემო',

	
	'park_plan_your_trip' => 'დაგეგმეთ მოგზაურობა',

	'park_passes' => 'ბილეთები',
	'park_date_and_time' => 'თარიღი და დრო',
	'park_total' => 'ჯამი',
	'park_checkout' => 'ბილეთის ყიდვა',


	'trip_title_trips' => 'ტურები',
	'trip_sort_park' => 'პარკის მიხედვით',
	'trip_sort_days' => 'დღეების მიხედვით',
	'trip_sort_level' => 'სირთულის მიხედვით',
	'trip_sort_activity' => 'თავგადასავლის მიხედვით',
	'trip_sort_season' => 'სეზონის მიხედვით',
	'trip_sort_region' => 'რეგიონის მიხედვით',
	'trip_days' => 'დღე',
	'trip_distance' => 'კმ',
	'trip_route' => 'მარშრუტი',
	'trip_highlights' => 'ღირსშესანიშნაობები',
	'trip_description' => 'აღწერა',
	'trip_season' => 'სეზონი',
	'trip_level' => 'სირთულის დონე',
	'trip_download_attachment' => 'რუკის გადმოწერა',
	'trip_similar' => 'მსგავსი ტურები',
	'trip_explore' => 'აღმოაჩინე თავგადასავლები',
	'trip_not_match' => 'რეზულტატი არ მოიძებნა',

	'trip_level_1' => 'მარტივი',
	'trip_level_2' => 'საშუალო',
	'trip_level_3' => 'რთული',

	'trip_load_more' => 'მეტის ნახვა',

	'trip_clear_filter' => 'ყველა',


	'parkInner_overview' => 'პარკის მიმოხილვა',
	'parkInner_see_more' => 'მეტის ნახვა',
	'parkInner_contact' => 'კონტაქტი',
	'parkInner_operating_hours' => 'სამუშაო საათები',
	'parkInner_spring' => 'გაზაფხული',
	'parkInner_summer' => 'ზაფხული',
	'parkInner_fall' => 'შემოდგომა',
	'parkInner_winter' => 'ზამთარი',
	'parkInner_fees' => 'ფასიანი სერვისები',
	'parkInner_adults' => 'ზრდასრული',
	'parkInner_students' => 'სტუდენტი და მოსწავლე',
	'parkInner_foreign' => 'უცხოეთის მოქალაქე',
	'parkInner_combo' => 'კომბო ბილეთი',

	'parkInner_nearby' => 'იხილეთ ახლოს მდებარე პარკები',
	'parkInner_km' => 'კილომეტრის',
	'parkInner_away' => 'დაშორებით',
	'parkInner_explore' => 'ნახვა',
	
	'parkInner_start_planning' => 'მედია',
	'parkInner_read_more' => 'დეტალურად',
	'parkInner_recommends' => 'რეკომენდაციები',

	'parkInner_to_see' => 'ადგილები რომლებიც უნდა ნახო',

	'recommen_we_recommend' => 'მედია',
	'recommen_we_recommend_too' => 'ჩვენ ასევე გირჩევთ',


	'aroundMe_distance' => 'დისტანცია',
	'aroundMe_km' => 'კმ',
	'aroundMe_time' => 'დრო',
	'aroundMe_driving' => 'საათი მანქანით',
	'aroundMe_working_hours' => 'სამუშაო საათები',

	'footer_choose_park' => 'აირჩიეთ პარკი',
	'footer_national_parks' => 'ეროვნული პარკები',
	'footer_park_Prometheus' => 'პრომეთეს მღვიმე',
	'footer_park_Martvili' => 'მარტვილის კანიონი',
	'footer_park_Okatse' => 'ოკაცეს კანიონი',
	'footer_park_Sataplia' => 'სათაფლია',
	'footer_park_Kazbegi' => 'ყაზბეგის ეროვნული პარკი',
	'footer_park_Kolkheti' => 'კოლხეთის ეროვნული პარკი',
	'footer_park_Borjomi' => 'ბორჯომხარაგაულის ეროვნული პარკი',

	'footer_adventure' => 'თავგადასავალი',
	'footer_adventure_Boat' => 'სანაოსნო ტურები',
	'footer_adventure_Hiking' => 'ლაშქრობა',
	'footer_adventure_Cave' => 'სპელეო ტურები',
	'footer_adventure_Bike' => 'ველო ტურები',
	'footer_adventure_Zip' => 'ზიპ-ლაინი',
	'footer_adventure_Diving' => 'ყვინთვა',
	'footer_adventure_Birdwatching' => 'ფრინველებზე დაკვირვება',

	'footer_plan_trip' => 'დაგეგმეთ ტური',
	'footer_trip_Prometheus' => 'პრომეთეს მღვიმის ბილიკი',
	'footer_trip_Martvili' => 'მარტვილის კანიონის ბილიკი',
	'footer_trip_Okatse' => 'ოკაცეს კანიონის ბილიკი',
	'footer_trip_Boat' => 'სანაოსნო ტური პალიასტომი-ფიჩორი',
	'footer_trip_Black' => 'შავი კლდეების ტბის ბილიკი',
	'footer_trip_Nikoloz' => 'ნიკოლოზ რომანოვის ბილიკი',
	'footer_trip_Colchic' => 'კოლხური ტყის ბილიკი',

	'footer_about_us' => 'ჩვენს შესახებ',
	'footer_about_parks' => 'ეროვნული პარკების შესახებ',
	'footer_contact_us' => 'საკონტაქტო ინფორმაცია',
	'footer_partners' => 'ჩვენი პარტნიორები',

	'footer_Learn&Explore' => 'სწავლა & აღმოჩენა',
	'footer_JuniorRangers' => 'ჯუნიორ რეინჯერები',
	'footer_ForKids' => 'ბავშვებისთვის',
	'footer_ForTeachers' => 'მასწავლებლებს',
	'footer_BecomeARanger' => 'გახდი რეინჯერი',
	'footer_RangerCenters' => 'რეინჯერების ცენტრი',
	'footer_RangerInformation' => 'რეინჯერების შესახებ',
	'footer_educational_programs' => 'სასწავლო პროგრამები',
	'footer_for_rangers' => 'რეინჯერებისთვის',


	'occupied_bichvinta' => 'ბიჭვინთა-მიუსერის ნაკრძალი - ოკუპირებულია რუსეთის ფედერაციის მიერ',
	'occupied_liaxvi' => 'ლიახვის ნაკრძალი - ოკუპირებულია რუსეთის ფედერაციის მიერ',
	'occupied_ritsa' => 'რიწის ნაკრძალი - ოკუპირებულია რუსეთის ფედერაციის მიერ',
	'occupied_gumismta' => 'ფსხუ-გუმისმთის ნაკრძალი - ოკუპირებულია რუსეთის ფედერაციის მიერ',


	'seo_keywords' => 'ალგეთის ეროვნული პარკი,ბორჯომ-ხარაგაულის ეროვნული პარკი,ვაშლოვანის ეროვნული პარკი,თბილისის ეროვნული პარკი,თუშეთის ეროვნული პარკი,კინტრიშის ეროვნული პარკი,კოლხეთის ეროვნული პარკი,ლაგოდეხის ეროვნული პარკი,მარტვილის კანიონი,მაჭახელას ეროვნული პარკი,მტირალას ეროვნული პარკი,ნავენახევის მღვიმე,ოკაცეს (კინჩხას) ჩანჩქერი,ოკაცეს კანიონი,პრომეთეს მღვიმე,სათაფლია,ფშავ-ხევსურეთის ეროვნული პარკი,ქობულეთის ეროვნული პარკი,ყაზბეგის ეროვნული პარკი,ჭაჭუნას ეროვნული პარკი,ჯავახეთის ეროვნული პარკი,ეროვნული პარკები საქართველოში,საუკეთესო პარკები საქართველოში,საქართველოს პარკები,ლაშქრობა ეროვნულ პარკებში,ადგილები საქართველოში,საუკეთესო ადგილები საქართველოში,საქართველოს ბუნება,მოგზაურობა საქართველოში,რა გავაკეთო საქართველოში,უცნობი ადგილები საქართველოში,საქართველოს ტურისტული ადგილები,საუკეთესო ადგილები საქართველოში,ადგილები რომლებიც უნდა ნახო საქართველოში,ექსტრემალური ტურები საქართველოში,ჰაიკინგი საქართველოში,ზაფხული საქართველოში,ზამთარი საქართველოში,გაზაფხული საქართველოში,შემოდგომა საქართველოში,მოგზაურობა კავკასიაში,ტურები საქართველოში,საქართველოს ეროვნული პარკები,ეროვნული პარკები ევროპაში,თავგადასავლები საქართველოში,ეკო-ტურიზმი საქართველოში,საუკეთესო სამოგზაურო ტურები საქართველოში,ერთდღიანი ტური საქართველოში,ორდღიანი ტური საქართველოში,სამდღიანი ტური საქართველოში,ქართული თავგადასავლები,აღმოაჩინე საქართველო,სალაშქრო ადგილები საქართველოში,სამოგზაურო ადგილები საქართველოში,მთები საქართველოში,სალაშქრო ტურები საქართველოში,დასვენება საქართველოში,ქართული მოგონებები,ჩემი ქართული მოგონებები,როგორ დავისვენო საქართველოში,ტბები საქართველოში,საპიკნიკე ადგილები საქართველოში,საქართველოს ტყეები,ტურისტული თავშესაფრები საქართველოში,მღვიმეები საქართველოში,სეზონები საქართველოში,სალაშქრო ტურები,სათავგადასავლო ტურები,კაიაკი საქართველოში,საცხენოსნო ტურები საქართველოში,კანიონინგი საქართველოში,ველო ტურები საქართველოში,ფრინველებზე დაკვირვება საქართველოში,ჯიპ ტურები საქართველოში,სანაოსნო ტურები საქართველოში,ზიპ-ლაინი საქართველოშ,სპელეო ტურები საქართველოში,',
];