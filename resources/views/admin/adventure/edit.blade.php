@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>თავგადასავლის დამატება</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ route('adventuresEdit', ['id' => $item->id]) }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ პარკი</label>
                        <div class="col-sm-10">
                            <select name="park_id" class="form-control"  required>
                                <option value="0">საქართველოს თავგადასავალი</option>
                                @foreach($parks as $park)
                                    @if($park->id == $item->park_id)
                                        <option value="{{ $park->id }}" class="pop_option">{{ $park->name }}</option>
                                    @endif
                                    <option value="{{ $park->id }}">{{ $park->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ზღვის დონიდან (მეტრებში)</label>
                        <div class="col-sm-10">
                            <input type="text" name="sea_level" class="form-control" required placeholder="მაგ: 1500" value="{{ $item->sea_level }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ქართულად (GE)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" value="{{ $item->name }}" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_en" class="form-control" value="{{ $item->name_en }}"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_ru" class="form-control" value="{{ $item->name_ru }}"  >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დეტალები ქართულად (GE)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description" class="form-control" required >{{ $item->description }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დეტალები ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_en" class="form-control"  >{{ $item->description_en }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დეტალები რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_ru" class="form-control"  >{{ $item->description_ru }}</textarea>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group multi-select">
                        <label class="col-sm-2 control-label">აირჩიეთ სეზონი</label>
                        <div class="hide-class">{{ $item->seasons }}</div>
                        <div class="col-xs-10 m-l-n " style="padding-left: 30px">
                            <select class="form-control" name="seasons[]" multiple="" required  >
                                <option value="winter">ზამთარი</option>
                                <option value="spring">გაზაფხული</option>
                                <option value="summer">ზაფხული</option>
                                <option value="autumn">შემოდგომა</option>
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ აიკონი</label>
                        <div class="col-sm-10">
                            <input type="file" name="icon[]" class="form-control" accept="image/*">
                        </div>
                    </div>
                    <?php $images = json_decode($item->icon); ?>
                    <img src="{{ asset("files/$images[0]") }}" width="250" height="100">

                    <div class="hr-line-dashed"></div>

                    <div class="pac-card" id="pac-card">
                        <input id="pac-input" type="text"
                            placeholder="Enter a location">
                    </div>

                    <div id="map"></div>
                    <button type="button" class="btn btn-danger delete-alert" delete-id='{{ route('markerDeletor', ['table' => 'adventures', 'value' => 'coordinates', 'id' => $item->id ]) }}' >მარკერების წაშლა</button>
                    <input type="hidden" name="coordinates" class="coordinates_input" value="{{ $item->coordinates }}" required>

                    <div class="hr-line-dashed"></div>

                    <input type="radio" name="map_icon" value="{{ $item->map_icon }}" checked>
                    <img src="{{ $item->map_icon }}">
                    <div class="hr-line-dashed"></div>

                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/1.png">
                    <img src="/main/img/adventure-icons/1.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/2.png">
                    <img src="/main/img/adventure-icons/2.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/3.png">
                    <img src="/main/img/adventure-icons/3.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/4.png">
                    <img src="/main/img/adventure-icons/4.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/5.png">
                    <img src="/main/img/adventure-icons/5.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/6.png">
                    <img src="/main/img/adventure-icons/6.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/7.png">
                    <img src="/main/img/adventure-icons/7.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/8.png">
                    <img src="/main/img/adventure-icons/8.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/9.png">
                    <img src="/main/img/adventure-icons/9.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/10.png">
                    <img src="/main/img/adventure-icons/10.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/11.png">
                    <img src="/main/img/adventure-icons/11.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/12.png">
                    <img src="/main/img/adventure-icons/12.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/13.png">
                    <img src="/main/img/adventure-icons/13.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/14.png">
                    <img src="/main/img/adventure-icons/14.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/15.png">
                    <img src="/main/img/adventure-icons/15.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/16.png">
                    <img src="/main/img/adventure-icons/16.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/17.png">
                    <img src="/main/img/adventure-icons/17.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/18.png">
                    <img src="/main/img/adventure-icons/18.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/19.png">
                    <img src="/main/img/adventure-icons/19.png">
                    <input type="radio" name="map_icon" value="/main/img/adventure-icons/20.png">
                    <img src="/main/img/adventure-icons/20.png">

                    
                    <div class="hr-line-dashed"></div>   

                   
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button"><a href="{{ route('adventuresView') }}">უკან დაბრუნება</a></button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                        <button type="button" class="btn btn-danger pull-right delete-alert" style="margin-right: 15px;" delete-id='{{ route('adventuresDelete',['id' => $item->id]) }}'>წაშლა</button>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClQIm9MilVHr1pXYLpEjUZV3jeSb3sBmE&libraries=places&callback=initMap">
    </script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">

    function initMap(){
        return;
    }
       
    var markers = [];
    var uniqueId = 1;
    var myMarkers = JSON.parse($('.coordinates_input').val());
    var myCoordinates = JSON.parse($('.coordinates_input').val());
    var map;
    window.onload = function () {
        var mapOptions = {
            center: new google.maps.LatLng(41.7151, 44.8271),
            zoom: 7,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        
        for(var i in myCoordinates){
            var marker = new google.maps.Marker({
                position: myCoordinates[i],
                map: map
            });
        }
        //Attach click event handler to the map.
        google.maps.event.addListener(map, 'click', function (e) {
 
            //Determine the location where the user has clicked.
            var location = e.latLng;
 
            //Create a marker and placed it on the map.
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            
            myMarkers.push(location);
            $('.coordinates_input').val(JSON.stringify(myMarkers));
            console.log(JSON.stringify(myMarkers));
 
            //Set unique id
            marker.id = uniqueId;
            uniqueId++;
 
            //Attach click event handler to the marker.
            google.maps.event.addListener(marker, "click", function (e) {
                var content = 'Latitude: ' + location.lat() + '<br />Longitude: ' + location.lng();
                content += "<br /><input type = 'button' va;ue = 'წაშლა' onclick = 'DeleteMarker(" + marker.id + ");' value = 'Delete' />";
                var infoWindow = new google.maps.InfoWindow({
                    content: content
                });
                infoWindow.open(map, marker);
            });
 
            //Add marker to the array.
            markers.push(marker);
        });
        searchPlace()
    };


    function searchPlace(){
            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            // Set the data fields to return when the user selects a place.
            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
              map: map,
              anchorPoint: new google.maps.Point(0, -29)
            });

            autocomplete.addListener('place_changed', function() {
              
              marker.setVisible(false);
              var place = autocomplete.getPlace();
              autocomplete.setTypes([]);
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
              }

              // If the place has a geometry, then present it on a map.
              if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
              } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
              }
              marker.setPosition(place.geometry.location);
              marker.setVisible(false);

              
            });
        }
    function DeleteMarker(id) {
        //Find and remove the marker from the Array
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].id == id) {
                //Remove the marker from Map                  
                markers[i].setMap(null);
 
                //Remove the marker from array.
                markers.splice(i, 1);
                myMarkers.splice(i, 1);
                $('.coordinates_input').val(JSON.stringify(myMarkers));
                return;
            }
        }

    };

    </script>
@endsection