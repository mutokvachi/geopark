@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/main/css/bootstrap-tagsinput.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{{ $info['title'] }}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ $info['form_action'] }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}




                    {{-- content starts here --}}

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ პარკი</label>
                        <div class="col-sm-10">
                            <select name="park_id" class="form-control"  required>
                                @foreach($parks as $park)
                                    @if($park->id == $item->park_id)
                                        <option value="{{ $park->id }}" class="pop_option">{{ $park->name }}</option>
                                    @endif
                                    <option value="{{ $park->id }}">{{ $park->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ სირთულე</label>
                        <div class="col-sm-10">
                            <select name="level" class="form-control"  required>
                                <option value="{{ $item->level }}" class="pop_option">{{ $item->level }}</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">რიგითობა</label>
                        <div class="col-sm-10">
                            <input type="text" name="sort_by" class="form-control" value="{{ $item->sort_by }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-xs-5 m-l-n multi-select">
                            <label>აირჩიეთ სეზონი</label>
                            <br>
                            <div class="hide-class">{{ $item->seasons }}</div>
                            <div class="col-xs-10">
                                <select class="form-control" name="seasons[]" multiple="" required  >
                                    <option value="winter">ზამთარი</option>
                                    <option value="spring">გაზაფხული</option>
                                    <option value="summer">ზაფხული</option>
                                    <option value="autumn">შემოდგომა</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required value="{{ $item->name }}" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_en" class="form-control"  value="{{ $item->name_en }}" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_ru" class="form-control"  value="{{ $item->name_ru }}" >
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ტურის დღეები</label>
                        <div class="col-sm-10">
                            <input type="text" name="trip_days" class="form-control" required value="{{ $item->trip_days }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ტურის დისტანცია</label>
                        <div class="col-sm-10">
                            <input type="text" name="trip_distance" class="form-control" required value="{{ $item->trip_distance }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გასავლელი გზა ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" name="route" class="form-control" required value="{{ $item->route }}" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გასავლელი გზა ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="route_en" class="form-control"  value="{{ $item->route_en }}" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გასავლელი გზა რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="route_ru" class="form-control"  value="{{ $item->route_ru }}" >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ჰაილაითები ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" name="highlights" class="form-control" value="{{ $item->highlights }}" data-role="tagsinput">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ჰაილაითები ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="highlights_en" class="form-control" value="{{ $item->highlights_en }}"  data-role="tagsinput">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ჰაილაითები რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="highlights_ru" class="form-control" value="{{ $item->highlights_ru }}"  data-role="tagsinput">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">რუქის კოორდინატები</label>
                        <div class="col-xs-5">
                            <input type="hidden" style="" name="coordinate_lng" value="{{ $item->coordinate_lng }}" class="form-control" placeholder="longitude" required >
                        </div>
                        <div class="col-xs-5">
                            <input type="hidden" style="" name="coordinate_lat" value="{{ $item->coordinate_lat }}" class="form-control" placeholder="latitude" required >
                        </div>
                    </div>
                    <div class="pac-card" id="pac-card">
                        <input id="pac-input" type="text"
                            placeholder="Enter a location">
                    </div>
                    <div id="map"></div>

                    <div class="hr-line-dashed"></div>

                    <div class="col-xs-2"></div>
                    <div class="col-xs-10">
                        <?php $my_image = json_decode($item->image); ?>
                        <img src="{{ asset("files/$my_image[0]") }}" width="250" height="100">
                    </div>
                    <div class="clear"></div><br>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ სურათი</label>
                        <div class="col-sm-10">
                            <input type="file" name="image[]" class="form-control" accept="image/*">
                        </div>
                    </div>         
                    <br>




                    {{-- content ends here --}}

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button">
                                <a href="{{ $info['redirect_back'] }}">უკან დაბრუნება</a>
                            </button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                        <button type="button" class="btn btn-danger pull-right delete-alert" style="margin-right: 15px;" delete-id='{{ $info['delete_route'] }}'>წაშლა</button>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClQIm9MilVHr1pXYLpEjUZV3jeSb3sBmE&libraries=places&callback=initMap">
    </script>
    <script type="text/javascript" src="{{ asset('/main/js/bootstrap-tagsinput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        var map;
        var markers = [];
        var coordinates = false;

        function changeCoordinates(){
            $('[name=coordinate_lat]').attr('value', coordinates.lat);
            $('[name=coordinate_lng]').attr('value', coordinates.lng);
        }


        function initMap() {
            var haightAshbury = {lat: {{ $item->coordinate_lat }}, lng: {{ $item->coordinate_lng }} };

            map = new google.maps.Map(document.getElementById('map'), {
              zoom: 7,
              center: haightAshbury,
            });

            map.addListener('click', function(event) {
                clearMarkers();
                addMarker(event.latLng);
            });
            addMarker(haightAshbury);
            searchPlace();
        }

        function searchPlace(){
            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            // Set the data fields to return when the user selects a place.
            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
              map: map,
              anchorPoint: new google.maps.Point(0, -29)
            });

            autocomplete.addListener('place_changed', function() {
              
              marker.setVisible(false);
              var place = autocomplete.getPlace();
              autocomplete.setTypes([]);
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
              }

              // If the place has a geometry, then present it on a map.
              if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
              } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
              }
              marker.setPosition(place.geometry.location);
              marker.setVisible(false);

              
            });
        }

      // Adds a marker to the map and push to the array.
        function addMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            coordinates = {lat: lat, lng: lng};
            changeCoordinates();
        }

      // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }
    </script>
@endsection