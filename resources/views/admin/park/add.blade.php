@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/inspinia/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>პარკის დამატება</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ route('parkAdd') }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alias</label>
                        <div class="col-sm-10">
                            <input type="text" name="alias" class="form-control" required >
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ახალი დანიშნულებები</label>
                        <div class="col-sm-10">
                            <select name="new_dest" class="form-control" required  >
                                <option value="0">არა</option>
                                <option value="1">კი</option>
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">პირველი გამოჩნდეს ვიდეო გალერია</label>
                        <div class="col-sm-10">
                            <select name="show_video_gallery" class="form-control">
                                <option value="0">არა</option>
                                <option value="1">კი</option>
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ვიზიტორთა რაოდენობა</label>
                        <div class="col-sm-10">
                            <input type="text" name="popularity" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ოკუპირებულია რუსეთის მიერ</label>
                        <div class="col-sm-10">
                            <select name="occupation" class="form-control">
                                <option value="0">არა</option>
                                <option value="1">კი</option>
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აღწერა</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="description" required  ></textarea> 
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">რეგიონი</label>
                        <div class="col-sm-10">
                            <select name="region_id" class="form-control" required  >
                                @foreach($regions as $region)
                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დაარსების თარიღი</label>
                        <div class="col-sm-10">
                            <input type="date" name="establish_date" class="form-control" required >
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ვიზიტორთა მომსახურების სპეციალისტის სახელი (KA):</label>
                        <div class="col-xs-10">
                            <input type="text" data-role="tagsinput" name="specialist_name" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ვიზიტორთა მომსახურების სპეციალისტის სახელი (EN):</label>
                        <div class="col-xs-10">
                            <input type="text" data-role="tagsinput" name="specialist_name_en" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ვიზიტორთა მომსახურების სპეციალისტის სახელი (RU):</label>
                        <div class="col-xs-10">
                            <input type="text" data-role="tagsinput" name="specialist_name_ru" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ვიზიტორთა მომსახურების სპეციალისტის ნომერი:</label>
                        <div class="col-xs-10">
                            <input type="text" data-role="tagsinput" name="specialist_mobile" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ვიზიტორთა მომსახურების სპეციალისტის იმეილი:</label>
                        <div class="col-xs-10">
                            <input type="text" data-role="tagsinput" name="specialist_email" class="form-control"  >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">მანძილი თბილისიდან კილომეტრებში:</label>
                        <div class="col-xs-10">
                            <input type="text" name="dist_from_tbilisi" class="form-control" required placeholder="255">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ბილიკების რაოდენობა:</label>
                        <div class="col-xs-10">
                            <input type="text" name="park_paths" class="form-control" required placeholder="5">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label class="col-xs-2 control-label">ზღვის დონიდან </label>
                        <div class="col-xs-10">
                            <input type="text" name="sea_level" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ზღვის დონიდან EN </label>
                        <div class="col-xs-10">
                            <input type="text" name="sea_level_en" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ზღვის დონიდან RU</label>
                        <div class="col-xs-10">
                            <input type="text" name="sea_level_ru" class="form-control"  >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label class="col-xs-2 control-label">ადმინისტრაციის მისამართი </label>
                        <div class="col-xs-10">
                            <input type="text"  name="administration_address" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ადმინისტრაციის მისამართი EN </label>
                        <div class="col-xs-10">
                            <input type="text" name="administration_address_en" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ადმინისტრაციის მისამართი RU</label>
                        <div class="col-xs-10">
                            <input type="text" name="administration_address_ru" class="form-control"  >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div> 

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-xs-5 m-l-n" style="padding-left: 30px">
                            <label>აირჩიეთ ტიპი</label>
                            <select class="form-control" name="type_id[]" multiple="" required  >
                                <option value="1">მთა</option>
                                <option value="2">ზღვა</option>
                                <option value="3">კანიონი</option>
                                <option value="4">მდინარე</option>
                            </select>
                        </div>
                        <div class="col-xs-5 m-l-n" style="padding-left: 45px">
                            <label>აირჩიეთ სეზონი</label>
                            <select class="form-control" name="seasons[]" multiple="" required  >
                                <option value="winter">ზამთარი</option>
                                <option value="spring">გაზაფხული</option>
                                <option value="summer">ზაფხული</option>
                                <option value="autumn">შემოდგომა</option>
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">სამუშაო საათები</label>
                        <div class="col-xs-5">
                            <label>ზამთარი</label>
                            <textarea class="form-control"  name="operating_hours_1"></textarea> 
                        </div>
                        <div class="col-xs-5">
                            <label>გაზაფხული</label>
                            <textarea class="form-control"  name='operating_hours_2'></textarea> 
                        </div>

                        <label class="col-sm-2 control-label"></label>
                        <div class="col-xs-5">
                            <label>ზაფხული</label>
                            <textarea class="form-control"  name='operating_hours_3'></textarea> 
                        </div>
                        <div class="col-xs-5">
                            <label>შემოდგომა</label>
                            <textarea class="form-control"  name='operating_hours_4'></textarea> 
                        </div>
                    </div>

                    <br><br>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">შესვლის ფასები</label>
                        <div class="col-xs-5">
                            <label>სტუდენტი</label>
                            <textarea class="form-control"  name='fees_passes_1' ></textarea> 
                        </div>
                        <div class="col-xs-5">
                            <label>საქართველოს მოქალაქე</label>
                            <textarea class="form-control"  name='fees_passes_2' ></textarea> 
                        </div>

                        <label class="col-sm-2 control-label"></label>
                        <div class="col-xs-5">
                            <label>უცხოეთის მოქალაქე</label>
                            <textarea class="form-control"  name='fees_passes_3' ></textarea> 
                        </div>

                        <div class="col-xs-5">
                            <label>კომბო ბილეთი</label>
                            <textarea class="form-control"  name='fees_passes_4' ></textarea> 
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელმძღვანელო (guide)</label>
                        <div class="col-xs-5">
                            <label>ქართულად</label>
                            <textarea class="form-control" required name='guide'></textarea> 
                        </div>
                        <div class="col-xs-5">
                            <label>ინგლისურად</label>
                            <textarea class="form-control"  name='guide_en'></textarea> 
                        </div>

                        <label class="col-sm-2 control-label"></label>
                        <div class="col-xs-5">
                            <label>რუსულად</label>
                            <textarea class="form-control"  name='guide_ru'></textarea> 
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">მითითებები</label>
                        <div class="col-xs-5">
                            <label>ქართულად</label>
                            <textarea class="form-control" required name='guideline'></textarea> 
                        </div>
                        <div class="col-xs-5">
                            <label>ინგლისურად</label>
                            <textarea class="form-control"  name='guideline_en'></textarea> 
                        </div>

                        <label class="col-sm-2 control-label"></label>
                        <div class="col-xs-5">
                            <label>რუსულად</label>
                            <textarea class="form-control"  name='guideline_ru'></textarea> 
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label" >რუქის კოორდინატები</label>
                        <div class="col-xs-5">
                            <input type="hidden" style="" name="coordinate_long" class="form-control" placeholder="longitude" required >
                        </div>
                        <div class="col-xs-5">
                            <input type="hidden" style="" name="coordinate_lat" class="form-control" placeholder="latitude" required >
                        </div>
                    </div>

                    <div class="pac-card" id="pac-card">
                        <input id="pac-input" type="text"
                            placeholder="Enter a location">
                    </div>
                    
                    <div id="map"></div>


                    <div class="hr-line-dashed"></div>

                    {{-- <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ ფაილები</label>
                        <div class="col-sm-10">
                            <input type="file" name="attachments[]" multiple="" class="form-control" accept=".xlsx,.xls,.doc, .docx,.ppt, .pptx,.txt,.pdf">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div> --}}

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ სურათი</label>
                        <div class="col-sm-10">
                            <input type="file" name="banner[]" class="form-control"  required accept="image/*">
                        </div>
                    </div>
                    
                    <div class="hr-line-dashed"></div>
                   
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button"><a href="{{ route('parksView') }}">უკან დაბრუნება</a></button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                        {{-- <div class="errorBag" style="display: none;">{{ json_encode(Session::get('errorBag')) }}</div> --}}
                        
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClQIm9MilVHr1pXYLpEjUZV3jeSb3sBmE&libraries=places&callback=initMap">
    </script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/inspinia/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
@endsection
    
@section('javascript')
    <script type="text/javascript">
        // alertErrors($('.errorBag').html());

        var map;
        var markers = [];
        var coordinates = false;

        function changeCoordinates(){
            $('[name=coordinate_lat]').attr('value', coordinates.lat);
            $('[name=coordinate_long]').attr('value', coordinates.lng);
        }


        function initMap() {
            var haightAshbury = {lat: 41.7151, lng: 44.8271 };

            map = new google.maps.Map(document.getElementById('map'), {
              zoom: 7,
              center: haightAshbury,
            });

            map.addListener('click', function(event) {
                clearMarkers();
                addMarker(event.latLng);
            });
            searchPlace();
        }

        function searchPlace(){
            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            // Set the data fields to return when the user selects a place.
            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
              map: map,
              anchorPoint: new google.maps.Point(0, -29)
            });

            autocomplete.addListener('place_changed', function() {
              
              marker.setVisible(false);
              var place = autocomplete.getPlace();
              autocomplete.setTypes([]);
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
              }

              // If the place has a geometry, then present it on a map.
              if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
              } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
              }
              marker.setPosition(place.geometry.location);
              marker.setVisible(false);

              
            });
        }

      // Adds a marker to the map and push to the array.
        function addMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            coordinates = {lat: lat, lng: lng};
            changeCoordinates();
        }

      // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }

    </script>
@endsection