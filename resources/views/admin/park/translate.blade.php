@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>პარკის დამატება ინგლისურ ენაზე (EN)</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="overflow: hidden;">
                <div class="col-xs-6">
                    <form method="post" action="{{ route('parkEdit', ['id' => $item->id]) }}" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">სახელი</label>
                            <div class="col-sm-10">
                                <input type="text" name="name_en" class="form-control"  value="{{ $item->name_en }}" required>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">აღწერა</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description_en" required>{{ $item->description_en }}</textarea> 
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-xs-5">
                                <label>ზამთარი</label>
                                <textarea required class="form-control " name="operating_hours_1_en">{{ $item->operating_hours_1_en }}</textarea> 
                            </div>
                            <div class="col-xs-5">
                                <label>გაზაფხული</label>
                                <textarea required class="form-control " name='operating_hours_2_en'>{{ $item->operating_hours_2_en }}</textarea> 
                            </div>

                            <label class="col-sm-2 control-label"></label>
                            <div class="col-xs-5">
                                <label>ზაფხული</label>
                                <textarea required class="form-control " name='operating_hours_3_en'>{{ $item->operating_hours_3_en }}</textarea> 
                            </div>
                            <div class="col-xs-5">
                                <label>შემოდგომა</label>
                                <textarea required class="form-control " name='operating_hours_4_en'>{{ $item->operating_hours_4_en }}</textarea> 
                            </div>
                        </div>

                        <br><br>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label "></label>
                            <div class="col-xs-5">
                                <label>ზრდასრულები</label>
                                <textarea required class="form-control " name='fees_passes_1_en'>{{ $item->fees_passes_1_en }}</textarea> 
                            </div>
                            <div class="col-xs-5">
                                <label>ოჯახის შესვლა</label>
                                <textarea required class="form-control " name='fees_passes_2_en'>{{ $item->fees_passes_2_en }}</textarea> 
                            </div>

                            <label class="col-sm-2 control-label"></label>
                            <div class="col-xs-5">
                                <label>ჯგუფის შესვლა</label>
                                <textarea required class="form-control " name='fees_passes_3_en'>{{ $item->fees_passes_3_en }}</textarea> 
                            </div>
                            <div class="col-xs-5">
                                <label>კომბო ბილეთი</label>
                                <textarea required class="form-control " name='fees_passes_4_en'>{{ $item->fees_passes_4_en }}</textarea> 
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-white" type="button"><a href="{{ route('parksView') }}">უკან დაბრუნება</a></button>
                                <button class="btn btn-primary" type="submit">დამატება</button>

                            </div>
                        </div><br>

                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif

                        <div class="alert alert-danger my_warning hide-class">
                            ყველა ველის შევსება აუცილებელია !
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>პარკის დამატება რუსულ ენაზე (RU)</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="overflow: hidden;">
                <div class="col-xs-6">
                    <form method="post" action="{{ route('parkEdit', ['id' => $item->id]) }}" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">სახელი</label>
                            <div class="col-sm-10">
                                <input type="text" name="name_ru" class="form-control" required value="{{ $item->name_ru }}" >
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">აღწერა</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description_ru" required>{{ $item->description_ru }}</textarea> 
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-xs-5">
                                <label>ზამთარი</label>
                                <textarea required class="form-control " name="operating_hours_1_ru">{{ $item->operating_hours_1_ru }}</textarea> 
                            </div>
                            <div class="col-xs-5">
                                <label>გაზაფხული</label>
                                <textarea required class="form-control " name='operating_hours_2_ru'>{{ $item->operating_hours_2_ru }}</textarea> 
                            </div>

                            <label class="col-sm-2 control-label"></label>
                            <div class="col-xs-5">
                                <label>ზაფხული</label>
                                <textarea required class="form-control " name='operating_hours_3_ru'>{{ $item->operating_hours_3_ru }}</textarea> 
                            </div>
                            <div class="col-xs-5">
                                <label>შემოდგომა</label>
                                <textarea required class="form-control " name='operating_hours_4_ru'>{{ $item->operating_hours_4_ru }}</textarea> 
                            </div>
                        </div>

                        <br><br>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label "></label>
                            <div class="col-xs-5">
                                <label>ზრდასრულები</label>
                                <textarea required class="form-control " name='fees_passes_1_ru'>{{ $item->fees_passes_1_ru }}</textarea> 
                            </div>
                            <div class="col-xs-5">
                                <label>ოჯახის შესვლა</label>
                                <textarea required class="form-control " name='fees_passes_2_ru'>{{ $item->fees_passes_2_ru }}</textarea> 
                            </div>

                            <label class="col-sm-2 control-label"></label>
                            <div class="col-xs-5">
                                <label>ჯგუფის შესვლა</label>
                                <textarea required class="form-control " name='fees_passes_3_ru'>{{ $item->fees_passes_3_ru }}</textarea> 
                            </div>
                            <div class="col-xs-5">
                                <label>კომბო ბილეთი</label>
                                <textarea required class="form-control " name='fees_passes_4_ru'>{{ $item->fees_passes_4_ru }}</textarea> 
                            </div>

                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-white" type="button"><a href="{{ route('parksView') }}">უკან დაბრუნება</a></button>
                                <button class="btn btn-primary send_data" type="submit">დამატება</button>

                            </div>
                        </div><br>

                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif

                        <div class="alert alert-danger my_warning hide-class">
                            ყველა ველის შევსება აუცილებელია !
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
	<script type="text/javascript">
        $(document).ready(function(){
            $('.collapse-link').click();
        });

    </script>
@endsection