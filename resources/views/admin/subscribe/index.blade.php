@extends('admin.layouts.master')

@section('head')

    <link href="{{ asset('plugins/inspinia/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <style type="text/css">
        .dataTables_info,
        .dataTables_length,
        .pagination{
            display: none;
        }

        tbody tr{
            cursor: pointer;
        }
    </style>
@endsection

@section('nav')
    @parent
@endsection

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>გამომწერების სია</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            {{-- <div class="btn btn-primary"><a href="{{ route('alertsAddView') }}" style="color: #fff;">+ შეტყობინების დამატება</a></div> --}}
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <th>ID</th>
                        <th>ელ-ფოსტა</th>
                        <th>ევენთები</th>
                        <th>ტურები</th>
                        <th>შეტყობინებები</th>
                        <th>სერვისები</th>
                    </thead>
                    <?php $arr = ['on'=>'აქტიურია'];?>
                    <tbody>
                        @foreach($items as $item)
                            <tr class="gradeX" data-target='{{ $item->id }}'>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $arr[$item->events] or '' }}</td>
                                <td>{{ $arr[$item->trips] or '' }}</td>
                                <td>{{ $arr[$item->alerts] or '' }}</td>
                                <td>{{ $arr[$item->services] or '' }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>ელ-ფოსტა</th>
                            <th>ევენთები</th>
                            <th>ტურები</th>
                            <th>შეტყობინებები</th>
                            <th>სერვისები</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('plugins/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 500,
                responsive: true,
                buttons: [
                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ]

            });
        });
    </script>
@endsection