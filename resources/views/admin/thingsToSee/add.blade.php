@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>სანახავის ადგილის დამატება</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ route('thingsToSeeAdd') }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ პარკი</label>
                        <div class="col-sm-10">
                            <select name="park_id" class="form-control"  required>
                                @foreach($parks as $park)
                                    <option value="{{ $park->id }}">{{ $park->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ზღვის დონიდან სიმაღლე(მეტრებში)</label>
                        <div class="col-sm-10">
                            <input type="text" name="height" class="form-control" required >
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ქართულად (GE)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_en" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_ru" class="form-control"  >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">მდებარეობა ქართულად (GE)</label>
                        <div class="col-sm-10">
                            <input type="text" name="location" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">მდებარეობა ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="location_en" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">მდებარეობა რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="location_ru" class="form-control"  >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ღამისთევის ლინკი</label>
                        <div class="col-sm-10">
                            <input type="text" name="overnight_link" class="form-control" placeholder="არაა აუცილებელი">
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ვიზიტორთა მომსახურების სპეციალისტის ნომერი:</label>
                        <div class="col-xs-10">
                            <input type="text" name="specialist_mobile" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">ვიზიტორთა მომსახურების სპეციალისტის იმეილი:</label>
                        <div class="col-xs-10">
                            <input type="text" name="specialist_email" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">მისასვლელი გზა ქართულად (GE)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="road_to" class="form-control" required ></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">მისასვლელი გზა ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="road_to_en" class="form-control"  ></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">მისასვლელი გზა რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="road_to_ru" class="form-control"  ></textarea>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    
                    
                    

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დეტალები ქართულად (GE)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description" class="form-control" required ></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დეტალები ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_en" class="form-control"  ></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დეტალები რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_ru" class="form-control"  ></textarea>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">რუქის კოორდინატები</label>
                        <div class="col-xs-5">
                            <input type="hidden" style="" name="coordinate_long" class="form-control" placeholder="longitude" required >
                        </div>
                        <div class="col-xs-5">
                            <input type="hidden" style="" name="coordinate_lat" class="form-control" placeholder="latitude" required >
                        </div>
                    </div>

                    <div class="pac-card" id="pac-card">
                        <input id="pac-input" type="text"
                            placeholder="Enter a location">
                    </div>

                    <div id="map"></div>


                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ სურათი</label>
                        <div class="col-sm-10">
                            <input type="file" name="image[]" class="form-control"  required accept="image/*">
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button">
                                <a href="{{ route('thingsToSeeView') }}">უკან დაბრუნება</a>
                            </button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
     <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClQIm9MilVHr1pXYLpEjUZV3jeSb3sBmE&libraries=places&callback=initMap">
    </script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">

        var map;
        var markers = [];
        var coordinates = false;

        function changeCoordinates(){
            $('[name=coordinate_lat]').attr('value', coordinates.lat);
            $('[name=coordinate_long]').attr('value', coordinates.lng);
        }


        function initMap() {
            var haightAshbury = {lat: 41.7151, lng: 44.8271 };

            map = new google.maps.Map(document.getElementById('map'), {
              zoom: 7,
              center: haightAshbury,
            });

            map.addListener('click', function(event) {
                clearMarkers();
                addMarker(event.latLng);
            });
            searchPlace();
        }

        function searchPlace(){
            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            // Set the data fields to return when the user selects a place.
            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
              map: map,
              anchorPoint: new google.maps.Point(0, -29)
            });

            autocomplete.addListener('place_changed', function() {
              
              marker.setVisible(false);
              var place = autocomplete.getPlace();
              autocomplete.setTypes([]);
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
              }

              // If the place has a geometry, then present it on a map.
              if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
              } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
              }
              marker.setPosition(place.geometry.location);
              marker.setVisible(false);

              
            });
        }

      // Adds a marker to the map and push to the array.
        function addMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            coordinates = {lat: lat, lng: lng};
            changeCoordinates();
        }

      // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }

    </script>
@endsection