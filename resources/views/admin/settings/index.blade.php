@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>საიტის მონაცემები</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ route('settingsEdit') }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="hr-line-dashed" style="border-color: red;border-width: 2px;"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">სოც.ქსელის ლინკები:</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">fb</label>
                        <div class="col-sm-10">
                            <input type="text" name="social_fb" value="{{ $settings['social']['fb'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group" style="display: none;">
                        <label class="col-sm-2 control-label">twitter</label>
                        <div class="col-sm-10">
                            <input type="text" name="social_twitter" value="{{ $settings['social']['twitter'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group" style="display: none;">
                        <label class="col-sm-2 control-label">vk</label>
                        <div class="col-sm-10">
                            <input type="text" name="social_vk" value="{{ $settings['social']['vk'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">instagram</label>
                        <div class="col-sm-10">
                            <input type="text" name="social_instagram" value="{{ $settings['social']['instagram'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">youtube</label>
                        <div class="col-sm-10">
                            <input type="text" name="social_youtube" value="{{ $settings['social']['youtube'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">pinterest</label>
                        <div class="col-sm-10">
                            <input type="text" name="social_pinterest" value="{{ $settings['social']['pinterest'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed" style="border-color: red;border-width: 2px;"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">მთავარი გვერდი (KA):</label>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="mainpage_ka_title" value="{{ $settings['mainpage_ka']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="mainpage_ka_description" value="{{ $settings['mainpage_ka']['description'] }}" class="form-control" required >
                        </div>
                    </div>
                    

                    <div class="hr-line-dashed" style="border-color: red;border-width: 2px;"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">მთავარი გვერდი (EN):</label>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="mainpage_en_title" value="{{ $settings['mainpage_en']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="mainpage_en_description" value="{{ $settings['mainpage_en']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed" style="border-color: red;border-width: 2px;"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">მთავარი გვერდი (RU):</label>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="mainpage_ru_title" value="{{ $settings['mainpage_ru']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="mainpage_ru_description" value="{{ $settings['mainpage_ru']['description'] }}" class="form-control" required >
                        </div>
                    </div>
                    

                    <div class="hr-line-dashed" style="border-color: red;border-width: 2px;"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ფუტერი ქართულად (KA):</label>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ბილეთი - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_ticket_title" value="{{ $settings['footer_ka']['ticket']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ბილეთი - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_ticket_description" value="{{ $settings['footer_ka']['ticket']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">ბილეთი - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_ticket_link" style="display: none;" disabled value="{{ $settings['footer_ka']['ticket']['link'] }}" class="form-control" required >
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ავტობუსი - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_bus_title" value="{{ $settings['footer_ka']['bus']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ავტობუსი - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_bus_description" value="{{ $settings['footer_ka']['bus']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">ავტობუსი - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_bus_link" style="display: none;" disabled value="{{ $settings['footer_ka']['bus']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გიდი - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_guide_title" value="{{ $settings['footer_ka']['guide']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გიდი - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_guide_description" value="{{ $settings['footer_ka']['guide']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">გიდი - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_guide_link" style="display: none;" disabled value="{{ $settings['footer_ka']['guide']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">რუქა - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_map_title" value="{{ $settings['footer_ka']['map']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">რუქა - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_map_description" value="{{ $settings['footer_ka']['map']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">რუქა - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_map_link" style="display: none;" disabled value="{{ $settings['footer_ka']['map']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ინფო - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_info_title" value="{{ $settings['footer_ka']['info']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ინფო - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_info_description" value="{{ $settings['footer_ka']['info']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">ინფო - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_ka_info_link" style="display: none;" disabled value="{{ $settings['footer_ka']['info']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed" style="border-color: red;border-width: 2px;"></div>
                    

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ფუტერი ინგლისურად (EN):</label>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ბილეთი - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_ticket_title" value="{{ $settings['footer_en']['ticket']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ბილეთი - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_ticket_description" value="{{ $settings['footer_en']['ticket']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">ბილეთი - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_ticket_link" style="display: none;" disabled value="{{ $settings['footer_en']['ticket']['link'] }}" class="form-control" required >
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ავტობუსი - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_bus_title" value="{{ $settings['footer_en']['bus']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ავტობუსი - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_bus_description" value="{{ $settings['footer_en']['bus']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">ავტობუსი - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_bus_link" style="display: none;" disabled value="{{ $settings['footer_en']['bus']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გიდი - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_guide_title" value="{{ $settings['footer_en']['guide']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გიდი - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_guide_description" value="{{ $settings['footer_en']['guide']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">გიდი - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_guide_link" style="display: none;" disabled value="{{ $settings['footer_en']['guide']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">რუქა - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_map_title" value="{{ $settings['footer_en']['map']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">რუქა - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_map_description" value="{{ $settings['footer_en']['map']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">რუქა - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_map_link" style="display: none;" disabled value="{{ $settings['footer_en']['map']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ინფო - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_info_title" value="{{ $settings['footer_en']['info']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ინფო - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_info_description" value="{{ $settings['footer_en']['info']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">ინფო - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_en_info_link" style="display: none;" disabled value="{{ $settings['footer_en']['info']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed" style="border-color: red;border-width: 2px;"></div>
                    

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ფუტერი რუსულად (RU):</label>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ბილეთი - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_ticket_title" value="{{ $settings['footer_ru']['ticket']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ბილეთი - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_ticket_description" value="{{ $settings['footer_ru']['ticket']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">ბილეთი - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_ticket_link" style="display: none;" disabled value="{{ $settings['footer_ru']['ticket']['link'] }}" class="form-control" required >
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ავტობუსი - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_bus_title" value="{{ $settings['footer_ru']['bus']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ავტობუსი - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_bus_description" value="{{ $settings['footer_ru']['bus']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">ავტობუსი - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_bus_link" style="display: none;" disabled value="{{ $settings['footer_ru']['bus']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გიდი - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_guide_title" value="{{ $settings['footer_ru']['guide']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გიდი - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_guide_description" value="{{ $settings['footer_ru']['guide']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">გიდი - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_guide_link" style="display: none;" disabled value="{{ $settings['footer_ru']['guide']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">რუქა - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_map_title" value="{{ $settings['footer_ru']['map']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">რუქა - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_map_description" value="{{ $settings['footer_ru']['map']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">რუქა - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_map_link" style="display: none;" disabled value="{{ $settings['footer_ru']['map']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ინფო - სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_info_title" value="{{ $settings['footer_ru']['info']['title'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ინფო - არწერა</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_info_description" value="{{ $settings['footer_ru']['info']['description'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- <label class="col-sm-2 control-label">ინფო - ლინკი</label> --}}
                        <div class="col-sm-10">
                            <input type="text" name="footer_ru_info_link" style="display: none;" disabled value="{{ $settings['footer_ru']['info']['link'] }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="hr-line-dashed" style="border-color: red;border-width: 2px;"></div>



                   
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary send_data" type="submit">რედაქტირება</button>
                        </div>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    @parent
@endsection