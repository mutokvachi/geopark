<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="{{ asset('plugins/inspinia/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/style.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">IN+</h1>
            </div>
            <h3>Welcome to {{ Config::get('app.url') }}</h3>
            <form class="m-t" role="form" action="{{ route('adminAuth') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <input type="email" class="form-control" name='email' placeholder="Email" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('plugins/inspinia/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/bootstrap.min.js') }}"></script>

</body>

</html>
