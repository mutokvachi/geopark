@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/inspinia/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{{ $info['title'] }}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ $info['form_action'] }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}




                    {{-- content starts here --}}

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ცენტრი (lng, lat)</label>
                        <div class="col-sm-10">
                            <input type="text" data-role="tagsinput" required name="center" value="{{ $item->center }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">longitude</label>
                        <div class="col-sm-10">
                            <input type="text" data-role="tagsinput" required name="lng" value="{{ $item->lng }}" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">latitude</label>
                        <div class="col-sm-10">
                            <input type="text" data-role="tagsinput" required name="lat" value="{{ $item->lat }}" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელები ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" data-role="tagsinput" required name="name" value="{{ $item->name }}" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელები ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" data-role="tagsinput" name="name_en" value="{{ $item->name_en }}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელები რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" data-role="tagsinput" name="name_ru" value="{{ $item->name_ru }}" class="form-control"  >
                        </div>
                    </div>


                    {{-- content ends here --}}

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/inspinia/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        
    </script>
@endsection