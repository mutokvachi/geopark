@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>ვიდეოს დამატება</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ route('videoGalleryAdd') }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ პარკი</label>
                        <div class="col-sm-10">
                            <select name="park_id" class="form-control"  required>
                                @foreach($parks as $park)
                                    <option value="{{ $park->id }}">{{ $park->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ქართულად (GE)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_en" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_ru" class="form-control"  >
                        </div>
                    </div>

                    

                    <div class="hr-line-dashed"></div>

                    {{-- <div class="btn btn-success show-video">video file</div> --}}
                    <div class="btn btn-success show-youtube">youtube</div>

                    <div class="form-group get-video" style="display: none;">
                        <label class="col-sm-2 control-label">ატვირთეთ ვიდეო</label>
                        <div class="col-sm-10">
                            <input type="file" name="video[]" class="form-control"  accept="video/*">
                        </div>
                    </div>

                    <input type="hidden" name="type" class="video-type" value="youtube">

                    <div class="form-group get-youtube">
                        <label class="col-sm-2 control-label">youtube ლინკი</label>
                        <div class="col-sm-10">
                            <input type="text" name="youtube_link" class="form-control" >
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button">
                                <a href="{{ route('videoGalleryView') }}">უკან დაბრუნება</a>
                            </button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        $('.show-video').click(function(){
            $('.get-youtube').hide();
            $('.get-video').show();
            $('.video-type').val('video');
        });

        $('.show-youtube').click(function(){
            $('.get-video').hide();
            $('.get-youtube').show();
            $('.video-type').val('youtube');
        });
    </script>
@endsection