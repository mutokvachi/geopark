@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>პარკის გარშემო ობიექტის რედაქტირება</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ route('aroundParkEdit', ['id' => $item->id]) }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ პარკი</label>
                        <div class="col-sm-10">
                            <select name="park_id" class="form-control"  required>

                                @foreach($parks as $park)
                                    @if($park->id == $item->park_id)
                                        <option value="{{ $park->id }}" class="pop_option">{{ $park->name }}</option>
                                    @endif
                                    <option value="{{ $park->id }}">{{ $park->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ კატეგორია</label>
                        <div class="col-sm-10">
                            <select name="place_id" class="form-control"  required>
                                @foreach($places as $place)
                                    @if($place->id == $item->place_id)
                                        <option value="{{ $place->id }}" class="pop_option">{{ $place->name }}</option>
                                    @endif
                                    <option value="{{ $place->id }}">{{ $place->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ქართულად (GE)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" value="{{ $item->name }}" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_en" value="{{ $item->name_en }}" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_ru" value="{{ $item->name_ru }}" class="form-control"  >
                        </div>
                    </div>
                    
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <div class="col-xs-4">
                                <label>ფასი</label>
                                <input type="text" name="price" value="{{ $item->price }}" class="form-control" placeholder="არასავალდებულო" >
                            </div>
                            <div class="col-xs-4">
                                <label>სამუშაო საათები</label>
                                <input type="text" name="working_hours" value="{{ $item->working_hours }}" class="form-control" placeholder="არასავალდებულო" >
                            </div>
                            <div class="col-xs-4">
                                <label>ტელეფონი</label>
                                <input type="text" name="mobile" value="{{ $item->mobile }}" class="form-control" placeholder="არასავალდებულო" >
                            </div>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <div class="col-xs-4">
                                <label>ემაილი</label>
                                <input type="text" name="email" value="{{ $item->email }}" class="form-control" placeholder="არასავალდებულო" >
                            </div>
                            <div class="col-xs-4">
                                <label>საიტი</label>
                                <input type="text" name="website" value="{{ $item->website }}" class="form-control" placeholder="არასავალდებულო" >
                            </div>
                            <div class="col-xs-4"></div>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">დეტალები ქართულად (GE)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description" class="form-control" required >{{ $item->description }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დეტალები ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_en" class="form-control"  >{{ $item->description_en }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დეტალები რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_ru" class="form-control"  >{{ $item->description_ru }}</textarea>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გადახდა</label>
                        <div class="col-sm-10">
                            @if(!empty($item->payment))
                                <input type="checkbox" name="payment" class="form-control" checked>
                            @else
                                <input type="checkbox" name="payment" class="form-control" >
                            @endif
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">პარკინგი</label>
                        <div class="col-sm-10">
                            @if(!empty($item->parking))
                                <input type="checkbox" name="parking" class="form-control" checked>
                            @else
                                <input type="checkbox" name="parking" class="form-control" >
                            @endif
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">მუსიკა</label>
                        <div class="col-sm-10">
                            @if(!empty($item->music))
                                <input type="checkbox" name="music" class="form-control" checked>
                            @else
                                <input type="checkbox" name="music" class="form-control" >
                            @endif
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სამზარეულო</label>
                        <div class="col-sm-10">
                            @if(!empty($item->kitchen))
                                <input type="checkbox" name="kitchen" class="form-control" checked value="test">
                            @else
                                <input type="checkbox" name="kitchen" class="form-control" >
                            @endif
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">WI FI</label>
                        <div class="col-sm-10">
                            @if(!empty($item->wifi))
                                <input type="checkbox" name="wifi" class="form-control" checked value="test">
                            @else
                                <input type="checkbox" name="wifi" class="form-control" >
                            @endif
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">რუქის კოორდინატები</label>
                        <div class="col-xs-5">
                            <input type="hidden" name="coordinate_long" class="form-control" placeholder="longitude" required value="{{ $item->coordinate_long }}">
                        </div>
                        <div class="col-xs-5">
                            <input type="hidden" name="coordinate_lat" class="form-control" placeholder="latitude" required value="{{ $item->coordinate_lat }}">
                        </div>
                    </div>

                    <div class="pac-card" id="pac-card">
                        <input id="pac-input" type="text"
                            placeholder="Enter a location">
                    </div>
                    <div id="map"></div>


                    <div class="hr-line-dashed"></div>



                    <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ სურათები</label>
                        <div class="col-sm-10">
                            <input type="file" name="image[]" multiple class="form-control" accept="image/*">
                        </div>
                    </div>

                    <?php $images = json_decode($item->image); ?>
                    <div class="hr-line-dashed"></div>
                    <div class="things_to_see" style="margin: 0 auto;">
                        @foreach($images as $image)
                            <div class="col-xs-2 " style="position: relative; overflow: hidden;">
                                <p delete-id="{{ route('aroundParkImageDel', ['id' => $item->id, 'name' => $image ]) }}" class="delete-alert" style="position: absolute; right: 0; top: 0;font-size: 20px">X</p>
                                <img src="{{ asset("files/$image") }}" width="250" height="100">
                            </div>
                        @endforeach
                    </div>
                    <div style="clear: both;"></div>

                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button">
                                <a href="{{ route('aroundParkView') }}">უკან დაბრუნება</a>
                            </button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                        <button type="button" class="btn btn-danger pull-right delete-alert" style="margin-right: 15px;" delete-id='{{ route('aroundParkDelete',['id' => $item->id]) }}'>წაშლა</button>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClQIm9MilVHr1pXYLpEjUZV3jeSb3sBmE&libraries=places&callback=initMap">
    </script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        var map;
        var markers = [];
        var coordinates = false;

        function changeCoordinates(){
            $('[name=coordinate_lat]').attr('value', coordinates.lat);
            $('[name=coordinate_long]').attr('value', coordinates.lng);
        }


        function initMap() {
            var haightAshbury = {lat: {{ $item->coordinate_lat }}, lng: {{ $item->coordinate_long }} };

            map = new google.maps.Map(document.getElementById('map'), {
              zoom: 7,
              center: haightAshbury,
            });

            map.addListener('click', function(event) {
                clearMarkers();
                addMarker(event.latLng);
            });
            addMarker(haightAshbury);
            searchPlace();
        }

        function searchPlace(){
            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            // Set the data fields to return when the user selects a place.
            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
              map: map,
              anchorPoint: new google.maps.Point(0, -29)
            });

            autocomplete.addListener('place_changed', function() {
              
              marker.setVisible(false);
              var place = autocomplete.getPlace();
              autocomplete.setTypes([]);
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
              }

              // If the place has a geometry, then present it on a map.
              if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
              } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
              }
              marker.setPosition(place.geometry.location);
              marker.setVisible(false);

              
            });
        }

      // Adds a marker to the map and push to the array.
        function addMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            coordinates = {lat: lat, lng: lng};
            changeCoordinates();
        }

      // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }

    </script>
@endsection