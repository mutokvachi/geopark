<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <link href="{{ asset('plugins/inspinia/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('/plugins/summernote/css/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/style.css') }}" rel="stylesheet">
    @yield('head')
</head>
<body>
    <div id="wrapper">
        @section('nav')
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> <span>
                                <img alt="image" class="img-circle" src="{{ asset('plugins/inspinia/img/profile_small.jpg') }}" />
                                 </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>
                                 </span> <span class="text-muted text-xs block">Administrator</span> </span> </a>
                            </div>
                            <div class="logo-element">
                                IN+
                            </div>
                        </li>
                        <li class="active">
                            <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">მთავარი</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active-nav-parks"><a href="{{ route('parksView') }}">პარკები</a></li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li class="active-nav-regions"><a href="{{ route('regionsView') }}">რეგიონები</a></li>
                            </ul>
                            

                            <ul class="nav nav-second-level">
                                <li class="active-nav-nature">
                                    <a href="{{ route('natureView') }}">Nature & Culture</a></li>
                            </ul>
                             <ul class="nav nav-second-level">
                                <li class="active-nav-slider">
                                    <a href="{{ route('sliderView') }}">მთავარი სლაიდერი</a></li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li class="active-nav-thingsToSee">
                                    <a href="{{ route('thingsToSeeView') }}">სანახავი ადგილები</a></li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li class="active-nav-imageGallery">
                                    <a href="{{ route('imageGalleryView') }}">სურათების გალერეა</a></li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li class="active-nav-videoGallery">
                                    <a href="{{ route('videoGalleryView') }}">ვიდეო გალერეა</a></li>
                            </ul>
                            
                            <ul class="nav nav-second-level">
                                <li class="active-nav-events">
                                    <a href="{{ route('eventsView') }}">ივენთები</a></li>
                            </ul>
                            <ul class="nav nav-second-level">
                                <li class="active-nav-alerts">
                                    <a href="{{ route('alertsView') }}">შეტყობინებები</a></li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active-nav-settings">
                                    <a href="{{ route('settings') }}">Settings</a>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active-nav-recommend">
                                    <a href="{{ route('recommendView') }}">რეკომენდაციები</a>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active-nav-toSee">
                                    <a href="{{ route('toSeeView') }}">ადგილები რომლებიც უნდა ნახო</a>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active-nav-getHere">
                                    <a href="{{ route('getHereView') }}">GetHere</a>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active-nav-map">
                                    <a href="{{ route('mapEditView', ['id'=>1]) }}">მთავარი მაპი</a>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active-nav-downloadable">
                                    <a href="{{ route('downloadableView') }}">გადმოსაწეი ფაილები</a>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active-nav-basicInfo">
                                    <a href="{{ route('basicInfoView') }}">Basic Info</a>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active-nav-program">
                                    <a href="{{ route('programView') }}">რეინჯერების პროგრამები</a>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active-nav-instagram">
                                    <a href="{{ route('instagramView') }}">მოგონებები/ინსტაგრამი</a>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active">
                                    <a href="#">თავგადასავლები<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <ul class="nav nav-second-level">
                                            <li class="active-nav-adventures">
                                                <a href="{{ route('adventuresView') }}">თავგადასავლები</a></li>
                                        </ul>

                                        <ul class="nav nav-second-level">
                                            <li class="active-nav-adventureImage">
                                                <a href="{{ route('adventureImageView') }}">სურათები</a></li>
                                        </ul>
                                    </ul>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active">
                                    <a href="#">საყიდელი ნივთები<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li class="active-nav-toBuy">
                                            <a href="{{ route('toBuyView') }}">ნივთები</a>
                                        </li>
                                        <li class="active-nav-toBuyShops">
                                            <a href="{{ route('toBuyShopsView') }}">მაღაზიები</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active">
                                    <a href="#">გარშემო ობიექტები<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li class="active-nav-aroundPark">
                                            <a href="{{ route('aroundParkView') }}">ობიექტები</a>
                                        </li>

                                        <li class="active-nav-place">
                                            <a href="{{ route('placeView') }}">კატეგორიები</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                            <ul class="nav nav-second-level">
                                <li class="active">
                                    <a href="#">ტურები<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li class="active-nav-trip">
                                            <a href="{{ route('tripView') }}">ტურები</a>
                                        </li>
                                        <li class="active-nav-tripStep">
                                            <a href="{{ route('tripStepView') }}">ტურის დღეები</a>
                                        </li>
                                        <li class="active-nav-tripAdventure">
                                            <a href="{{ route('tripAdventureView') }}">თავგადასავლები</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>


                        
                        {{-- <li>
                            <a href="widgets.html"><i class="fa fa-th-large"></i> <span class="nav-label">Widgets</span></a>
                        </li> --}}

                        <li class="active">
                            <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">ADMINISTATOR</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active-nav-memory">
                                    <a href="{{ route('memoryView') }}">Memories</a>
                                </li>
                                <li class="active-nav-subscribeControll">
                                    <a href="{{ route('subscribeControllView') }}">Subscribers</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        @show

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="{{ route('adminLogout') }}">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                        <li>
                            <a class="right-sidebar-toggle">
                                <i class="fa fa-tasks"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row">
                <div>
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('plugins/inspinia/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/plugins/select2/select2.full.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('plugins/inspinia/js/inspinia.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('/plugins/summernote/js/summernote.min.js') }}"></script>

    @yield('footer')

    @yield('javascript')
    <script type="text/javascript">
        var segment = window.location.pathname.split('/')[2];
        $('.active-nav-'+segment).addClass('active');

    </script>
</body>
</html>
