@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{{ $info['title'] }}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ $info['form_action'] }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}




                    {{-- Content --}}


                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ ტური</label>
                        <div class="col-sm-10">
                            <select name="trip_id" class="form-control"  required>
                                @foreach($trips as $trip)
                                    <option value="{{ $trip->id }}">{{ $trip->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-xs-5 m-l-n" style="padding-left: 30px">
                            <label>აირჩიეთ ტიპი</label>
                            <select class="form-control" name="transport[]" multiple="" required  >
                                <option value="car">მანქანა</option>
                                <option value="bus">ავტობუსი</option>
                                <option value="horse">ცხენი</option>
                                <option value="atv">კვადროციკლი</option>
                                <option value="bike">ველოსიპედი</option>
                                <option value="boat">ნავი</option>
                                <option value="hiking">ფეხით სიარული</option>
                                {{-- <option value="transport_icon_train">მატარებელი</option> --}}
                                {{-- <option value="transport_icon_miniBus">მარშუტკა</option> --}}
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დრო (წუთებში)</label>
                        <div class="col-sm-10">
                            <input type="text" name="minutes" class="form-control" required placeholder="მაგ: 60">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_en" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_ru" class="form-control"  >
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">დღე N:</label>
                        <div class="col-sm-10">
                            <input type="text" name="trip_days" class="form-control" required placeholder="2">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ტურის დისტანცია (KM ებში)</label>
                        <div class="col-sm-10">
                            <input type="text" name="trip_distance" class="form-control" required placeholder="15">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გასავლელი გზა ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" name="route" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გასავლელი გზა ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="route_en" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გასავლელი გზა რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="route_ru" class="form-control"  >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აღწერა ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description" class="form-control" required ></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აღწერა ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_en" class="form-control"  ></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აღწერა რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_ru" class="form-control"  ></textarea>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ სურათი</label>
                        <div class="col-sm-10">
                            <input type="file" name="image[]" class="form-control"  required accept="image/*">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ დოკუმენტი</label>
                        <div class="col-sm-10">
                            <input type="file" name="attachment[]" class="form-control">
                        </div>
                    </div>

                    <br>


                    {{-- content end --}}


                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button">
                                <a href="{{ $info['redirect_back'] }}">უკან დაბრუნება</a>
                            </button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    @parent
@endsection