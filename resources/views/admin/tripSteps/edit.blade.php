@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{{ $info['title'] }}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ $info['form_action'] }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}




                    {{-- content starts here --}}

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ ტური</label>
                        <div class="col-sm-10">
                            <select name="trip_id" class="form-control"  required>
                                @foreach($trips as $trip)
                                    @if($trip->id == $item->trip_id)
                                        <option value="{{ $trip->id }}" class="pop_option">{{ $trip->name }}</option>
                                    @endif
                                    <option value="{{ $trip->id }}">{{ $trip->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>

                   
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-xs-5 m-l-n multi-select" style="padding-left: 30px">
                            <label>აირჩიეთ ტრანსპორტი</label>
                            <div class="hide-class">{{ $item->transport }}</div>
                            <select class="form-control" name="transport[]" multiple="" required  >
                                <option value="car">მანქანა</option>
                                <option value="bus">ავტობუსი</option>
                                <option value="horse">ცხენი</option>
                                <option value="atv">კვადროციკლი</option>
                                <option value="bike">ველოსიპედი</option>
                                <option value="boat">ნავი</option>
                                <option value="hiking">ფეხით სიარული</option>
                                {{-- <option value="transport_icon_train">მატარებელი</option> --}}
                                {{-- <option value="transport_icon_miniBus">მარშუტკა</option> --}}
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">დრო (წუთებში)</label>
                        <div class="col-sm-10">
                            <input type="text" name="minutes" class="form-control" required value="{{ $item->minutes }}" placeholder="მაგ: 60">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required value="{{ $item->name }}" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_en" class="form-control"  value="{{ $item->name_en }}" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_ru" class="form-control"  value="{{ $item->name_ru }}" >
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">დღე N:</label>
                        <div class="col-sm-10">
                            <input type="text" name="trip_days" class="form-control" required value="{{ $item->trip_days }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ტურის დისტანცია</label>
                        <div class="col-sm-10">
                            <input type="text" name="trip_distance" class="form-control" required value="{{ $item->trip_distance }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გასავლელი გზა ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" name="route" class="form-control" required value="{{ $item->route }}" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გასავლელი გზა ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="route_en" class="form-control"  value="{{ $item->route_en }}" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">გასავლელი გზა რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="route_ru" class="form-control"  value="{{ $item->route_ru }}" >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აღწერა ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description" class="form-control" required >{{ $item->description }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აღწერა ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_en" class="form-control"  >{{ $item->description_en }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აღწერა რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_ru" class="form-control"  >{{ $item->description_ru }}</textarea>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="col-xs-2"></div>
                    <div class="col-xs-10">
                        <?php $my_image = json_decode($item->image); ?>
                        <img src="{{ asset("files/$my_image[0]") }}" width="250" height="100">
                    </div>
                    <div class="clear"></div><br>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ სურათი</label>
                        <div class="col-sm-10">
                            <input type="file" name="image[]" class="form-control" accept="image/*">
                        </div>
                    </div>         

                    <div class="hr-line-dashed"></div>

                    <div class="col-xs-2"></div>
                    <div class="col-xs-10">
                        <?php $my_attachment = json_decode($item->attachment); ?>
                        <a href="{{ asset("files/$my_attachment[0]") }}">გადმოწერა</a>
                    </div>
                    <div class="clear"></div><br>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ დოკუმენტი</label>
                        <div class="col-sm-10">
                            <input type="file" name="attachment[]" class="form-control" >
                        </div>
                    </div>         
                    <br>


                    {{-- content ends here --}}

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button">
                                <a href="{{ $info['redirect_back'] }}">უკან დაბრუნება</a>
                            </button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                        <button type="button" class="btn btn-danger pull-right delete-alert" style="margin-right: 15px;" delete-id='{{ $info['delete_route'] }}'>წაშლა</button>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxfx_VISzjubKfXCaS_TA1i8hLdK7i7Us&callback=initMap">
    </script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    @parent        
@endsection