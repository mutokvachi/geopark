@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
    <style type="text/css">
        .insta-image.active{
            border: 5px solid lightgreen;
        }
    </style>
@endsection

@section('nav')
    @parent
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{{ $info['title'] }}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ $info['form_action'] }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}


                    {{-- Content --}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ პარკი</label>
                        <div class="col-sm-10">
                            <select name="park_id" class="form-control"  required>
                                @foreach($parks as $park)
                                    <option value="{{ $park->id }}">{{ $park->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Hashtag #</label>
                        <div class="col-sm-10">
                            <input type="text" name="hashtag" class="form-control hashtag-value" required >
                            <br>
                            <button class="btn btn-warning search-hashtags"  type="button">სურათების ძებნა</button>
                        </div>
                    </div>

                    <div class="form-group" style="display: none;">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <input type="text" name="hashtag_image" class="hashtag_image form-control hashtag-value">
                        </div>
                    </div>                    

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10 insta-images">
                            
                        </div>
                    </div>


                    {{-- content end --}}


                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button">
                                <a href="{{ $info['redirect_back'] }}">უკან დაბრუნება</a>
                            </button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        function getHashtagImages(){
            $('.search-hashtags').click(function(){
                var serial = {hashtag: $('.hashtag-value').val()};
                $.ajax({
                    method: 'get',
                    url: '/instagram/photos/'+serial.hashtag,
                    success:function(res){
                        $('.insta-images').empty();
                        for(var i in res){
                            $('.insta-images').append(`
                                <img src="`+res[i]+`" width="300" height="150" style="margin: 10px;" class="insta-image">
                            `);
                        }
                    }
                });
            });
        }
        getHashtagImages();

        function saveInstaImage(){
            $('body').on('click','.insta-image',function(){
                $('.insta-image').removeClass('active');
                var attr = $(this).attr('src');
                $(this).addClass('active');
                $('.hashtag_image').attr('value', attr);
            });
        }

        saveInstaImage();

    </script>
@endsection