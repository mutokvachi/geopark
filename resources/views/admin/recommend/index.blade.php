@extends('admin.layouts.master')

@section('head')

    <link href="{{ asset('plugins/inspinia/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <style type="text/css">
        .dataTables_info,
        .dataTables_length,
        .pagination{
            display: none;
        }

        tbody tr{
            cursor: pointer;
        }
    </style>
@endsection

@section('nav')
    @parent
@endsection

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>{{ $info['title'] }}</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="btn btn-primary">
            	<a href="{{ $info['add_view'] }}" style="color: #fff;">
            	+ {{ $info['add_region'] }}
		        </a>
		    </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <th>ID</th>
                        <th>სახელი</th>
                        <th>სურათი</th>
                        <th>პარკი</th>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                            <?php $img = json_decode($item->img)[0]; ?>
                            <tr class="gradeX" data-target='{{ $item->id }}'>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->date }}</td>
                                <td>{{ $parks[$item->park_id] or ''}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>სახელი</th>
                            <th>სურათი</th>
                            <th>პარკი</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('plugins/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 500,
                responsive: true,
                buttons: [
                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ]

            });
        });

        $('tbody tr').click(function(){
            var attr = $(this).attr('data-target');
            var url = "{{ $info['edit_view'] }}";
            url = url.split('/');
            url.splice(-1,1);
            url = url.join('/');

            window.location.href = url+'/'+attr;
        });
    </script>
@endsection