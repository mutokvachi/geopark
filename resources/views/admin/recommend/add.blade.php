@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{{ $info['title'] }}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ $info['form_action'] }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}


                    {{-- Content --}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ პარკი</label>
                        <div class="col-sm-10">
                            <select name="park_id" class="form-control"  required>
                                @foreach($parks as $park)
                                    <option value="{{ $park->id }}">{{ $park->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">თარიღი</label>
                        <div class="col-sm-10">
                            <input type="date" name="date" class="form-control" required>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_en" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_ru" class="form-control"  >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group summernote-counter" data-id="0">
                        <label class="col-sm-2 control-label">დეტალები ქართულად (GE)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description" class="form-control nonSummernote" required ></textarea>
                        </div>
                    </div>

                    <div class="form-group summernote-counter" data-id="1">
                        <label class="col-sm-2 control-label">დეტალები ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_en" class="form-control nonSummernote"  ></textarea>
                        </div>
                    </div>

                    <div class="form-group summernote-counter" data-id="2">
                        <label class="col-sm-2 control-label">დეტალები რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description_ru" class="form-control nonSummernote"></textarea>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ატვირთეთ სურათი</label>
                        <div class="col-sm-10">
                            <input type="file" name="img[]" class="form-control" accept="image/*" required>
                        </div>
                    </div>


                    {{-- content end --}}


                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button">
                                <a href="{{ $info['redirect_back'] }}">უკან დაბრუნება</a>
                            </button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            var textSelected;
            $('textarea').summernote({
                placeholder: 'შეავსეთ ველი',
                tabsize: 2,
                height: 400,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['color', ['color']],
                    ['insert', ['picture']],
                ],
                callbacks: {
                    onImageUpload: function(files) {
                        sendFile(files[0]);
                    }
                }
            });

            function sendFile(file, editor, welEditable) {
                data = new FormData();
                data.append("file", file);
                data.append("_token", '{{ csrf_token() }}');
                $.ajax({
                    data: data,
                    type: 'POST',
                    url: "{{ route('recommendImageUpload') }}",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data){
                        $('.form-group[data-id='+textSelected+'] textarea').summernote('editor.insertImage', data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus+" "+errorThrown);
                    }
                });
            }

            $(".note-insert").click(function(){
                var index = $(this).closest('.summernote-counter').attr('data-id');
                textSelected = index;
            });
            
        });
    </script>
@endsection