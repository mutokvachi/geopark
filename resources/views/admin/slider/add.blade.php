@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{{ $info['title'] }}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ $info['form_action'] }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}


                    {{-- Content --}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ პარკი</label>
                        <div class="col-sm-10">
                            <select name="park_id" class="form-control"  required>
                                <option value="mainPage">მთავარი გვერდი</option>
                                <option value="geoAdventures">თავგადასავლების გვერდი</option>
                                <option value="tripsPage">ტურების გვერდი</option>
                                <option value="footerServices">სერვისები</option>
                                <option value="footerAttractions">შესვლის ნებართვები</option>
                                <option value="footerGuide">Guide</option>
                                <option value="footerInfo">Info</option>
                                <option value="footerTransportation">ტრანსპორტი</option>
                                <option value="parkList">ეროვნული პარკების სიის გვერდი</option>
                                @foreach($parks as $park)
                                    <option value="{{ $park->alias }}">{{ $park->name }}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">რიგითობა</label>
                        <div class="col-sm-10">
                            <input type="text" name="sort_by" class="form-control" placeholder="999">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ტიპი</label>
                        <div class="col-sm-10">
                            <select name="type" class="form-control type-changer"  required>
                                <option value="photo">ფოტო</option>
                                <option value="video">yotube ვიდეო</option>
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_en" class="form-control"  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_ru" class="form-control"  >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ღილაკის ლინკი: <small>აუცილებელი არაა</small></label>
                        <div class="col-sm-10">
                            <input type="text" name="btn_link" class="form-control" placeholder="მაგ: /site/testPark">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group photo-gallery">
                        <label class="col-sm-2 control-label">ატვირთეთ სურათი</label>
                        <div class="col-sm-10">
                            <input type="file" name="img[]" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group youtube_link">
                        <label class="col-sm-2 control-label">Youtube ვიდეოს ლინკი</label>
                        <div class="col-sm-10">
                            <input type="text" name="youtube_link" class="form-control">
                        </div>
                    </div>




                    {{-- content end --}}


                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button">
                                <a href="{{ $info['redirect_back'] }}">უკან დაბრუნება</a>
                            </button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        var typeChangerVal = $('.type-changer').val();
        
        function typeCheckVal(element){
            if(element == 'video'){
                $(".photo-gallery").hide()
                $('.youtube_link').show();
            }else{
                $(".photo-gallery").show()
                $('.youtube_link').hide();
            }
        }
        typeCheckVal(typeChangerVal);
        $('.type-changer').on('change',function(){
            typeCheckVal($(this).val());
        });
    </script>
@endsection