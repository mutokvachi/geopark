@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>{{ $info['title'] }}</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ $info['form_action'] }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}




                    {{-- content starts here --}}

                    <div class="form-group">
                        <label class="col-sm-2 control-label">აირჩიეთ პარკი</label>
                        <div class="col-sm-10">
                            <select name="park_id" class="form-control"  required>

                                @foreach($parks as $park)
                                    @if($park->alias == $item->park_id)
                                        <option value="{{ $park->alias }}" class="pop_option">{{ $park->name }}</option>
                                    @endif
                                    <option value="{{ $park->alias }}">{{ $park->name }}</option>
                                @endforeach                                

                                @if($item->park_id == 'geoAdventures')
                                    <option value="geoAdventures" class="pop_option">თავგადასავლების გვერდი</option>
                                @else
                                    <option value="geoAdventures">თავგადასავლების გვერდი</option>
                                @endif

                                @if($item->park_id == 'tripsPage')
                                    <option value="tripsPage" class="pop_option">ტურების გვერდი</option>
                                @else
                                    <option value="tripsPage">ტურების გვერდი</option>
                                @endif

                                @if($item->park_id == 'mainPage')
                                    <option value="mainPage" class="pop_option">მთავარი გვერდი</option>
                                @else
                                    <option value="mainPage">მთავარი გვერდი</option>
                                @endif

                                @if($item->park_id == 'footerServices')
                                    <option value="footerServices" class="pop_option">სერვისები</option>
                                @else
                                    <option value="footerServices">სერვისები</option>
                                @endif

                                @if($item->park_id == 'footerAttractions')
                                    <option value="footerAttractions" class="pop_option">შესვლის ნებართვები</option>
                                @else
                                    <option value="footerAttractions">შესვლის ნებართვები</option>
                                @endif

                                @if($item->park_id == 'footerGuide')
                                    <option value="footerGuide" class="pop_option">Guide</option>
                                @else
                                    <option value="footerGuide">Guide</option>
                                @endif

                                @if($item->park_id == 'footerInfo')
                                    <option value="footerInfo" class="pop_option">Info</option>
                                @else
                                    <option value="footerInfo">Info</option>
                                @endif

                                @if($item->park_id == 'footerTransportation')
                                    <option value="footerTransportation" class="pop_option">ტრანსპორტი</option>
                                @else
                                    <option value="footerTransportation">ტრანსპორტი</option>
                                @endif

                                @if($item->park_id == 'parkList')
                                    <option value="parkList" class="pop_option">ეროვნული პარკების სიის გვერდი</option>
                                @else
                                    <option value="parkList">ეროვნული პარკების სიის გვერდი</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">რიგითობა</label>
                        <div class="col-sm-10">
                            <input type="text" name="sort_by" class="form-control" value="{{ $item->sort_by }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ტიპი</label>
                        <div class="col-sm-10">
                            <?php $arr = ['photo'=>'ფოტო','video'=>'ვიდეო მასალა']; ?>
                            <select name="type" class="form-control type-changer"  required>
                                @if(isset($item->type))
                                    <option value="{{ $item->type }}" class="pop_option">{{ $arr[$item->type] }}</option>
                                @endif
                                <option value="photo">ფოტო</option>
                                <option value="video">yotube ვიდეო</option>
                            </select>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ქართულად (KA)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required value="{{ $item->name }}" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი ინგლისურად (EN)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_en" class="form-control" value="{{ $item->name_en }}" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სახელი რუსულად (RU)</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_ru" class="form-control" value="{{ $item->name_ru }}" >
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">ღილაკის ლინკი: <small>აუცილებელი არაა</small></label>
                        <div class="col-sm-10">
                            <input type="text" name="btn_link" class="form-control" placeholder="მაგ: /site/testPark" value="{{ $item->btn_link }}">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="col-xs-2"></div>
                    <div class="col-xs-10">
                        <?php $my_file = json_decode($item->img); ?>
                        @if(isset($item->type))
                            @if($item->type == 'photo')
                                <img src="{{ asset("/files/$my_file[0]") }}" width="250" height="100">
                            @else
                                <iframe width="250" height="150" src="{{ $item->youtube_link }}">
                                </iframe>
                            @endif
                        @else
                            <img src="{{ asset("/files/$my_file[0]") }}" width="250" height="100">
                        @endif
                    </div>
                    <div class="clear"></div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group photo-gallery">
                        <label class="col-sm-2 control-label">ატვირთეთ სურათი</label>
                        <div class="col-sm-10">
                            <input type="file" name="img[]" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group youtube_link">
                        <label class="col-sm-2 control-label">Youtube ვიდეოს ლინკი</label>
                        <div class="col-sm-10">
                            <input type="text" name="youtube_link" class="form-control" value="{{ $item->youtube_link }}">
                        </div>
                    </div>



                    {{-- content ends here --}}

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button">
                                <a href="{{ $info['redirect_back'] }}">უკან დაბრუნება</a>
                            </button>
                            <button class="btn btn-primary send_data" type="submit">დამატება</button>
                        </div>
                        <button type="button" class="btn btn-danger pull-right delete-alert" style="margin-right: 15px;" delete-id='{{ $info['delete_route'] }}'>წაშლა</button>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        var typeChangerVal = $('.type-changer').val();
        
        function typeCheckVal(element){
            if(element == 'video'){
                $(".photo-gallery").hide()
                $('.youtube_link').show();
            }else{
                $(".photo-gallery").show()
                $('.youtube_link').hide();
            }
        }
        typeCheckVal(typeChangerVal);
        $('.type-changer').on('change',function(){
            typeCheckVal($(this).val());
        });
    </script>
@endsection