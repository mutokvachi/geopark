@extends('admin.layouts.master')

@section('head')
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/main/css/bootstrap-tagsinput.css') }}">

@endsection

@section('nav')
    @parent
@endsection

@section('content')
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>რეგიონის დამატება</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" action="{{ route('memoryEdit', ['id' => $item->id]) }}" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">პარკი</label>
                        <div class="col-sm-10">
                            <input type="text" disabled class="form-control" value="{{ $item->park->name }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სათაური</label>
                        <div class="col-sm-10">
                            <input type="text" name="title" class="form-control"  value="{{ $item->title }}" required>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">ენა</label>
                        <div class="col-sm-10">
                            <select name="lang" class="form-control"  required>
                                <option value="{{ $item->lang }}" class="pop_option">{{ $item->lang }}</option>
                                <option value="ka">ka</option>
                                <option value="en">en</option>
                                <option value="ru">ru</option>
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <input type="hidden" name="date" value="{{ date('d-M-Y') }}">

                    <div class="col-xs-2"></div>
                    <div class="col-xs-10">
                        <?php $images = json_decode($item->images); ?>
                        @foreach($images as $img)
                            <img src="{{ asset("/main/img/$img") }}" width="250" height="100" style="margin-bottom: 5px;">
                        @endforeach
                    </div>
                    <div class="clear"></div>
                    <br>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">ტეგები</label>
                        <div class="col-sm-10">
                            <input type="text" name="tags" value="{{ $item->tags }}" data-role="tagsinput" required>
                        </div>
                    </div>

                    

                    <div class="hr-line-dashed"></div>

                    @if($item->type == 'blog')
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ტექსტი</label>
                            <div class="col-sm-10">
                                <textarea type="text" name="extra" class="form-control" required >{{ $item->extra }}</textarea>
                            </div>
                        </div>
                    @elseif($item->type == 'video')
                        <?php $videos = json_decode($item->extra); ?>
                        @foreach($videos as $key => $video)
                        <div class="form-group">
                        <label class="col-sm-2 control-label">ვიდეოების ლინკები: {{ $key }}</label>
                            <div class="col-sm-10">
                                <input type="text" name="extra[]" class="form-control"  value="{{ $video }}" >
                            </div>
                        </div>
                        @endforeach
                    @endif

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">სტატუსი</label>
                        <div class="col-sm-10">
                            <select name="status" class="form-control"  required>
                                <?php $values = ['განხილვის პროცესშია', 'დადასტურებულია', 'არ დადასტურდა']; ?>
                                <option value="{{ $item->status }}" class="pop_option">{{ $values[$item->status] }}</option>
                                <option value="0">განხილვის პროცესშია</option>
                                <option value="1">დადასტურებულია</option>
                                <option value="2">არ დადასტურდა</option>
                            </select>
                        </div>
                    </div>


                   
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button"><a href="{{ route('memoryView') }}">უკან დაბრუნება</a></button>
                            <button class="btn btn-primary send_data" type="submit">რედაქტირება</button>
                        </div>
                    </div><br>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert alert-danger my_warning hide-class">
                        ყველა ველის შევსება აუცილებელია !
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/main/js/bootstrap-tagsinput.js') }}"></script>
@endsection

@section('javascript')
    @parent
@endsection