@extends('admin.layouts.master')

@section('head')

    <link href="{{ asset('plugins/inspinia/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <style type="text/css">
    	.dataTables_info,
    	.dataTables_length,
    	.pagination{
    		display: none;
    	}

    	tbody tr{
    		cursor: pointer;
    	}
    </style>
@endsection

@section('nav')
    @parent
@endsection

@section('content')
    <div class="ibox float-e-margins">
	    <div class="ibox-title">
	        <h5>ყველა მონაცემის სია</h5>
	        <div class="ibox-tools">
	            <a class="collapse-link">
	           		<i class="fa fa-chevron-up"></i>
	            </a>
	        </div>
	    </div>
	    <div class="ibox-content">
            {{-- <div class="btn btn-primary"><a href="{{ route('regionAddView') }}" style="color: #fff;">+ რეგიონის დამატება</a></div> --}}
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <?php $values = ['განხილვის პროცესშია', 'დადასტურებულია', 'არ დადასტურდა']; ?>
                    <thead>
                        <th>ID</th>
                        <th>მდებარეობა</th>
                        <th>ავტორი</th>
                        <th>ავტორის ID</th>
                        <th>ენა</th>
                        <th>სტატუსი</th>
                    </thead>
                    <tbody>

                        @foreach($items as $item)
                            <tr class="gradeX" data-target='{{ $item->id }}'>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->title }}</td>
                                <td class="center">{{ $item->user->name }}</td>
                                <td class="center">{{ $item->user->id }}</td>
                                <td class="center">{{ $item->lang }}</td>
                                <td class="center">{{ $values[$item->status] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>მდებარეობა</th>
                            <th>ავტორი</th>
                            <th>ავტორის ID</th>
                            <th>ენა</th>
                            <th>სტატუსი</th>
                        </tr>
                    </tfoot>
                </table>
	        </div>
	    </div>
	</div>
@endsection

@section('footer')
    <script src="{{ asset('plugins/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
    	$(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 500,
                responsive: true,
                buttons: [
                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                   		}
                    }
                ]

            });
        });

        $('tbody tr').click(function(){
        	var attr = $(this).attr('data-target');
        	var url = "{{ route('memoryEditView', ['id' => 1] ) }}";
        	url = url.split('/');
        	url.splice(-1,1);
        	url = url.join('/');

        	window.location.href = url+'/'+attr;
        });
    </script>
@endsection