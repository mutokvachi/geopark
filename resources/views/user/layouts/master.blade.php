<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="{{ asset('/main/img/favicon.png') }}" type="image/x-icon"/>
	<link rel="shortcut icon" href="{{ asset('/main/img/favicon.png') }}" type="image/x-icon"/>
	<meta name="description" content="National Parks Of Georgia">
	<meta name="keywords" content="{{ __('website.seo_keywords') }}">
	<meta property="fb:article_style" content="National Parks Of Georgia">
	<meta property="og:site_name" content="National Parks">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ Config::get('app.url') }}">
	<meta property="og:title" content="{{ $meta['title'] or 'National Parks Of Georgia'}}">
	<meta property="og:image" content="{{ $meta['image'] or '/main/img/logo-brand.png' }}">
	<meta property="og:description" content="{{ $meta['description'] or 'geopark.ge' }}"> 

	{{-- pinterest --}}
	<meta name="p:domain_verify" content="6354e01645bdee4f486d984cffa7a3ae"/> 


	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/fonts.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/owl.carousel.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/scrollbar.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/animate.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/plugins/jssocials/jssocials.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/plugins/jssocials/jssocials-theme-classic.css') }}">
	@yield('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/responsive.css') }}">
	@if(Request::segment(1) == 'ka')
		<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/geo.css') }}">
	@endif
	
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	 fbq('init', '2426992114048827'); 
	fbq('track', 'PageView');
	</script>
	<noscript>
	 <img height="1" width="1" 
	src="https://www.facebook.com/tr?id=2426992114048827&ev=PageView
	&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MWHN7DC');</script>
	<!-- End Google Tag Manager -->


</head>
<body>
	<div class="main-site-loader">
		<div class="center">
			<div class="logo">
				@if(App::getLocale() == 'ka')
					<img src="{{ asset('files/load-ka.png') }}">
				@else
					<img src="{{ asset('files/load-en.png') }}">
				@endif
			</div>
			<img class="gif" src="{{ asset('files/load.gif') }}">
		</div>
	</div>
	@section('header')
		<div class="top_nav blck verlag t-up">
			@if(Request::segment(3) == 'aroundMe')
				<a href="{{ route('index', ['lang'=>App::getLocale()]) }}" class="arroud_me t-16-px bl pull-left">
					<span class="txt">{{ __('website.main_back_to') }}</span>
				</a>
			@else
				<a href="{{ route('masterAroundMe', ['lang'=>App::getLocale()]) }}" class="arroud_me t-16-px bl pull-left">
					<span class="txt">{{ __('website.around_me') }}</span>
				</a>
			@endif
			<div class="socials">
				<div class="flwUs">{{ __('website.follow_us') }}</div>
				<a href="{{ Config::get('setting.social.fb') }}" target="_blank" class="social i-fb"></a>
				<a href="{{ Config::get('setting.social.instagram') }}" target="_blank" class="social i-insta"></a>
				<a href="{{ Config::get('setting.social.youtube') }}" target="_blank" class="social i-youtube"></a>
				<a href="{{ Config::get('setting.social.pinterest') }}" target="_blank" class="social i-pinterest"></a>
				<div class="clearfix"></div>
			</div>
			<div class="pull-right">
				
				@if(Session::has('user'))
					<div class="pull-left relative regUser">
						<div class="u_drop">
							<div class="u-av photo">
								<?php $avatar = Session::get('user')->img; ?>
								<img src="{{ asset("/main/img/$avatar") }}">
							</div>
							<div class="welcome">
								<p class="devinne t-12-px t-cp m-0">{{ __('website.header_hello') }}</p>
								<h4 class="user_name circular t-14-px m-0">{{ explode(' ', Session::get('user')->name)[0]  }}</h4>
							</div>
						</div>
						<div class="u_dropdown">
							<ul>
								<li>
									<a href="{{ route('favourites', ['lang'=>App::getLocale()]) }}">{{ __('website.header_favorities') }}</a>
								</li>
								{{-- <li>
									<a href="#">{{ __('website.header_tickets') }}</a>
								</li> --}}
								<li>
									<a href="{{ route('memories', ['lang' => App::getLocale()]) }}">{{ __('website.header_memories') }}</a>
								</li>
								{{-- <li>
									<a href="#">{{ __('website.header_places') }}</a>
								</li> --}}
								<li>
									<a href="{{ route('profileView', ['lang' => App::getLocale()]) }}">{{ __('website.header_profile') }}</a>
								</li>
								<li>
									<a href="{{ route('changePassView', ['lang' => App::getLocale()]) }}">{{ __('website.header_change_password') }}</a>
								</li>
								<li>
									<a href="{{ route('logout', ['lang' => App::getLocale()]) }}">{{ __('website.header_log_out') }}</a>
								</li>
							</ul>
						</div>
					</div>
				@else
					<a href="{{ route('loginView', ['lang' => App::getLocale()]) }}" class="logBtn login t-12-px relative">
						<span class="txt">{{ __('website.header_login') }}</span>
					</a>
				@endif
				<div class="langBtn t-12-px drk-gry relative">
					<?php 
						$my_lang = App::getLocale(); 
						$my_langs = ['en' => 'english', 'ka' => 'georgian']; 
						// 'ru' => 'russian'  ||||||  add to add russian language
					?>
					<a href="#" data-path="{{ $_SERVER['REQUEST_URI'] }}" class="{{ $my_lang }} disabled">
						<span class="flag {{ __("website.$my_langs[$my_lang]") }}"></span>
						{{ __("website.$my_langs[$my_lang]") }}
					</a>
					<?php
						unset($my_langs[$my_lang]);
					?>
					<div class="language-selector lang-down absolute">
						@foreach($my_langs as $key => $lng)
							<a href="#" data-path="{{ $_SERVER['REQUEST_URI'] }}" data-lang="{{ $key }}" class="{{ $key }}">
								<span class="flag {{ __("website.$lng") }}"></span>
								{{ __("website.$lng") }}
							</a>
						@endforeach
					</div>
				</div>
				@if(Session::has('user'))
					<a href="{{ route('favourites', ['lang'=>App::getLocale()]) }}" class="heartBtn drk-rd"></a>
				@endif
			</div>
		</div>
	@show
	
	@section('navbar')
		@include('user.layouts.navbar.navbarMain')
	@show

	@section('content')
		content goes here
	@show

	@section('footer')
		<?php $lng = App::getLocale(); ?>
		<div class="blck_nav blck t-wht verlag ">
			<div class="web-container">
				<div class="in_box blck">
					<p class="title t-up t-16-px">
						{{ Config::get("setting.footer_$lng.ticket.title") }}
					</p>
					<p class="txt">
						{{ Config::get("setting.footer_$lng.ticket.description") }}
					</p>
					<a href="{{ route('services', ['lang'=>App::getLocale()]) }}" class="rMoreBtn">{{ __('website.master_read_more') }}</a>
				</div>
				<div class="in_box blck">
					<p class="title t-up t-16-px">
						{{ Config::get("setting.footer_$lng.bus.title") }}
					</p>
					<p class="txt">
						{{ Config::get("setting.footer_$lng.bus.description") }}
					</p>
					<a href="{{ route('transportation', ['lang'=>App::getLocale()]) }}" class="rMoreBtn">{{ __('website.master_read_more') }}</a>
				</div>
				<div class="in_box blck">
					<p class="title t-up t-16-px">
						{{ Config::get("setting.footer_$lng.guide.title") }}
					</p>
					<p class="txt">
						{{ Config::get("setting.footer_$lng.guide.description") }}
					</p>
					<a href="{{ route('guide', ['lang'=>App::getLocale()]) }}" class="rMoreBtn">{{ __('website.master_read_more') }}</a>
				</div>
				<div class="in_box blck">
					<p class="title t-up t-16-px">
						{{ Config::get("setting.footer_$lng.info.title") }}
					</p>
					<p class="txt">
						{{ Config::get("setting.footer_$lng.info.description") }}
					</p>
					<a href="{{ route('basicInfo', ['lang'=>App::getLocale()]) }}" class="rMoreBtn">{{ __('website.master_read_more') }}</a>
				</div>
			</div>
		</div>

		<div class="footer-lower-map"></div>

		<footer id="footer" class="drk-gry">
			<div class="web-container">
				<div class="footer-col l-col">
					<div class="row">
						<ul>
							<li class="col-md-3 col-sm-3">
								<ul>
									<li>
										<p class="t-up">{{ __('website.footer_national_parks') }}</p>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/site/prometheuscave">
											{{ __('website.footer_park_Prometheus') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/site/martvilicanyon">
											{{ __('website.footer_park_Martvili') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/site/okatsecanyon">
											{{ __('website.footer_park_Okatse') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/site/sataplia">
											{{ __('website.footer_park_Sataplia') }} 
										</a>
									</li>
									{{-- <li>
										<a href="/{{ App::getLocale() }}/site/kazbeginp">
											{{ __('website.footer_park_Kazbegi') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/site/kolxetinp">
											{{ __('website.footer_park_Kolkheti') }} 
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/site/borjomi-kharagaulinp">
											{{ __('website.footer_park_Borjomi') }} 
										</a>
									</li> --}}
									<li class="seeAll">
										<a href="{{ route('masterParks', ['lang'=>App::getLocale()]) }}">{{ __('website.master_see_more') }}</a>
									</li>
								</ul>
							</li>
							<li class="col-md-3 col-sm-3">
								<ul>
									<li>
										<p class="t-up">{{ __('website.footer_adventure') }}</p>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/master/geoAdventures/47">
											{{ __('website.footer_adventure_Boat') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/master/geoAdventures/45">
											{{ __('website.footer_adventure_Hiking') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/master/geoAdventures/53">
											{{ __('website.footer_adventure_Cave') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/master/geoAdventures/50">
											{{ __('website.footer_adventure_Bike') }}
										</a>
									</li>
									{{-- <li>
										<a href="/{{ App::getLocale() }}/master/geoAdventures">
											{{ __('website.footer_adventure_Zip') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/master/geoAdventures">
											{{ __('website.footer_adventure_Diving') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/master/geoAdventures">
											{{ __('website.footer_adventure_Birdwatching') }}
										</a>
									</li> --}}
									<li class="seeAll">
										<a href="{{ route('masterGeoAdventures', ['lang'=>App::getLocale()]) }}">{{ __('website.master_see_more') }}</a>
									</li>
								</ul>
							</li>
							<li class="col-md-3 col-sm-3">
								<ul>
									<li>
										<p class="t-up">
											{{ __('website.footer_plan_trip') }}
										</p>
									</li>
									<li>
										<a href="{{ route('siteTripsInner', ['lang'=>App::getLocale(), 'park'=>'prometheuscave', 'id'=> 55]) }}">
											{{ __('website.footer_trip_Prometheus') }}
										</a>
									</li>
									<li>
										<a href="{{ route('siteTripsInner', ['lang'=>App::getLocale(), 'park'=>'martvilicanyon', 'id'=> 59]) }}">
											{{ __('website.footer_trip_Martvili') }}
										</a>
									</li>
									{{-- <li>
										<a href="#">
											{{ __('website.footer_trip_Okatse') }}
										</a>
									</li> --}}
									{{-- <li>
										<a href="#">
											{{ __('website.footer_trip_Boat') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/site/lagodekhinp/tripsInner/42">
											{{ __('website.footer_trip_Black') }}
										</a>
									</li>
									<li>
										<a href="/{{ App::getLocale() }}/site/borjomi-kharagaulinp/tripsInner/3">
											{{ __('website.footer_trip_Nikoloz') }}
										</a>
									</li>
									<li>
										<a href="#">
											{{ __('website.footer_trip_Colchic') }}
										</a>
									</li> --}}
									<li class="seeAll">
										<a href="{{ route('masterTrips', ['lang'=>App::getLocale()]) }}">{{ __('website.master_see_more') }}</a>
									</li>
								</ul>
							</li>
							<li class="col-md-3 col-sm-3">
								<ul>
									<li>
										<p class="t-up">
											{{ __('website.footer_about_us') }}
										</p>
									</li>
									<li>
										<a href="#">
											{{ __('website.footer_about_parks') }}
										</a>
									</li>
									<li>
										<a href="#">
											{{ __('website.footer_contact_us') }}
										</a>
									</li>
									<li>
										<a href="#">
											{{ __('website.footer_partners') }}
										</a>
									</li>
								</ul>
							</li>
						</ul>
						</div>
					<div class="partners">
						<div class="icon">
							<a href="http://nationalparks.ge" target="_blank">
								<img src="{{ asset('/main/img/logo-brand.png') }}">
							</a>
						</div>
						<div class="icon">
							<a href="http://mepa.gov.ge" target="_blank">
								<img src="{{ asset('/main/img/mepa.png') }}">
							</a>
						</div>
						<div class="icon">
							<a href="http://apa.gov.ge" target="_blank">
								<img src="{{ asset('/main/img/apa.png') }}">
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="footer-col m-col blck">
					<div class="row">
						<ul>
							<li class="col-md-7">
								<ul>
									<li>
										<p class="t-up">{{ __('website.footer_educational_programs') }}</p>
									</li>
									<li>
										<a href="#!{{-- {{ route('programs', ['lang'=>App::getLocale(), 'program'=>'Learn&Explore']) }} --}}">{{ __('website.footer_Learn&Explore') }}</a>
									</li>
									<li>
										<a href="#!{{-- {{ route('programs', ['lang'=>App::getLocale(), 'program'=>'JuniorRangers']) }} --}}">{{ __('website.footer_JuniorRangers') }}</a>
									</li>
									<li>
										<a href="#!{{-- {{ route('programs', ['lang'=>App::getLocale(), 'program'=>'ForKids']) }} --}}">{{ __('website.footer_ForKids') }}</a>
									</li>
									<li>
										<a href="#!{{-- {{ route('programs', ['lang'=>App::getLocale(), 'program'=>'ForTeachers']) }} --}}">{{ __('website.footer_ForTeachers') }}</a>
									</li>
								</ul>
							</li>
							<li class="col-md-5">
								<ul>
									<li>
										<p class="t-up">{{ __('website.footer_for_rangers') }}</p>
									</li>
									<li>
										<a href="#!{{-- {{ route('programs', ['lang'=>App::getLocale(), 'program'=>'BecomeARanger']) }} --}}">{{ __('website.footer_BecomeARanger') }}</a>
									</li>
									<li>
										<a href="#!{{-- {{ route('programs', ['lang'=>App::getLocale(), 'program'=>'RangerCenters']) }} --}}">{{ __('website.footer_RangerCenters') }}</a>
									</li>
									<li>
										<a href="#!{{-- {{ route('programs', ['lang'=>App::getLocale(), 'program'=>'RangerInformation']) }} --}}">{{ __('website.footer_RangerInformation') }}</a>
									</li>
								</ul>
							</li>
						</ul>
						{{-- <a href="#" class="rMoreBtn">get involved</a> --}}
					</div>
				</div>
				<div class="footer-col r-col">
					<form method="post" onkeypress="return event.keyCode != 13;">
						<label class="t-up t-20-px">{{ __('website.main_sign_up_newsletter') }}</label>
						<br>
						<input type="hidden" name="main-subscription-lang" value="{{ App::getLocale() }}">
						<input type="text" class="main-subscription-input" placeholder="{{ __('website.login_email') }}">
						<button type="button" class="main-subscription-btn t-up verlagB lght-gry">{{ __('website.subscribe_btn_subscribe') }}</button>
					</form>

					{{-- <p>{{ __('website.main_accept_terms_sub') }}<a href="#">{{ __('website.main_terms_conditions') }}</a></p> --}}
					<div class="follow_us">
						<p>{{ __('website.follow_us') }}</p>
					</div>
					<div class="socials">
						<a href="{{ Config::get('setting.social.fb') }}" target="_blank" class="social i-fb"></a>
						<a href="{{ Config::get('setting.social.instagram') }}" target="_blank" class="social i-insta"></a>
						<a href="{{ Config::get('setting.social.youtube') }}" target="_blank" class="social i-youtube"></a>
						<a href="{{ Config::get('setting.social.pinterest') }}" target="_blank" class="social i-pinterest"></a>
						<div class="clearfix"></div>
					</div>
					<div class="rights_reserved">
						<p>{{ __('website.main_copyright') }}</p>
					</div>
				</div>
			</div>
		</footer>
	@show

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MWHN7DC"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<script type="text/javascript" src="{{ asset('/main/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/main/js/owl.carousel.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/main/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/main/js/scrollbar.min.js') }}"></script>
	@yield('js_sources')
	<script type="text/javascript" src="{{ asset('/plugins/jssocials/jssocials.min.js') }}"></script>


	{{-- pure chat --}}
	<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '0295df2c-8e57-446a-8002-a7d3ab6dcb6c', f: true }); done = true; } }; })();</script>
	{{-- end pure chat --}}

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131260771-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-131260771-1');
	</script>

	<script type="text/javascript" src="{{ asset('/main/js/app.js') }}"></script>
	<script type="text/javascript">
		function shareButton(){
			$("#share").jsSocials({
	            shares: ["facebook", "twitter", "googleplus", "linkedin"],
	        });

	        $('.soc.fb').attr('href', $('.jssocials-share-facebook a').attr('href'));
	        $('.soc.twitter').attr('href', $('.jssocials-share-twitter a').attr('href'));
	        // $('.soc.gplus').attr('href', $('.jssocials-share-googleplus a').attr('href'));
	        $('.soc.in').attr('href', $('.jssocials-share-linkedin a').attr('href'));

		}
		shareButton();

		function searchEngine(){
			var keyPressInterval = null;
			$('.my-search-form').on('keyup keypress', function(e) {
				var keyCode = e.keyCode || e.which;
				var value = $('.my-search-input').val();
				$('.my-search-input').attr('placeholder', '');
				$('.my-suggestions-input').attr('placeholder', '');

				if (keyCode === 13) { 
					e.preventDefault();
					return false;
				}

				if(value.length <= 0){
					// $('.my-suggestions-input').attr('placeholder', '');
					$('.my-search-input').attr('placeholder', '{{ __('website.main_search') }}...');
					return $('.search-list').hide();
				}

				clearInterval(keyPressInterval);
				keyPressInterval = setTimeout(getLiveSearchResults,200);
				

				
			});

			function getLiveSearchResults(){
				var serial = {_token: '{{ csrf_token() }}', keyword: $('.my-search-input').val(), lang: '{{ App::getLocale() }}' };
				$.ajax({
					method: 'post',
					url: '{{ route('search') }}',
					data:serial,
					success:function(res){
						// console.log(res);
						$('.search-list').mCustomScrollbar('destroy');
						var keyword = res.keyword;
						
						if(keyword){
							var searchVal = serial.keyword;
							searchVal = keyword.substring(0, searchVal.length);
							if(serial.keyword == searchVal)
								$('.my-suggestions-input').attr('placeholder', keyword);
						}
							

						$('.search-list').empty();


						if(serial.lang == 'ka')
							var name = 'name';
						else
							var name = 'name_'+serial.lang; 

						var resultEmpty = true;
						for(var i in res.parks){
							$('.search-list').append(`
								<li>
									<a href="/{{ App::getLocale() }}/site/`+res.parks[i].alias+`">`+res.parks[i].name+`</a>
								</li>
							`);
							for(var t in res.parks[i].adventures){
								$('.search-list').append(`
									<li>
										<a href="/{{ App::getLocale() }}/site/`+res.parks[i].alias+`/adventuresInner/`+res.parks[i].adventures[t].id+`">`+res.parks[i].adventures[t].name+`</a>
									</li>
								`);
							}
							resultEmpty = false;
						}

						for(var i in res.trips){
							$('.search-list').append(`
								<li>
									<a href="/{{ App::getLocale() }}/site/`+res.trips[i].park.alias+`/tripsInner/`+res.trips[i].id+`">`+res.trips[i].name+` - `+res.trips[i].park.name+`</a>
								</li>
							`);
							resultEmpty = false;
						}

						for(var i in res.adventures){
							if(res.adventures[i].park_id != 0){
								$('.search-list').append(`
									<li>
										<a href="/{{ App::getLocale() }}/site/`+res.adventures[i].park.alias+`/adventuresInner/`+res.adventures[i].id+`">`+res.adventures[i].name+` - `+res.adventures[i].park.name+`</a>
									</li>
								`);
							}

							resultEmpty = false;
						}

						for(var i in res.arounds){
							$('.search-list').append(`
								<li>
									<a href="/{{ App::getLocale() }}/site/`+res.arounds[i].park.alias+`/aroundInner/`+res.arounds[i].id+`">`+res.arounds[i].name+`</a>
								</li>
							`);
							resultEmpty = false;
						}

						if(resultEmpty === true){
							$('.search-list').append(`
								<li style="color:red;">
									{{ __('website.main_search_result') }}
								</li>
							`);
						}

						$('.search-list').show();
						$('.search-list').mCustomScrollbar({
					        scrollEasing:'linear',
					        scrollInertia:300,
					    });

					}
				});
			}
		}
		searchEngine();
    	document.onreadystatechange = function () {
		    if (document.readyState === "interactive"){
		    	setTimeout(function(){
    				$('.main-site-loader').fadeOut();
		    	},200);
		    }
		}
		function hideSloganOnVideo(){
			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			 	function hideSlogan(){
			 		$('.slide_video').closest('.slide_bg').find('.slogan h2').css('display','none');
			 	}
			 	hideSlogan();
			}
		}
		hideSloganOnVideo();
    </script>
    @yield('javascript')
</body>
</html>