<div class="l_nav l_nav_ex">
	<ul>
		<li>
			<a href="{{ route('favourites', ['lang'=>App::getLocale()]) }}" data-activator="favorities">{{ __('website.profile_favorities') }}</a>
		</li>
		{{-- <li>
			<a href="#" data-activator="tickets">{{ __('website.profile_tickets') }}</a>
		</li> --}}
		<li>
			<a href="{{ route('memories', ['lang' => App::getLocale()]) }}" data-activator="memories">{{ __('website.profile_memories') }}</a>
		</li>
		{{-- <li>
			<a href="#" data-activator="places">{{ __('website.profile_places') }}</a>
		</li> --}}
		<li>
			<a href="{{ route('profileView', ['lang' => App::getLocale()]) }}" data-activator="profile">{{ __('website.profile_profile') }}</a>
		</li>
		@if(!empty(Auth::user()->password))
			<li>
				<a href="{{ route('changePassView', ['lang' => App::getLocale()]) }}" data-activator="password">{{ __('website.profile_password') }}</a>
			</li>
		@endif
	</ul>
</div>