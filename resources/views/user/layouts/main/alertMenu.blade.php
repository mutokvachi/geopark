<div class="bottom_nav">
	<ul>
		<li>
			<div class="resBtn">
				<span class="top-line"></span>
				<span class="ex-midd-line"></span>
				<span class="midd-line"></span>
				<span class="bott-line"></span>
			</div>
		</li>
		<li>
			<a href="{{ route('siteParksInner', ['lang'=>App::getLocale(), 'park'=>Request::segment(3)]) }}">{{ __('website.alertMenu_overview') }}</a>
		</li>
		<li>
			<a href="{{ route('siteAdventures',['lang'=>App::getLocale(),'park'=>Request::segment(3)]) }}">{{ __('website.alertMenu_adventures') }}</a>
		</li>
		<li><a href="{{ route('siteGetHere',['lang'=>App::getLocale(),'park'=>Request::segment(3)]) }}">{{ __('website.alertMenu_getThere') }}</a></li>
		<li>
			<a href="{{ route('siteNatureCulture',['lang'=>App::getLocale(),'park'=>Request::segment(3)]) }}">{{ __('website.alertMenu_nature') }}</a>
		</li>
		<li>
			<a href="{{ route('siteMemoriesView',['lang'=>App::getLocale(),'park'=>Request::segment(3)]) }}">{{ __('website.alertMenu_memories') }}</a>
		</li>
		<li>
			<a href="{{ route('siteEvents', ['lang'=>App::getLocale(), 'park'=>Request::segment(3)]) }}">{{ __('website.alertMenu_events') }}</a>
		</li>
		<li>
			<a href="{{ route('siteAroundPark', ['lang'=>App::getLocale(), 'park'=>Request::segment(3)]) }}">{{ __('website.alertMenu_eatSleep') }}</a>
		</li>
		<li>
			<a href="{{ route('siteTobuy', ['lang'=>App::getLocale(), 'park'=>Request::segment(3)]) }}">{{ __('website.alertMenu_things') }}</a>
		</li>
		<li>
			<a href="{{ route('siteGuide', ['lang'=>App::getLocale(), 'park'=>Request::segment(3)]) }}">{{ __('website.alertMenu_guide') }}</a>
		</li>
	</ul>
	@if(isset($alerts))
		<div class="alert_cont relative">
			<button class="alerts verlagB">
				{{ __('website.alertMenu_alert') }}
				<span class="absolute circular t-12-px quant wht">0</span>
			</button>
			<div class="drk-rd alert_box">
				<div class="info">
					@foreach($alerts as $alert)
						<div class="data">
							<h3>{{ $alert->date }}</h3>
							<p>{!! Stuff::trans($alert, 'description') !!}</p>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	@endif
	<div class="clearfix"></div>
</div>