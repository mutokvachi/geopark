
<div class="top_section_bg">
	<div class="top-pagination verlag t-wht t-18-px"></div>
	<div class="top_sight_img">
		<div class="loaderDotCont">
			<?xml version="1.0" encoding="utf-8"?>
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 140 140" style="enable-background:new 0 0 140 140;" xml:space="preserve">
				<g>
					<path class="dot" d="M83.2,1.2c-1.4-0.3-2.8,0.7-3,2c-0.2,1.4,0.7,2.7,2,2.9c1.3,0.2,2.6-0.6,2.9-1.9C85.6,2.9,84.7,1.5,83.2,1.2z"
						/>
					<path class="dot" d="M95.3,4.7C94,4.2,92.5,4.9,92,6.2c-0.4,1.3,0.2,2.7,1.5,3.2c1.2,0.5,2.7-0.1,3.2-1.4
						C97.3,6.7,96.7,5.2,95.3,4.7z"/>
					<path class="dot" d="M107.4,13.8c0.8-1.1,0.4-2.7-0.8-3.5c-1.2-0.8-2.8-0.3-3.5,0.9c-0.7,1.2-0.3,2.7,0.9,3.4
						C105.1,15.3,106.6,14.9,107.4,13.8z"/>
					<path class="dot" d="M116.7,17.8c-1.1-1-2.7-0.9-3.6,0.2c-0.9,1.1-0.8,2.7,0.2,3.5c1,0.9,2.5,0.9,3.5-0.1
						C117.8,20.4,117.8,18.7,116.7,17.8z"/>
					<path class="dot" d="M125.3,26.9c-0.4-0.6-1.1-0.9-1.7-1c-0.7-0.1-1.3,0.1-1.9,0.6c-0.5,0.4-0.8,1.1-0.9,1.7
						c-0.1,0.6,0.1,1.3,0.5,1.8c0.8,1,2.4,1.3,3.5,0.5C125.9,29.7,126.2,28.1,125.3,26.9z"/>
					<path class="dot" d="M131,40.9c1.3-0.6,1.7-2.1,1.1-3.4c-0.6-1.3-2.3-1.7-3.4-1c-1.2,0.7-1.6,2.2-1,3.4
						C128.2,41,129.8,41.5,131,40.9z"/>
					<path class="dot" d="M132.1,50.6c0.4,1.3,1.7,2,3.1,1.7c1.3-0.4,2.1-1.8,1.7-3.2c-0.5-1.4-1.9-2.1-3.2-1.6
						C132.4,47.9,131.7,49.3,132.1,50.6z"/>
					<path class="dot" d="M134.6,62c0.2,1.3,1.4,2.3,2.8,2.2c1.4-0.1,2.4-1.4,2.2-2.8c-0.2-1.4-1.5-2.4-2.8-2.2
						C135.4,59.4,134.4,60.7,134.6,62z"/>
					<path class="dot" d="M137.6,71.3c-1.4,0-2.5,1.1-2.6,2.4c-0.1,1.3,0.9,2.5,2.3,2.7c1.4,0.1,2.6-0.9,2.7-2.4
						C140.1,72.5,139,71.3,137.6,71.3z"/>
					<path class="dot" d="M136.3,83.3c-1.3-0.3-2.7,0.6-3,1.9c-0.3,1.3,0.5,2.6,1.8,3c1.3,0.4,2.8-0.4,3.1-1.9
						C138.5,84.9,137.7,83.6,136.3,83.3z"/>
					<path class="dot" d="M132.9,94.9c-1.3-0.5-2.7,0.1-3.3,1.3c-0.5,1.2,0,2.7,1.2,3.3c1.3,0.6,2.7,0,3.3-1.3
						C134.8,97,134.1,95.4,132.9,94.9z"/>
					<path class="dot" d="M127.4,105.8c-1.2-0.7-2.7-0.4-3.4,0.7c-0.8,1.1-0.5,2.6,0.6,3.5c1.1,0.8,2.7,0.5,3.5-0.7
						C128.9,108.1,128.6,106.5,127.4,105.8z"/>
					<path class="dot" d="M116.6,115.5c-0.9,1-1,2.5,0,3.5c0.9,1,2.6,1,3.6,0c1-1.1,1-2.7-0.1-3.6C119.1,114.5,117.5,114.6,116.6,115.5z
						"/>
					<path class="dot" d="M107.7,123.1c-1.1,0.8-1.4,2.3-0.6,3.5c0.8,1.2,2.4,1.5,3.5,0.6c1.2-0.8,1.4-2.4,0.6-3.5
						C110.4,122.6,108.8,122.3,107.7,123.1z"/>
					<path class="dot" d="M97.7,129c-1.2,0.6-1.8,2-1.2,3.3s2,1.8,3.4,1.2c1.3-0.6,1.8-2.2,1.2-3.4C100.3,128.9,98.9,128.5,97.7,129z"/>
					<path class="dot" d="M86.7,133c-1.3,0.3-2.1,1.7-1.8,3c0.3,1.3,1.7,2.2,3.1,1.8c1.4-0.4,2.2-1.8,1.8-3.1
						C89.4,133.4,88,132.6,86.7,133z"/>
					<path class="dot" d="M77,135.5c-0.5-0.4-1.1-0.6-1.8-0.6c-1.3,0.1-2.4,1.3-2.3,2.7c0,1.4,1.3,2.5,2.7,2.3c0.7-0.1,1.4-0.4,1.8-0.9
						c0.4-0.5,0.6-1.2,0.5-1.9C77.8,136.5,77.5,135.9,77,135.5z"/>
					<path class="dot" d="M63.5,134.8c-1.3-0.2-2.6,0.8-2.8,2.2c-0.2,1.4,0.8,2.6,2.3,2.8c1.5,0.1,2.7-0.9,2.8-2.3
						C65.9,136.1,64.9,134.9,63.5,134.8z"/>
					<path class="dot" d="M52.1,132.6c-1.3-0.3-2.7,0.4-3.1,1.7c-0.4,1.3,0.3,2.8,1.7,3.1c1.4,0.4,2.8-0.4,3.1-1.8
						C54.1,134.4,53.3,133,52.1,132.6z"/>
					<path class="dot" d="M41.2,128.4c-1.2-0.6-2.7-0.1-3.4,1.1c-0.7,1.2-0.2,2.7,1.1,3.4c1.3,0.7,2.8,0.1,3.4-1.2
						C42.9,130.5,42.4,129,41.2,128.4z"/>
					<path class="dot" d="M31.2,122.3c-1.1-0.8-2.6-0.6-3.5,0.5c-0.9,1.1-0.7,2.7,0.5,3.6c1.2,0.8,2.8,0.6,3.6-0.5
						C32.6,124.7,32.3,123.1,31.2,122.3z"/>
					<path class="dot" d="M19,114.4c-1,0.9-1.2,2.6-0.1,3.6c1,1.1,2.6,1.1,3.6,0.1c1-1,1-2.6,0.1-3.5C21.6,113.6,20.1,113.5,19,114.4z"
						/>
					<path class="dot" d="M13.8,104.3c-0.6-0.1-1.3-0.1-1.9,0.3c-1.2,0.7-1.6,2.3-0.8,3.5c0.8,1.2,2.4,1.5,3.5,0.7
						c1.1-0.8,1.4-2.3,0.7-3.5C15,104.8,14.4,104.4,13.8,104.3z"/>
					<path class="dot" d="M6.7,93.7c-1.3,0.5-1.9,2-1.4,3.3c0.6,1.3,2.1,1.9,3.3,1.3c1.3-0.6,1.8-2,1.3-3.3C9.4,93.8,8,93.2,6.7,93.7z"
						/>
					<path class="dot" d="M6.4,83.9c-0.3-1.3-1.6-2.2-2.9-1.9c-1.4,0.2-2.3,1.6-2,3c0.3,1.4,1.7,2.3,3,1.9C5.9,86.6,6.7,85.2,6.4,83.9z"
						/>
					<path class="dot" d="M5,72.3c0-1.3-1.2-2.4-2.6-2.4c-1.4,0-2.5,1.2-2.5,2.6c0,1.5,1.3,2.5,2.6,2.4C4,74.8,5,73.7,5,72.3z"/>
					<path class="dot" d="M2.8,62.8C4.2,63,5.5,62,5.6,60.7c0.2-1.3-0.7-2.6-2.1-2.9c-1.4-0.3-2.7,0.7-2.9,2.1
						C0.5,61.4,1.5,62.7,2.8,62.8z"/>
					<path class="dot" d="M5.2,50.9c1.3,0.4,2.7-0.4,3.1-1.6c0.4-1.3-0.2-2.7-1.5-3.2c-1.3-0.5-2.8,0.2-3.2,1.6
						C3.1,49.1,3.9,50.6,5.2,50.9z"/>
					<path class="dot" d="M13,38.7c0.7-1.2,0.2-2.7-0.9-3.4s-2.7-0.3-3.5,1c-0.7,1.3-0.2,2.8,1,3.5C10.9,40.3,12.4,39.8,13,38.7z"/>
					<path class="dot" d="M19.6,29c0.8-1,0.7-2.6-0.3-3.5c-1-0.9-2.7-0.8-3.6,0.3c-0.9,1.1-0.7,2.7,0.4,3.6C17.2,30.2,18.7,30,19.6,29z"
						/>
					<path class="dot" d="M28,17.1c-0.8-1.1-2.5-1.3-3.6-0.3c-1.1,0.9-1.2,2.6-0.3,3.6c0.9,1,2.5,1.1,3.5,0.2
						C28.7,19.8,28.9,18.2,28,17.1z"/>
					<path class="dot" d="M38.2,10.5c-0.7-1.2-2.2-1.7-3.5-0.9c-1.3,0.7-1.6,2.3-0.9,3.5c0.8,1.2,2.3,1.5,3.4,0.8
						C38.4,13.2,38.8,11.7,38.2,10.5z"/>
					<path class="dot" d="M49.3,5.7c-0.4-1.3-1.9-2-3.3-1.5c-1.4,0.5-2,2-1.5,3.3c0.5,1.3,2,1.9,3.2,1.4C49,8.5,49.7,7.1,49.3,5.7z"/>
					<path class="dot" d="M61.1,3.1C61,1.7,59.6,0.7,58.2,1c-1.4,0.3-2.4,1.6-2.1,2.9c0.3,1.3,1.6,2.3,2.9,2C60.4,5.7,61.3,4.5,61.1,3.1
						z"/>
					<path class="dot" d="M70.7,0c-1.5-0.1-2.6,1.2-2.6,2.5c0,1.4,1.2,2.4,2.5,2.5c1.3,0,2.5-1.1,2.6-2.4S72.2,0,70.7,0z"/>
				</g>
			</svg>

		</div>
		{{-- <canvas id="next-loader" width="140" height="140"></canvas> --}}
		

		<div class="photo center">

			@if(isset($slides) && count($slides) > 0)
				@if(count($slides) > 1)
					<img src="{{ Stuff::imgUrl(json_decode($slides[1]->img)[0], true) }}">
				@else
					<img src="{{ Stuff::imgUrl(json_decode($slides[0]->img)[0], true) }}">
				@endif
			@endif

			<!-- AQ UNDA CHAVARDES MEORE SLAIDIS FOTO chatvirtvisas -->
		</div>
	</div>
	<div id="top-slider" class="owl-carousel owl-theme">
		{{-- <div class="item">
			<div class="slide_bg">
				<div class="slogan verlagB">
					<h2>ტესტ სათაური</h2>	
				</div>
				<div class="slide_video">
					<div class="vidControls">
						<button class="mute"></button>
						<button class="pause"></button>
					</div>
					<iframe title="YouTube video player" width="100%" height="100%" src="https://www.youtube.com/embed/VnfUV0rdn8w?rel=0&autoplay=1&showinfo=0&iv_load_policy=3&controls=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div> --}}
		@foreach($slides as $slide)
			<div class="item">
				<div class="slide_bg">
					<div class="slogan verlagB">
						<h2>{{ Stuff::trans($slide, 'name') }}</h2>
						@if(!empty($slide->btn_link) > 0)
							<a href="{{ '/'.App::getLocale().$slide->btn_link }}" class="yllwBtn">{{ __('website.main_start_exploring') }}</a>
						@endif
					</div>
					@if($slide->type == 'video')
						<div class="slide_video">
							<div class="vidControls">
								<button class="mute"></button>
								<button class="pause"></button>
							</div>
							<div class="slider-yt-box">
								<?php 
									$youtube_id = explode('/', $slide->youtube_link); 
									$id_length = count($youtube_id)-1;
									$youtube_id = $youtube_id[$id_length];
								?>
								<div class="yt-box" data-id="{{ $youtube_id }}"></div>
							</div>
						</div>
					@else
						<div class="photo">
							<?php $slide_img = json_decode($slide->img)[0]; ?>
							<img src="{{ asset("/files/$slide_img") }}">
						</div>
					@endif
				</div>
			</div>
		@endforeach
	</div>
	<div class="favouritesBtn">
		@if(Session::has('user') && isset($favourite))
			@if($favourite == 1)
				<a href="" class="add active favourite-controller"><span class="txt">{{ __('website.main_add_favourites') }}</span></a>
			@else
				<a href="" class="add favourite-controller"><span class="txt">{{ __('website.main_add_favourites') }}</span></a>
			@endif
			<div class="share">
				<div class="shareBtns">
					<a href="#" class="soc fb"></a>
					<a href="#" class="soc twitter"></a>
					{{-- <a href="#" class="soc vk"></a> --}}
					<a href="#" class="soc in"></a>
					{{-- <a href="#" class="soc gplus"></a> --}}
					{{-- <a href="#" class="soc gmail"></a> --}}
				</div>
			</div>
		@else
			@if(Request::segment(4) == 'memoriesInner')
				<a href="{{ route('loginView', ['lang'=>App::getLocale()]) }}" class="add"><span class="txt">{{ __('website.main_add_favourites') }}</span></a>
				<div class="share">
					<div class="shareBtns">
						<a href="#" class="soc fb"></a>
						<a href="#" class="soc twitter"></a>
						{{-- <a href="#" class="soc vk"></a> --}}
						<a href="#" class="soc in"></a>
						{{-- <a href="#" class="soc gplus"></a> --}}
						{{-- <a href="#" class="soc gmail"></a> --}}
					</div>
				</div>
			@else
				<div class="share pull-left">
					<div class="shareBtns">
						<a href="#" class="soc fb"></a>
						<a href="#" class="soc twitter"></a>
						{{-- <a href="#" class="soc vk"></a> --}}
						<a href="#" class="soc in"></a>
						{{-- <a href="#" class="soc gplus"></a> --}}
						{{-- <a href="#" class="soc gmail"></a> --}}
					</div>
				</div>
			@endif
		@endif

		<div id="share" class="share_clicker" style="display: none;"></div> {{-- Share Button --}}
	</div>
	<div class="attractionBtn ">
		<div class="attr_pass">
			<a href="{{ route('services', ['lang'=>App::getLocale()]) }}" class="inline-block">
				<p>{{ __('website.main_attraction_passes') }}</p>
			</a>
			<a href="{{ route('services', ['lang'=>App::getLocale()]) }}" class="tktBtn"></a>
		</div>
	</div>
</div>