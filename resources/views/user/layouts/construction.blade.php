<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>National Parks</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        *{
            margin:0;
            padding: 0;
        }
        @font-face{
            font-family:BPG_DejaVu;
            src: url("/main/fonts/DejaVuSans.ttf");
        }
        @font-face{
            font-family:CircularStd-Bold;
            src: url("/main/fonts/CircularStd-Bold.otf");
        }
        body{
        /*    overflow-x: hidden;*/
        }
        .background_img{
            width: 100vw;
            height: 100vh;
            min-height: 600px;
            background:#000 url(/main/img/backgroundnat.jpg) no-repeat;
            background-size: cover;
            background-position: center;
            display: flex;
            justify-content: center;
            align-items: center;
            position: relative;
        }
        .primer_content{
            background: url(/main/img/logoo.png) no-repeat;
            background-position: center top;
            max-width: 700px;
            display: flex;
            color:#FFF;
            justify-content: center;
            flex-wrap: wrap;
            padding-top: 332px;
        }
        .primer_content p{
            font:bold 20px BPG_DejaVu;
            text-align: center;
            text-shadow: 0 0 3px rgba(0,0,0,0.5);
        }
        .primer_content a{
            font:normal 25px CircularStd-Bold;
            margin: 50px 0px 0px 0px;
            color:#187947;
            text-decoration: none;
            display: block;
            background: linear-gradient(to right, #8FC640 , #DED71F);
            padding: 10px 20px 5px 20px;
            border-radius: 30px;
        }
        .footer{
            position: absolute;
            bottom: 0;
            font:bold 20px BPG_DejaVu;
            color:#FFF;
            text-shadow: 0 0 3px rgba(0,0,0,0.5);
            margin-bottom: 15px;
        }




        @media screen and (max-width: 558px) {
            .primer_content {
                background-size: 98vw auto;
                padding-top: calc(100vw - 30%);
            }
        }
    </style>
</head>
<body>
    <div class="background_img">
        <div class="primer_content">
            <p>საიტი შემუშავების პროცესშია, გთხოვთ ისარგებლოთ დაცული ტერიტორიების სააგენტოს ვებგვერდით</p>
            <a href="http://apa.gov.ge/ge/eco-tourism">APA.GOV.GE</a>
        </div>
        <div class="footer">
            www.nationalparks.ge
        </div>
    </div>
    <script>
        window.onload = function(){
            setTimeout(function(){
                location.href = "http://apa.gov.ge/ge/eco-tourism";
            },3000);
        }
    </script>
</body>
</html>