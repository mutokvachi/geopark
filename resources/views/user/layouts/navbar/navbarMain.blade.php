<div class="web-container relative top_nav_cont">
	<div class="fixed_bg"></div>
	<nav class="nav_bar t-up t-18-px verlagB">
		@include('user.layouts.navbar.index')
	</nav>
	<div id="search-box">
		<div class="bg"></div>
		@include('user.layouts.navbar.search')
	</div>
</div>