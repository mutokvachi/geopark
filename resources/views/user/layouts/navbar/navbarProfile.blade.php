<div class="gry relative z-1">
	<nav class="nav_bar gray t-up t-18-px verlagB">
		@include('user.layouts.navbar.index')
	</nav>
	<div id="search-box">
		<div class="bg"></div>
		@include('user.layouts.navbar.search')
	</div>
</div>