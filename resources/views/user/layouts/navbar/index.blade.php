<div class="logo">
	<a href="{{ route('index', ['lang'=>App::getLocale()]) }}">
		@if(App::getLocale() == 'ka')
			<img class="res_logo_img" src="{{ asset('/main/img/res-logo-brand.png') }}"></img>
			<img class="res_logo_img_fixed" src="{{ asset('/main/img/res-logo-brand-c.png') }}"></img>
			<img class="logo_img" src="{{ asset('/main/img/ka-logo-brand.png') }}">
			<img class="logo_img_fixed" src="{{ asset('/main/img/ka-logo-brand-c.png') }}">
		@else
			<img class="res_logo_img" src="{{ asset('/main/img/res-logo-brand.png') }}"></img>
			<img class="res_logo_img_fixed" src="{{ asset('/main/img/res-logo-brand-c.png') }}"></img>
			<img class="logo_img" src="{{ asset('/main/img/logo-brand.png') }}">
			<img class="logo_img_fixed" src="{{ asset('/main/img/logo-brand-c.png') }}">
		@endif
	</a>
</div>
<div class="pull-right">
	<div class="hamburger_menu">
		<div class="hamBtn">
			<span class="top-line"></span>
			<span class="midd-line"></span>
			<span class="bott-line"></span>
		</div>
		<div class="searchBtn">
			<span class="o"></span>
			<span class="l"></span>
			<span class="l ll"></span>
		</div>
		<div class="res-clear"></div>
		<ul>
			<li><a href="{{ route('masterParks', ['lang'=>App::getLocale(), 'type'=>"new_dest"]) }}">{{ __('website.new_destinations') }}</a></li>
			<li>
				<a href="{{ route('masterParks', ['lang'=>App::getLocale()]) }}">{{ __('website.national_parks') }}</a>
			</li>
			<li>
				<a href="{{ route('masterGeoAdventures', ['lang'=>App::getLocale()]) }}">{{ __('website.adventure') }}</a>
			</li>
			<li><a href="{{ route('masterTrips', ['lang'=>App::getLocale()]) }}">{{ __('website.plan_your_trip') }}</a></li>
		</ul>
	</div>
</div>