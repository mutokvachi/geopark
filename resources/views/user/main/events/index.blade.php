@extends('user.layouts.master')

@section('head')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/bootstrap-datepicker.standalone.min.css') }}">
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="memories_feed for_ev">
		<div class="p-15-lr p-20-tb">
			<h2 class="blck-title t-blck m-30-b">{{ __('website.events_events') }}</h2>
			<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-5">
					<div id="datepicker"></div>
				</div>
				<div class="col-lg-9 col-md-8 col-sm-7 no-padding">
					@foreach($events as $event)
						<div class="col-lg-6 col-md-12">
							<a href="{{ route('siteEventsInner', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$event->id]) }}">
								<div class="m_cont">
									<div class="photo">
										<?php $ev_img = json_decode($event->image)[0]; ?>
										<img src="{{ Stuff::imgUrl($ev_img, true) }}">
									</div>
									<div class="comment">
										<h4 class="w-title-ex t-30-px">{{ Stuff::trans($event, 'name') }}</h4>
										<div class="autor data p-15-lr">
											<div class="flex-s t-blck p-10-tb">
												<span class="verlagB t-up block w-50-per">{{ __('website.events_date') }}:</span>
												<span class="georgia block w-50-per">{{ $event->date }}</span>
											</div>
											<div class="flex-s t-blck p-10-tb">
												<span class="verlagB t-up block w-50-per">{{ __('website.events_location') }}:</span>
												<span class="georgia block w-50-per">{{ Stuff::trans($event, 'location') }}</span>
											</div>
											<div class="flex-s t-blck p-10-tb">
												<span class="verlagB t-up block w-50-per">{{ __('website.events_price') }}:</span>
												<span class="georgia block w-50-per">
													<span class="from">{{ __('website.events_from') }}</span>
													<span class="num">{{ $event->price }}</span>
													<span class="currency">GEL</span>
												</span>
											</div>
										</div>
									</div>
								</div>
							</a>
						</div>
					@endforeach
				</div>
				<form method="get" class="filter-form" action="{{ route('siteEvents', ['lang'=>App::getLocale(), 'park'=>Request::segment(3)]) }}">
					<input type="hidden" class="search_date" name="date" value="">
				</form>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	<script type="text/javascript" src="{{ asset('/main/js/bootstrap-datepicker.min.js') }}"></script>
@endsection

@section('javascript')
	<script type="text/javascript">
		
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();
			favouriteControll();

			$('.bottom_nav ul li').eq(6).find('a').addClass('active');

		});
		var datepicker = $('#datepicker').datepicker({});
		datepicker.on('changeDate', function(e){
			var date = e.format('yyyy-mm-dd');
			$('.search_date').val(date);
			$('.filter-form').submit();
		});

		
	</script>
@endsection


