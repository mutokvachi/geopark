@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="ev_inner for_ev">
		<div class="web-container p-15-lr m-40-t m-40-b">
			<h2 class="b-title t-blck m-30-b">{{ Stuff::trans($event, 'name') }}</h2>
			<div class="row">
				<div class="col-md-6">
					<div class="details">
						<div class="date">
							<div class="txt verlagB t-up">{{ __('website.events_date') }}:</div>
							<div class="txt georgia">
								<span class="t-cp">{{ $event->date }}</span>
							</div>	
						</div>
						<div class="location">
							<div class="txt verlagB t-up">{{ __('website.events_location') }}:</div>
							<div class="txt georgia">{{ Stuff::trans($event, 'location') }}</div>	
						</div>
						<div class="price">
							<div class="txt verlagB t-up">{{ __('website.events_price') }}:</div>
							<div class="txt georgia">
								<span class="num m-5-r">{{ __('website.events_from') }} {{ $event->price }}</span>
								<span class="currency">GEL</span>
							</div>	
						</div>
					</div>
					<div class="dscrb">
						<p>{!! Stuff::trans($event, 'description') !!}</p>
						{{-- <div class="flex">
							<a href="#" class="yllwBtn block">view website</a>
						</div> --}}
					</div>
				</div>
				<div class="col-md-6">
					<div class="ev_photo photo">
						<?php $ev_img = json_decode($event->image)[0];?>
						<img src="{{ Stuff::imgUrl($ev_img, true) }}">
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();

			$('.bottom_nav ul li').eq(6).find('a').addClass('active');
		});
	</script>
@endsection


