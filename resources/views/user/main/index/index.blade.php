@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
@endsection

@section('content')
	<div class="intro_section gry">
		<div class="col-lg-6 no-padding">
			<?php $my_lng = App::getLocale(); ?>
			<div class="t-right wht">
				<h2 class="title devinne t-40-px t-blck">{{ Config::get("setting.mainpage_$my_lng.title") }}</h2>
				<p class="t-14-px">{{ Config::get("setting.mainpage_$my_lng.description") }}</p>
			</div>
			<div class="adventures">
				<h4 class="title">{{ __('website.main_adventures_title') }}</h4>
				<div class="icons">
					<?php
						$adventure_icons = [
							'ტური პონტონით' => 'adv-1-i',
							'ზიპ ლაინი' => 'adv-2-i',
							'ზიპ-ლაინი' => 'adv-2-i',
							'ყვინთვა' => 'adv-3-i',
							'მღვიმეში ყვინთვა' => 'adv-3-i',
							'კანიონში ყვინთვა' => 'adv-3-i',
							'სანაოსნო ტური' => 'adv-4-i',
							'ტური კატერით' => 'adv-5-i',
							'საპიკნიკე' => 'adv-6-i',
							'სპორტული თევზაობა' => 'adv-7-i',
							'ვიზიტორთა ცენტრი' => 'adv-8-i',
							'კლდეზე ცოცვა' => 'adv-9-i',
							'თოვლის ფეხსაცმლის ტური' => 'adv-10-i',
							'თოვლის ფეხსაცმელების ტური' => 'adv-10-i',
							'ველო ტური' => 'adv-11-i',
							'ტური კაიაკით' => 'adv-12-i',
							'ლაშქრობა' => 'adv-13-i',
							'სანაოსნო ტური' => 'adv-14-i',
							'სპელეო ტური' => 'adv-15-i',
							'ფრინველებზე დაკვირვება' => 'adv-16-i',
							'ტურისტული თავშესაფარი' => 'adv-17-i',
							'საცხენოსნო ტური' => 'adv-18-i',
							'ფრინველებზე დაკვირვების კოშკურა' => 'adv-19-i',
							'საფარი ტური' => 'adv-20-i',
						];
					?>

					@foreach($all_adventures as $key => $current_adventure)
						<?php $icon = json_decode($current_adventure[0]->icon)[0]; ?>
						<?php $adventure_seasons = json_decode($current_adventure[0]->seasons);?>
						<?php
							$my_season = '';
							foreach($adventure_seasons as $key => $adventure_season){
								if($key == 0)
									$my_season = $adventure_season;
								else
									$my_season = $my_season.','.$adventure_season;
							}
							if(isset($adventure_icons[$current_adventure[0]->name])){
								$class = $adventure_icons[$current_adventure[0]->name]; 
							}
							else{
								continue;
							}
						?>
						<a href="#" data-season="{{ $my_season }}" id="{{ $current_adventure[0]->name }}" class="adv-i {{ $class }}" style="background-size: unset;">
							<div class="infoBox">{{ Stuff::trans($current_adventure[0], 'name') }}</div>
						</a>
					@endforeach
					{{-- <a href="#" data-season="summer,spring,autumn" id="pontoni" class="adv-i adv-1-i adventuresAct" style="background-size: unset;">
						<div class="infoBox">ტური პონტონით</div>
					</a>
					<a href="#" data-season="autumn" id="ziplain" class="adv-i adv-2-i adventuresAct">
						<div class="infoBox">ზიპ ლაინი</div>
					</a>
					<a href="#" data-season="summer,spring" id="diving" class="adv-i adv-3-i adventuresAct">
						<div class="infoBox">ყვინთვა</div>
					</a>
					<a href="#" data-season="autumn" id="boattour" class="adv-i adv-4-i adventuresAct">
						<div class="infoBox">სანაოსნო ტური</div>
					</a>
					<a href="#" data-season="summer,spring,autumn,winter" id="kateri" class="adv-i adv-5-i adventuresAct">
						<div class="infoBox">ტური კატერით</div>
					</a>
					<a href="#" data-season="summer,spring" id="piknik" class="adv-i adv-6-i adventuresAct">
						<div class="infoBox">საპიკნიკე</div>
					</a>
					<a href="#" data-season="autumn" id="fishing" class="adv-i adv-7-i adventuresAct">
						<div class="infoBox">სპორტული თევზაობა</div>
					</a>
					<a href="#" data-season="summer,spring,autumn" id="center" class="adv-i adv-8-i adventuresAct">
						<div class="infoBox">ვიზიტორთა ცენტრი</div>
					</a>
					<a href="#" data-season="summer,spring,autumn,winter" id="climbing" class="adv-i adv-9-i adventuresAct">
						<div class="infoBox">კლდეზე ცოცვა</div>
					</a>
					<a href="#" data-season="spring,autumn" id="snow" class="adv-i adv-10-i adventuresAct">
						<div class="infoBox">თოვლის ფეხსაცმლის ტური</div>
					</a>
					<a href="#" data-season="summer,spring,autumn,winter" id="biking" class="adv-i adv-11-i adventuresAct">
						<div class="infoBox">ველო ტური</div>
					</a>
					<a href="#" data-season="summer,spring,autumn" id="kayking" class="adv-i adv-12-i adventuresAct">
						<div class="infoBox">ტური კაიაკით</div>
					</a>
					<a href="#" data-season="autumn" id="hiking" class="adv-i adv-13-i adventuresAct">
						<div class="infoBox">ლაშქრობა</div>
					</a>
					<a href="#" data-season="summer,spring,autumn" id="boating" class="adv-i adv-14-i adventuresAct">
						<div class="infoBox">სანაოსნო ტური</div>
					</a>
					<a href="#" data-season="summer,spring,autumn" id="speleo" class="adv-i adv-15-i adventuresAct">
						<div class="infoBox">სპელეო ტური</div>
					</a>
					<a href="#" data-season="summer,autumn" id="bird" class="adv-i adv-16-i adventuresAct">
						<div class="infoBox">ფრინველებზე დაკვირვება</div>
					</a>
					<a href="#" data-season="summer,spring,autumn" id="shelter" class="adv-i adv-17-i adventuresAct">
						<div class="infoBox">ტურისტული თავშესაფარი</div>
					</a>
					<a href="#" data-season="spring,autumn" id="hoerse" class="adv-i adv-18-i adventuresAct">
						<div class="infoBox">საცხენოსნო ტური</div>
					</a>
					<a href="#" data-season="summer,spring,autumn" id="tower" class="adv-i adv-19-i adventuresAct">
						<div class="infoBox">ფრინველებზე დაკვირვების კოშკურა</div>
					</a>
					<a href="#" data-season="autumn,winter" id="suv" class="adv-i adv-20-i adventuresAct">
						<div class="infoBox">საფარი ტური</div>
					</a>  --}}
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="seasons">
				<h4 class="title">{{ __('website.main_mainpage_seasons') }}</h4>
				<div class="icons">
					<a href="#" id="spring" class="season-i season-4-i"></a>
					<a href="#" id="summer" class="season-i season-1-i"></a>
					<a href="#" id="autumn" class="season-i season-2-i"></a>
					<a href="#" id="winter" class="season-i season-3-i"></a>
					<div class="clearfix"></div>
				</div>
				<button onclick="defaultAll()">{{ __('website.default_button') }}</button>
			</div>
		</div>
		<div class="col-lg-6 no-padding">
			<div class="adventures-map">
				<div class="height_range">
					<div class="box">
						<div class="dots">
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
						</div>
					</div>
				</div>
				<div class="sortBy">
					<div class="sortBtn">
						<div class="sort_label sort_placeholder">{{ __('website.main_sort_region') }}</div>
						<ul class="sort_list sort-by-region">
							<li data-href="all"><span>{{ __('website.trip_clear_filter') }}</span></li>
							@foreach($regions as $current_region)
								<li data-href="{{ $current_region->id }}"><span>{{ Stuff::trans($current_region, 'name') }}</span></li>
							@endforeach
						</ul>
					</div>
					<div class="sortBtn">
						<div class="sort_label sort_placeholder">{{ __('website.main_sort_park') }}</div>
						<ul class="sort_list sort-by-parks">
							<li data-href="all" >
								<span>
									{{ __('website.trip_clear_filter') }}
								</span>
							</li>
							@foreach($all_parks as $current)
								{{-- <a href="{{ route('siteParksInner', ['lang'=>App::getLocale(), 'park'=>$current->alias]) }}"> --}}
									<li data-href="{{ Stuff::trans($current, 'name') }}" >
										<span>
											{{ Stuff::trans($current, 'name') }}
										</span>
									</li>
								{{-- </a> --}}
							@endforeach
						</ul>
					</div>
					<div class="sortBtn">
						<div class="sort_label sort_placeholder">{{ __('website.main_sort_activity') }}</div>
						<ul class="sort_list sort-by-adventure">
							<li data-href="all">
								<span>{{ __('website.trip_clear_filter') }}</span>
							</li>
							@foreach($all_adventures as $key => $my_adventure)
								@if(isset($adventure_icons[$my_adventure[0]->name]))
									<li data-href="{{ $key }}">
										<span>{{ Stuff::trans($my_adventure[0], 'name') }}</span>
									</li>
								@endif
							@endforeach
						</ul>
					</div>
				</div>
				<!-- <div class="map" data-lat="42.3173294" data-lng="42.2338442"></div> -->

				{{-- [
				{"height":100,"position":{"lat":41.73201210104606,"lng":44.30832229872283}},
				{"height":123,"position":{"lat":41.53201210104606,"lng":44.30832229872283}},
				{"height":1244,"position":{"lat":41.33201210104606,"lng":44.30832229872283}},
				{"height":1000,"position":{"lat":41.23201210104606,"lng":44.30832229872283}},
				{"height":2550,"position":{"lat":41.13201210104606,"lng":44.30832229872283}}
				] --}}

				<div class="map hu" data-json='{{ json_encode($json) }}' 

				data-catJson='{{ json_encode($categoryJson) }}'>
				<div class="parks_json_map" style="display: none;">{{ json_encode($advJson) }}</div>
				<script type="text/javascript">
					var parksJson = document.querySelector('.parks_json_map').innerHTML;
					var mainJson = JSON.parse(parksJson);


					// [
					// 	{ position: [{lat:41.672911819602085,lng:404.82971191406256}], title: 'ალგეთის ეროვნული პარკი',adventures: 'txilamuri,curva,tevzaoba', regionID: 3 },
					// 	{ position: [{lat:41.42625319507272,lng:406.12609863281256}], title: 'ბორჯომ-ხარაგაულის ეროვნული პარკი',adventures: 'xtunva,cekva,tevzaoba', regionID: 4 },
					// 	{ position: [{lat:41.83682786072714,lng:402.4237060546875}], title: 'თუშეთის ეროვნული პარკი',adventures: 'sma,qeifi,tevzaoba', regionID: 7 }
					// ];

				</script>


				</div>
			</div>
		</div>
	</div>

	<div class="adventure-nav drk-wht main_adventures_nav">
		<div class="no-padding my-adventure-cont">
			<div class="bg">
				<span class="y-title absolute">{{ __('website.adventures_banner_adventure') }}</span>
				<img src="/main/img/adventures-bg.jpg">
			</div>
			@foreach($adventures as $key => $my_adventure)
				@if($key == 0)
					<div class="dscrb active" data-show="{{ $my_adventure->id }}">
				@else
					<div class="dscrb" data-show="{{ $my_adventure->id }}">
				@endif
					<div class="coordinates_input" style="display: none;">{{ $my_adventure->coordinates }}</div>
					<div class="article">
						<h2 class="b-title">{{ Stuff::trans($my_adventure, 'name') }}</h2>
						<p class="t-16-px">{!! Stuff::trans($my_adventure, 'description') !!}</p>
					</div>
					<div class="avail">
						<h4 class="grn-title">{{ __('website.adventures_available_in') }}</h4>
						<div class="seasons">
							<?php $seasons = json_decode($my_adventure->seasons); ?>
							@foreach($seasons as $season)
								<span class="{{ $season }}">{{ __("website.adventures_$season") }}</span>
							@endforeach
						</div>
						<div>
							{{-- <a href="#" class="pull-left m-20-t t-blck yllwBtn block">available trips</a> --}}
							<div class="clearfix"></div>
						</div>
						@if(count($my_adventure->imgs) > 0)
							<div class="sld_cont relative">
								<div class="l_arrw"></div>
								<div class="r_arrw"></div>
								<div class="photos owl-carousel owl-theme">
									@foreach($my_adventure->imgs as $adv_image)
										<div class="photo item">
											<div class="center aLink">
												@if(!empty($adv_image->link))
													<?php $imgLink = '/'.App::getLocale().$adv_image->link; ?>
													<a href="{{ $imgLink or '#!'}}">
														<h3>{{ Stuff::trans($adv_image, 'name') }}</h3>
													</a>
												@else
													<a class="preventDefault">
														<h3>{{ Stuff::trans($adv_image, 'name') }}</h3>
													</a>
												@endif
											</div>
											<?php $advImgUrl = json_decode($adv_image->img)[0]; ?>
											<img src="{{ asset("/files/$advImgUrl") }}">
										</div>
									@endforeach
								</div>
							</div>
						@endif
					</div>
				</div>
			@endforeach
			<div class="adv-nav devinne">
				<div class="resBtn">
					<span class="top-line"></span>
					<span class="midd-line"></span>
					<span class="ex-midd-line"></span>
					<span class="bott-line"></span>
				</div>
				<ul>
					@foreach($adventures as $adventure)
						<li data-show="{{ $adventure->id }}">{{ Stuff::trans($adventure, 'name') }}</li>
					@endforeach
					{{--<li data-show="hiking">hiking</li>
					<li data-show="mountain">mountain climbing</li>--}}
				</ul>
			</div>
		</div>
	</div>

	<div class="planning_section blck">
		<h2 class="y-title">{{ __('website.parkInner_start_planning') }}</h2>
		<div id="planning-slider" class="owl-carousel owl-theme">
			@foreach($recommends as $recommend)
				<div class="item">
					<div class="dscrb_cont">
						<h2 class="w-title">{{ Stuff::trans($recommend, 'name') }}</h2>
						<div class="overflower">
							<p class="t-wht t-16-px">{!! Stuff::trans($recommend, 'description') !!}</p>
						</div>
						<a href="{{ route('siteRecommendInnerView', ['lang'=>App::getLocale(), 'park'=>$recommend->park->alias, 'id'=>$recommend->id]) }}" class="yllwBtn">{{ __('website.parkInner_read_more') }}</a>
					</div>
					<div class="photo">
						<img src="{{ Stuff::imgUrl(json_decode($recommend->img)[0], true) }}">
					</div>
					<div class="clearfix"></div>
				</div>
			@endforeach
		</div>
		<div class="slide-loader" data-owl="#planning-slider">
			<span class="title">{{ __('website.parkInner_recommends') }}</span>
			<div class="bullets">
				@foreach($recommends as $key => $recommend)	
					@if($key == 0)
						<span class="crcl active">
					@else
						<span class="crcl active">
					@endif
						<span class="center">{{ $key+1 }}</span>
						<span class="half">
							<span class="border clipped"></span>
						</span>
						<span class="border fixed"></span>
					</span>
				@endforeach
			</div>
			{{-- <a href="#" class="devinne">View All</a> --}}
		</div>
	</div>

	<div class="memories">
		<h2 class="memories-title ex-w-title">{{ __('website.siteMemories_memories') }}</h2>
		<div id="memories-slider" class="owl-carousel owl-theme">
			@foreach($memories as $memory)
				<div class="item">
					<div class="bg photo">
						<div class="autor gry">
							<div class="user_avatar t-up t-wht verlagB t-16-px">
								<div class="photo">
									<?php $avatar = $memory->user->img; ?>
									<img src="{{ asset("/main/img/$avatar") }}">
								</div>
								<p>{{ $memory->user->name }}</p>
							</div>
						</div>
						<div class="absolute">
							<h2 class="w-title ">
								<a href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>$memory->park->alias, 'id'=>$memory->id]) }}" style="color: inherit;">
									{{ $memory->title }}
								</a>
							</h2>
							@if($memory->type == "blog")
								<div class="text-cont">
									<p class="t-wht t-16-px devinne">{!! $memory->extra !!}</p>
								</div>
								<a class="_seeMore" href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>$memory->park->alias, 'id'=>$memory->id]) }}">{{ __('website.master_read_more') }}</a>
							@endif
						</div>
						<?php $bg = json_decode($memory->images)[0]; ?>
						<img src="{{ asset("/main/img/$bg") }}">
					</div>
				</div>
			@endforeach
		</div>
		<div class="slide-loader" data-owl="#memories-slider">
			<span class="title">{{ __('website.siteMemories_featured_memories') }}</span>
			<div class="bullets">
				@for($i = 1; $i <= count($memories); $i++)	
					<span class="crcl active">
						<span class="center">{{ $i }}</span>
						<span class="half">
							<span class="border clipped"></span>
						</span>
						<span class="border fixed"></span>
					</span>
				@endfor
			</div>
			{{-- <a href="{{ route('siteMemoriesView', ['lang'=>App::getLocale(), 'park'=>Request::segment(3)]) }}" class="devinne">{{ __('website.siteMemories_view_all') }}</a> --}}
		</div>
	</div>
	<div class="coors-center" style="display: none;">{{ json_encode($coors['center']) }}</div>
  	<div class="coors-pins" style="display: none;">{{ json_encode($coors['pins']) }}</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek"></script>
@endsection

@section('javascript')
	<script type="text/javascript">
		
		function initFlightsMap(){
			$('.footer-lower-map').html(`
				<div class="index_bott_map">
					<div class="map"></div>
				</div>
			`);
		}
		var seasFunc;
		$(function(){
			initFlightsMap();
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();

			sortDropdown();
			adventureShow();
			adventureNav();
			$('.favouritesBtn').remove();
			$('.attractionBtn').addClass('_ex');
			initMap();
			seasFunc = new seasonMarkers();
		});

		function _(selector){
			return document.querySelectorAll(selector);
		}

		var mobileMapZoom;
		if($(document).width() < 500)
			mobileMapZoom = 6;
		else
			mobileMapZoom = 7;


		function initFlightsGoogleMap(){
			var infoW = null;
			function addListenerToMarker(marker, infowindow, map){
				marker.addListener('click', function(){
					if( infoW != null ){
						infoW.close();
					}
					infowindow.open(map, marker);
					infoW = infowindow;
				});
			}
			var element = $('.index_bott_map .map').get()[0];
			var tbilisi = JSON.parse($('.coors-center').html());
  			var pins = JSON.parse($('.coors-pins').html());
			
			var map = new google.maps.Map(element,{
				zoom: 4,
		        center: tbilisi,
		        mapTypeId: google.maps.MapTypeId.HYBRID,
			});
			var markers = [];
			var marker;
			for(var i in pins){
				var contentString = pins[i].info;
				var div = document.createElement('div');
				div.setAttribute('class','flightInfoWindow');
				div.innerHTML = contentString;
				var infowindow = new google.maps.InfoWindow({
					content: div
		        });
				marker = new google.maps.Marker({
					position : pins[i],
					map:map,
					icon: '/main/img/map-pin-by.png',
				});
				addListenerToMarker(marker, infowindow, map);
				markers.push({
					marker:marker,
					type: "other"
				});
			}
			var tbilisiMarker = new google.maps.Marker({
				position:tbilisi,
				map:map,
				icon: '/main/img/tb-pin.png',	
			});

			var geodesicPolies = [];

			function update(){
				for(var i in markers){
					geodesicPolies.push(new google.maps.Polyline({
					    strokeColor: '#fcde13',
					    strokeOpacity: 1.0,
					    strokeWeight: 3,
					    geodesic: true,
					    map: map
					}));
					var path = [markers[i].marker.getPosition(), tbilisiMarker.getPosition()];
					geodesicPolies[i].setPath(path);
					var heading = google.maps.geometry.spherical.computeHeading(path[0], path[1]);
					var distance = google.maps.geometry.spherical.computeDistanceBetween(path[0], path[1]);
				}
			}
			update();
		}
		$('.height_range .box .dots .dot').hover(function(){
			var $dotDivs = $('.adventures-map .dot');
			var thisIndex = $(this).index() + 1;
			var dotNum = $dotDivs.length;
			var heightMax = 3000;
			// var thisHeight = marker.mountHeight;
			// var seasons = JSON.parse(marker.seasons);
			// var activeDotIndex = dotNum - Math.floor(thisHeight/heightMax * dotNum);
			var activeDotIndex = heightMax - Math.floor((heightMax / dotNum) * thisIndex);
			$('.height_range .box .dots .dot').removeClass('active');
			$(this).addClass('active');
			$(this).html(`<div class="val"><span class="number">`+activeDotIndex+`</span><span class="unit">meters</span></div>`);
		});
		$('.height_range .box .dots .dot').click(function(){
			var $dotDivs = $('.adventures-map .dot');
			var thisIndex = $(this).index() + 1;
			var dotNum = $dotDivs.length;
			var heightMax = 3000;
			// var thisHeight = marker.mountHeight;
			// var seasons = JSON.parse(marker.seasons);
			// var activeDotIndex = dotNum - Math.floor(thisHeight/heightMax * dotNum);
			var activeDotIndex = heightMax - Math.floor((heightMax / dotNum) * thisIndex);
			multipleHeightMarkerMapHeight(document.querySelector('.adventures-map .map'),activeDotIndex);
			$('.height_range .box .dots .dot').removeClass('active');
			$(this).addClass('active');
			$(this).html(`<div class="val"><span class="number">`+activeDotIndex+`</span><span class="unit">meters</span></div>`);
		});

		function setMarkerListener(marker){
			marker.addListener('click', function(){
				var $dotDivs = $('.adventures-map .dot');
				var dotNum = $dotDivs.length;
				var heightMax = 3000;
				var thisHeight = marker.mountHeight;
				var seasons = JSON.parse(marker.seasons);

				var activeDotIndex = dotNum - Math.floor(thisHeight/heightMax * dotNum);

		        $dotDivs.removeClass('active');
		        $activeDotDiv = $dotDivs.eq(activeDotIndex);
		        $activeDotDiv.addClass('active');
	            $activeDotDiv.html(`<div class="val"><span class="number">`+marker.mountHeight+`</span><span class="unit">meters</span></div>`);




		        var id = marker.adventures;

		        $('.intro_section .adventures .icons > a').removeClass('adventuresAct');

		        $('.intro_section .adventures .icons > a[id="'+id+'"]').addClass('adventuresAct');


				$('.icons .season-i').removeClass('active');
		        for( var i = 0; i < seasons.length; i++ ){
		        	for( var a = 0; a < $('.icons .season-i').length; a ++){
		        		if( seasons[i] === $('.icons .season-i').eq(a).attr('id') ){
		        			
		        			$('.icons .season-i').eq(a).addClass('active');
		        		}
		        	}
		        }

			});
		}

		function multipleHeightMarkerMap(element){
			var infoW = null;
			function addListenerToMarker(marker, infowindow, map){
				marker.addListener('click', function(){
					if( infoW != null ){
						infoW.close();
					}
					infowindow.open(map, marker);
					infoW = infowindow;
				});
			}

			var markerAttrs = JSON.parse(element.getAttribute('data-json'));
			// here goes mobile zoom
			var map = new google.maps.Map(element,{
				zoom: mobileMapZoom,
		        center: {lat: 42.17968819665963, lng: 403.33557128906256},
		        mapTypeId: 'roadmap'
			});
			var markers = [];
			
			var icon = "/main/img/map-pin-by.png";
			var ocup = [
			{"position":{"lat":43.17113228474479,"lng":40.43243408203125},"title":"{{ __('website.occupied_bichvinta') }}","adventures":"","regionID":"9","park_alias":"bichvintamiuserisnakrdzali","occupation":"1"},
			{"position":{"lat":42.377054331358856,"lng":44.25264042355741},"title":"{{ __('website.occupied_liaxvi') }}","adventures":"","regionID":"10","park_alias":"liakhvisnakrdzali","occupation":"1"},
			{"position":{"lat":43.4459400050658,"lng":40.527191162109375},"title":"{{ __('website.occupied_ritsa') }}","adventures":"","regionID":"9","park_alias":"ritsisnakrdzali","occupation":"1"},
			{"position":{"lat":43.34715236003302,"lng":41.0064697265625},"title":"{{ __('website.occupied_gumismta') }}","adventures":"","regionID":"9","park_alias":"gumismta","occupation":"1"}
			];
			

			mainJson = mainJson.concat(ocup);

			for(var i = 0; i<mainJson.length; i++){
				if( mainJson[i].occupation == "1" ){
					icon = "/main/img/ocup-by-russia.svg";
				}else{
					icon = "/main/img/map-pin-by.png";
				}
				var marker = new google.maps.Marker({
					position : mainJson[i].position,
					map:map,
					icon: icon,
					// icon: markerAttrs[i].ico,
					
				});
				var box = document.createElement('div');
				box.setAttribute('class',"parkInfoWindow");
				var h3 = document.createElement('a');
				var href_link = "/{{ App::getLocale() }}/site/"+mainJson[i].park_alias;
				if(mainJson[i].occupation == "1"){
					href_link = "#!";
				}
				h3.setAttribute('href', href_link);
				h3.style.fontSize = '18px';
				h3.style.color = '#333';
				h3.innerText = mainJson[i].title;

				var ul = document.createElement('ul');
				var advent =  mainJson[i].adventures.split(',');

				for( var u = 0; u < advent.length; u++ ){
					var li = document.createElement('li');
					li.innerText = advent[u];
					ul.prepend(li);
				}

				box.prepend(h3);
				box.appendChild(ul);

				var infowindow = new google.maps.InfoWindow({
    				content:box
    			});
				addListenerToMarker(marker, infowindow, map);
				markers.push(marker);
			}
			
		}

		function multipleHeightMarkerMapHeight(element,height){
			var minHeight = height - 200;
			var maxHeight = height + 200;
			var infoW = null;
			function addListenerToMarker(marker, infowindow, map){
				marker.addListener('click', function(){
					if( infoW != null ){
						infoW.close();
					}
					infowindow.open(map, marker);
					infoW = infowindow;
				});
			}
			var markerAttrs = JSON.parse(element.getAttribute('data-json'));
			// here goes mobile zoom
			var map = new google.maps.Map(element,{
				zoom: mobileMapZoom,
		        center: {lat:42.105628, lng:43.593089},
		        mapTypeId: 'roadmap'
			});
			var markers = [];
			var latlngbounds = new google.maps.LatLngBounds();
			for(var i = 0; i<markerAttrs.length; i++){
				if( (markerAttrs[i].height >= minHeight) && (markerAttrs[i].height <= maxHeight) ){
				var marker = new google.maps.Marker({
					position : JSON.parse(markerAttrs[i].position)[0],
					map:map,
					icon: '/main/img/map-pin-by.png',
					// icon: markerAttrs[i].ico,
					mountHeight: JSON.parse(markerAttrs[i].height),
					adventures: markerAttrs[i].adventure_id,
					seasons: markerAttrs[i].seasons
				});
				var infowindow = new google.maps.InfoWindow({
    				content:markerAttrs[i].name
    			});
				addListenerToMarker(marker, infowindow, map);
				markers.push(marker);
			}
			}

			for(var i = 0; i< markers.length; i++){
				setMarkerListener(markers[i]);
			}
		}
		function multipleHeightMarkerMapClick(element,id){
			var infoW = null;
			function addListenerToMarker(marker, infowindow, map){
				marker.addListener('click', function(){
					if( infoW != null ){
						infoW.close();
					}
					infowindow.open(map, marker);
					infoW = infowindow;
				});
			}
			var markerAttrs = JSON.parse(element.getAttribute('data-catJson'));

			var allmarkers = [];

			for( var k = 0; k < id.length; k++ ){
				for( var c = 0; c < markerAttrs[id[k]].length; c++ ){
					allmarkers.push(markerAttrs[id[k]][c]);
				}
			}

			
			// if(typeof  markerAttrs[id] == 'undefined'){
			// 	return false;
			// }

			// here goes mobile zoom
			var map = new google.maps.Map(element,{
				zoom: mobileMapZoom,
		        // center: JSON.parse(markerAttrs[id][0].position)[0],
		        center: {lat: 42.264959, lng: 43.500335},
		        mapTypeId: 'roadmap'
			});
			var markers = [];
			var latlngbounds = new google.maps.LatLngBounds();
			for(var i = 0; i<allmarkers.length; i++){
				var marker = new google.maps.Marker({
					position : JSON.parse(allmarkers[i].position)[0],
					map:map,
					icon: '/main/img/map-pin-by.png',
					// icon: markerAttrs[i].ico,
					mountHeight: JSON.parse(allmarkers[i].height),
					adventures: allmarkers[i].adventure_id,
					seasons: allmarkers[i].seasons
				});
				var myLang = '{{ App::getLocale() }}'; 
				var infowindow = new google.maps.InfoWindow({
    				content:'<a href="/'+myLang+'/site/'+allmarkers[i].park_alias+'/adventuresInner/'+allmarkers[i].redirect_id+'" style="color: #222 !important;" class="parkInfoWindow">'+allmarkers[i].name+'</a>'
    			});
				addListenerToMarker(marker, infowindow, map);
			    latlngbounds.extend(marker.getPosition());
				markers.push(marker);
			}

			for(var i = 0; i< markers.length; i++){
				setMarkerListener(markers[i]);
			}
			// map.fitBounds(latlngbounds);
		}


		function initMap() {
			var element = document.querySelector('.adventures-map .map');
			multipleHeightMarkerMap(element);

    		$('.index_bott_map .map').gMap(function(element, lat, lng){
    			lat = parseInt(lat);
    			lng = parseInt(lng)
    			var position = {lat: lat, lng: lng};
    			var map = new google.maps.Map(element,{
    				zoom: 7,
			        center: position,
			        scrollwheel: true,
			        gestureHandling: 'greedy',
			        mapTypeControl:false,
			        fullscreenControl: false,
			        mapTypeId: 'satellite'
    			});
    		});

    		initFlightsGoogleMap();
		}

		var adventArray = [];

		$('.intro_section .adventures .icons > a').on('click',function(ev){
			ev.preventDefault();
			var id = $(this).attr('id');

			// $('.intro_section .adventures .icons > a').removeClass('adventuresAct');
			if( $(this).hasClass('adventuresAct') ){
				$(this).removeClass('adventuresAct');
				for( var i = 0; i < adventArray.length; i++ ){
					if( id == adventArray[i] ){
						adventArray.splice(i,1);
					}
				}
			}else{
				adventArray.push(id);
				$(this).addClass('adventuresAct');
			}
			multipleHeightMarkerMapClick(document.querySelector('.adventures-map .map'),adventArray);

		});
		function seasonMarkers(){
			this.map = _('.adventures-map .map')[0];
			this.json = JSON.parse(this.map.getAttribute('data-json'));
			this.jsonLength = this.json.length;

			this.seasonFilter = function(seasonName){
				var infoW = null;
				function addListenerToMarker(marker, infowindow, map){
					marker.addListener('click', function(){
						if( infoW != null ){
							infoW.close();
						}
						infowindow.open(map, marker);
						infoW = infowindow;
					});
				}
				
				

				var map = new google.maps.Map(this.map,{
					zoom: 7,
			        center: {lat: 42.264959, lng: 43.500335},
			        mapTypeId: 'roadmap'
				});
				var nmn;
				var markers = [];
				for( var n = 0; n < this.jsonLength; n++ ){
					nmn = JSON.parse(this.json[n].seasons);
					for( var m = 0; m < nmn.length; m++ ){
						if( nmn[m] == seasonName ){
							
							var marker = new google.maps.Marker({
								position : JSON.parse(this.json[n].position)[0],
								map:map,
								icon: '/main/img/map-pin-by.png',
								mountHeight: this.json[n].height,
								adventures: this.json[n].adventure_id,
								seasons: this.json[n].seasons
							});
							var infowindow = new google.maps.InfoWindow({
								content:this.json[n].name
							});
							addListenerToMarker(marker, infowindow, map);
							markers.push(marker);

						}
					}
				}
				



				for(var i = 0; i< markers.length; i++){
					setMarkerListener(markers[i]);
				}
			}
			
		}

$('.icons .season-i').click(function(ev){
		ev.preventDefault();



	var JSON_Parse = {
	    length: 0,
	    season: [],
	    where: 'data-season',
	    object: '',
	    process: function(data){
	    	this.object = data;
	        this.length = data.length;
	        
	        for(var i = 0; i < this.length; i++){
	            this.season.push(data[i].getAttribute('data-season'));
	        }
	    },
	    research: function(element){
	        var regex = new RegExp( element, 'g' );
	        $('.intro_section .adventures .icons > a').removeClass('adventuresAct');
	        for( var i = 0; i < this.length; i++ ){
	            if( this.season[i].match(regex) != null ){
	                $('.intro_section .adventures .icons > a').eq(i).addClass('adventuresAct');
	            }
	        }
	    }
	}
	
	seasFunc.seasonFilter($(this).attr('id'));

	JSON_Parse.process(document.querySelectorAll('.intro_section .adventures .icons > a'));
	JSON_Parse.research($(this).attr('id'));

	});

	$('.sort-by-region li').click(function(e){
		e.preventDefault();
		var linkTo = $(this).attr('data-href');
		if( linkTo === 'all' ){
			$('button[onclick="defaultAll()"]').trigger('click');
			return false;
		}
		var element = document.querySelector('.adventures-map .map');
		var infoW = null;
		function addListenerToMarker(marker, infowindow, map){
			marker.addListener('click', function(){
				if( infoW != null ){
					infoW.close();
				}
				infowindow.open(map, marker);
				infoW = infowindow;
			});
		}


		var map = new google.maps.Map(element,{
			zoom: mobileMapZoom,
	        center: {lat: 42.17968819665963, lng: 403.33557128906256},
	        mapTypeId: 'roadmap'
		});

		for( var i = 0; i < mainJson.length; i++ ){

			if( mainJson[i].regionID == linkTo ){

			
			var markers = [];
			
				var marker = new google.maps.Marker({
					position : mainJson[i].position,
					map:map,
					icon: '/main/img/map-pin-by.png',
					// icon: markerAttrs[i].ico,
					
				});
				var box = document.createElement('div');
				box.setAttribute('class',"parkInfoWindow");
				var h3 = document.createElement('h3');
				h3.innerText = mainJson[i].title;

				var ul = document.createElement('ul');

				var advent =  mainJson[i].adventures.split(',');

				for( var u = 0; u < advent.length; u++ ){
					var li = document.createElement('li');
					li.innerText = advent[u];
					ul.prepend(li);
				}

				box.prepend(h3);
				box.appendChild(ul);

				var infowindow = new google.maps.InfoWindow({
    				content:box
    			});
				addListenerToMarker(marker, infowindow, map);
				markers.push(marker);
			



			}
		}
	});



	$('.sort-by-parks li').click(function(e){
		e.preventDefault();
		var linkTo = $(this).attr('data-href');
		if( linkTo === 'all' ){
			$('button[onclick="defaultAll()"]').trigger('click');
			return false;
		}
		var element = document.querySelector('.adventures-map .map');

		for( var i = 0; i < mainJson.length; i++ ){
			if( mainJson[i].title == linkTo ){
			var infoW = null;
			function addListenerToMarker(marker, infowindow, map){
				marker.addListener('click', function(){
					if( infoW != null ){
						infoW.close();
					}
					infowindow.open(map, marker);
					infoW = infowindow;
				});
			}


			var map = new google.maps.Map(element,{
				zoom: mobileMapZoom,
		        center: {lat: 42.17968819665963, lng: 403.33557128906256},
		        mapTypeId: 'roadmap'
			});
			var markers = [];
			
				var marker = new google.maps.Marker({
					position : mainJson[i].position,
					map:map,
					icon: '/main/img/map-pin-by.png',
					// icon: markerAttrs[i].ico,
					
				});
				var box = document.createElement('div');
				box.setAttribute('class',"parkInfoWindow");
				var h3 = document.createElement('h3');
				h3.innerText = mainJson[i].title;

				var ul = document.createElement('ul');

				var advent =  mainJson[i].adventures.split(',');

				for( var u = 0; u < advent.length; u++ ){
					var li = document.createElement('li');
					li.innerText = advent[u];
					ul.prepend(li);
				}

				box.prepend(h3);
				box.appendChild(ul);

				var infowindow = new google.maps.InfoWindow({
    				content:box
    			});
				addListenerToMarker(marker, infowindow, map);
				markers.push(marker);
			
			}
		}
	});

	$('.sort-by-adventure li').click(function(){
		var linkTo = $(this).attr('data-href');
		if( linkTo === 'all' ){
			$('button[onclick="defaultAll()"]').trigger('click');
			return false;
		}
		
		$('.intro_section .adventures .icons > a').each(function(){

			if( $(this).attr('id') == linkTo ){
				$(this).trigger('click');
			}
		});
	});


	function defaultAll(){
		initMap();
		$('.intro_section .adventures .icons > a').removeClass('adventuresAct');
		$('.height_range .box .dots .dot').removeClass('active');
	}
	$('body').on('click','.preventDefault',function(ev){
		ev.preventDefault();
	});

	function removeArrow(){
		setTimeout(function(){
			var length = $('.my-adventure-cont .dscrb.active .owl-item').length;

			if(length > 3){
				$('.my-adventure-cont .dscrb.active .l_arrw').show();
				$('.my-adventure-cont .dscrb.active .r_arrw').show();
			}
			else{
				$('.my-adventure-cont .dscrb.active .l_arrw').hide();
				$('.my-adventure-cont .dscrb.active .r_arrw').hide();
			}

		}, 200);
	}
	function arrowController(){
		
		removeArrow();

		$('.adv-nav li').click(function(){
			removeArrow();
		});
	}

	arrowController();

	</script>
@endsection


