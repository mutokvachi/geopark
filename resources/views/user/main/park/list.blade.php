@extends('user.layouts.master')

@section('head')
	@parent
	<style type="text/css">
		@if($page == 'new_dest')
			.hamburger_menu ul li:nth-child(1) a{
				color: #ffce00;
			}
		@else
			.hamburger_menu ul li:nth-child(2) a{
				color: #ffce00;
			}
		@endif
	</style>
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
@endsection

@section('content')
	<div class="t-ex-sort blck">
		<div class="web-container">
			<div class="sorts">
				<div class="sortBtn">
					<div class="sort_label sort_placeholder" id="alphabet-sort">{{ __('website.park_sor_alphabet') }}</div>
				</div>
				<div class="sortBtn">
					<div class="sort_label sort_placeholder">{{ __('website.park_sort_region') }}</div>
					<ul class="sort_list region_sorter">
						<li region-id='0'><span>{{ __('website.park_all_regions') }}</span></li>
						@foreach($regions as $region)
							<li region-id='{{ $region->id }}'><span>{{ Stuff::trans($region, 'name') }}</span></li>
						@endforeach
					</ul>
				</div>
				{{-- <div class="sortBtn">
					<div class="sort_label sort_placeholder">CHOOSE BY TYPE</div>
					<ul class="sort_list">
						<li><span>magari ponti</span></li>
						<li><span>ise ra ponti</span></li>
						<li><span>BMW-shi seqsi</span></li>
						<li><span>cudi ponti</span></li>
					</ul>
				</div> --}}
				<div class="sortBtn">
					<div class="sort_label sort_placeholder">{{ __('website.park_sort_adventure') }}</div>
					<ul class="sort_list adventure-filter">
						<li adventure-id="0"><span>{{ __('website.park_all_adventures') }}</span></li>
						@foreach($adventures as $adventure)
							<li adventure-id="{{ $adventure->name }}"><span>{{ Stuff::trans($adventure, 'name') }}</span></li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
	@if(count($my_parks) > 0)
	<div class="parks_list">
		<div class="list">
			<div class="resBtn">
				<span class="top-line"></span>
				<span class="midd-line"></span>
				<span class="ex-midd-line"></span>
				<span class="bott-line"></span>
			</div>
			<ul class="devinne t-blck" id="list">
				@foreach($my_parks as $my_park)
					<?php $adventureIds = []; ?>
					@foreach($my_park->adventures as $adventureId)
						<?php $adventureIds[] = $adventureId->name; ?>
					@endforeach
					<li data-show="{{ $my_park->id }}" sort-popularity="{{ $my_park->popularity or rand(0, 10000) }}" adventure-ids="{{ json_encode($adventureIds) }}" class="show" region-target="{{ $my_park->region_id }}">{{ Stuff::trans($my_park, 'name') }}</li>
				@endforeach
			</ul>
		</div>
		@foreach($my_parks as $key => $own_park)
			<div class="content_cont" data-show="{{ $own_park->id }}">
				<div class="content">
					<h2 class="b-title">
						<a href="{{ route('siteParksInner', ['lang'=>App::getLocale(), 'park'=>$own_park->alias]) }}" style="color: #333;">
							{{ Stuff::trans($own_park, 'name') }}
						</a>
					</h2>
					<div class="_scroller">
						<p class="t-16-px devinne">{!! Stuff::trans($own_park, 'description') !!}</p>
					</div>
					<div class="adv_parks">
						<h3 class="t-20-px devinne">{{ __('website.park_adventures') }}</h3>
						<div class="icons">
							@foreach($own_park->adventures as $own_adventure)
								<a href="{{ route('siteAdventuresInner', ['lang'=>App::getLocale(),'park'=>$own_park->alias, $own_adventure->id]) }}" title="{{ Stuff::trans($own_adventure,'name') }}" class="adv-i-b adv-1-i" style="background-image: url('{{ Stuff::imgUrl(json_decode($own_adventure->icon)[0], true)  }}')"></a>
							@endforeach
						</div>
					</div>
					<div class="seasons">
						<h3 class="t-20-px devinne">{{ __('website.park_seasons') }}</h3>
						<div class="icons season-icons">

							<?php 
								$seasons = json_decode($own_park->seasons); 
								$season_arr = ['winter'=> 3, 'summer'=>1,'autumn'=>2, 'spring'=>4];
							?>
							@foreach($seasons as $season)
								<a href="#" class="season-i-b season-{{ $season_arr[$season] }}-i"></a>
							@endforeach
						</div>
					</div>
					<div class="clearfix"></div>
					<a href="{{ route('siteParksInner', ['lang'=>App::getLocale(), 'park'=>$own_park->alias]) }}" class="_link yllwBtn t-blck">{{ __('website.park_discover') }}</a>
					<a href="{{ route('attraction', ['lang'=>App::getLocale(), 'park'=>$own_park->alias]) }}" class="yllwBtn t-blck">{{ __('website.park_buy') }}</a>
				</div>
				<div class="photo-map">
					<div class="photo">
						<img src="{{ Stuff::imgUrl(json_decode($own_park->banner)[0], true) }}">
					</div>
					<div class="mini_map">
						<span class="cls">&times;</span>
						<a href="#" class="yllwBtn center">{{ __('website.park_view_map') }}</a>

						<div class="map" data-lat="{{ $own_park->coordinate_lat }}" data-lng="{{ $own_park->coordinate_long }}"></div>
					</div>
				</div>
			</div>
		@endforeach
	</div>
	@else
		<h2 class="text-center text-danger" style="margin:55px 0px;">{{ __('website.trip_not_match') }}</h2>
	@endif

	<div class="clearfix"></div>
	<br><br>
	<br><br>
	<div class="intro_section gry _ex">
		<div class="col-lg-6 no-padding">
			<?php $my_lng = App::getLocale(); ?>
			<div class="t-right wht">
				<h2 class="title devinne t-40-px t-blck">{{ Config::get("setting.mainpage_$my_lng.title") }}</h2>
				<p class="t-14-px">{{ Config::get("setting.mainpage_$my_lng.description") }}</p>
			</div>
			<div class="adventures">
				<h4 class="title">{{ __('website.main_adventures_title') }}</h4>
				<div class="icons">
					<?php
						$adventure_icons = [
							'ტური პონტონით' => 'adv-1-i',
							'ზიპ ლაინი' => 'adv-2-i',
							'ყვინთვა' => 'adv-3-i',
							'სანაოსნო ტური' => 'adv-4-i',
							'ტური კატერით' => 'adv-5-i',
							'საპიკნიკე' => 'adv-6-i',
							'სპორტული თევზაობა' => 'adv-7-i',
							'ვიზიტორთა ცენტრი' => 'adv-8-i',
							'კლდეზე ცოცვა' => 'adv-9-i',
							'თოვლის ფეხსაცმლის ტური' => 'adv-10-i',
							'ველო ტური' => 'adv-11-i',
							'ტური კაიაკით' => 'adv-12-i',
							'ლაშქრობა' => 'adv-13-i',
							'სანაოსნო ტური' => 'adv-14-i',
							'სპელეო ტური' => 'adv-15-i',
							'ფრინველებზე დაკვირვება' => 'adv-16-i',
							'ტურისტული თავშესაფარი' => 'adv-17-i',
							'საცხენოსნო ტური' => 'adv-18-i',
							'ფრინველებზე დაკვირვების კოშკურა' => 'adv-19-i',
							'საფარი ტური' => 'adv-20-i',
						];
					?>

					@foreach($all_adventures as $key => $current_adventure)
						<?php $icon = json_decode($current_adventure[0]->icon)[0]; ?>
						<?php $adventure_seasons = json_decode($current_adventure[0]->seasons);?>
						<?php
							$my_season = '';
							foreach($adventure_seasons as $key => $adventure_season){
								if($key == 0)
									$my_season = $adventure_season;
								else
									$my_season = $my_season.','.$adventure_season;
							}
							if(isset($adventure_icons[$current_adventure[0]->name])){
								$class = $adventure_icons[$current_adventure[0]->name]; 
							}
							else{
								continue;
							}
						?>
						<a href="#" data-season="{{ $my_season }}" id="{{ $current_adventure[0]->name }}" class="adv-i {{ $class }}" style="background-size: unset;">
							<div class="infoBox">{{ Stuff::trans($current_adventure[0], 'name') }}</div>
						</a>
					@endforeach
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="seasons">
				<h4 class="title">{{ __('website.main_mainpage_seasons') }}</h4>
				<div class="icons">
					<a href="#" id="spring" class="season-i season-4-i"></a>
					<a href="#" id="summer" class="season-i season-1-i"></a>
					<a href="#" id="autumn" class="season-i season-2-i"></a>
					<a href="#" id="winter" class="season-i season-3-i"></a>
					<div class="clearfix"></div>
				</div>
				<button onclick="defaultAll()">{{ __('website.default_button') }}</button>
			</div>
		</div>
		<div class="col-lg-6 no-padding">
			<div class="adventures-map">
				<div class="height_range">
					<div class="box">
						<div class="dots">
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
						</div>
					</div>
				</div>
				<div class="sortBy">
					<div class="sortBtn">
						<div class="sort_label sort_placeholder">{{ __('website.main_sort_region') }}</div>
						<ul class="sort_list sort-by-region">
							<li data-href="all"><span>{{ __('website.trip_clear_filter') }}</span></li>
							@foreach($regions as $current_region)
								<li data-href="{{ $current_region->id }}"><span>{{ Stuff::trans($current_region, 'name') }}</span></li>
							@endforeach
						</ul>
					</div>
					<div class="sortBtn">
						<div class="sort_label sort_placeholder">{{ __('website.main_sort_park') }}</div>
						<ul class="sort_list sort-by-parks">
							<li data-href="all" >
								<span>
									{{ __('website.trip_clear_filter') }}
								</span>
							</li>
							@foreach($all_parks as $current)
								{{-- <a href="{{ route('siteParksInner', ['lang'=>App::getLocale(), 'park'=>$current->alias]) }}"> --}}
									<li data-href="{{ Stuff::trans($current, 'name') }}" >
										<span>
											{{ Stuff::trans($current, 'name') }}
										</span>
									</li>
								{{-- </a> --}}
							@endforeach
						</ul>
					</div>
					<div class="sortBtn">
						<div class="sort_label sort_placeholder">{{ __('website.main_sort_activity') }}</div>
						<ul class="sort_list sort-by-adventure">
							<li data-href="all">
								<span>{{ __('website.trip_clear_filter') }}</span>
							</li>
							@foreach($all_adventures as $key => $my_adventure)
								@if(isset($adventure_icons[$my_adventure[0]->name]))
									<li data-href="{{ $key }}">
										<span>{{ Stuff::trans($my_adventure[0], 'name') }}</span>
									</li>
								@endif
							@endforeach
						</ul>
					</div>
				</div>
				<!-- <div class="map" data-lat="42.3173294" data-lng="42.2338442"></div> -->

				{{-- [
				{"height":100,"position":{"lat":41.73201210104606,"lng":44.30832229872283}},
				{"height":123,"position":{"lat":41.53201210104606,"lng":44.30832229872283}},
				{"height":1244,"position":{"lat":41.33201210104606,"lng":44.30832229872283}},
				{"height":1000,"position":{"lat":41.23201210104606,"lng":44.30832229872283}},
				{"height":2550,"position":{"lat":41.13201210104606,"lng":44.30832229872283}}
				] --}}

				<div class="map hu" data-json='{{ json_encode($json) }}' 

				data-catJson='{{ json_encode($categoryJson) }}'>
				<div class="parks_json_map" style="display: none;">{{ json_encode($advJson) }}</div>
				<script type="text/javascript">
					var parksJson = document.querySelector('.parks_json_map').innerHTML;
					var mainJson = JSON.parse(parksJson);


					// [
					// 	{ position: [{lat:41.672911819602085,lng:404.82971191406256}], title: 'ალგეთის ეროვნული პარკი',adventures: 'txilamuri,curva,tevzaoba', regionID: 3 },
					// 	{ position: [{lat:41.42625319507272,lng:406.12609863281256}], title: 'ბორჯომ-ხარაგაულის ეროვნული პარკი',adventures: 'xtunva,cekva,tevzaoba', regionID: 4 },
					// 	{ position: [{lat:41.83682786072714,lng:402.4237060546875}], title: 'თუშეთის ეროვნული პარკი',adventures: 'sma,qeifi,tevzaoba', regionID: 7 }
					// ];

				</script>


				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek"></script>
@endsection

@section('javascript')
	<script type="text/javascript">
		function regionSorter(){
			$('.region_sorter li').click(function(e){
				e.preventDefault();
				var id = $(this).attr('region-id');
				$('.adventure-filter').closest('.sortBtn').find('.sort_placeholder').html('{{ __('website.park_all_adventures') }}')

				
				$('.parks_list .devinne li').addClass('show');
				$('.parks_list .devinne li').not('li[region-target='+id+']').removeClass('show');
				$('.parks_list .devinne li.show').eq(0).trigger('click');


				if(id == 0){
					$('.parks_list .devinne li').addClass('show');
					$('.parks_list .devinne li.show').eq(0).trigger('click');
				}
			});
		}
		
		function adventureSorter(){
			$('.adventure-filter li').click(function(){
				var id = $(this).attr('adventure-id');
				console.log(id);
				$('.region_sorter').closest('.sortBtn').find('.sort_placeholder').html('{{ __('website.park_all_regions') }}')
				$('.parks_list .devinne li').addClass('show');
				
				if(id == 0){
					$('.parks_list .devinne li').eq(0).trigger('click');
					return;
				}

				$('.parks_list .devinne li').each(function(){
					var adventureID = $(this).attr('adventure-ids');
					adventureID = JSON.parse(adventureID);
					var status = false;
					for(var i in adventureID){
						if(adventureID[i] == id)
							status = true;
					}
					if(status == false)
						$(this).removeClass('show');
				});
				$('.parks_list .devinne li.show').eq(0).trigger('click');
			});
		}

		var seasFunc;
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			miniMapBtn();
			sortDropdown();
			parkShow();

			regionSorter();
			adventureSorter();
			initMap();
			seasFunc = new seasonMarkers();

			$('.bottom_nav ul li').eq(3).find('a').addClass('active');
		});


		$('.season-icons').click(function(ev){
			ev.preventDefault();
		});

		function _(selector){
			return document.querySelectorAll(selector);
		}

		function sortUnorderedList(ul, sortDescending) {
          if(typeof ul != "string"){
          	return false;
          }
          ul = document.getElementById(ul);

          var lis = _('.parks_list .devinne li.show');
          var vals = [];
          var fullLi = [];
          

          for(var i = 0; i < lis.length; i++){
            vals.push(Number(lis[i].getAttribute('sort-popularity')));
            fullLi.push(lis[i]);
          }

          


          if(sortDescending){
            vals.sort(function(a,b){
	          	return b - a
	          });
          }else{
          	vals.sort(function(a,b){
	          	return a - b
	          });
          }


          ul.innerHTML = "";
          for(var i = 0; i < vals.length; i++){
          	for( var u = 0; u < fullLi.length; u++ ){
          		if( vals[i] == fullLi[u].getAttribute("sort-popularity") ){
            		ul.append(fullLi[u]);
          		}
          	}
          }
        }
        
        window.onload = function() {
			var desc = true;
			document.getElementById("alphabet-sort").onclick = function() {
				sortUnorderedList("list", desc);
				desc = !desc;
				return false;
			}
        }


        $('.height_range .box .dots .dot').hover(function(){
			var $dotDivs = $('.adventures-map .dot');
			var thisIndex = $(this).index() + 1;
			var dotNum = $dotDivs.length;
			var heightMax = 3000;
			// var thisHeight = marker.mountHeight;
			// var seasons = JSON.parse(marker.seasons);
			// var activeDotIndex = dotNum - Math.floor(thisHeight/heightMax * dotNum);
			var activeDotIndex = heightMax - Math.floor((heightMax / dotNum) * thisIndex);
			$('.height_range .box .dots .dot').removeClass('active');
			$(this).addClass('active');
			$(this).html(`<div class="val"><span class="number">`+activeDotIndex+`</span><span class="unit">meters</span></div>`);
		});

		$('.height_range .box .dots .dot').click(function(){
			var $dotDivs = $('.adventures-map .dot');
			var thisIndex = $(this).index() + 1;
			var dotNum = $dotDivs.length;
			var heightMax = 3000;
			// var thisHeight = marker.mountHeight;
			// var seasons = JSON.parse(marker.seasons);
			// var activeDotIndex = dotNum - Math.floor(thisHeight/heightMax * dotNum);
			var activeDotIndex = heightMax - Math.floor((heightMax / dotNum) * thisIndex);
			multipleHeightMarkerMapHeight(document.querySelector('.adventures-map .map'),activeDotIndex);
			$('.height_range .box .dots .dot').removeClass('active');
			$(this).addClass('active');
			$(this).html(`<div class="val"><span class="number">`+activeDotIndex+`</span><span class="unit">meters</span></div>`);
		});

		function setMarkerListener(marker){
			marker.addListener('click', function(){
				var $dotDivs = $('.adventures-map .dot');
				var dotNum = $dotDivs.length;
				var heightMax = 3000;
				var thisHeight = marker.mountHeight;
				var seasons = JSON.parse(marker.seasons);

				var activeDotIndex = dotNum - Math.floor(thisHeight/heightMax * dotNum);

		        $dotDivs.removeClass('active');
		        $activeDotDiv = $dotDivs.eq(activeDotIndex);
		        $activeDotDiv.addClass('active');
	            $activeDotDiv.html(`<div class="val"><span class="number">`+marker.mountHeight+`</span><span class="unit">meters</span></div>`);




		        var id = marker.adventures;

		        $('.intro_section .adventures .icons > a').removeClass('adventuresAct');

		        $('.intro_section .adventures .icons > a[id="'+id+'"]').addClass('adventuresAct');


				$('.icons .season-i').removeClass('active');
		        for( var i = 0; i < seasons.length; i++ ){
		        	for( var a = 0; a < $('.icons .season-i').length; a ++){
		        		if( seasons[i] === $('.icons .season-i').eq(a).attr('id') ){
		        			
		        			$('.icons .season-i').eq(a).addClass('active');
		        		}
		        	}
		        }

			});
		}

		function multipleHeightMarkerMap(element){
			var infoW = null;
			function addListenerToMarker(marker, infowindow, map){
				marker.addListener('click', function(){
					if( infoW != null ){
						infoW.close();
					}
					infowindow.open(map, marker);
					infoW = infowindow;
				});
			}

			var markerAttrs = JSON.parse(element.getAttribute('data-json'));

			var map = new google.maps.Map(element,{
				zoom: 7,
		        center: {lat: 42.17968819665963, lng: 403.33557128906256},
		        mapTypeId: 'roadmap'
			});
			var markers = [];

			var icon = "/main/img/map-pin-by.png";
			var ocup = [
			{"position":{"lat":43.17113228474479,"lng":40.43243408203125},"title":"{{ __('website.occupied_bichvinta') }}","adventures":"","regionID":"9","park_alias":"bichvintamiuserisnakrdzali","occupation":"1"},
			{"position":{"lat":42.377054331358856,"lng":44.25264042355741},"title":"{{ __('website.occupied_liaxvi') }}","adventures":"","regionID":"10","park_alias":"liakhvisnakrdzali","occupation":"1"},
			{"position":{"lat":43.4459400050658,"lng":40.527191162109375},"title":"{{ __('website.occupied_ritsa') }}","adventures":"","regionID":"9","park_alias":"ritsisnakrdzali","occupation":"1"},
			{"position":{"lat":43.34715236003302,"lng":41.0064697265625},"title":"{{ __('website.occupied_gumismta') }}","adventures":"","regionID":"9","park_alias":"gumismta","occupation":"1"}
			];
			

			mainJson = mainJson.concat(ocup);

			for(var i = 0; i<mainJson.length; i++){
				if( mainJson[i].occupation == "1" ){
					icon = "/main/img/ocup-by-russia.svg";
				}else{
					icon = "/main/img/map-pin-by.png";
				}
				var marker = new google.maps.Marker({
					position : mainJson[i].position,
					map:map,
					icon: icon,
					// icon: markerAttrs[i].ico,
					
				});
				var box = document.createElement('div');
				box.setAttribute('class',"parkInfoWindow");
				var h3 = document.createElement('a');
				var href_link = "/{{ App::getLocale() }}/site/"+mainJson[i].park_alias;
				if(mainJson[i].occupation == "1"){
					href_link = "#!";
				}
				h3.setAttribute('href', href_link);
				h3.style.fontSize = '18px';
				h3.style.color = '#333';
				h3.innerText = mainJson[i].title;

				var ul = document.createElement('ul');
				var advent =  mainJson[i].adventures.split(',');

				for( var u = 0; u < advent.length; u++ ){
					var li = document.createElement('li');
					li.innerText = advent[u];
					ul.prepend(li);
				}

				box.prepend(h3);
				box.appendChild(ul);

				var infowindow = new google.maps.InfoWindow({
    				content:box
    			});
				addListenerToMarker(marker, infowindow, map);
				markers.push(marker);
			}
			
		}

		function multipleHeightMarkerMapHeight(element,height){
			var minHeight = height - 200;
			var maxHeight = height + 200;
			var infoW = null;
			function addListenerToMarker(marker, infowindow, map){
				marker.addListener('click', function(){
					if( infoW != null ){
						infoW.close();
					}
					infowindow.open(map, marker);
					infoW = infowindow;
				});
			}
			var markerAttrs = JSON.parse(element.getAttribute('data-json'));

			var map = new google.maps.Map(element,{
				zoom: 7,
		        center: {lat:42.105628, lng:43.593089},
		        mapTypeId: 'roadmap'
			});
			var markers = [];
			var latlngbounds = new google.maps.LatLngBounds();
			for(var i = 0; i<markerAttrs.length; i++){
				if( (markerAttrs[i].height >= minHeight) && (markerAttrs[i].height <= maxHeight) ){
				var marker = new google.maps.Marker({
					position : JSON.parse(markerAttrs[i].position)[0],
					map:map,
					icon: '/main/img/map-pin-by.png',
					// icon: markerAttrs[i].ico,
					mountHeight: JSON.parse(markerAttrs[i].height),
					adventures: markerAttrs[i].adventure_id,
					seasons: markerAttrs[i].seasons
				});
				var infowindow = new google.maps.InfoWindow({
    				content:markerAttrs[i].name
    			});
				addListenerToMarker(marker, infowindow, map);
				markers.push(marker);
			}
			}

			for(var i = 0; i< markers.length; i++){
				setMarkerListener(markers[i]);
			}
		}
		function multipleHeightMarkerMapClick(element,id){
			var infoW = null;
			function addListenerToMarker(marker, infowindow, map){
				marker.addListener('click', function(){
					if( infoW != null ){
						infoW.close();
					}
					infowindow.open(map, marker);
					infoW = infowindow;
				});
			}
			var markerAttrs = JSON.parse(element.getAttribute('data-catJson'));

			var allmarkers = [];

			for( var k = 0; k < id.length; k++ ){
				for( var c = 0; c < markerAttrs[id[k]].length; c++ ){
					allmarkers.push(markerAttrs[id[k]][c]);
				}
			}

			
			// if(typeof  markerAttrs[id] == 'undefined'){
			// 	return false;
			// }

			var map = new google.maps.Map(element,{
				zoom: 7,
		        // center: JSON.parse(markerAttrs[id][0].position)[0],
		        center: {lat: 42.264959, lng: 43.500335},
		        mapTypeId: 'roadmap'
			});
			var markers = [];
			var latlngbounds = new google.maps.LatLngBounds();
			for(var i = 0; i<allmarkers.length; i++){
				var marker = new google.maps.Marker({
					position : JSON.parse(allmarkers[i].position)[0],
					map:map,
					icon: '/main/img/map-pin-by.png',
					// icon: markerAttrs[i].ico,
					mountHeight: JSON.parse(allmarkers[i].height),
					adventures: allmarkers[i].adventure_id,
					seasons: allmarkers[i].seasons
				});
				var myLang = '{{ App::getLocale() }}'; 
				var infowindow = new google.maps.InfoWindow({
    				content:'<a href="/'+myLang+'/site/'+allmarkers[i].park_alias+'/adventuresInner/'+allmarkers[i].redirect_id+'" style="color: #222 !important;" class="parkInfoWindow">'+allmarkers[i].name+'</a>'
    			});
				addListenerToMarker(marker, infowindow, map);
			    latlngbounds.extend(marker.getPosition());
				markers.push(marker);
			}

			for(var i = 0; i< markers.length; i++){
				setMarkerListener(markers[i]);
			}
			// map.fitBounds(latlngbounds);
		}


		function initMap() {
			var element = document.querySelector('.adventures-map .map');
			multipleHeightMarkerMap(element);

    		$('.index_bott_map .map').gMap(function(element, lat, lng){
    			lat = parseInt(lat);
    			lng = parseInt(lng)
    			var position = {lat: lat, lng: lng};
    			var map = new google.maps.Map(element,{
    				zoom: 7,
			        center: position,
			        scrollwheel: false,
			        gestureHandling: 'greedy',
			        mapTypeId: 'satellite'
    			});
    			var marker = new google.maps.Marker({
    				position : position,
    				map:map,
    				icon: '/main/img/map-pin.png',
    			});
    		});

			function miniMapInit(gps = false){
	    		$('.mini_map .map').gMap(function(element, lat, lng){
	    			lat = JSON.parse(lat);
	    			lng = JSON.parse(lng);
	    			var position = {lat: lat, lng: lng};
	    			var map = new google.maps.Map(element,{
	    				zoom: 7,
				        center: position,
				        scrollwheel: false,
				        gestureHandling: 'greedy',
				        mapTypeControl:false,
				        fullscreenControl: false,
				        mapTypeId: 'hybrid'
	    			});
	    			var marker = new google.maps.Marker({
	    				position : position,
	    				map:map,
	    				icon: '/main/img/map-pin.png',
	    			});

	    			if(gps){
	    				marker = new google.maps.Marker({
		    				position : gps,
		    				map:map,
		    				icon: '/main/img/my_location.svg',
		    			});
	    			}

	    		});
			}

    		function showPosition(position, error) {
				var aroundmeLNG = {'ka':"ჩემი მდებარეობა",'en':"My Location",'ru':"мое местонахождение"};
			    var test = "Latitude: " + position.coords.latitude + 
			    "<br>Longitude: " + position.coords.longitude;
			    gpsCoordinates = {lat: position.coords.latitude, lng: position.coords.longitude};
				miniMapInit(gpsCoordinates);
			}

			function showError(err){
			    miniMapInit();
			}
			navigator.geolocation.getCurrentPosition(showPosition, showError);

		}


		// function initMap() {
		// 	var element = document.querySelector('.adventures-map .map');
		// 	multipleHeightMarkerMap(element);

  //   		$('.index_bott_map .map').gMap(function(element, lat, lng){
  //   			lat = parseInt(lat);
  //   			lng = parseInt(lng)
  //   			var position = {lat: lat, lng: lng};
  //   			var map = new google.maps.Map(element,{
  //   				zoom: 7,
		// 	        center: position,
		// 	        scrollwheel: true,
		// 	        gestureHandling: 'greedy',
		// 	        mapTypeControl:false,
		// 	        fullscreenControl: false,
		// 	        mapTypeId: 'satellite'
  //   			});
  //   		});

  //   		// initFlightsGoogleMap();
		// }


		var adventArray = [];

		$('.intro_section .adventures .icons > a').on('click',function(ev){
			ev.preventDefault();
			var id = $(this).attr('id');

			// $('.intro_section .adventures .icons > a').removeClass('adventuresAct');
			if( $(this).hasClass('adventuresAct') ){
				$(this).removeClass('adventuresAct');
				for( var i = 0; i < adventArray.length; i++ ){
					if( id == adventArray[i] ){
						adventArray.splice(i,1);
					}
				}
			}else{
				adventArray.push(id);
				$(this).addClass('adventuresAct');
			}
			multipleHeightMarkerMapClick(document.querySelector('.adventures-map .map'),adventArray);

		});

		function seasonMarkers(){
			this.map = _('.adventures-map .map')[0];
			this.json = JSON.parse(this.map.getAttribute('data-json'));
			this.jsonLength = this.json.length;

			this.seasonFilter = function(seasonName){
				var infoW = null;
				function addListenerToMarker(marker, infowindow, map){
					marker.addListener('click', function(){
						if( infoW != null ){
							infoW.close();
						}
						infowindow.open(map, marker);
						infoW = infowindow;
					});
				}
				
				

				var map = new google.maps.Map(this.map,{
					zoom: 7,
			        center: {lat: 42.264959, lng: 43.500335},
			        mapTypeId: 'roadmap'
				});
				var nmn;
				var markers = [];
				for( var n = 0; n < this.jsonLength; n++ ){
					nmn = JSON.parse(this.json[n].seasons);
					for( var m = 0; m < nmn.length; m++ ){
						if( nmn[m] == seasonName ){
							
							var marker = new google.maps.Marker({
								position : JSON.parse(this.json[n].position)[0],
								map:map,
								icon: '/main/img/map-pin-by.png',
								mountHeight: this.json[n].height,
								adventures: this.json[n].adventure_id,
								seasons: this.json[n].seasons
							});
							var infowindow = new google.maps.InfoWindow({
								content:this.json[n].name
							});
							addListenerToMarker(marker, infowindow, map);
							markers.push(marker);

						}
					}
				}
				



				for(var i = 0; i< markers.length; i++){
					setMarkerListener(markers[i]);
				}
			}
			
		}

		$('.icons .season-i').click(function(ev){
				ev.preventDefault();



	var JSON_Parse = {
	    length: 0,
	    season: [],
	    where: 'data-season',
	    object: '',
	    process: function(data){
	    	this.object = data;
	        this.length = data.length;
	        
	        for(var i = 0; i < this.length; i++){
	            this.season.push(data[i].getAttribute('data-season'));
	        }
	    },
	    research: function(element){
	        var regex = new RegExp( element, 'g' );
	        $('.intro_section .adventures .icons > a').removeClass('adventuresAct');
	        for( var i = 0; i < this.length; i++ ){
	            if( this.season[i].match(regex) != null ){
	                $('.intro_section .adventures .icons > a').eq(i).addClass('adventuresAct');
	            }
	        }
	    }
	}
	
	seasFunc.seasonFilter($(this).attr('id'));

	JSON_Parse.process(document.querySelectorAll('.intro_section .adventures .icons > a'));
	JSON_Parse.research($(this).attr('id'));

	});

	$('.sort-by-region li').click(function(e){
		e.preventDefault();
		var linkTo = $(this).attr('data-href');
		if( linkTo === 'all' ){
			$('button[onclick="defaultAll()"]').trigger('click');
			return false;
		}
		var element = document.querySelector('.adventures-map .map');
		var infoW = null;
		function addListenerToMarker(marker, infowindow, map){
			marker.addListener('click', function(){
				if( infoW != null ){
					infoW.close();
				}
				infowindow.open(map, marker);
				infoW = infowindow;
			});
		}


		var map = new google.maps.Map(element,{
			zoom: 7,
	        center: {lat: 42.17968819665963, lng: 403.33557128906256},
	        mapTypeId: 'roadmap'
		});

		for( var i = 0; i < mainJson.length; i++ ){

			if( mainJson[i].regionID == linkTo ){

			
			var markers = [];
			
				var marker = new google.maps.Marker({
					position : mainJson[i].position,
					map:map,
					icon: '/main/img/map-pin-by.png',
					// icon: markerAttrs[i].ico,
					
				});
				var box = document.createElement('div');
				box.setAttribute('class',"parkInfoWindow");
				var h3 = document.createElement('h3');
				h3.innerText = mainJson[i].title;

				var ul = document.createElement('ul');

				var advent =  mainJson[i].adventures.split(',');

				for( var u = 0; u < advent.length; u++ ){
					var li = document.createElement('li');
					li.innerText = advent[u];
					ul.prepend(li);
				}

				box.prepend(h3);
				box.appendChild(ul);

				var infowindow = new google.maps.InfoWindow({
    				content:box
    			});
				addListenerToMarker(marker, infowindow, map);
				markers.push(marker);
			



			}
		}
	});



	$('.sort-by-parks li').click(function(e){
		e.preventDefault();
		var linkTo = $(this).attr('data-href');
		if( linkTo === 'all' ){
			$('button[onclick="defaultAll()"]').trigger('click');
			return false;
		}
		var element = document.querySelector('.adventures-map .map');

		for( var i = 0; i < mainJson.length; i++ ){
			if( mainJson[i].title == linkTo ){
			var infoW = null;
			function addListenerToMarker(marker, infowindow, map){
				marker.addListener('click', function(){
					if( infoW != null ){
						infoW.close();
					}
					infowindow.open(map, marker);
					infoW = infowindow;
				});
			}


			var map = new google.maps.Map(element,{
				zoom: 7,
		        center: {lat: 42.17968819665963, lng: 403.33557128906256},
		        mapTypeId: 'roadmap'
			});
			var markers = [];
			
				var marker = new google.maps.Marker({
					position : mainJson[i].position,
					map:map,
					icon: '/main/img/map-pin-by.png',
					// icon: markerAttrs[i].ico,
					
				});
				var box = document.createElement('div');
				box.setAttribute('class',"parkInfoWindow");
				var h3 = document.createElement('h3');
				h3.innerText = mainJson[i].title;

				var ul = document.createElement('ul');

				var advent =  mainJson[i].adventures.split(',');

				for( var u = 0; u < advent.length; u++ ){
					var li = document.createElement('li');
					li.innerText = advent[u];
					ul.prepend(li);
				}

				box.prepend(h3);
				box.appendChild(ul);

				var infowindow = new google.maps.InfoWindow({
    				content:box
    			});
				addListenerToMarker(marker, infowindow, map);
				markers.push(marker);
			
			}
		}
	});

	$('.sort-by-adventure li').click(function(){
		var linkTo = $(this).attr('data-href');
		if( linkTo === 'all' ){
			$('button[onclick="defaultAll()"]').trigger('click');
			return false;
		}
		
		$('.intro_section .adventures .icons > a').each(function(){

			if( $(this).attr('id') == linkTo ){
				$(this).trigger('click');
			}
		});
	});


	function defaultAll(){
		initMap();
		$('.intro_section .adventures .icons > a').removeClass('adventuresAct');
		$('.height_range .box .dots .dot').removeClass('active');
	}
	$('body').on('click','.preventDefault',function(ev){
		ev.preventDefault();
	});

	function removeArrow(){
		setTimeout(function(){
			var length = $('.my-adventure-cont .dscrb.active .owl-item').length;

			if(length > 3)
				$('.my-adventure-cont .dscrb.active .r_arrw').show();
			else
				$('.my-adventure-cont .dscrb.active .r_arrw').hide();

		}, 200);
	}
	function arrowController(){
		
		removeArrow();

		$('.adv-nav li').click(function(){
			removeArrow();
		});
	}

	arrowController();

	</script>
@endsection


