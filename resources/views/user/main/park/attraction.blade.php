@extends('user.layouts.master')

@section('head')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('main/css/datepicker.min.css') }}">
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="buy_tkt_cont none"> {{-- REMOVE CLASS "none" --}}
		<?php $lng = App::getLocale(); ?>
		<div class="web-container p-15-lr t-blck">
			<h2 class="verlagB t-40-px t-up m-30-b">{{ Config::get("setting.footer_$lng.ticket.title") }}</h2>
			<div class="dscrb_txt">
				<p>{{ Config::get("setting.footer_$lng.ticket.description") }}</p>
			</div>
			<form>
				<div class="row">
					<div class="col-md-5 col-sm-12">
						<h2 class="verlagB t-30-px t-up m-30-b">{{ __('website.park_passes') }}:</h2>
						<div class="counter_container">
							<div class="pass_box m-10-b t-14-p">
								<div class="data pull-left">
									<p class="verlagB t-up m-0">the tent</p>
									<p class="devinne">
										<span class="m-5-r">5</span>
										<span>gel</span>
										<span>/per person</span>
									</p>
								</div>
								<div class="val_count">
									<input type="number" min="0" max="10" value="0">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="pass_box m-10-b t-14-p">
								<div class="data pull-left">
									<p class="verlagB t-up m-0">the tent</p>
									<p class="devinne">
										<span class="m-5-r">5</span>
										<span>gel</span>
										<span>/per person</span>
									</p>
								</div>
								<div class="val_count">
									<input type="number" min="0" max="10" value="0">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<h2 class="verlagB t-30-px t-up m-30-b">{{ __('website.park_date_and_time') }}</h2>
						<div class='datePicker'>
							<input id='calendar' type='text' lang="ka" name='pick date' placeholder='choose date'/>
						</div>
						<div class="timePicker">
							<input id="time" type="text" placeholder="time" maxlength="5" size="5" />
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<h2 class="verlagB t-30-px t-up m-30-b text-center">{{ __('website.park_total') }}</h2>
						<div class="checkout_box">
							<div class="sum t-wht">
								<span class="m-5-r">256</span>
								<span class="curr">gel</span>
							</div>
							<button type="submit" class="yllw verlagB t-blck t-15-px t-up">{{ __('website.park_checkout') }}</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="price_img_container">
		<div class="price_img">
			@if(App::getLocale() == 'ka')
				@if($park->alias == 'okatsecanyon')
					<a href="/main/img/price-img/1.png" download="ოკაცეს კანიონი"><img src="/main/img/price-img/1.png"></a>
				@elseif($park->alias == 'martvilicanyon')
					<a href="/main/img/price-img/2.png" download="მარტვილის კანიონი"><img src="/main/img/price-img/2.png"></a>
				@elseif($park->alias == 'sataplia')
					<a href="/main/img/price-img/3.png" download="სათაფლიას მღვიმე"><img src="/main/img/price-img/3.png"></a>
				@elseif($park->alias == 'prometheuscave')
					<a href="/main/img/price-img/4.png" download="პრომეთეს მღვიმე"><img src="/main/img/price-img/4.png"></a>
				@elseif($park->alias == 'chachunanp')
					<a href="/main/img/price-img/5.png" download="ჭაჭუნას ეროვნული პარკი"><img src="/main/img/price-img/5.png"></a>
				@elseif($park->alias == 'javakhetinp')
					<a href="/main/img/price-img/6.png" download="ჯავახეთის ეროვნული პარკი"><img src="/main/img/price-img/6.png"></a>
				@elseif($park->alias == 'kintrishinp')
					<a href="/main/img/price-img/7.png" download="კინტრიშის ეროვნული პარკი"><img src="/main/img/price-img/7.png"></a>
				@elseif($park->alias == 'kobuletinp')
					<a href="/main/img/price-img/8.png" download="ქობულეთის ეროვნული პარკი"><img src="/main/img/price-img/8.png"></a>
				@elseif($park->alias == 'mtiralanp')
					<a href="/main/img/price-img/9.png" download="მტირალას ეროვნული პარკი"><img src="/main/img/price-img/9.png"></a>
				@elseif($park->alias == 'kolxetinp')
					<a href="/main/img/price-img/10.png" download="კოლხეთის ეროვნული პარკი"><img src="/main/img/price-img/10.png"></a>
				@elseif($park->alias == 'vashlovaninp')
					<a href="/main/img/price-img/11.png" download="ვაშლოვანის ეროვნული პარკი"><img src="/main/img/price-img/11.png"></a>
				@elseif($park->alias == 'lagodekhinp')
					<a href="/main/img/price-img/13.png" download="ლაგოდეხის ეროვნული პარკი"><img src="/main/img/price-img/13.png"></a>
				@elseif($park->alias == 'borjomi-kharagaulinp')
					<a href="/main/img/price-img/14.png" download="ბორჯომ-ხარაგაულის ეროვნული პარკი"><img src="/main/img/price-img/14.png"></a>
				@elseif($park->alias == 'tushetinp')
					<a href="/main/img/price-img/15.png" download="თუშეთის ეროვნული პარკი"><img src="/main/img/price-img/15.png"></a>
				@else
					<h1 style="margin-top: 35px; margin-bottom: 35px;" class="empty_header">ამ პარკში დრობით არ არის ფასიანი სერვისები</h1>
				@endif
			@else
				@if($park->alias == 'okatsecanyon')
					<a href="/main/img/price-img/Okatse Canyon.png" download="ოკაცეს კანიონი"><img src="/main/img/price-img/Okatse Canyon.png"></a>
				@elseif($park->alias == 'martvilicanyon')
					<a href="/main/img/price-img/Martvili Canyon.png" download="Martvili Canyon"><img src="/main/img/price-img/Martvili Canyon.png"></a>
				@elseif($park->alias == 'sataplia')
					<a href="/main/img/price-img/Sataplia Cave.png" download="Sataplia Cave"><img src="/main/img/price-img/Sataplia Cave.png"></a>
				@elseif($park->alias == 'prometheuscave')
					<a href="/main/img/price-img/Prometheus Cave.png" download="Prometheus Cave"><img src="/main/img/price-img/Prometheus Cave.png"></a>
				@elseif($park->alias == 'chachunanp')
					<a href="/main/img/price-img/Chachuna National Park.png" download="Chachuna National Park"><img src="/main/img/price-img/Chachuna National Park.png"></a>
				@elseif($park->alias == 'javakhetinp')
					<a href="/main/img/price-img/Javakheti National Park.png" download="Javakheti National Park"><img src="/main/img/price-img/Javakheti National Park.png"></a>
				@elseif($park->alias == 'kintrishinp')
					<a href="/main/img/price-img/Kintrishi National Park.png" download="Kintrishi National Park"><img src="/main/img/price-img/Kintrishi National Park.png"></a>
				@elseif($park->alias == 'kobuletinp')
					<a href="/main/img/price-img/Kobuleti National Park.png" download="Kobuleti National Park"><img src="/main/img/price-img/Kobuleti National Park.png"></a>
				@elseif($park->alias == 'mtiralanp')
					<a href="/main/img/price-img/Mtirala National Park.png" download="Mtirala National Park"><img src="/main/img/price-img/Mtirala National Park.png"></a>
				@elseif($park->alias == 'kolxetinp')
					<a href="/main/img/price-img/Kolkheti National Park.png" download="Kolkheti National Park"><img src="/main/img/price-img/Kolkheti National Park.png"></a>
				@elseif($park->alias == 'vashlovaninp')
					<a href="/main/img/price-img/Vashlovani National Park.png" download="Vashlovani National Park"><img src="/main/img/price-img/11.png"></a>
				@elseif($park->alias == 'lagodekhinp')
					<a href="/main/img/price-img/Lagodekhi National Park.png" download="Lagodekhi National Park"><img src="/main/img/price-img/Lagodekhi National Park.png"></a>
				@elseif($park->alias == 'borjomi-kharagaulinp')
					<a href="/main/img/price-img/Borjom-Kharagauli National Park.png" download="Borjom-Kharagauli National Park"><img src="/main/img/price-img/Borjom-Kharagauli National Park.png"></a>
				@elseif($park->alias == 'tushetinp')
					<a href="/main/img/price-img/15-en.png" download="Tusheti National Park"><img src="/main/img/price-img/15-en.png"></a>
				@else
					<h1 style="margin-top: 35px; margin-bottom: 35px;">No paid services detected</h1>
				@endif
			@endif
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	<script type="text/javascript" src="{{ asset('/main/js/datepicker.min.js') }}"></script>
@endsection

@section('javascript')
	<script type="text/javascript">
		
		$(function(){
			setCarousels();
			fourBullets();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();
			valueCounter();

		});


	</script>
@endsection
