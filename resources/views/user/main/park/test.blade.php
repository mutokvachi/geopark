@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
@endsection

@section('content')
	<h1>hello world</h1>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">

		$(function(){
			setCarousels();
			fourBullets();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();

		});


	</script>
@endsection


