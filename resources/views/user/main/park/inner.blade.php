@extends('user.layouts.master')

@section('head')
	<style type="text/css">
	.controls {
	  margin-top: 10px;
	  border: 1px solid transparent;
	  box-sizing: content-box;
	  height: 40px;
	  outline: none;
	  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
	}

	#origin-input,
	#destination-input {
	  background-color: #fff;
	  font-family: Roboto;
	  font-size: 15px;
	  font-weight: 300;
	  margin-left: 12px;
	  padding: 0 11px 0 13px;
	  text-overflow: ellipsis;
	  width: 300px;
	}

	#mode-selector {
	  color: #fff;
	  background-color: #4d90fe;
	  margin-left: 12px;
	  padding: 5px 11px 0px 11px;
	}

	#mode-selector label {
	  font-family: Roboto;
	  font-size: 13px;
	  font-weight: 300;
	}
	</style>
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="park_overview">
		<div class="col-md-6 dscrb">
			<h2 class="t-40-px t-blck m-15-tb verlagB t-up m-30-b">
				{{-- {{ __('website.parkInner_overview') }} --}}
				{{ Stuff::trans($park, 'name') }}
			</h2>
			<div class="hideText">
				<p>{!! Stuff::trans($park, 'description') !!}</p>
			</div>
			@if(!empty($contacts[0]['name']))
				<div class="contact_info">
					<h3 class="grn-title">{{ __('website.parkInner_contact') }}</h3>
					<div class="contact_box">
						@foreach($contacts as $contact)
							<div class="c_info f_lex">
								<div class="c-ico c_name">{{ $contact['name'] }}</div>
								<a href="tel:{{ $contact['number'] }}" class="c-ico c_phone">{{ $contact['number'] }}</a>
								<a href="mailto:{{ $contact['email'] }}" class="c-ico c_mail">{{ $contact['email'] }}</a>
							</div>
						@endforeach
					</div>
					<div class="addrs_box">
						<p class="c-ico c_addrs"><a style="color:#333; " href="{{ route('siteGetHere', ['lang'=>App::getLocale(), $park->alias]) }}">{{ Stuff::trans($park, 'administration_address') }}</a></p>
					</div>
				</div>
			@endif
			<div class="clearfix"></div>
			<div class="info season_info">
				<h3 class="grn-title">{{ __('website.parkInner_operating_hours') }}</h3>
				<div class="col-md-3 col-sm-6">
					<p class="circular t-blck s-spring  p-50-lr t-16-px">{{ __('website.parkInner_spring') }}</p>
					<div class="details p-50-l">
						<span class="block wk">
							{!! Stuff::trans($park, 'operating_hours_2') !!}
						</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<p class="circular t-blck t-16-px p-50-lr s-summer">{{ __('website.parkInner_summer') }}</p>
					<div class="details p-50-l">
						<span class="block wk">
							{!! Stuff::trans($park, 'operating_hours_3') !!}
						</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<p class="circular p-50-lr  t-blck s-fall t-16-px">{{ __('website.parkInner_fall') }}</p>
					<div class="details p-50-l">
						<span class="block wk">
							{!! Stuff::trans($park, 'operating_hours_4') !!}
						</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<p class="circular p-50-lr s-winter t-blck t-16-px">{{ __('website.parkInner_winter') }}</p>
					<div class="details p-50-l">
						<span class="block wk">
							{!! Stuff::trans($park, 'operating_hours_1') !!}
						</span>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="info tkt_info">
				<h3 class="grn-title">{{ __('website.parkInner_fees') }}</h3>
				@if(!empty($park->fees_passes_2) && $park->fees_passes_2 != '<p><br></p>')
					<div class="fees_passes col-sm-3 icn adults-i">
						<p class="circular t-blck t-16-px">{{ __('website.parkInner_adults') }}</p>
						<div class="details">
							<span class="block prc">
								{!! Stuff::trans($park, 'fees_passes_2') !!}
							</span>
						</div>
					</div>
				@endif

				@if(!empty($park->fees_passes_1) && $park->fees_passes_1 != '<p><br></p>')
					<div class="fees_passes col-sm-3 icn family-i">
						<p class="circular t-blck t-16-px">{{ __('website.parkInner_students') }}</p>
						<div class="details">
							<span class="block prc">
								{!! Stuff::trans($park, 'fees_passes_1') !!}
							</span>
						</div>
					</div>
				@endif

				@if(!empty($park->fees_passes_3) && $park->fees_passes_3 != '<p><br></p>')
					<div class="fees_passes col-sm-3 icn group-i">
						<p class="circular t-blck t-16-px">{{ __('website.parkInner_foreign') }}</p>
						<div class="details">
							<span class="block prc">
								{!! Stuff::trans($park, 'fees_passes_3') !!}
							</span>
						</div>
					</div>
				@endif

				@if(!empty($park->fees_passes_4) && $park->fees_passes_4 != '<p><br></p>')
					<div class="fees_passes col-sm-3 icn group-i">
						<p class="circular t-blck t-16-px">{{ __('website.parkInner_combo') }}</p>
						<div class="details">
							<span class="block prc">
								{!! Stuff::trans($park, 'fees_passes_4') !!}
							</span>
						</div>
					</div>
				@endif
			</div>
		</div>
		<div class="col-md-6 no-padding">
			<div class="overview_map relative">
				<div class="height_range">
					<div class="box">
						<div class="dots">
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
							<span class="dot"></span>
						</div>
					</div>
				</div>
				<a href="{{ route('masterTrips', ['lang'=>App::getLocale(), 'park_id='.$park->id]) }}" class="planYourTrip">{{ __('website.park_plan_your_trip') }}</a>
				<?php $overview_json = json_encode($json); ?>
				<div class="map" id="park_in_page" data-json="{{ $overview_json or '[]' }}">
				</div>
			</div>	
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="adventure-nav drk-wht">
		<div class="no-padding my-adventure-cont">
			<div class="bg">
				<span class="y-title absolute">{{ __('website.adventures_banner_adventure') }}</span>
				<img src="/main/img/adventures-bg.jpg">
			</div>
			@foreach($adventures as $key => $my_adventure)
				@if($key == 0)
					<div class="dscrb active" data-show="{{ $my_adventure->id }}">
				@else
					<div class="dscrb" data-show="{{ $my_adventure->id }}">
				@endif
					<div class="article">
						<h2 class="b-title">
							<a style="color: inherit;" href="{{ route('siteAdventuresInner', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$my_adventure->id]) }}">
								{{ Stuff::trans($my_adventure, 'name') }}
							</a>
						</h2>
						<p class="t-16-px">{!! Stuff::trans($my_adventure, 'description') !!}</p>
					</div>
					<div class="avail">
						<h4 class="grn-title">{{ __('website.adventures_available_in') }}</h4>
						<div class="seasons">
							<?php $seasons = json_decode($my_adventure->seasons); ?>
							@foreach($seasons as $season)
								<span class="{{ $season }}">{{ __("website.adventures_$season") }}</span>
							@endforeach
						</div>
						<div>
							<a href="{{ route('masterTrips', ['lang'=>App::getLocale(), 'adventure_id'=>$my_adventure->id]) }}" class="pull-left m-20-t t-blck yllwBtn block">{{ __('website.adventures_available_trips') }}</a>
							<div class="clearfix"></div>
						</div>
						@if(count($my_adventure->imgs) > 0)
							<div class="sld_cont relative">
								<div class="l_arrw"></div>
								<div class="r_arrw"></div>
								<div class="photos owl-carousel owl-theme">
									@foreach($my_adventure->imgs as $adv_image)
										<div class="photo item">
											<div class="center aLink">
												@if(!empty($adv_image->link))
													<?php $imgLink = '/'.App::getLocale().$adv_image->link; ?>
													<a href="{{ $imgLink or '#!'}}">
														<h3>{{ Stuff::trans($adv_image, 'name') }}</h3>
													</a>
												@else
													<a class="preventDefault">
														<h3>{{ Stuff::trans($adv_image, 'name') }}</h3>
													</a>
												@endif
											</div>
											<?php $advImgUrl = json_decode($adv_image->img)[0]; ?>
											<img src="{{ asset("/files/$advImgUrl") }}">
										</div>
									@endforeach
								</div>
							</div>
						@endif
					</div>
				</div>
			@endforeach
			<div class="adv-nav devinne no-after">
				<div class="resBtn">
					<span class="top-line"></span>
					<span class="midd-line"></span>
					<span class="ex-midd-line"></span>
					<span class="bott-line"></span>
				</div>
				<ul>
					@foreach($adventures as $adventure)
						<li data-show="{{ $adventure->id }}">{{ Stuff::trans($adventure, 'name') }}</li>
					@endforeach
					{{--<li data-show="hiking">hiking</li>
					<li data-show="mountain">mountain climbing</li>--}}
				</ul>
			</div>
		</div>
	</div>
	<div class="things_to_see gry">
		<div class="web-container">
			<h2 class="y-title">{{ __('website.parkInner_to_see') }}</h2>
			<div class="row">
				@foreach($toSeeObjects as $key => $toSees)
					<h2 style="color: #fff;margin-bottom: 25px;">{{ __("website.park_toSee_type_$key") }}</h2>
					@foreach($toSees as $key => $toSee)
						<div class="col-md-6 col-lg-4">
							<div class="box">
								<div class="photo">
									<img src="{{ Stuff::imgUrl(json_decode($toSee->images)[0], true) }}">
								</div>
								<div class="dscrb">
									<a href="{{ route('siteToSeeInner', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$toSee->id]) }}" class="w-title t-30-px">{{ Stuff::trans($toSee, 'name') }}</a>
									<div style="overflow: hidden;max-height: 77px;">{!! Stuff::trans($toSee, 'description') !!}</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					@endforeach
					<div style="clear: both;"></div>
				@endforeach
			</div>
		</div>
	</div>
	<div class="pano_album">
		<div class="album_nav active" data-switch="photo">
			@foreach($photos as $photo)
				<div class="photo">
					<?php $my_photo = json_decode($photo->image)[0]; ?>
					<img src="{{ Stuff::imgUrl($my_photo, true) }}" data-title="{{ Stuff::trans($photo, 'name') }}">
				</div>
			@endforeach
		</div>
		<div class="album_nav f_vid" data-switch="video">
			@foreach($my_videos as $video_item)
				<div class="my-video">
					<div class="video-type" style="display: none;">{{ $video_item->type }}</div> 
					@if($video_item->type == 'video')
						<div class="playBtn"></div>
						<?php $my_vid = json_decode($video_item->video)[0]; ?>
						<video width="100%" height="100%" class="photo" vid-type="video">
							<source src="{{ Stuff::imgUrl($my_vid, true) }}" type="video/mp4">
						</video>
					@else
						<div class="playBtn"></div>
						<object data="{{ $video_item->youtube_link }}" width="100%" height="100%" controls="none"></object>
					@endif
				</div>
			@endforeach
		</div>
		<div class="show_photo relative">
			<div class="switch">
				<span class="activeBg"></span>
				<button class="switch-case left active-case" data-switch="photo">{{ __('website.memories_photo_gallery') }}</button>
				<button class="switch-case right" data-switch="video">{{ __('website.memories_video_gallery') }}</button>
			</div>
			<div class="content photo active" data-switch="photo">
				<!-- album_nav photos here -->
				<h2 class="album_title"></h2>
				<img src="">
			</div>
			<div class="content video" data-switch="video">
				<!-- album_nav videos here -->
			</div>
		</div>
	</div>
	<div class="park_map relative">
		{{-- <div class="map" data-lat="42.3173294" data-lng="42.2338442"></div> --}}
		<div class="how_to_get_here_map">
		<div id="mapDir" class="map" data-lat="{{ $park->coordinate_lat }}" data-lng="{{ $park->coordinate_long }}"></div>
		<div class="direction_window">
				<input id="origin-input" class="from_field controls" type="text" name="" placeholder="From Where?">
		</div>
	</div>
	</div>
	<div class="comm_seealso clearfix">
		<div class="col-md-6 p-20-l-r o-hidden drk-wht">
			<div class="reviews">
						<h2 class="blck-title m-20-t m-40-b">{{ __('website.around_my_comments') }}</h2>
						@if(isset($user))
							<div class="add_review">
								<div class="autor-img photo">
									<img src="{{ Stuff::imgUrl($user->img) }}">
								</div>
								<form>
									<div class="r-stars">
										<span class="circular t-14-px pull-left">{{ __('website.around_my_rate') }}:</span>
										<div class='rating-stars'>
											<ul class='stars'>
												<li class='star' data-value='1'>
													<span class="s-i"></span>
												</li>
												<li class='star' data-value='2'>
													<span class="s-i"></span>
												</li>
												<li class='star' data-value='3'>
													<span class="s-i"></span>
												</li>
												<li class='star' data-value='4'>
													<span class="s-i"></span>
												</li>
												<li class='star' data-value='5'>
													<span class="s-i"></span>
												</li>
											</ul>
										</div>
									</div>
									<textarea onkeyup="auto_grow(this)" placeholder="Write a comment" rows="1" resize="none" class="r-field write-comment"></textarea>
								</form>
							</div>
						@endif
						<div class="comments">
							@foreach($comments as $comment)
								<div class="comment">
									<div class="autor-img photo">
										<img src="{{ Stuff::imgUrl($comment->user->img) }}">
									</div>
									<div class="ttl">
										<div class='rating-stars'>
											<ul class='stars' data-ranked="{{ $comment->rating }}">
												@for($i = 1;$i <= 5; $i++)
													@if($i <= $comment->rating)
														<li class='star selected' data-value='{{ $i }}'>
															<span class="s-i"></span>
														</li>
													@else
														<li class='star' data-value='{{ $i }}'>
															<span class="s-i"></span>
														</li>
													@endif
												@endfor
											</ul>
										</div>
										<h4 class="autor-name verlagB t-blck t-20-px">{{ $comment->user->name }}</h4>
									</div>
									<div class="txt">
										<p>{!! $comment->description !!}</p>
									</div>
								</div>
							@endforeach
						</div>
					</div>
		</div>
		<div class="see_also col-md-6 gry p-20-l-r o-hidden">
			<div class="top_title">
				<h2 class="ex-w-title pull-left">{{ __('website.parkInner_nearby') }}</h2>
				{{-- <div class="switch pull-right">
					<span class="activeBg"></span>
					<button class="switch-case left active-case">nearby parks</button>
					<button class="switch-case right">similar parks</button>
				</div> --}}
			</div>
			<div class="also_parks">
				@foreach($nearbyParks as $nearby)
					@if($nearby->id != $park->id)
						<div class="park_detail">
							<div class="row">
								<div class="col-lg-5 col-md-12 col-sm-6">
									<div class="photo">
										<img src="{{ Stuff::imgUrl(json_decode($nearby->banner)[0], true) }}">
									</div>
								</div>
								<div class="col-lg-7 col-md-12 col-sm-6">
									<h3 class="t-wht t-20-px">{{ Stuff::trans($nearby, 'name') }}</h3>
									<p class="a_to_b georgia t-14-px">
										<span>{{ round($nearby->distance) }}</span>
										<span class="km">{{ __('website.parkInner_km') }}</span>
										<span>{{ __('website.parkInner_away') }}</span>
									</p>
									{{-- <p class="drive_dur t-14-px">
										<span>2</span>
										<span class="h">Hour</span>
										<span>Drive</span>
									</p> --}}
									<a href="{{ route('siteParksInner', ['lang'=>App::getLocale(), 'park'=>$nearby->alias]) }}" class="georgia">{{ __('website.parkInner_explore') }}</a>
								</div>
							</div>
						</div>
					@endif
				@endforeach
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&sensor=false&key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek"></script>
@endsection

@section('javascript')
	<script type="text/javascript">
		
		function switchVideoGallery(){
			var status = '{{ $park->show_video_gallery }}';
			console.log(status);
			if(status == 1){
				$('.switch-case.right').click();
			}
		}

		$(function(){
			setCarousels();
			fourBullets();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();

			adventureShow();
			parkPanoAlbum();
			ratingStars();
			initMap();
			switchVideoGallery();
			$('.bottom_nav ul li').eq(1).find('a').addClass('active');
		});

		function setMarkerListener(marker){
			marker.addListener('click', function(){
				var $dotDivs = $('.overview_map .dot');
				var dotNum = $dotDivs.length;
				var heightMax = 5000;
				var thisHeight = marker.mountHeight;
				var activeDotIndex = dotNum - Math.floor(thisHeight/heightMax * dotNum);

		        $dotDivs.removeClass('active');
		        $activeDotDiv = $dotDivs.eq(activeDotIndex);
		        $activeDotDiv.addClass('active');
	            $activeDotDiv.html(`<div class="val"><span class="number">`+marker.mountHeight+`</span><span class="unit">meters</span></div>`);
			});
		}

		function multipleHeightMarkerMap(element){
			function addListenerToMarker(marker, infowindow, map){
				marker.addListener('click', function(){
					infowindow.open(map, marker);
				});
			}
			var markerAttrs = JSON.parse(element.getAttribute('data-json'));

			

			if( markerAttrs.length == 0 ){

				var map = new google.maps.Map(element,{
					zoom: 7,
			        center: {lat:42.105628, lng:43.593089},
			        scrollwheel: false,
			        gestureHandling: 'greedy',
			        mapTypeId: 'roadmap'
				});

				return false;
			}

			var map = new google.maps.Map(element,{
				zoom: 9.5,
		        center: JSON.parse(markerAttrs[0].position)[0],
		        scrollwheel: false,
		        gestureHandling: 'greedy',
		        mapTypeId: 'roadmap'
			});

			var markers = [];
			
			for(var i = 0; i<markerAttrs.length; i++){
				var marker = new google.maps.Marker({
					position : JSON.parse(markerAttrs[i].position)[0],
					map:map,
					// icon: '/main/img/map-pin-by.png',
					icon: markerAttrs[i].ico,
					mountHeight: JSON.parse(markerAttrs[i].height)
				});
				markers.push(marker);
				var contentString = "<div class='InfoWindow'><a href=/{{App::getLocale()}}/site/{{ $park->alias  }}/adventuresInner/"+markerAttrs[i].id+">"+markerAttrs[i].name+"</a></div>";
    			var infowindow = new google.maps.InfoWindow({
    				content:contentString
    			});
    			addListenerToMarker(marker, infowindow, map);
			}

			for(var i = 0; i< markers.length; i++){
				setMarkerListener(markers[i]);
			}
		}

		function initMaps() {
			var element = document.querySelector('.overview_map .map');
			multipleHeightMarkerMap(element);
			$('.park_map .map').gMap(function(element, lat, lng){
    			lat = JSON.parse(lat);
    			lng = JSON.parse(lng)
    			var position = {lat: lat, lng: lng};
    			var map = new google.maps.Map(element,{
    				zoom: 7,
			        center: position,
			        scrollwheel: false,
			        gestureHandling: 'cooperative',
			        mapTypeControl:false,
			        mapTypeId: 'satellite'
    			});
    			var marker = new google.maps.Marker({
    				position : position,
    				map:map,
    				icon: '/main/img/map-pin.png',
    			});
    		});
		}
		initMaps();
		function commentController(){
			var commentAvailable = true;
			$(".write-comment").on("keypress",function(e) {
				
			    var key = e.keyCode;
			    if (key == 13) {

					if(commentAvailable == false)
						return;
					commentAvailable = false;

		    		var reviews = $('form .rating-stars .star.selected').length;
		    		var text = $('.write-comment').val();
		    		var type = 'park'; 
		    		var type_id = '{{ Request::segment(3) }}'; 
		    		var token = '{{ csrf_token() }}';
		    		var username = '{{ $user->name or '' }}'; 
		    		var userImg = '{{ $user->img or '' }}';

		    		var serial = {
		    			rating: reviews,
		    			_token: token,
		    			type: type,
		    			type_id: type_id,
		    			description: text,
		    		};

		    		$.ajax({
		    			url: '{{ route('commentAction', ['lang'=>App::getLocale()]) }}',
		    			method: 'post',
		    			data:serial,
		    			success:function(res){
		    				function clearContent(){
		    					$('.write-comment').val('');
		    					$('form .rating-stars .star.selected').removeClass('selected');
		    				}

		    				if(res == 'success'){
		    					var my_star = '';
		    					for(var i = 1;i <= 5;i++){
		    						if(i <= JSON.parse(reviews)){
		    						console.log(reviews);
			    						my_star += `
			    							<li class='star selected' data-value='`+i+`'>
												<span class="s-i"></span>
											</li>
			    						`;
		    						}else{
		    							my_star += `
			    							<li class='star' data-value='`+i+`'>
												<span class="s-i"></span>
											</li>
			    						`;
		    						}
		    					} 
		    					$('.comments .mCSB_container').prepend(`
		    						<div class="comment">
										<div class="autor-img photo">
											<img src="/main/img/`+userImg+`">
										</div>
										<div class="ttl">
											<div class='rating-stars'>
												<ul class='stars' data-ranked=`+reviews+`">
													`+my_star+`
												</ul>
											</div>
											<h4 class="autor-name verlagB t-blck t-20-px">`+username+`</h4>
										</div>
										<div class="txt">
											<p>`+text+`</p>
										</div>
									</div>


		    					`);
		    			
		    					clearContent();
		    				}else if(res == 'spam'){
		    					alert('{{ __('website.main_comment_spam') }}');
		    					clearContent();
		    				}else{
		    					var html = '';
		    					for(var i in res){
		    						html += res[i]+'\n';
		    					}
		    					alert(html);
		    				}
		    				
		    				commentAvailable = true;
		    			}
		    		});
			    }
			});
		}


		var lat = $('#mapDir').data('lat');
		var lng = $('#mapDir').data('lng');

		function initMap() {
		  var map = new google.maps.Map(document.getElementById('mapDir'), {
		    mapTypeControl: false,
		    center: {
		      lat: lat,
		      lng: lng
		    },
		    zoom: 11.5
		  });
		  var marker = new google.maps.Marker({
		    map: map,
		    icon: '/main/img/map-pin-by.png',
		    position: new google.maps.LatLng(lat, lng)
		  });
		  new AutocompleteDirectionsHandler(map);
		  // initFlightsGoogleMap();
		}

		/**
		 * @constructor
		 */
		function AutocompleteDirectionsHandler(map) {
		  this.map = map;
		  this.originPlaceId = null;
		  this.travelMode = 'DRIVING';
		  var originInput = document.getElementById('origin-input');
		  this.directionsService = new google.maps.DirectionsService;
		  this.directionsDisplay = new google.maps.DirectionsRenderer;
		  this.directionsDisplay.setMap(map);

		  var originAutocomplete = new google.maps.places.Autocomplete(
		    originInput, {
		      placeIdOnly: true
		    });

		  this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
		  this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
		}

		// Sets a listener on a radio button to change the filter type on Places
		// Autocomplete.
		AutocompleteDirectionsHandler.prototype.setupClickListener = function(id, mode) {
		  var radioButton = document.getElementById(id);
		  var me = this;
		  radioButton.addEventListener('click', function() {
		    me.travelMode = mode;
		    me.route();
		  });
		};

		AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
		  var me = this;
		  autocomplete.bindTo('bounds', this.map);
		  autocomplete.addListener('place_changed', function() {
		    var place = autocomplete.getPlace();
		    if (!place.place_id) {
		      window.alert("Please select an option from the dropdown list.");
		      return;
		    }
		    me.originPlaceId = place.place_id;
		    me.route();
		  });
		};

		AutocompleteDirectionsHandler.prototype.route = function() {
		  if (!this.originPlaceId) {
		    return;
		  }
		  var me = this;
		  this.directionsService.route({
		    origin: {
		      'placeId': this.originPlaceId
		    },
		    destination: new google.maps.LatLng(lat, lng),
		    travelMode: this.travelMode
		  }, function(response, status) {
		    if (status === 'OK') {
		      me.directionsDisplay.setDirections(response);
		    } else {
		      window.alert('Directions request failed due to ' + status);
		    }
		  });
		};


		commentController();
		if($(".tkt_info div").length == 0)
			$('.tkt_info').remove();

		$('body').on('click','.preventDefault',function(ev){
			ev.preventDefault();
		});

		function removeArrow(){
			setTimeout(function(){
				var length = $('.my-adventure-cont .dscrb.active .owl-item').length;

				if(length > 3){
					$('.my-adventure-cont .dscrb.active .l_arrw').show();
					$('.my-adventure-cont .dscrb.active .r_arrw').show();
				}
				else{
					$('.my-adventure-cont .dscrb.active .l_arrw').hide();
					$('.my-adventure-cont .dscrb.active .r_arrw').hide();
				}

				console.log(length);
			}, 200);
		}
		function arrowController(){
			
			removeArrow();

			$('.adv-nav li').click(function(){
				removeArrow();
			});
		}

		arrowController();
	</script>
@endsection


