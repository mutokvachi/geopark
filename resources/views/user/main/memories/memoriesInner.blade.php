@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="user_memory">
		<div class="web-container p-15-lr p-20-tb">
			<h2 class="blck-title t-blck m-30-b">{{ __('website.memories_memoriesInner') }}</h2>
			<div class="cont">
				<div class="row">
					<div class="col-md-3">
						<div class="photo m_img">
							<?php $banner = json_decode($memory->images)[0]; ?>
							<img src="{{ asset("main/img/$banner") }}">
						</div>
					</div>
					<div class="col-md-9">
						<h2 class="b-title t-30-px">{{ $memory->title }}</h2>
						<div class="text">
							<div class="user_avatar relative">
								<div class="avatar photo">
									<?php $my_avatar = $memory->user->img; ?>
									<img src="{{ asset("main/img/$my_avatar") }}">
								</div>
								<div class="name">
									<span class="block verlagB t-16-px t-blck t-up">{{ $memory->user->name }}</span>
								</div>
							</div>
							@if($memory->type == 'blog')
								<p class="georgia">{!! $memory->extra !!}</p>
							@elseif($memory->type == 'image')
								<?php $images = json_decode($memory->images); ?>
		                    	@foreach($images as $image)
			                        <img src="{{ asset("main/img/$image") }}" width="250" height="100">
		                        @endforeach
							@else
								<?php $videos = json_decode($memory->extra); ?>
								@foreach($videos as $video)
									<iframe width="350" height="250" src="{{ $video }}">
									</iframe>
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="memories_feed">
		<div class="web-container p-15-lr p-20-tb">
			<h2 class="blck-title t-blck m-30-b">{{ __('website.memories_other_memories') }}</h2>
			<div class="row">
				@foreach($recommends as $recommend)
					<div class="col-md-6 col-lg-4">
						<a href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=> Request::segment(3), 'id' => $recommend->id]) }}">
							<div class="m_cont">
								<div class="photo">
									<?php $rec_banner = json_decode($recommend->images)[0]; ?>
									<img src="{{ asset("main/img/$rec_banner") }}">
								</div>
								<div class="comment">
									<h4 class="w-title-ex t-30-px">{{ $recommend->title }}</h4>
									<div class="autor">
										<div class="name absolute t-right center-y">
											<span class="block verlagB t-16-px t-blck t-up">{{ $recommend->user->name }}</span>
										</div>
										<div class="avatar photo absolute">
											<?php $my_rec_avatar = $recommend->user->img; ?>
											<img src="{{ asset("main/img/$my_rec_avatar") }}">
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
				@endforeach
			</div>
		</div>
	</div>	
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		function myFavourite(){
			<?php $fav_action = $favourite == 0 ? $fav_action = 1 : $fav_action = 0; ?>
			var favouriteLink = '{{ route('favouritesControll', ['type'=>'memoriesInner','type_id'=>Request::segment(5), 'controll'=>$fav_action ]) }}';
			favouriteControll(favouriteLink);
		}
		
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();
			myFavourite();
			$('.bottom_nav ul li').eq(5).find('a').addClass('active');
		});
	</script>
@endsection


