@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	@if(count($memories) > 0)
		<div class="memories">
			<h2 class="memories-title ex-w-title">{{ __('website.siteMemories_memories') }}</h2>
			<div id="memories-slider" class="owl-carousel owl-theme">
				<?php 
					$memories_length = count($memories) > 4 ? 4 : count($memories); 
				?>
				@for($i = 0; $i < $memories_length; $i++)
					<?php $memory = $memories[$i]; ?>
					<div class="item">
						<div class="bg photo">
							<div class="autor gry">
								<div class="user_avatar t-up t-wht verlagB t-16-px">
									<div class="photo">
										<?php $avatar = $memory->user->img; ?>
										<img src="{{ asset("/main/img/$avatar") }}">
									</div>
									<p>{{ $memory->user->name }}</p>
								</div>
							</div>
							<div class="absolute">
								<h2 class="w-title ">
									<a href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$memory->id]) }}" style="color: inherit;">
										{{ $memory->title }}
									</a>
								</h2>
								@if($memory->type == "blog")
									<div class="text-cont">
										<p class="t-wht t-16-px devinne">{!! $memory->extra !!}</p>
									</div>
									<a class="_seeMore" href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$memory->id]) }}">{{ __('website.master_read_more') }}</a>
								@endif
							</div>
							<?php $bg = json_decode($memory->images)[0]; ?>
							<img src="{{ asset("/main/img/$bg") }}">
						</div>
					</div>
				@endfor
			</div>
			<div class="slide-loader" data-owl="#memories-slider">
				<span class="title">{{ __('website.siteMemories_featured_memories') }}</span>
				<div class="bullets">
					@for($i = 1; $i <= $memories_length; $i++)	
						<span class="crcl active">
							<span class="center">{{ $i }}</span>
							<span class="half">
								<span class="border clipped"></span>
							</span>
							<span class="border fixed"></span>
						</span>
					@endfor
				</div>
				{{-- <a href="#" class="devinne">{{ __('website.siteMemories_view_all') }}</a> --}}
			</div>
		</div>

		<div class="memories_feed">
			<div id="inner-memories-slider" class="owl-carousel owl-theme">
				<?php
					$left_memories = count($memories)-4;  
					$others = ceil($left_memories/6); 
				?>
				@if($left_memories > 0)
					@for($l = 0; $l < $others; $l++)
						<div class="item">
							<div class="web-container p-15-lr p-20-tb">
								<div class="row">
									<?php
										$counter = $l*6;
										// $newLength = $left_memories-(($l+4)*6) < 0 ? $left_memories : $counter+6;

										$newLength = $left_memories-($counter+6) < 0 ? $left_memories+4 : $counter+10;
									?>
									@for($f = $counter+4; $f < $newLength; $f++)
										<div class="col-md-6 col-lg-4">
											<a href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$memories[$f]]) }}">
												<div class="m_cont">
													<div class="photo">
														<?php $my_bg = json_decode($memories[$f]->images)[0]; ?>
														<img src="{{ asset("main/img/$my_bg") }}">
													</div>
													<div class="comment">
														<h4 class="w-title-ex t-30-px">{{ $memories[$f]->title }}</h4>
														<div class="autor">
															<div class="name absolute t-right center-y">
																<span class="block verlagB t-16-px t-blck t-up">{{ $memories[$f]->user->name }}</span>
																
															</div>
															<div class="avatar photo absolute">
																<?php $my_user_avatar = $memories[$f]->user->img; ?>
																<img src="{{ asset("main/img/$my_user_avatar") }}">
															</div>
														</div>
													</div>
												</div>
											</a>
										</div>
									@endfor
								</div>
							</div>
						</div>
					@endfor
				@endif
			</div>
			<div class="slide-loader center-x" data-owl="#inner-memories-slider">
				<div class="bullets">
					@for($z = 1; $z <= $others; $z++)	
						<span class="crcl b-gry active">
							<span class="center t-gry">{{ $z }}</span>
							<span class="half">
								<span class="border clipped"></span>
							</span>
							<span class="border fixed"></span>
						</span>
					@endfor
				</div>
			</div>
		</div>
	@endif


	<div class="inst_photos">
		<h2 class="blck-title p-15-tb p-15-lr">{{ __('website.siteMemories_instagram_feed') }}</h2>
		@foreach($instagrams as $insta)
			<div class="col-md-4 col-lg-3 no-padding photo_cont relative">
				<div class="autor_img absolute">
					{{-- <div class="photo">
						<img src="{{ $insta->hashtag_image }}">
					</div>
					<span class="name t-up p-20-l-r t-wht t-12-px verlagB">jason statham</span> --}}
					<div class="clearfix"></div>
					<div class="h_tags t-wht georgia">
					<span>#{{ $insta->hashtag }}</span>
				</div>
				</div>
				<span class="absolute insta-i"></span>
				<div class="photo absolute">
					<img src="{{ $insta->hashtag_image }}">
				</div>
			</div>
		@endforeach
		<div class="clearfix"></div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();
			favouriteControll();
			$('.bottom_nav ul li').eq(5).find('a').addClass('active');
		});
	</script>
@endsection


