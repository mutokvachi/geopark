@extends('user.layouts.master')

@section('head')
	@parent
	<style type="text/css">
		.hamburger_menu ul li:nth-child(4) a{
			color: #ffce00;
		}
	</style>
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="trips_section drk-wht">
		<div class="web-container p-15-lr">
			<h2 class="verlagB t-30-px m-40-t m-30-b t-up t-blck">{{ Stuff::trans($trip, 'name') }}</h2>
			<div class="trip_details">
				<div class="details">
					<div class="day_km">
						<div class="day">
							<span class="devinne t-50-px m-5-r">{{ $trip->trip_days }}</span>
							<span class="verlagL t-20-px">{{ __('website.trip_days') }}</span>
						</div>
						<div class="km">
							<span class="devinne t-50-px m-5-r">{{ $trip->trip_distance }}</span>
							<span class="verlagL t-20-px">{{ __('website.trip_distance') }}</span>
						</div>
					</div>
					<div class="block trip_row route">
						<span class="devinne t-30-px">{{ __('website.trip_route') }}:</span>
						<span class="from verlagL t-16-px t-cp">{{ Stuff::trans($trip, 'route') }}</span>
					</div>
					<div class="block trip_row hghlghts">
						<span class="devinne t-30-px">{{ __('website.trip_highlights') }}:</span>
						<span class="verlagL t-20-px t-cp add-spacing">{{ Stuff::trans($trip, 'highlights') }}</span>
					</div>
					{{-- <div class="block trip_row descr">
						<span class="devinne t-30-px">{{ __('website.trip_description') }}:</span>
						<span class="from verlagL t-16-px t-cp">შემთხვევითად გენერირებული ტექსტი</span>
					</div> --}}
					<div class="block trip_row season">
						<span class="devinne t-30-px">{{ __('website.trip_season') }}:</span>
						<div class="seasons">
							<?php $seasons = json_decode($trip->seasons); ?>
							@if(!empty($seasons))
								@foreach($seasons as $season)
									<span class="{{ $season }}">{{ __("website.adventures_$season") }}</span>
								@endforeach
							@endif
						</div>
					</div>
					<div class="block trip_row tour_level">
						<span class="devinne t-30-px">{{ __('website.trip_level') }}:</span>
						<span class="from verlagL t-16-px t-cp">{{ __("website.trip_level_$trip->level") }}</span>
					</div>
				</div>
				@if(Session::has('user') && isset($favourite))
					<div class="favouritesBtn favouriteBtn">
						@if($favourite == 1)
							<a href="" class="add active favourite-controller"><span class="txt">{{ __('website.main_add_favourites') }}</span></a>
						@else
							<a href="" class="add favourite-controller"><span class="txt">{{ __('website.main_add_favourites') }}</span></a>
						@endif
					</div>
				@else
					<div class="favouritesBtn favouriteBtn">
						<a href="{{ route('loginView', ['lang'=>App::getLocale()]) }}" class="add"><span class="txt">{{ __('website.main_add_favourites') }}</span></a>
					</div>
				@endif
			</div>
			<div class="clearfix"></div>
			@foreach($steps as $step)
				<div class="trip_dscrb t-blck">
					<div class="row">
						<div class="col-md-6">
							<h3 class="b-title">{{ __('website.trip_days') }}<span class="day">{{ $step->trip_days }}</span></h3>
							<p class="verlagL t-20-px t-cp dest">
								<span>{{ Stuff::trans($step, 'route') }}</span>
							</p>
							<p class="devinne">{!! Stuff::trans($step, 'description') !!}</p>
							<div class="exp_support">
								@if(count($step->adventures) > 0)
									<h3 class="devinne t-30-px">{{ __('website.trip_explore') }}</h3>
								@endif
								@foreach($step->adventures as $adv)
									@if(count($adv->adventure->imgs) > 0)
										<div class="col-sm-3 col-xs-6 ex-padding">
											<a href="{{ route('siteAdventuresInner', ['lang'=>App::getLocale(), 'park'=>$adv->adventure->park->alias, 'id'=>$adv->adventure->id]) }}" style="color: #222;">
												<div class="photo">
													<img src="{{ Stuff::imgUrl(json_decode($adv->adventure->imgs[0]->img)[0], true) }}">
												</div>
												<p class="verlagB t-16-px t-cp"> {{ Stuff::trans($adv->adventure, 'name') }}</p>
											</a>
										</div>
									@endif
								@endforeach
							</div>
						</div>
					<div class="col-md-6">
							<div class="r_attr">
								<div class="trans_details">
									<div class="box">
										<div class="info">
											<span class="num">{{ $step->trip_distance }}</span>
											<span class="km">{{ __('website.trip_distance') }}</span>
										</div>
										<div class="info">
											<span class="h">{{ $step->minutes }}</span>
										</div>
										<div class="tr_by">
											<?php $transports = json_decode($step->transport); ?>
											@foreach($transports as $transport)
												<span class="icon {{ $transport }}"></span>
											@endforeach
										</div>
									</div>
								</div>
								<div class="photo">
									<img src="{{ Stuff::imgUrl(json_decode($step->image)[0], true) }}">
								</div>
								<div class="download">
									<a href="{{ Stuff::imgUrl(json_decode($step->attachment)[0], true) }}">{{ __('website.trip_download_attachment') }}</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
	<div class="com_mem_section">
		<div class="col-md-6 p-15-l p-20-l-r o-hidden">
			<div class="reviews">
				<h2 class="blck-title m-20-t m-40-b">{{ __('website.around_my_comments') }}</h2>
				@if(isset($user))
					<div class="add_review">
						<div class="autor-img photo">
							<img src="{{ Stuff::imgUrl($user->img) }}">
						</div>
						<form>
							<div class="r-stars">
								<span class="circular t-14-px pull-left">{{ __('website.around_my_rate') }}:</span>
								<div class='rating-stars'>
									<ul class='stars'>
										<li class='star' data-value='1'>
											<span class="s-i"></span>
										</li>
										<li class='star' data-value='2'>
											<span class="s-i"></span>
										</li>
										<li class='star' data-value='3'>
											<span class="s-i"></span>
										</li>
										<li class='star' data-value='4'>
											<span class="s-i"></span>
										</li>
										<li class='star' data-value='5'>
											<span class="s-i"></span>
										</li>
									</ul>
								</div>
							</div>
							<textarea onkeyup="auto_grow(this)" placeholder="Write a comment" rows="1" resize="none" class="r-field write-comment"></textarea>
						</form>
					</div>
				@endif
				<div class="comments">
					@foreach($comments as $comment)
						<div class="comment">
							<div class="autor-img photo">
								<img src="{{ Stuff::imgUrl($comment->user->img) }}">
							</div>
							<div class="ttl">
								<div class='rating-stars'>
									<ul class='stars' data-ranked="{{ $comment->rating }}">
										@for($i = 1;$i <= 5; $i++)
											@if($i <= $comment->rating)
												<li class='star selected' data-value='{{ $i }}'>
													<span class="s-i"></span>
												</li>
											@else
												<li class='star' data-value='{{ $i }}'>
													<span class="s-i"></span>
												</li>
											@endif
										@endfor
									</ul>
								</div>
								<h4 class="autor-name verlagB t-blck t-20-px">{{ $comment->user->name }}</h4>
							</div>
							<div class="txt">
								<p>{!! $comment->description !!}</p>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>

		@if(count($memories) > 0)
			<div class="col-md-6 no-padding">
				<div class="memories">
					<h2 class="memories-title ex-w-title">{{ __('website.siteMemories_memories') }}</h2>
					<div id="memories-slider" class="owl-carousel owl-theme">
						@foreach($memories as $memory)
							<div class="item">
								<div class="bg photo">
									<div class="autor">
										<div class="user_avatar t-up t-wht verlagB t-16-px">
											<div class="photo">
												<?php $avatar = $memory->user->img; ?>
												<img src="{{ asset("/main/img/$avatar") }}">
											</div>
											<p>{{ $memory->user->name }}</p>
										</div>
									</div>
									<div class="absolute">
										<h2 class="w-title ">
											<a href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$memory->id]) }}" style="color: inherit;">
												{{ $memory->title }}
											</a>
										</h2>
										@if($memory->type == "blog")
											<p class="t-wht t-16-px devinne">{!! $memory->extra !!}</p>
										@endif
									</div>
									<?php $bg = json_decode($memory->images)[0]; ?>
									<img src="{{ asset("/main/img/$bg") }}">
								</div>
							</div>
						@endforeach
					</div>
					<div class="slide-loader" data-owl="#memories-slider">
						<span class="title">{{ __('website.siteMemories_featured_memories') }}</span>
						<div class="bullets">
							@for($i = 1; $i <= count($memories); $i++)	
								<span class="crcl active">
									<span class="center">{{ $i }}</span>
									<span class="half">
										<span class="border clipped"></span>
									</span>
									<span class="border fixed"></span>
								</span>
							@endfor
						</div>
						<a href="{{ route('siteMemoriesView', ['lang'=>App::getLocale(), 'park'=>Request::segment(3)]) }}" class="devinne">{{ __('website.siteMemories_view_all') }}</a>
					</div>
				</div>
			</div>
		@endif
	</div>
	<div class="similar_trips drk-wht">
		<div class="trips">
			<div class="web-container p-15-lr">
				<h2 class="verlagB t-30-px t-blck t-up m-20-b pull-left">{{ __('website.trip_similar') }}</h2>
				<div class="clearfix"></div>
				<div class="row">
					@foreach($similar_trips as $my_trip)
						<div class="col-md-6 m-15-tb">
							<div class="photo_map">
								<div class="photo">
									<img src="{{ Stuff::imgUrl(json_decode($my_trip->image)[0], true) }}">
								</div>
								<div class="trip_maps">
									<div class="map" data-lat="{{ $my_trip->coordinate_lat }}" data-lng="{{ $my_trip->coordinate_lng }}"></div>
								</div>
							</div>
							<div class="info_box">
								<a href="{{ route('siteTripsInner', ['lang'=>App::getLocale(), 'park'=> Request::segment(3),'id'=> $my_trip->id]) }}">
									<div class="name">
										<h2 class="w-title t-30-px">{{ Stuff::trans($my_trip, 'name') }}</h2>
									</div>
								</a>
								<div class="details">
									<div class="duration">
										<span class="cuant block">{{ $my_trip->trip_days }}</span>
										<span class="days block">{{ __('website.trip_days') }}</span>
									</div>
									<div class="events">
										<?php 
											$highlights = explode(',', Stuff::trans($my_trip, 'highlights'));
										?>
										@foreach($highlights as $highlight)
											<span class="block">{{ $highlight }}</span>
										@endforeach
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js_sources')
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek&callback=initMap">
    </script>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		function myFavourite(){
			<?php $fav_action = $favourite == 0 ? $fav_action = 1 : $fav_action = 0; ?>
			var favouriteLink = '{{ route('favouritesControll', ['type'=>'tripsInner','type_id'=>Request::segment(5), 'controll'=>$fav_action ]) }}';
			favouriteControll(favouriteLink);
		}

		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();

			ratingStars();
			myFavourite();
		});

		function initMap() {
    		$('.map').gMap(function(element, lat, lng){
    			lat = JSON.parse(lat);
    			lng = JSON.parse(lng)
    			var position = {lat: lat, lng: lng};
    			var map = new google.maps.Map(element,{
    				zoom: 5,
			        center: position,
			        scrollwheel: false,
			        gestureHandling: 'greedy',
			        disableDefaultUI: true,
			        mapTypeId: 'satellite'
    			});
    			var marker = new google.maps.Marker({
    				position : position,
    				map:map,
    				icon: '/main/img/map-pin.png',
    			});
    		});
		}

		function commentController(){
			var commentAvailable = true;
			$(".write-comment").on("keypress",function(e) {
				
			    var key = e.keyCode;
			    if (key == 13) {

					if(commentAvailable == false)
						return;
					commentAvailable = false;

		    		var reviews = $('form .rating-stars .star.selected').length;
		    		var text = $('.write-comment').val();
		    		var type = 'tripsInner'; 
		    		var type_id = '{{ Request::segment(5) }}'; 
		    		var token = '{{ csrf_token() }}';
		    		var username = '{{ $user->name or '' }}'; 
		    		var userImg = '{{ $user->img or '' }}';

		    		var serial = {
		    			rating: reviews,
		    			_token: token,
		    			type: type,
		    			type_id: type_id,
		    			description: text,
		    		};

		    		$.ajax({
		    			url: '{{ route('commentAction', ['lang'=>App::getLocale()]) }}',
		    			method: 'post',
		    			data:serial,
		    			success:function(res){
		    				function clearContent(){
		    					$('.write-comment').val('');
		    					$('form .rating-stars .star.selected').removeClass('selected');
		    				}

		    				if(res == 'success'){
		    					var my_star = '';
		    					for(var i = 1;i <= 5;i++){
		    						if(i <= JSON.parse(reviews)){
			    						my_star += `
			    							<li class='star selected' data-value='`+i+`'>
												<span class="s-i"></span>
											</li>
			    						`;
		    						}else{
		    							my_star += `
			    							<li class='star' data-value='`+i+`'>
												<span class="s-i"></span>
											</li>
			    						`;
		    						}
		    					} 
		    					$('.comments .mCSB_container').prepend(`
		    						<div class="comment">
										<div class="autor-img photo">
											<img src="/main/img/`+userImg+`">
										</div>
										<div class="ttl">
											<div class='rating-stars'>
												<ul class='stars' data-ranked=`+reviews+`">
													`+my_star+`
												</ul>
											</div>
											<h4 class="autor-name verlagB t-blck t-20-px">`+username+`</h4>
										</div>
										<div class="txt">
											<p>`+text+`</p>
										</div>
									</div>


		    					`);
		    			
		    					clearContent();
		    				}else if(res == 'spam'){
		    					alert('{{ __('website.main_comment_spam') }}');
		    					clearContent();
		    				}else{
		    					var html = '';
		    					for(var i in res){
		    						html += res[i]+'\n';
		    					}
		    					alert(html);
		    				}
		    				
		    				commentAvailable = true;
		    			}
		    		});
			    }
			});
		}
		commentController();

		function minutesToHour(){
			$('.trans_details .info .h').each(function(key, item){
				var mins = parseInt(item.innerHTML);
				var minutes = mins / 60;
				var hours = Math.floor(minutes);
				var minute = mins - (hours*60);
				item.innerHTML = hours+' {{ __('website.main_hour_h') }}/'+minute+' {{ __('website.main_hour_m') }}';
			});
		}
		minutesToHour();

		function addSpacing(){
			var html = $('.add-spacing').html();
			html = html.split(',');
			html = html.join(', ');
			$('.add-spacing').html(html);
		}
		addSpacing();

	</script>
@endsection


