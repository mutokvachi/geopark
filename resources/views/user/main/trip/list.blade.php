@extends('user.layouts.master')

@section('head')
	@parent
	<style type="text/css">
		.hamburger_menu ul li:nth-child(4) a{
			color: #ffce00;
		}
		.my-trip-item{
			display: none;
		}
		.my-trip-item.active{
			display: block;
		}
		.load-more{
			font-weight: bold;
			margin: 35px auto 0 auto;
		    background-color: #e53c40;
		    width: 160px;
		    height: 50px;
		    line-height: 50px;
		    color: #fff;
		    font-family: 'VerlagBook';
		    text-align: center;
		    text-transform: uppercase;
		    z-index: 3;
		    transition: .2s;
		    cursor: pointer;
		}
		.load-more:hover{
		    background-color: #d8383b;
		}
	</style>
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	{{-- @include('user.layouts.main.alertMenu') --}}
@endsection

@section('content')
	<div class="trips drk-wht">
		<div class="web-container p-15-lr">
			<h2 class="verlagB t-30-px t-blck t-up m-20-b pull-left">{{ __('website.trip_title_trips') }}</h2>
			<div class="trip_sorts">
				<div class="sortBtn">
					<div class="sort_label sort_placeholder my_filter" data-attr="park_id">{{ __('website.trip_sort_park') }}</div>
					<ul class="sort_list">
						<li data-target="park_id" target_info=""><span>{{ __('website.trip_clear_filter') }}</span></li>

						@foreach($sort_parks as $sort_park)
							<li data-target="park_id" target_info="{{ $sort_park->id }}"><span>{{ Stuff::trans($sort_park, 'name') }}</span></li>
						@endforeach
					</ul>
				</div>
				<div class="sortBtn">
					<div class="sort_label sort_placeholder my_filter" data-attr="trip_days">{{ __('website.trip_sort_days') }}</div>
					<ul class="sort_list">
						<li data-target="trip_days" target_info=""><span>{{ __('website.trip_clear_filter') }}</span></li>
						<li data-target="trip_days" target_info="1"><span>1</span></li>
						<li data-target="trip_days" target_info="2"><span>2</span></li>
						<li data-target="trip_days" target_info="3"><span>3</span></li>
						<li data-target="trip_days" target_info="4"><span>4</span></li>
						<li data-target="trip_days" target_info="5"><span>5</span></li>
						<li data-target="trip_days" target_info="6"><span>6</span></li>
						<li data-target="trip_days" target_info="7"><span>7</span></li>
					</ul>
				</div>

				<div class="sortBtn">
					<div class="sort_label sort_placeholder my_filter" data-attr="level">{{ __('website.trip_sort_level') }}</div>
					<ul class="sort_list">
						
						<li data-target="level" target_info=""><span>{{ __('website.trip_clear_filter') }}</span></li>
						<li data-target="level" target_info="1"><span>{{ __('website.trip_level_1') }}</span></li>
						<li data-target="level" target_info="2"><span>{{ __('website.trip_level_2') }}</span></li>
						<li data-target="level" target_info="3"><span>{{ __('website.trip_level_3') }}</span></li>
					</ul>
				</div>
				{{-- <div class="sortBtn">
					<div class="sort_label sort_placeholder">{{ __('website.trip_sort_activity') }}</div>
					<ul class="sort_list">
						<li><span>gulavi</span></li>
						<li><span>dros tareba</span></li>
						<li><span>bolo xinkali</span></li>
						<li><span>magar kacobis dge</span></li>
					</ul>
				</div>
				<div class="sortBtn">
					<div class="sort_label sort_placeholder">{{ __('website.trip_sort_season') }}</div>
					<ul class="sort_list">
						<li><span>spring</span></li>
						<li><span>summer</span></li>
						<li><span>autumn</span></li>
						<li><span>winter</span></li>
					</ul>
				</div>
				<div class="sortBtn">
					<div class="sort_label sort_placeholder">{{ __('website.trip_sort_region') }}</div>
					<ul class="sort_list">
						<li><span>tbilisi</span></li>
						<li><span>anaklia</span></li>
						<li><span>batumi</span></li>
						<li><span>kakheti</span></li>
					</ul>
				</div> --}}
			</div>
			<div class="clearfix"></div>
			<div class="row filter-place">
				@if(empty($trips))
					<h2 class="text-center text-danger">{{ __('website.trip_not_match') }}</h2>
				@endif
				@foreach($trips as $key => $trip)
					@if($filter === false)
						@if($key < 4)
							<div class="col-md-6 m-15-tb my-trip-item active">
						@else
							<div class="col-md-6 m-15-tb my-trip-item">
						@endif
					@else
						<div class="col-md-6 m-15-tb my-trip-item active">
					@endif
						<div class="photo_map">
							<div class="photo">
								<img src="{{ Stuff::imgUrl(json_decode($trip->image)[0], true) }}">
							</div>
							<div class="trip_maps">
								<div class="map createMap" data-lat="{{ $trip->coordinate_lat }}" data-lng="{{ $trip->coordinate_lng }}"></div>
							</div>
						</div>
						<div class="info_box">
							<a href="{{ route('siteTripsInner', ['lang'=>App::getLocale(), 'park'=> $trip->park->alias,'id'=> $trip->id]) }}">
								<div class="name">
									<h2 class="w-title t-30-px">{{ Stuff::trans($trip, 'name') }}</h2>
								</div>
							</a>
							<div class="details">
								<div class="duration">
									<span class="cuant block">{{ $trip->trip_days }}</span>
									<span class="days block">{{ __('website.trip_days') }}</span>
								</div>
								<div class="events">
									<?php 
										$highlights = explode(',', Stuff::trans($trip, 'highlights'));
									?>
									@foreach($highlights as $highlight)
										<span class="block">{{ $highlight }}</span>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
		@if($filter === false)
			<div class="load-more text-center" >{{ __('website.trip_load_more') }}</div>
		@endif
	</div>
	<div class="sortBy_data" style="display: none;">
		<div class="park_id"></div>
		<div class="trip_days"></div>
		<div class="level"></div>
		{{-- <div class="season_{{$lang}}"></div> --}}
		<div class="_token">{{ csrf_token() }}</div>
	</div>
@endsection

@section('js_sources')
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek&callback=initMap">
    </script>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			sortDropdown();

			// $('.bottom_nav ul li').eq(8).find('a').addClass('active');
		});

		function initMap() {
    		$('.map.createMap').gMap(function(element, lat, lng){
    			lat = JSON.parse(lat);
    			lng = JSON.parse(lng)
    			var position = {lat: lat, lng: lng};
    			var map = new google.maps.Map(element,{
    				zoom: 5,
			        center: position,
			        scrollwheel: false,
			        gestureHandling: 'greedy',
			        disableDefaultUI: true,
			        mapTypeId: 'satellite'
    			});
    			var marker = new google.maps.Marker({
    				position : position,
    				map:map,
    				icon: '/main/img/map-pin.png',
    			});
    		});
		}


		// Filter starts here
		function toTimestamp(strDate){
		   var datum = Date.parse(strDate);
		   return datum/1000;
		}

		var timeout = false;
		$('.my_filter').bind("DOMSubtreeModified",function(){
			clearTimeout(timeout);
			var myClass = $(this).attr('data-attr'); 
			var html = false;
			if(myClass == 'date'){
				html = $(this).html();
				html = html.split('/');
				html = html[2]+'-'+html[0]+'-'+html[1];
			}
			else{
				html = $(this).html();
				$(this).closest('.sortBtn').find('.sort_list li').each(function(key, item){
					if(item.querySelector('span').innerHTML == html){
						html = item.getAttribute('target_info');
						return; 
					}
				});
			}
			$('.sortBy_data .'+myClass).html(html);
			timeout = setTimeout(getFilter, 100);
		});

		function getFilter(){
			var serial = {};
			$('.sortBy_data div').each(function(key, item){
				var name = item.className;
				var prop = item.innerHTML;
				if(prop != '')
					serial[name] = prop;
			});

			$.ajax({
				url: '{{ route('tourFilter') }}',
				method: 'post',
				data: serial,
				success:function(res){
					$('.load-more').hide();
					if(res == '')
						$('.filter-place').html("<h2>{{ __('website.main_empty_section') }}</h2>");
					else{
						$('.filter-place').empty();
						var html = '';
						var counter = 0;
						for(var i in res){
							counter++;
							var lang = '{{ App::getLocale() }}' == 'ka' ? '' : '{{ '_'.App::getLocale() }}';
							var image = JSON.parse(res[i].image)[0];
							var highlights = res[i]["highlights"+lang+""];
							highlights = highlights.split(',');
							var highlight = '';
							for(var j in highlights){
								highlight += `<span class="block">`+highlights[j]+`</span>`;
							}

							html += `
								<div class="col-md-6 m-15-tb">
									<div class="photo_map">
										<div class="photo">
											<img src="/files/`+image+`">
										</div>
										<div class="trip_maps">
											<div class="map createMap" data-lat="`+res[i].coordinate_lat+`" data-lng="`+res[i].coordinate_lng+`"></div>
										</div>
									</div>
									<div class="info_box">
										<a href="/{{ App::getLocale() }}/site/`+res[i].park.alias+`/tripsInner/`+res[i].id+`">
											<div class="name">
												<h2 class="w-title t-30-px">`+res[i]["name"+lang+""]+`</h2>
											</div>
										</a>
										<div class="details">
											<div class="duration">
												<span class="cuant block">`+res[i].trip_days+`</span>
												<span class="days block">{{ __('website.trip_days') }}</span>
											</div>
											<div class="events">
												`+highlight+`
											</div>
										</div>
									</div>
								</div>
								
							`;
							if(counter == res.length){
								$('.filter-place').html(html);
								initMap();
							}
						}
					}
				}
			});
		}
		// Filter End


		var loadingPage = 0;
		$('.load-more').click(function(){
			loadingPage++;
			$('.createMap').removeClass('createMap');
			var serial = {_token: '{{ csrf_token() }}', page: loadingPage};
			$.ajax({
				url: '{{ route('tourShowMore') }}',
				method: 'post',
				data: serial,
				success:function(res){
						var html = '';
						var counter = 0;
						if(res.length < 4)
							$('.load-more').hide();

						for(var i in res){
							counter++;
							var lang = '{{ App::getLocale() }}' == 'ka' ? '' : '{{ '_'.App::getLocale() }}';
							var image = JSON.parse(res[i].image)[0];
							var highlights = res[i]["highlights"+lang+""];
							highlights = highlights.split(',');
							var highlight = '';
							for(var j in highlights){
								highlight += `<span class="block">`+highlights[j]+`</span>`;
							}

							html += `
								<div class="col-md-6 m-15-tb">
									<div class="photo_map">
										<div class="photo">
											<img src="/files/`+image+`">
										</div>
										<div class="trip_maps">
											<div class="map createMap" data-lat="`+res[i].coordinate_lat+`" data-lng="`+res[i].coordinate_lng+`"></div>
										</div>
									</div>
									<div class="info_box">
										<a href="/{{ App::getLocale() }}/site/`+res[i].park.alias+`/tripsInner/`+res[i].id+`">
											<div class="name">
												<h2 class="w-title t-30-px">`+res[i]["name"+lang+""]+`</h2>
											</div>
										</a>
										<div class="details">
											<div class="duration">
												<span class="cuant block">`+res[i].trip_days+`</span>
												<span class="days block">{{ __('website.trip_days') }}</span>
											</div>
											<div class="events">
												`+highlight+`
											</div>
										</div>
									</div>
								</div>
								
							`;
							if(counter == res.length){
								$('.filter-place').append(html);
								initMap();
							}
						}
				}
			});
		});

		function dayToDays(){
			var lang = '{{ App::getLocale() }}';
			
			if(lang == 'ka')
				return;
			
			$('.trips .details .duration .cuant').each(function(){
				if($(this).text() == 1){
					$(this).closest('.duration').find('.days').text('Day');
				} else{
					$(this).closest('.duration').find('.days').text('Days');
				}
			}); 

		}
		dayToDays();
	</script>
@endsection


