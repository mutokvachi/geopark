@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="memories_feed for_ev for_buy">
		<div class="web-container p-15-lr p-20-tb">
			<h2 class="blck-title t-blck m-30-b">{{ __('website.thing_thigs_to_buy') }}</h2>
			<div class="row">
				@foreach($things as $thing)
					<div class="col-md-6">
						<a href="{{ route('siteTobuyInner', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$thing->id]) }}">
							<div class="m_cont">
								<div class="photo">
									<?php $img = json_decode($thing->images)[0]; ?>
									<img src="{{ Stuff::imgUrl($img, true) }}">
								</div>
								<div class="comment">
									<h4 class="w-title-ex t-30-px">{{ Stuff::trans($thing, 'name') }}</h4>
									<div class="autor data">
										<ul class="verlagB t-blck t-up">
											<?php 
												$cats = explode(',', Stuff::trans($thing, 'type'));
											?>
											@foreach($cats as $cat)
												<li>{{ $cat }}</li>
											@endforeach
										</ul>
									</div>
								</div>
							</div>
						</a>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();

			$('.bottom_nav ul li').eq(8).find('a').addClass('active');
		});
	</script>
@endsection


