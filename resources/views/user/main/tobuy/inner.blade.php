@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="p_around_park drk-wht">	
		<div class="web-container p-15-lr p-20-tb">
			<h2 class="blck-title t-blck m-30-b">{{ __('website.thing_thigs_to_buy') }}</h2>
			<div class="row">
				<div class="col-md-3">
					<div class="place_view_slider">
						<div class="slider_img photo">
							<img src="{{ Stuff::imgUrl(json_decode($thing->images)[0], true) }}">
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="th_to_buy">
						<h2 class="t-blck devinne t-30-px m-15-tb">{{ Stuff::trans($thing, 'name') }}</h2>
						<ul class="verlagB t-blck t-up">
							<?php 
								$cats = explode(',', Stuff::trans($thing, 'type'));
							?>
							@foreach($cats as $cat)
								<li>{{ $cat }}</li>
							@endforeach
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="dscrb">
						@if(count($thing->shops) > 0)
							<h3 class="t-up verlagB t-blck t-20-px m-30-b">{{ __('website.thing_where_buy') }}</h3>
						@endif
						@foreach($thing->shops as $shop)
							<div class="place to_buy">
								<h2 class="t-blck devinne t-30-px t-cp">{{ Stuff::trans($shop, 'name') }}</h2>
								<div class='rating-stars'>
									<ul class='stars' data-ranked="$shop->rating">
										@for($i = 1; $i <= 5;$i++)
											@if($i <= $shop->rating)
												<li class='star selected' data-value='{{ $i }}'>
													<span class="s-i"></span>
												</li>
											@else
												<li class='star' data-value='{{ $i }}'>
													<span class="s-i"></span>
												</li>
											@endif
										@endfor
									</ul>
								</div>
								<div class="clearfix"></div>
								<div class="details">
									<div class="col-lg-4 col-md-12 m-15-tb no-padding o-hidden">
										<div class="t-16-px t-blck">
											<div class="col-sm-12 p-5-tb p-0-lr">
												<span class="verlagB t-up">{{ Stuff::trans($shop, 'address') }}</span>
											</div>
											<div class="col-sm-12 p-5-tb p-0-lr">
												<span class="verlagB t-up">{{ Stuff::trans($shop, 'region') }}</span>
											</div>
											<div class="col-sm-12 p-5-tb p-0-lr">
												{{-- <a href="#" class="f_on_map">find on map</a> --}}
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-12 m-15-tb no-padding o-hidden">
										<div class="t-16-px t-blck">
											<div class="col-sm-6 p-5-tb p-0-lr">
												<span class="verlagB t-up">{{ __('website.thing_hours') }}:</span>
											</div>
											<div class="col-sm-6 p-5-tb p-0-lr">
												<span class="devinne">{{ $shop->working_hours }}</span>
											</div>
										</div>
										<div class="t-16-px t-blck">
											<div class="col-sm-6 p-5-tb p-0-lr">
												<span class="verlagB t-up">{{ __('website.thing_type') }}:</span>
											</div>
											<div class="col-sm-6 p-5-tb p-0-lr">
												<span class="devinne">{{ Stuff::trans($shop, 'type') }}</span>
											</div>
										</div>
										<div class="t-16-px t-blck">
											<div class="col-sm-6 p-5-tb p-0-lr">
												<span class="verlagB t-up">{{ __('website.thing_phone') }}:</span>
											</div>
											<div class="col-sm-6 p-5-tb p-0-lr">
												<span class="devinne">{{ $shop->phone }}</span>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-12 m-15-tb no-padding o-hidden">
										<div class="t-16-px t-blck">
											<div class="col-sm-6 p-5-tb p-0-lr">
												<span class="verlagB t-up">{{ __('website.thing_email') }}:</span>
											</div>
											<div class="col-sm-6 p-5-tb p-0-lr">
												<span class="devinne">{{ $shop->email  }}</span>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="t-16-px t-blck">
											<div class="col-sm-6 p-5-tb p-0-lr">
												<span class="verlagB t-up">{{ __('website.thing_website') }}:</span>
											</div>
											<div class="col-sm-6 p-5-tb p-0-lr">
												<span class="devinne">{{ $shop->website }}</span>
											</div>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>	
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();

			$('.bottom_nav ul li').eq(8).find('a').addClass('active');
		});
	</script>
@endsection


