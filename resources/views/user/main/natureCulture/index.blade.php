@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="nature_album">
		<div class="nature_bg"></div>
		<div class="nature_photo">
			<span class="cls">×</span>
			<img src="">
		</div>
	</div>
	<div class="nat_cult wht">
		<div class="n_nav wht">
			<div class="switch m-0-auto">
				<span class="activeBg"></span>
				<button class="switch-case left active-case"><a style="display: block;padding: 10px 0px;" href="{{ route('siteNatureCulture', ['lang'=>App::getLocale(), 'park'=>Request::segment(3)]) }}">{{ __('website.master_nature_title') }}</a></button>
				<button class="switch-case right"><a style="display: block;padding: 10px 0px;" href="{{ route('siteNatureCulture', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'optional'=>'culture']) }}">{{ __('website.master_culture_title') }}</a></button>
			</div>
			<ul class="devinne t-25-px t-cp">
				@foreach($natures as $nature)
					<li data-show="{{ $nature->id }}">{{ Stuff::trans($nature, 'name') }}</li>
				@endforeach
			</ul>
		</div>
		@foreach($natures as $culture)
			<div class="content drk-wht" data-show="{{ $culture->id }}">
				<h2 class="b-title m-20-b t-cp">{{ Stuff::trans($culture, 'name') }}</h2>
				<p>{!! Stuff::trans($culture, 'upper_description') !!}</p>
				<div class="nat_slider">
					<div class="r_arrw"></div>
					<div class="row m-20-t photo_slider owl-carousel owl-theme">
						<?php $culture_imgs = json_decode($culture->images); ?>
						@foreach($culture_imgs as $img)
							<div class="col-md-12 item">
								<div class="photo sm">
									<img src="{{ Stuff::imgUrl($img, true) }}">
								</div>
							</div>
						@endforeach
					</div>
				</div>
				<p>{!! Stuff::trans($culture, 'lower_description') !!}</p>
			</div>
		@endforeach
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			natCultShow();
			resBtn();
			alertBox();
			natCultSlide();

			$('.bottom_nav ul li').eq(4).find('a').addClass('active');
			
			var type = '{{ $nature_type }}';
			if(type == 'culture')
				$('.switch-case.right').click();
			else
				$('.switch-case.left').addClass('active-case');
		});


	</script>
@endsection


