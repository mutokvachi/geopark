@extends('user.layouts.master')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/jquery.nstSlider.min.css') }}">
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="places_section drk-wht">
		<div class="web-container p-15-lr p-20-tb">
			<h2 class="blck-title t-blck m-30-b">{{ __('website.around_places_around') }}</h2>
			{{-- <div class="thr-switch">
				<button class="thrd active">{{ __('website.around_to_stay') }}</button>
				<button class="thrd">{{ __('website.around_to_eat') }}</button>
				<button class="thrd">{{ __('website.around_tourist_companies') }}</button>
			</div> --}}
			<div class="clearfix"></div>
		</div>
		<div class="filter_cont absolute">
			<div class="filter blck">
				<span class="cls"></span>
				<form method="GET" class="filter-form" action="{{ route('siteAroundPark', ['lang'=>App::getLocale(),'park'=>Request::segment(3)]) }}">
					<label class="t-up t-20-px t-wht verlagB m-15-b">{{ __('website.around_price_range') }}</label>
					<div class="price_range m-15-b">
						<div class="nstSlider m-10-b" data-range_min="0" data-range_max="500" data-cur_min="{{ $_GET['price-from'] or '0' }}" data-cur_max="{{ $_GET['price-to'] or '480' }}">
						    <div class="bar"></div>
						    <div class="leftGrip"></div>
						    <div class="rightGrip"></div>
						</div>
						<div class="leftLabel devinne t-16-px t-wht pull-left"><span class="m-5-r price-from"></span>Gel</div>
						<div class="rightLabel devinne t-16-px t-wht pull-right"><span class="m-5-r price-to"></span>Gel</div>
						<div class="clearfix"></div>
					</div>
					<input type="hidden" name="price-from">
					<input type="hidden" name="price-to">
					<input type="hidden" name="stars-from">
					<input type="hidden" name="stars-to">
					<label class="t-up t-20-px t-wht verlagB m-15-b">{{ __('website.around_rating') }}</label>
					<div class="rating_range m-30-b">
						<div class="nstSlider m-10-b" data-range_min="0" data-range_max="5" data-cur_min="{{ $_GET['stars-from'] or '0' }}" data-cur_max="{{ $_GET['stars-to'] or '5' }}">
						    <div class="bar"></div>
						    <div class="leftGrip"></div>
						    <div class="rightGrip"></div>
						</div>
						<div class="leftLabel devinne t-16-px t-wht pull-left"><span class="m-5-r stars-from"></span>{{ __('website.around_stars') }}</div>
						<div class="rightLabel devinne t-16-px t-wht pull-right"><span class="m-5-r stars-to"></span>{{ __('website.around_stars') }}</div>
						<div class="clearfix"></div>
					</div>
					<label class="t-up t-20-px t-wht verlagB m-15-b">{{ __('website.around_type') }}</label>
					@foreach($types as $key => $type)
						<div class="m-10-b">
							@if(isset($checkers[$type->id]))
						    	<input type="checkbox" checked name="types[{{ $type->id }}]" id="c-{{ $key }}" value="{{ $type->id }}">
				  			@else
				  				<input type="checkbox" name="types[{{ $type->id }}]" id="c-{{ $key }}" value="{{ $type->id }}">
				  			@endif
						    <label class="label-text devinne t-16-px" for="c-{{ $key }}">{{ Stuff::trans($type, 'type') }}</label>
					  	</div>
				  	@endforeach
				  	<div class="clearfix"></div>
				  	<br>
				  	<input type="button" class="yllwBtn filter-submitter" style="width: 100%;"  value="{{ __('website.around_filter_btn') }}">
				</form>
			</div>
			<div class="view_map">
				<div class="map" data-lat="42.3173294" data-lng="42.2338442"></div>
			</div>
		</div>
		<div class="content ex-margin  t-blck">
			@foreach($arounds as $around)
			<div class="box">
				<div class="place">
					<div class="photo">
						<a href="{{ route('siteAroundInner', ['lang'=>App::getLocale(),'park'=>Request::segment(3), 'id'=>$around->id]) }}">
							<?php $main_pic = json_decode($around->image)[0]; ?>
							<img src="{{ Stuff::imgUrl($main_pic, true) }}">
						</a>
					</div>
					<div class="details">
						<a href="{{ route('siteAroundInner', ['lang'=>App::getLocale(),'park'=>Request::segment(3), 'id'=>$around->id]) }}" class="devinne t-blck t-30-px">{{ Stuff::trans($around, 'name') }}</a>
						<div class='rating-stars'>
							<ul class='stars' data-ranked="3">
								@for($star = 1; $star <= 5; $star++)
									@if($star <= ceil($around->rating))
										<li class='star selected' data-value='{{ $star }}'>
											<span class="s-i"></span>
										</li>
									@else
										<li class='star' data-value='{{ $star }}'>
											<span class="s-i"></span>
										</li>
									@endif
								@endfor
							</ul>
						</div>
						<div class="price">
							<span class="t-16-px verlagB t-up">{{ __('website.around_price') }}:</span>
							<span class="t-16-px devinne">
								<span class="from">{{ $around->price }}</span>
								{{-- <span>-</span>
								<span class="to">120</span> --}}
								<span class="curr t-up">gel</span>
							</span>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>	
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	<script type="text/javascript" src="{{ asset('/main/js/jquery.nstSlider.min.js') }}"></script>
@endsection

@section('javascript')
	<script type="text/javascript">
		
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();
			favouriteControll();

			filterRange();
			aroundFilterBtn();
			$('.bottom_nav ul li').eq(7).find('a').addClass('active');
		});

		function getFilterData(){
			$('.filter-submitter').click(function(){
				var priceFrom = JSON.parse($('.price-from').html());
				var priceTo = JSON.parse($('.price-to').html());
				var starsFrom = JSON.parse($('.stars-from').html());
				var starsTo = JSON.parse($('.stars-to').html());
				$('input[name=price-from]').val(priceFrom);
				$('input[name=price-to]').val(priceTo);
				$('input[name=stars-from]').val(starsFrom);
				$('input[name=stars-to]').val(starsTo);
				$('.filter-form').submit();
			});
		}
		getFilterData();
	</script>
@endsection


