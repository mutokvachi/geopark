@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')

<style type="text/css">
  #origin-input{
    margin-top: 10px;
    border: 1px solid transparent;
    box-sizing: content-box;
    height: 40px;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 300px;
  }
</style>

	<div class="how_to_get_here_map">
		<div id="mapDir" class="map" data-lat="{{ $park->coordinate_lat }}" data-lng="{{ $park->coordinate_long }}"></div>
		<input id="origin-input" class="from_field controls" type="text" name="" placeholder="From Where?">
	</div>
	<div class="info_cont">
		@foreach($heres as $key => $here)
			@if($key % 2 == 0)
				<div class="info_section col-md-3 p-15-tb p-20-l-r">
			@else
				<div class="info_section col-md-3 drk-wht p-15-tb p-20-l-r">
			@endif
				<h3 class="grn-title">{{ Stuff::trans($here, 'name') }}</h3>
				<p class="georgia">{!! Stuff::trans($here, 'description') !!}</p>
			</div>
		@endforeach
	</div>
	<!-- <div class="choose_location_map">
		<div class="map"></div>
	</div> -->
  <div class="coors-center" style="display: none;">{{ json_encode($coors['center']) }}</div>
  <div class="coors-pins" style="display: none;">{{ json_encode($coors['pins']) }}</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&sensor=false&key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek"></script>
@endsection

@section('javascript')

	<script type="text/javascript">
		
		$(function(){
			$('.bottom_nav ul li').eq(3).find('a').addClass('active');
      setCarousels();
      fourBullets();
      // headerFontSize();
      setScrollBar();
      switchBtn();
      topSliderCounterBtn();
      resBtn();
      alertBox();
      initMap();

		});

		// function initMap() {
  //   		$('.map').gMap(function(element, lat, lng){
  //   			lat = parseInt(lat);
  //   			lng = parseInt(lng)
  //   			var position = {lat: lat, lng: lng};
  //   			var map = new google.maps.Map(element,{
  //   				zoom: 5,
		// 	        center: position,
		// 	        scrollwheel: false,
		// 	        gestureHandling: 'greedy',
		// 	        disableDefaultUI: true,
		// 	        mapTypeId: 'satellite'
  //   			});
  //   			var marker = new google.maps.Marker({
  //   				position : position,
  //   				map:map,
  //   				icon: '/main/img/map-pin.png',
  //   			});
  //   		});
		// }



function initFlightsGoogleMap(){
      function addListenerToMarker(marker, infowindow, map){
        marker.addListener('click', function(){
          infowindow.open(map, marker);
        });
      }
      var element = $('.choose_location_map .map').get()[0];
      var tbilisi = JSON.parse($('.coors-center').html());
        var pins = JSON.parse($('.coors-pins').html());
      
      var map = new google.maps.Map(element,{
        zoom: 4.5,
            center: tbilisi,
            mapTypeId: google.maps.MapTypeId.HYBRID,
      });
      var markers = [];
      var marker;
      for(var i in pins){
        var contentString = pins[i].info;
        var infowindow = new google.maps.InfoWindow({
          content: contentString
            });
        marker = new google.maps.Marker({
          position : pins[i],
          map:map,
          icon: '/main/img/map-pin-by.png',
        });
        addListenerToMarker(marker, infowindow, map);
        markers.push({
          marker:marker,
          type: "other"
        });
      }
      var tbilisiMarker = new google.maps.Marker({
        position:tbilisi,
        map:map,
        icon: '/main/img/map-pin-by.png', 
      });

      var geodesicPolies = [];

    function update(){
      for(var i in markers){
        geodesicPolies.push(new google.maps.Polyline({
            strokeColor: '#fcde13',
            strokeOpacity: 1.0,
            strokeWeight: 3,
            geodesic: true,
            map: map
        }));
        var path = [markers[i].marker.getPosition(), tbilisiMarker.getPosition()];
        geodesicPolies[i].setPath(path);
        var heading = google.maps.geometry.spherical.computeHeading(path[0], path[1]);
        var distance = google.maps.geometry.spherical.computeDistanceBetween(path[0], path[1]);
        console.log(distance.meters);
      }
    }
    update();
}

var lat = $('#mapDir').data('lat');
var lng = $('#mapDir').data('lng');

function initMap() {
  var map = new google.maps.Map(document.getElementById('mapDir'), {
    mapTypeControl: false,
    center: {
      lat: lat,
      lng: lng
    },
    zoom: 13
  });
  var marker = new google.maps.Marker({
    map: map,
    icon: '/main/img/map-pin-by.png',
    position: new google.maps.LatLng(lat, lng)
  });
  new AutocompleteDirectionsHandler(map);
  initFlightsGoogleMap();
}

/**
 * @constructor
 */
function AutocompleteDirectionsHandler(map) {
  this.map = map;
  this.originPlaceId = null;
  this.travelMode = 'DRIVING';
  var originInput = document.getElementById('origin-input');
  this.directionsService = new google.maps.DirectionsService;
  this.directionsDisplay = new google.maps.DirectionsRenderer;
  this.directionsDisplay.setMap(map);

  var originAutocomplete = new google.maps.places.Autocomplete(
    originInput, {
      placeIdOnly: true
    });

  this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
  this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
}



AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
  var me = this;
  autocomplete.bindTo('bounds', this.map);
  autocomplete.addListener('place_changed', function() {
    var place = autocomplete.getPlace();
    if (!place.place_id) {
      window.alert("Please select an option from the dropdown list.");
      return;
    }
    me.originPlaceId = place.place_id;
    me.route();
  });
};

AutocompleteDirectionsHandler.prototype.route = function() {
  if (!this.originPlaceId) {
    return;
  }
  var me = this;
  this.directionsService.route({
    origin: {
      'placeId': this.originPlaceId
    },
    destination: new google.maps.LatLng(lat, lng),
    travelMode: this.travelMode
  }, function(response, status) {
    if (status === 'OK') {
      me.directionsDisplay.setDirections(response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
};


		colorChange();
	</script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap" async defer></script> -->
@endsection


