@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="user_memory">
		<div class="web-container p-15-lr p-20-tb">
			<h2 class="blck-title t-blck m-30-b">{{ __('website.recommen_we_recommend') }}</h2>
			<div class="cont">
				<div class="row">
					<div class="col-md-3">
						<div class="photo m_img">
							<?php $banner = json_decode($recommendation->img)[0]; ?>
							<img src="{{ Stuff::imgUrl($banner, true) }}">
						</div>
					</div>
					<div class="col-md-9">
						<h2 class="b-title t-30-px">{{ Stuff::trans($recommendation, 'name') }}</h2>
						<div class="text">
							<div class="user_avatar relative">
								<div class="name">
									<span class="block verlagB t-16-px t-blck t-up">{{ $recommendation->date }}</span>
								</div>
							</div>
							<p class="georgia">{!! Stuff::trans($recommendation, 'description') !!}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="guide_cont">
		<div class="web-container p-15-lr p-20-tb t-blck">
			<div class="recommend m-40-b">
				<h2 class="verlagB t-up t-30-px m-20-b">{{ __('website.recommen_we_recommend_too') }} </h2>
				<div class="ex-row">
					@foreach($recommends as $recommend)
						<div class="col-lg-3 col-md-4 col-sm-6 p-5-lr m-40-b">
							<div class="box">
								<div class="photo">
									<img src="{{ Stuff::imgUrl(json_decode($recommend->img)[0], true) }}">
								</div>
								<div class="txt blck">
									<a href="{{ route('siteRecommendInnerView', ['lang'=>App::getLocale(), 'park'=>$recommend->park->alias, 'id'=>$recommend->id]) }}" class="devinne t-wht">{{ Stuff::trans($recommend, 'name') }}</a>
									<div class="clearfix"></div>
								</div>
								<div class="date t-16-px">
									<p class="devinne month t-cp">{{ $recommend->date }}</p>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();

			$('.bottom_nav ul li').eq(9).find('a').addClass('active');
		});
	</script>
@endsection


