@extends('user.layouts.master')

@section('head')
	@parent
	<style type="text/css">
		.guide_cont .guidelines ul{
			padding-left: 0px !important; 
		}
		.guide_cont .guidelines ul li::before{
			content: ' ';
		    color: transparent !important;
		    left: 0px !important;
		}
	</style>
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="guide_cont">
		<div class="web-container p-15-lr p-20-tb t-blck">
			<div class="row">
				<div class="col-md-8">
					<div class="guide m-40-b">
						<h2 class="verlagB t-up t-30-px m-20-b">{{ __('website.guide_guide') }}</h2>
						<p class="georgia">{!! Stuff::trans($park, 'guide') !!}</p>
					</div>
					@if(!empty(Stuff::trans($park, 'guideline')) && Stuff::trans($park, 'guideline') != '<p><br></p>')
					<div class="guidelines m-40-b">
						<h2 class="verlagB t-up t-30-px m-20-b">{{ __('website.guide_guideline') }}</h2>
						<ul class="georgia">
							<li>
								{!! Stuff::trans($park, 'guideline') !!}
							</li>
						</ul>
					</div>
					@endif
				</div>
				<div class="col-md-4">
					<div class="em_num t-wht">
						<span class="verlagB e_call">112</span>	
						<div class="e_t">
							<span class="block verlagB t-20-px t-up">{!! __('website.guide_emergency') !!}</span>	
						</div>
					</div>
					<div class="downloadables">
						<h2 class="verlagB t-up t-30-px m-20-b">{{ __('website.guide_download') }}</h2>
						@if(!empty($downloadables))
							@foreach($downloadables as $downloadable)
								<?php $my_file = json_decode($downloadable->file)[0]; ?>
								<a href="{{ asset("/files/$my_file") }}" class="d_link m-10-b block">{{ Stuff::trans($downloadable, 'name') }}</a>
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="recommend m-40-b">
				<h2 class="verlagB t-up t-30-px m-20-b">{{ __('website.guide_recommend') }} </h2>
				<div class="ex-row">
					@foreach($recommends as $recommend)
						<div class="col-lg-3 col-md-4 col-sm-6 p-5-lr m-40-b">
							<div class="box">
								<div class="photo">
									<img src="{{ Stuff::imgUrl(json_decode($recommend->img)[0], true) }}">
								</div>
								<div class="txt blck">
									<a href="{{ route('siteRecommendInnerView', ['lang'=>App::getLocale(), 'park'=>$recommend->park->alias, 'id'=>$recommend->id]) }}" class="devinne t-wht">{{ Stuff::trans($recommend, 'name') }}</a>
									<div class="clearfix"></div>
								</div>
								<div class="date t-16-px">
									<p class="devinne month t-cp">{{ $recommend->date }}</p>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();
			favouriteControll();
			$('.bottom_nav ul li').eq(9).find('a').addClass('active');
		});
	</script>
@endsection


