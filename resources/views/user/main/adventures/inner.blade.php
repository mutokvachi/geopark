@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="adventure_section">
		<div class="col-md-7 relative about drk-wht">
			<div class="photo absolute">
				<img src="/main/img/adventures-bg.jpg">
			</div>
			<div class="text_about">
				<h2 class="b-title m-20-b">{{ Stuff::trans($adventure, 'name') }}</h2>
				<p>{!! Stuff::trans($adventure, 'description') !!}
				</p>
			</div>
		</div>
		<div class="col-md-5 no-padding relative adventure_map">
			<div id="map" class="map"></div>
			<div class="coordinates_input" style="display: none;">{{ $adventure->coordinates }}</div>
		</div>
	</div>

	<div class="trips drk-wht">
		<div class="web-container p-15-lr">
			<h2 class="verlagB t-30-px t-blck t-up m-20-b pull-left">{{ __('website.trip_title_trips') }}</h2>
			<div style="clear: both;"></div>
			<div class="row filter-place">
				@if(empty($trips))
					<h2 class="text-center text-danger">{{ __('website.trip_not_match') }}</h2>
				@endif
				@foreach($trips as $trip)
					<div class="col-md-6 m-15-tb">
						<div class="photo_map">
							<div class="photo">
								<img src="{{ Stuff::imgUrl(json_decode($trip->image)[0], true) }}">
							</div>
							<div class="trip_maps">
								<div class="map" data-lat="{{ $trip->coordinate_lat }}" data-lng="{{ $trip->coordinate_lng }}"></div>
							</div>
						</div>
						<div class="info_box">
							<a href="{{ route('siteTripsInner', ['lang'=>App::getLocale(), 'park'=> $trip->park->alias,'id'=> $trip->id]) }}">
								<div class="name">
									<h2 class="w-title t-30-px">{{ Stuff::trans($trip, 'name') }}</h2>
								</div>
							</a>
							<div class="details">
								<div class="duration">
									<span class="cuant block">{{ $trip->trip_days }}</span>
									<span class="days block">{{ __('website.trip_days') }}</span>
								</div>
								<div class="events">
									<?php 
										$highlights = explode(',', Stuff::trans($trip, 'highlights'));
									?>
									@foreach($highlights as $highlight)
										<span class="block">{{ $highlight }}</span>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

	<div class="memories">
		<h2 class="memories-title ex-w-title">{{ __('website.siteMemories_memories') }}</h2>
		<div id="memories-slider" class="owl-carousel owl-theme">
			@foreach($memories as $memory)
				<div class="item">
					<div class="bg photo">
						<div class="autor gry">
							<div class="user_avatar t-up t-wht verlagB t-16-px">
								<div class="photo">
									<?php $avatar = $memory->user->img; ?>
									<img src="{{ asset("/main/img/$avatar") }}">
								</div>
								<p>{{ $memory->user->name }}</p>
							</div>
						</div>
						<div class="absolute">
							<h2 class="w-title ">
								<a href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$memory->id]) }}" style="color: inherit;">
									{{ $memory->title }}
								</a>
							</h2>
							@if($memory->type == "blog")
								<div class="text-cont">
									<p class="t-wht t-16-px devinne">{!! $memory->extra !!}</p>
								</div>
								<a class="_seeMore" href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$memory->id]) }}">{{ __('website.master_read_more') }}</a>
							@endif
						</div>
						<?php $bg = json_decode($memory->images)[0]; ?>
						<img src="{{ asset("/main/img/$bg") }}">
					</div>
				</div>
			@endforeach
		</div>
		<div class="slide-loader" data-owl="#memories-slider">
			<span class="title">{{ __('website.siteMemories_featured_memories') }}</span>
			<div class="bullets">
				@for($i = 1; $i <= count($memories); $i++)	
					<span class="crcl active">
						<span class="center">{{ $i }}</span>
						<span class="half">
							<span class="border clipped"></span>
						</span>
						<span class="border fixed"></span>
					</span>
				@endfor
			</div>
			{{-- <a href="#" class="devinne">{{ __('website.siteMemories_view_all') }}</a> --}}
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek&callback=initMap">
    </script>
@endsection

@section('javascript')
	<script type="text/javascript">
		

		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();
			adventureShow();

			$('.bottom_nav ul li').eq(2).find('a').addClass('active');
		});


	    function initMap() {
    		$('.map').gMap(function(element, lat, lng){
    			lat = parseInt(lat);
    			lng = parseInt(lng)
    			var position = {lat: lat, lng: lng};
    			var map = new google.maps.Map(element,{
    				zoom: 5,
			        center: position,
			        scrollwheel: false,
			        gestureHandling: 'greedy',
			        disableDefaultUI: true,
			        mapTypeId: 'satellite'
    			});
    			var marker = new google.maps.Marker({
    				position : position,
    				map:map,
    				icon: '/main/img/map-pin.png',
    			});
    		});
		}

		function setMultipleMarkers(elementId, coordinates){
		    var myCoordinates = JSON.parse(coordinates);
		    window.onload = function () {
		        var mapOptions = {
		            center: new google.maps.LatLng(41.7151, 44.8271),
		            zoom: 7,
		            mapTypeId: google.maps.MapTypeId.ROADMAP
		        };
		        var map = new google.maps.Map(document.getElementById(elementId), mapOptions);
		        
		        for(var i in myCoordinates){
		            var marker = new google.maps.Marker({
		                position: myCoordinates[i],
		                map: map
		            });
		        }
		    };
		}

		var coordinates = $('.coordinates_input').html();
		setMultipleMarkers('map', coordinates);

	</script>
@endsection


