@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="adventure-nav drk-wht">
		<div class="no-padding my-adventure-cont">
			<div class="bg">
				<span class="y-title absolute">{{ __('website.adventures_banner_adventure') }}</span>
				<img src="/main/img/adventures-bg.jpg">
			</div>
			@foreach($adventures as $key => $my_adventure)
				@if($key == 0)
					<div class="dscrb active" data-show="{{ $my_adventure->id }}">
				@else
					<div class="dscrb" data-show="{{ $my_adventure->id }}">
				@endif
					<div class="article">
						<h2 class="b-title">
							<a style="color: inherit;" href="{{ route('siteAdventuresInner', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$my_adventure->id]) }}">
								{{ Stuff::trans($my_adventure, 'name') }}
							</a>
						</h2>
						<p class="t-16-px">{!! Stuff::trans($my_adventure, 'description') !!}</p>
					</div>
					<div class="avail">
						<h4 class="grn-title">{{ __('website.adventures_available_in') }}</h4>
						<div class="seasons">
							<?php $seasons = json_decode($my_adventure->seasons); ?>
							@foreach($seasons as $season)
								<span class="{{ $season }}">{{ __("website.adventures_$season") }}</span>
							@endforeach
						</div>
						<div>
							<a href="{{ route('masterTrips', ['lang'=>App::getLocale(), 'adventure_id'=>$my_adventure->id]) }}" class="pull-left m-20-t t-blck yllwBtn block">{{ __('website.adventures_available_trips') }}</a>
							<div class="clearfix"></div>
						</div>
						@if(count($my_adventure->imgs) > 0)
							<div class="sld_cont relative">
								<div class="l_arrw"></div>
								<div class="r_arrw"></div>
								<div class="photos owl-carousel owl-theme">
									@foreach($my_adventure->imgs as $adv_image)
										<div class="photo item">
											<div class="center aLink">
												@if(!empty($adv_image->link))
													<?php $imgLink = '/'.App::getLocale().$adv_image->link; ?>
													<a href="{{ $imgLink or '#!'}}">
														<h3>{{ Stuff::trans($adv_image, 'name') }}</h3>
													</a>
												@else
													<a class="preventDefault">
														<h3>{{ Stuff::trans($adv_image, 'name') }}</h3>
													</a>
												@endif
											</div>
											<?php $advImgUrl = json_decode($adv_image->img)[0]; ?>
											<img src="{{ asset("/files/$advImgUrl") }}">
										</div>
									@endforeach
								</div>
							</div>
						@endif
					</div>
				</div>
			@endforeach
			<div class="adv-nav devinne no-after">
				<div class="resBtn">
					<span class="top-line"></span>
					<span class="midd-line"></span>
					<span class="ex-midd-line"></span>
					<span class="bott-line"></span>
				</div>
				<ul>
					@foreach($adventures as $adventure)
						<li data-show="{{ $adventure->id }}">{{ Stuff::trans($adventure, 'name') }}</li>
					@endforeach
					{{--<li data-show="hiking">hiking</li>
					<li data-show="mountain">mountain climbing</li>--}}
				</ul>
			</div>
		</div>
	</div>

	@if(count($memories) > 0)
		<div class="memories">
			<h2 class="memories-title ex-w-title">{{ __('website.siteMemories_memories') }}</h2>
			<div id="memories-slider" class="owl-carousel owl-theme">
				@foreach($memories as $memory)
					<div class="item">
						<div class="bg photo">
							<div class="autor gry">
								<div class="user_avatar t-up t-wht verlagB t-16-px">
									<div class="photo">
										<?php $avatar = $memory->user->img; ?>
										<img src="{{ asset("/main/img/$avatar") }}">
									</div>
									<p>{{ $memory->user->name }}</p>
								</div>
							</div>
							<div class="absolute">
								<h2 class="w-title ">
									<a href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$memory->id]) }}" style="color: inherit;">
										{{ $memory->title }}
									</a>
								</h2>
								@if($memory->type == "blog")
									<div class="text-cont">
										<p class="t-wht t-16-px devinne">{!! $memory->extra !!}</p>
									</div>
									<a class="_seeMore" href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>Request::segment(3), 'id'=>$memory->id]) }}">{{ __('website.master_read_more') }}</a>
								@endif
							</div>
							<?php $bg = json_decode($memory->images)[0]; ?>
							<img src="{{ asset("/main/img/$bg") }}">
						</div>
					</div>
				@endforeach
			</div>
			<div class="slide-loader" data-owl="#memories-slider">
				<span class="title">{{ __('website.siteMemories_featured_memories') }}</span>
				<div class="bullets">
					@for($i = 1; $i <= count($memories); $i++)	
						<span class="crcl active">
							<span class="center">{{ $i }}</span>
							<span class="half">
								<span class="border clipped"></span>
							</span>
							<span class="border fixed"></span>
						</span>
					@endfor
				</div>
				{{-- <a href="#" class="devinne">{{ __('website.siteMemories_view_all') }}</a> --}}
			</div>
		</div>
	@endif
	<div class="inst_photos">
		<h2 class="blck-title p-15-tb p-15-lr">{{ __('website.siteMemories_instagram_feed') }}</h2>
		@foreach($instagrams as $insta)
			<div class="col-md-4 col-lg-3 no-padding photo_cont relative">
				<div class="autor_img absolute">
					{{-- <div class="photo">
						<img src="{{ $insta->hashtag_image }}">
					</div>
					<span class="name t-up p-20-l-r t-wht t-12-px verlagB">jason statham</span> --}}
					<div class="clearfix"></div>
					<div class="h_tags t-wht georgia">
					<span>#{{ $insta->hashtag }}</span>
				</div>
				</div>
				<span class="absolute insta-i"></span>
				<div class="photo absolute">
					<img src="{{ $insta->hashtag_image }}">
				</div>
			</div>
		@endforeach
		<div class="clearfix"></div>
	</div>			
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		

		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();
			adventureShow();
			// ContTitleSwitcher();
			$('.bottom_nav ul li').eq(2).find('a').addClass('active');
		});

		$('body').on('click','.preventDefault',function(ev){
			ev.preventDefault();
		});
		function removeArrow(){
			setTimeout(function(){
				var length = $('.my-adventure-cont .dscrb.active .owl-item').length;

				if(length > 3){
					$('.my-adventure-cont .dscrb.active .l_arrw').show();
					$('.my-adventure-cont .dscrb.active .r_arrw').show();
				}
				else{
					$('.my-adventure-cont .dscrb.active .l_arrw').hide();
					$('.my-adventure-cont .dscrb.active .r_arrw').hide();
				}

			}, 200);
		}
		function arrowController(){
			
			removeArrow();

			$('.adv-nav li').click(function(){
				removeArrow();
			});
		}

		arrowController();

	</script>
@endsection


