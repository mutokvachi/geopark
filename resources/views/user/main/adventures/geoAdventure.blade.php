@extends('user.layouts.master')

@section('head')
	@parent
	<style type="text/css">
		.hamburger_menu ul li:nth-child(3) a{
			color: #ffce00;
		}
	</style>
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	{{-- @include('user.layouts.main.alertMenu') --}}
@endsection

@section('content')
	<div class="adventure-nav drk-wht geo_adventures">
		<div class="no-padding my-adventure-cont">
			<div class="bg">
				<span class="y-title absolute">{{ __('website.adventures_banner_adventure') }}</span>
				<img src="/main/img/adventures-bg.jpg">
			</div>
			@foreach($adventures as $key => $my_adventure)
				@if($key == 0)
					<div class="dscrb active" data-show="{{ $my_adventure->id }}">
				@else
					<div class="dscrb" data-show="{{ $my_adventure->id }}">
				@endif
					<div class="coordinates_input" style="display: none;">
						{{ $my_adventure->coordinates }}
					</div>

					<div class="map-coordinates" style="display: none;">{{ json_encode($json) }}</div>

					<script type="text/javascript">
						var test = document.querySelector('.map-coordinates').innerHTML;
						var adventureMap = JSON.parse(test);// [
						// {'title':'ხარაგაული', "lat":41.87921717672164,"lng":43.37249067625112, 'url':'#linki', 'activity':'climbing,show', 'destination':'xaragauli', 'season': 'summer,autumn,winter','ico':'/main/img/adventure-icons/2.png',},
						// {'title':'მესტია', "lat":42.386951440524854,"lng":45.3955078125, 'url':'#linki', 'activity':'kateri,curva', 'destination':'svaneti', 'season': 'summer,autumn','ico':'/main/img/adventure-icons/3.png'},
						// {'title':'ამბროლაური', "lat":41.89485272300788,"lng":46.3306633811236, 'url':'#linki', 'activity':'kateri', 'destination':'racha', 'season': 'summer,autumn,winter','ico':'/main/img/adventure-icons/1.png'},
						// {'title':'ონი', "lat":41.66162721430806,"lng":41.87713623046875, 'url':'#linki', 'activity':'show', 'destination':'racha', 'season': 'summer,autumn,winter','ico':'/main/img/adventure-icons/7.png'},
						// {'title':'ბარაკონი', "lat":41.465263117004,"lng":41.83935610031381, 'url':'#linki', 'activity':'climbing,curva', 'destination':'imereti', 'season': 'summer,winter','ico':'/main/img/adventure-icons/6.png'},
						// {'title':'კანიონი', "lat":41.18736394440827,"lng":46.47197355610126, 'url':'#linki', 'activity':'kateri,curva', 'destination':'imereti', 'season': 'summer','ico':'/main/img/adventure-icons/4.png'},
						// {'title':'იმერეთი', "lat":42.12177581863882,"lng":41.85800426246101, 'url':'#linki', 'activity':'show', 'destination':'imereti', 'season': 'summer,autumn,spring','ico':'/main/img/adventure-icons/5.png'},
						// {'title':'ოკაცეს კანიონი', "lat":41.86649282301993,"lng":44.9560546875, 'url':'#linki', 'activity':'climbing,navi', 'destination':'imereti', 'season': 'summer,autumn,spring','ico':'/main/img/adventure-icons/9.png'}
						// ];

					</script>
					<div class="article">
						<h2 class="b-title">{{ Stuff::trans($my_adventure, 'name') }}</h2>
						<p class="t-16-px">{!! Stuff::trans($my_adventure, 'description') !!}.</p>
					</div>
					<div class="avail">
						<h4 class="blck-title t-20-px m-10-b">{{ __('website.adventures_available_in') }}</h4>
							<div class="sortBtn">
								<div class="sort_label sort_placeholder sort-parks-adventure" data-sort="{{ $my_adventure->name }}">{{ __('website.memories_Park') }}</div>
								<ul class="sort_list my-parks-sort">
									{{-- here goes javascript --}}
								</ul>
							</div>
						<div>
							{{-- <a href="#" class="pull-left m-20-t t-blck yllwBtn block">available trips</a> --}}
							<div class="clearfix"></div>
						</div>
						@if(count($my_adventure->imgs) > 0)
							<div class="sld_cont relative">
								<div class="l_arrw"></div>
								<div class="r_arrw"></div>
								<div class="photos owl-carousel owl-theme">
									@foreach($my_adventure->imgs as $adv_image)
										<div class="photo item">
											<div class="center aLink">
												@if(!empty($adv_image->link))
													<?php $imgLink = '/'.App::getLocale().$adv_image->link; ?>
													<a href="{{ $imgLink or '#!'}}">
														<h3>{{ Stuff::trans($adv_image, 'name') }}</h3>
													</a>
												@else
													<a class="preventDefault">
														<h3>{{ Stuff::trans($adv_image, 'name') }}</h3>
													</a>
												@endif
											</div>
											<?php $advImgUrl = json_decode($adv_image->img)[0]; ?>
											<img src="{{ asset("/files/$advImgUrl") }}">
										</div>
									@endforeach
								</div>
							</div>
						@endif
					</div>
				</div>
			@endforeach
			<div class="adv-nav devinne no-after">
				<div class="resBtn">
					<span class="top-line"></span>
					<span class="midd-line"></span>
					<span class="ex-midd-line"></span>
					<span class="bott-line"></span>
				</div>
				<ul>
					@foreach($adventures as $adventure)
						<li data-show="{{ $adventure->id }}">{{ Stuff::trans($adventure, 'name') }}</li>
					@endforeach
					{{--<li data-show="hiking">hiking</li>
					<li data-show="mountain">mountain climbing</li>--}}
				</ul>
			</div>
		</div>
	</div>
	<div class="adventure_map">
		<div class="direction_window _ex">
			<form method="get">
				<select class="from_field" name="name">
					<option value="">{{ __('website.trip_sort_activity') }}</option>
					@foreach($grouped_adventures as $group_adventure)
						@if(isset($filter['name']) && $filter['name'] == $group_adventure[0]->name)
							<option value="{{ $group_adventure[0]->name }}" class="sort-filter-active">{{ Stuff::trans($group_adventure[0], 'name') }}</option>
						@else
							<option value="{{ $group_adventure[0]->name }}">{{ Stuff::trans($group_adventure[0], 'name') }}</option>
						@endif
					@endforeach
				</select>	
				<select class="from_field" name="park_id">
					<option value="">{{ __('website.trip_sort_park') }}</option>
					@foreach($grouped_destinations as $group_destination)
						@if(isset($filter['park_id']) && $filter['park_id'] == $group_destination->id)
							<option value="{{ $group_destination->id }}" class="sort-filter-active">{{ Stuff::trans($group_destination, 'name') }}</option>
						@else
							<option value="{{ $group_destination->id }}">{{ Stuff::trans($group_destination, 'name') }}</option>
						@endif
					@endforeach
				</select>	
				<select class="from_field" name="seasons">
					<option value="">{{ __('website.trip_sort_season') }}</option>
					@if(isset($filter['season']))
						<option value="{{ $filter['season'] }}" class="sort-filter-active">{{ __("website.adventures_".$filter['season']) }}</option>
					@endif
					<option value="summer">{{ __('website.adventures_summer') }}</option>
					<option value="winter">{{ __('website.adventures_winter') }}</option>
					<option value="autumn">{{ __('website.adventures_autumn') }}</option>
					<option value="spring">{{ __('website.adventures_spring') }}</option>
				</select>			
				<button class="yllwBtn" type="submit">{{ __('website.main_search') }}</button>
			</form>
		</div>
		<div id="map" class="map"></div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
<script type="text/javascript" src="{{ asset('/main/js/markerclusterer.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek">
    </script>
@endsection

@section('javascript')
	<script type="text/javascript">
		function scrollToMap(){
			var href = window.location.href.split('?'); 
			if(href[1]){
				$('html, body').animate({
			        scrollTop: $("#map").offset().top
			    }, 0);
			}
		}

		function clickOnCurrent(){
			var segment = '{{ Request::segment(4) }}';
			if(segment.length > 0)
				$('.adv-nav li[data-show='+segment+']').click();
		}

		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			sortDropdown();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();
			adventureShow();

			scrollToMap();
			clickOnCurrent();
			// $('.bottom_nav ul li').eq(2).find('a').addClass('active');
		});


		// function initMap(){
  //       	return;
	 //    }
		// function setMultipleMarkers(elementId, coordinates){
		//     var myCoordinates = JSON.parse(coordinates);
		//     window.onload = function () {
		//         var mapOptions = {
		//             center: new google.maps.LatLng(41.7151, 44.8271),
		//             zoom: 7,
		//             mapTypeId: google.maps.MapTypeId.ROADMAP
		//         };
		//         var map = new google.maps.Map(document.getElementById(elementId), mapOptions);
		        
		//         for(var i in myCoordinates){
		//             var marker = new google.maps.Marker({
		//                 position: myCoordinates[i],
		//                 map: map,
		//                 icon: '/main/img/map-pin.png',
		//             });
		//         }
		//     };
		// }

		// var coordinates = $('.dscrb.active .coordinates_input').html();
		// setMultipleMarkers('map', coordinates);



		function initMap() {

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 8,
                center: {
                    lat: 42.077626,
                    lng: 43.719692
                }
            });


            var infoWin = new google.maps.InfoWindow({
                
            });
            // Add some markers to the map.
            // Note: The code uses the JavaScript Array.prototype.map() method to
            // create an array of markers based on a given "locations" array.
            // The map() method here has nothing to do with the Google Maps API.
            var markers = locations.map(function(location, i) {
                var marker = new google.maps.Marker({
                    icon: adventureMap[i]['ico'],
                    position: location
                });

                var clickFunc = function(evt) {
                    var div = document.createElement('a');
                    div.setAttribute('href', location.url);
                    div.setAttribute('class','mappInfoWindows_a');
                    // div.setAttribute('data-url',location.url);
                    // var span = document.createElement('span');
                    // var h2 = document.createElement('h2');
                    // var img = document.createElement('img');
                    // var imgDiv = document.createElement('div');
                    // imgDiv.setAttribute('class','mappInfoimgbox');

                    // h2.innerText = location.title;
                    // img.src = location.img;

                    // imgDiv.appendChild(img);

                    // span.appendChild(h2);
                    // div.appendChild(imgDiv);
                    // div.appendChild(span);
                    div.innerText = location['title'];
                    div.style.color = 'black'

                    infoWin.setContent(div);
                    // console.log(location.info)
                    infoWin.open(map, marker);
                };
                google.maps.event.addListener(marker, 'click', clickFunc);
                return marker;
            });
            // console.log(markers);
            // infoWin.open(map, markers[1]);

            // markerCluster.setMarkers(markers);
            // Add a marker clusterer to manage the markers.
            var markerCluster = new MarkerClusterer(map, markers, {
                imagePath: "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m"
            });

            // window.setTimeout(function () {
            //     var placed_pins = $(".gmnoprint");
            //     // console.log(placed_pins);
            //     var pin_count = placed_pins.length - 6;
            //     if (pin_count >= 0) {
            //         var rand_num = Math.floor(Math.random() * pin_count);
            //         placed_pins.eq(rand_num).trigger("click");
            //     }
            // }, 1200);


        }
        var locations = adventureMap;


        google.maps.event.addDomListener(window, "load", initMap);


		$('.direction_window .yllwBtn').click(function(e){
			// e.preventDefault();
			locations = adventureMap2;
			google.maps.event.addDomListener(window, "load", initMap);
		});

		//  var JSON_Parse = {
		//     length: 0,
		//     activity: [],
		//     destination: [],
		//     season: [],
		//     process: function(data){
		//         this.length = data.length;
		        
		//         for(var i = 0; i < this.length; i++){
		//             this.activity.push(data[i].activity);
		//             this.destination.push(data[i].destination);
		//             this.season.push(data[i].season);
		//         }
		//     },
		//     research: function(where,element){
		//         var regex = new RegExp( element, 'g' );
		//         var lngth = this[where].length;
		//         for( var i = 0; i < lngth; i++ ){
		//             if( this[where][i].match(regex) != null ){
		//                 console.log(JSON[i].title)
		//             }
		//         }
		//     }
		// }


		// JSON_Parse.process(JSON);
		// JSON_Parse.research('activity','kateri');

	$('.sort-parks-adventure').click(function(){
		var that = $(this).closest('.sortBtn').find('.my-parks-sort');
		var serial = {lang: '{{ App::getLocale() }}', _token: '{{ csrf_token() }}', name: $(this).attr('data-sort')};
		$.ajax({
			url: '{{ route('adventureSortPark') }}',
			method: 'post',
			data:serial,
			success:function(res){
				var html = '';
				var lang = '{{ App::getLocale() }}';
				for(var i in res){
					if(lang == 'ka')
						var name = res[i].name;
					else
						var name = res[i]['name_'+lang];

					html += `
						<li>
							<span data-linkTo="/{{ App::getLocale() }}/site/`+res[i].alias+`">
								`+name+`
							</span>
						</li>						
					`;
				}

				if(that.find('li').length == 0){
					that.html(html);
					$('.my-parks-sort').mCustomScrollbar({
				        scrollEasing:'linear',
				        autoHideScrollbar:true,
				        scrollInertia:300,
				    }); 
				}else{
					return;
				}
			}
		});
		
	});


	$('body').on('click','.my-parks-sort li',function(){
		var linkTo = $(this).find('span').attr('data-linkTo');
		window.location.href = linkTo;
	});

	$('body').on('click','.preventDefault',function(ev){
		ev.preventDefault();
	});
	
	function removeArrow(){
		setTimeout(function(){
			var length = $('.my-adventure-cont .dscrb.active .owl-item').length;

			if(length > 3){
				$('.my-adventure-cont .dscrb.active .l_arrw').show();
				$('.my-adventure-cont .dscrb.active .r_arrw').show();
			}
			else{
				$('.my-adventure-cont .dscrb.active .l_arrw').hide();
				$('.my-adventure-cont .dscrb.active .r_arrw').hide();
			}

		}, 200);
	}
	function arrowController(){
		
		removeArrow();

		$('.adv-nav li').click(function(){
			removeArrow();
		});
	}

	function runIfFiltered(){
		$('.sort-filter-active').each(function(index,item){
			var html = $('.sort-filter-active').eq(index).html();
			var value = $('.sort-filter-active').eq(index).val();
			$('.sort-filter-active').eq(index).closest('select').find('option').eq(0).html(html);
			$('.sort-filter-active').eq(index).closest('select').find('option').eq(0).val(value);
		});
	}

	arrowController();
	resAdventureFilter();
	runIfFiltered();
	</script>
@endsection


