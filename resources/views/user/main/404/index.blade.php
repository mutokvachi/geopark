@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
@endsection

@section('navbar')
	{{-- @include('user.layouts.navbar.navbarMain') --}}
	{{-- @include('user.layouts.navbar.navbarProfile') --}}
	{{-- @include('user.layouts.main.mainSlider') --}}
	{{-- @include('user.layouts.main.alertMenu') --}}
@endsection

@section('content')
	<div class="layout_404">
		<div id="bg-layer"></div>
	</div>	
	<div class="page-404">
		<a class="logo" href="{{route('index',['lang'=>App::getLocale()])}}"></a>
		<div>
			<h1>Error 404</h1>
			<p>{{ __('website.page_404_not_found') }}</p>
			<a href="{{route('index',['lang'=>App::getLocale()])}}">{{ __('website.page_404_back') }}</a>
		</div>
	</div>
@endsection

@section('footer')
	<div class="empty-footer-div"></div>
@endsection

@section('js_sources')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		function getRandomInt(min,max) {
		  return Math.floor(Math.random() * (max - min + 1)) + min;
		}
		var rand = 'i-'+ getRandomInt(1,15);
		$(function(){
			$('#bg-layer').attr('data-bg',rand);
		});
	</script>
@endsection


