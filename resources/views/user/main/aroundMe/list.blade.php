@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	<style type="text/css">
		.main-map h2{
			padding-top: 75px;
		}
	</style>
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	{{-- @include('user.layouts.navbar.navbarProfile') --}}
	{{-- @include('user.layouts.main.mainSlider') --}}
	{{-- @include('user.layouts.main.alertMenu') --}}
@endsection

@section('content')
<div class="map_gps">
	<div class="map main-map" data-lat="{{ $coordinates[0] or 42 }}" data-lng="{{ $coordinates[1] or 42 }}"></div>
</div>
	<div class="all_parks_list" style="display: none;">{{ json_encode($all_parks) }}</div>
	<div class="memories_feed for_ev ar_me">
		<div class="web-container p-15-lr p-20-tb">
			<h2 class="blck-title t-blck m-30-b">{{ __('website.around_me') }}</h2>
			<div class="row">
				@foreach($my_parks as $my_park)
					<div class="col-md-6">
						<a href="{{ route('siteParksInner', ['lang'=>App::getLocale(), 'park'=>$my_park->alias]) }}">
							<div class="m_cont">
								<div class="photo">
									<img src="{{ Stuff::imgUrl(json_decode($my_park->banner)[0], true) }}">
								</div>
								<div class="comment">
									<h4 class="w-title-ex t-30-px">{{ Stuff::trans($my_park, 'name') }}</h4>
									<div class="autor data">
										<div class="data_name verlagB t-blck t-up">
											<span class="block">
												{{ __('website.aroundMe_distance') }}:
											</span>
											{{-- <span class="block">
												{{ __('website.aroundMe_time') }}:
											</span> --}}
											<span class="block">
												{{ __('website.aroundMe_working_hours') }}:
											</span>
										</div>
										<div class="ev_data georgia t-blck">
											<span class="block">
												@if(intval($my_park->distance))
													<span class="day">{{ intval($my_park->distance)}}</span>
												@else
													<span class="day"></span>
												@endif
												<span class="month">
													{{ __('website.aroundMe_km') }}
												</span>
											</span>
											{{-- <span class="block">
												<span>
													@if(intval($my_park->distance))
													{{ number_format(Stuff::drivingTime(intval($my_park->distance)), 1) }} 	{{ __('website.aroundMe_driving') }}
													@else
														{{ __('website.aroundMe_driving') }}
													@endif
												</span>
											</span> --}}
											<span class="block">
												<span class="from">
													<?php $working_hours = 'operating_hours_'.$year_season; ?>
													{!! Stuff::trans($my_park, $working_hours) !!}
												</span>
											</span>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek&callback=initMap">
    </script>
@endsection

@section('javascript')
	<script type="text/javascript">
		var currentLang = '{{ App::getLocale() }}';

		
		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();

			// $('.bottom_nav ul li').eq(3).find('a').addClass('active');
		});


		function initMap() {
			function showPosition(position, error) {
				var aroundPins = JSON.parse($('.all_parks_list').eq(0).html());
				var aroundmeLNG = {'ka':"ჩემი მდებარეობა",'en':"My Location",'ru':"мое местонахождение"};
			    var test = "Latitude: " + position.coords.latitude + 
			    "<br>Longitude: " + position.coords.longitude;
			    $('.main-map').attr('data-lat', position.coords.latitude);
			    $('.main-map').attr('data-lng', position.coords.longitude);

			    var infoW = null;
				function addListenerToMarker(marker, infowindow, map){
					marker.addListener('click', function(){
						if( infoW != null ){
							infoW.close();
						}
						infowindow.open(map, marker);
						infoW = infowindow;
					});
				}
			    $('.map').gMap(function(element, lat, lng){
	    			lat = Number(lat);
	    			lng = Number(lng);
	    			var position = {lat: lat, lng: lng};
	    			var map = new google.maps.Map(element,{
	    				zoom: 8,
				        center: position,
				        scrollwheel: false,
				        gestureHandling: 'greedy',
				        mapTypeId: 'hybrid',
				        mapTypeControl: false,
	    			});
	    			var marker = new google.maps.Marker({
	    				position : position,
	    				map:map,
	    				icon: '/main/img/my_location.svg',
	    			});

	    			var aroundDiv = document.createElement('div');
						aroundDiv.setAttribute('class','aroundmePins');
						aroundDiv.innerHTML = aroundmeLNG[currentLang];

	    			var infowindow = new google.maps.InfoWindow({
	    				content: aroundDiv
	    			});
					addListenerToMarker(marker, infowindow, map);

	    			for(var i = 0; i<aroundPins.length; i++){
						var marker = new google.maps.Marker({
							position : { lat: Number(aroundPins[i].coordinate_lat), lng: Number(aroundPins[i].coordinate_long) },
							map:map,
							icon: '/main/img/map-pin-by.png',
							// icon: markerAttrs[i].ico,
							
						});
						var aroundDiv = document.createElement('div');
						aroundDiv.setAttribute('class','aroundmePins');
						if( currentLang == "ka" ){
							aroundDiv.innerHTML = `<a href="/`+currentLang+`/site/`+aroundPins[i].alias+`">`+aroundPins[i].name+`</a>`;
			    			var infowindow = new google.maps.InfoWindow({
			    				content: aroundDiv
			    			});
							addListenerToMarker(marker, infowindow, map);
						}else{
							aroundDiv.innerHTML = `<a href="/`+currentLang+`/site/`+aroundPins[i].alias+`">`+aroundPins[i]["name_"+currentLang]+`</a>`;
			    			var infowindow = new google.maps.InfoWindow({
			    				content: aroundDiv
			    			});
							addListenerToMarker(marker, infowindow, map);
						}
	    			}


	    		});
			}

			function showError(err){
			    $('.main-map').html(`
			    		<h2 class="text-center mt-5">{{ __('website.master_geolocation_validation') }}</h2>
			    	`);
			}
			navigator.geolocation.getCurrentPosition(showPosition, showError);
    		
		}




	</script>
@endsection


