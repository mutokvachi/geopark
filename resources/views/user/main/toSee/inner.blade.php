@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
	@include('user.layouts.main.alertMenu')
@endsection

@section('content')
	<div class="p_around_park drk-wht">	
		<div class="web-container p-15-lr p-20-tb">
			<h2 class="blck-title t-blck m-30-b">{{ __('website.around_toSee') }}</h2>
			<div class="row">
				<div class="col-md-4">
					<div class="place_view_slider">
						<div class="slider_img photo">
							<img src="{{ Stuff::imgUrl(json_decode($around->images)[0], true) }}">
						</div>
						<div class="photo_pick">
							<div id="places-view-slider" class="owl-carousel owl-theme">
								<?php $around_photos = json_decode($around->images); ?>
								@foreach($around_photos as $img)
								<div class="item">
									<div class="photo_switcher">
										<div class="photo">
											<img src="{{ Stuff::imgUrl($img, true) }}">
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="place_view_map">
						<div class="map" data-lat="{{ $around->coordinate_lat }}" data-lng="{{ $around->coordinate_long }}"></div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="dscrb">
						<div class="place">
							<h2 class="t-blck georgia t-30-px t-cp">{{ Stuff::trans($around, 'name') }}</h2>
							<div class="clearfix"></div>
							<div class="details">
								
							</div>
						</div>
						<p class="georgia t-16-px">{!! Stuff::trans($around, 'description') !!}</p>
						@if(Session::has('user') && isset($favourite))
							<div class="favouritesBtn">
								@if($favourite == 1)
									<a href="" class="add active favourite-controller"><span class="txt">{{ __('website.main_add_favourites') }}</span></a>
								@else
									<a href="" class="add favourite-controller"><span class="txt">{{ __('website.main_add_favourites') }}</span></a>
								@endif
							</div>
						@else
							<div class="favouritesBtn">
								<a href="{{ route('loginView', ['lang'=>App::getLocale()]) }}" class="add"><span class="txt">{{ __('website.main_add_favourites') }}</span></a>
							</div>
						@endif
					</div>
					<div class="clearfix"></div>
					<div class="reviews">
						<h2 class="blck-title m-20-t m-40-b">{{ __('website.around_my_comments') }}</h2>
						@if(isset($user))
							<div class="add_review">
								<div class="autor-img photo">
									<img src="{{ Stuff::imgUrl($user->img) }}">
								</div>
								<form>
									<div class="r-stars">
										<span class="circular t-14-px pull-left">{{ __('website.around_my_rate') }}:</span>
										<div class='rating-stars'>
											<ul class='stars'>
												<li class='star' data-value='1'>
													<span class="s-i"></span>
												</li>
												<li class='star' data-value='2'>
													<span class="s-i"></span>
												</li>
												<li class='star' data-value='3'>
													<span class="s-i"></span>
												</li>
												<li class='star' data-value='4'>
													<span class="s-i"></span>
												</li>
												<li class='star' data-value='5'>
													<span class="s-i"></span>
												</li>
											</ul>
										</div>
									</div>
									<textarea onkeyup="auto_grow(this)" placeholder="Write a comment" rows="1" resize="none" class="r-field write-comment"></textarea>
								</form>
							</div>
						@endif
						<div class="comments">
							@foreach($comments as $comment)
								<div class="comment">
									<div class="autor-img photo">
										<img src="{{ Stuff::imgUrl($comment->user->img) }}">
									</div>
									<div class="ttl">
										<div class='rating-stars'>
											<ul class='stars' data-ranked="{{ $comment->rating }}">
												@for($i = 1;$i <= 5; $i++)
													@if($i <= $comment->rating)
														<li class='star selected' data-value='{{ $i }}'>
															<span class="s-i"></span>
														</li>
													@else
														<li class='star' data-value='{{ $i }}'>
															<span class="s-i"></span>
														</li>
													@endif
												@endfor
											</ul>
										</div>
										<h4 class="autor-name verlagB t-blck t-20-px">{{ $comment->user->name }}</h4>
									</div>
									<div class="txt">
										<p>{!! $comment->description !!}</p>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>			
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek&callback=initMap">
    </script>
@endsection

@section('javascript')
	<script type="text/javascript">
		function myFavourite(){
			<?php $fav_action = $favourite == 0 ? $fav_action = 1 : $fav_action = 0; ?>
			var favouriteLink = '{{ route('favouritesControll', ['type'=>'toSeeInner','type_id'=>Request::segment(5), 'controll'=>$fav_action ]) }}';
			favouriteControll(favouriteLink);
		}

		$(function(){
			setCarousels();
			fourBullets();
			// headerFontSize();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			alertBox();

			ratingStars();
			photoPicker();
			myFavourite();
			// $('.bottom_nav ul li').eq(7).find('a').addClass('active');
		});

		function initMap() {
    		$('.map').gMap(function(element, lat, lng){
    			lat = JSON.parse(lat);
    			lng = JSON.parse(lng);

    			var position = {lat: lat, lng: lng};
    			var map = new google.maps.Map(element,{
    				zoom: 5,
			        center: position,
			        scrollwheel: false,
			        gestureHandling: 'greedy',
			        disableDefaultUI: true,
			        mapTypeId: 'satellite'
    			});
    			var marker = new google.maps.Marker({
    				position : position,
    				map:map,
    				icon: '/main/img/map-pin.png',
    			});
    		});
		}


		function commentController(){
			var commentAvailable = true;
			$(".write-comment").on("keypress",function(e) {
				
			    var key = e.keyCode;
			    if (key == 13) {

					if(commentAvailable == false)
						return;
					commentAvailable = false;

		    		var reviews = $('form .rating-stars .star.selected').length;
		    		var text = $('.write-comment').val();
		    		var type = '{{ Request::segment(4) }}'; 
		    		var type_id = '{{ Request::segment(5) }}'; 
		    		var token = '{{ csrf_token() }}';
		    		var username = '{{ $user->name or '' }}'; 
		    		var userImg = '{{ $user->img or '' }}';

		    		var serial = {
		    			rating: reviews,
		    			_token: token,
		    			type: type,
		    			type_id: type_id,
		    			description: text,
		    		};

		    		$.ajax({
		    			url: '{{ route('commentAction', ['lang'=>App::getLocale()]) }}',
		    			method: 'post',
		    			data:serial,
		    			success:function(res){
		    				function clearContent(){
		    					$('.write-comment').val('');
		    					$('form .rating-stars .star.selected').removeClass('selected');
		    				}

		    				if(res == 'success'){
		    					var my_star = '';
		    					for(var i = 1;i <= 5;i++){
		    						if(i <= JSON.parse(reviews)){
		    						console.log(reviews);
			    						my_star += `
			    							<li class='star selected' data-value='`+i+`'>
												<span class="s-i"></span>
											</li>
			    						`;
		    						}else{
		    							my_star += `
			    							<li class='star' data-value='`+i+`'>
												<span class="s-i"></span>
											</li>
			    						`;
		    						}
		    					} 
		    					$('.comments .mCSB_container').prepend(`
		    						<div class="comment">
										<div class="autor-img photo">
											<img src="/main/img/`+userImg+`">
										</div>
										<div class="ttl">
											<div class='rating-stars'>
												<ul class='stars' data-ranked=`+reviews+`">
													`+my_star+`
												</ul>
											</div>
											<h4 class="autor-name verlagB t-blck t-20-px">`+username+`</h4>
										</div>
										<div class="txt">
											<p>`+text+`</p>
										</div>
									</div>


		    					`);
		    			
		    					clearContent();
		    				}else if(res == 'spam'){
		    					alert('{{ __('website.main_comment_spam') }}');
		    					clearContent();
		    				}else{
		    					var html = '';
		    					for(var i in res){
		    						html += res[i]+'\n';
		    					}
		    					alert(html);
		    				}
		    				
		    				commentAvailable = true;
		    			}
		    		});
			    }
			});
		}
		commentController();



	</script>
@endsection


