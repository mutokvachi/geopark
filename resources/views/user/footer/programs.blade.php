@extends('user.layouts.master')

@section('head')
	@parent
	<style type="text/css">
		#origin-input{
		  margin-top: 10px;
		  border: 1px solid transparent;
		  box-sizing: content-box;
		  height: 40px;
		  outline: none;
		  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		  background-color: #fff;
		  font-family: Roboto;
		  font-size: 15px;
		  font-weight: 300;
		  margin-left: 12px;
		  padding: 0 11px 0 13px;
		  text-overflow: ellipsis;
		  width: 300px;
		}
	</style>
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
@endsection

@section('content')
<div class="guide_cont drk-wht">
	<div class="web-container p-15-lr p-20-tb t-blck">
		<div class="row">
			<div class="col-md-8">
				<div class="guide m-40-b">
					<?php $lng = App::getLocale(); ?>
					<h2 class="verlagB t-up t-30-px m-20-b">{{ Stuff::trans($program, 'name') }}</h2>
					<p>{!! Stuff::trans($program, 'description') !!}</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&sensor=false&key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek"></script>
@endsection

@section('javascript')
	<script type="text/javascript">

		$(function(){
			setCarousels();
			fourBullets();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			sortDropdown();

		});

	</script>
@endsection


