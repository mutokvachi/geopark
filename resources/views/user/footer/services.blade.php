{{-- PAID SERVICES OUTER --}}
@extends('user.layouts.master')

@section('head')
	@parent
	<link rel="stylesheet" href="https://sc1-cdn.24ats.com/afp.min.css">
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
@endsection

@section('content')
<div class="memories_feed for_ev ar_me">
	<?php $lng = App::getLocale(); ?>
	<div class="web-container p-15-lr p-20-tb">
		<h2 class="blck-title t-blck m-30-b">{{ Config::get("setting.footer_$lng.ticket.title") }}</h2>
		<div class="txt georgia">
			<p>
				{{ Config::get("setting.footer_$lng.ticket.description") }}
			</p>
		</div>
	</div>
	<div class="web-container p-15-lr p-20-tb">
		<h2>{{ __('website.footer_choose_park') }}:</h2><br><br>
		<div class="row">
			@foreach($my_parks as $key => $my_park)
				@if(!empty($my_park->fees_passes_1) && $my_park->fees_passes_1 != '<p><br></p>')
					<div class="col-md-6 my-current-park-item active position-relative">
						<a href="{{ route('attraction', ['lang'=>App::getLocale(), 'park'=>$my_park->alias]) }}">
							<div class="m_cont">
								<div class="photo">
									<img src="{{ Stuff::imgUrl(json_decode($my_park->banner)[0], true) }}">
								</div>
								<div class="comment">
									<h4 class="w-title-ex t-30-px">{{ Stuff::trans($my_park, 'name') }}</h4>
									<div class="autor data">
										<div class="data_name verlagB t-blck t-up">
											<span class="block">
												{{ __('website.aroundMe_distance') }}:
											</span>
											{{-- <span class="block">
												{{ __('website.aroundMe_time') }}:
											</span> --}}
											<span class="block">
												{{ __('website.aroundMe_working_hours') }}:
											</span>
										</div>
										<div class="ev_data georgia t-blck">
											<span class="block">
												@if(intval($my_park->distance))
													<span class="day">{{ intval($my_park->distance)}}</span>
												@else
													<span class="day"></span>
												@endif
												<span class="month">
													{{ __('website.aroundMe_km') }}
												</span>
											</span>
											
											<span class="block">
												<span class="from">
													<?php $working_hours = 'operating_hours_'.$year_season; ?>
													{!! Stuff::trans($my_park, $working_hours) !!}
												</span>
											</span>
											{{-- <span class="block text-right"> --}}

												{{-- <span>
													@if(intval($my_park->distance))
													{{ number_format(Stuff::drivingTime(intval($my_park->distance)), 1) }} 	{{ __('website.aroundMe_driving') }}
													@else
														{{ __('website.aroundMe_driving') }}
													@endif
												</span> --}}
											{{-- </span> --}}
										</div>

									</div>
								</div>

							</div>
						</a>
						@if(isset($buyTickets[$my_park->id]))
							<?php $ticket_langs = ['ka'=>'ge', 'en'=>'en', 'ru'=>'ru']; ?>
							<div class="text-right" style="position: absolute;bottom: 15px;right: 15px;">
								<a class="arcom__btn btn btn-danger" href="{{ $buyTickets[$my_park->id].$ticket_langs[App::getLocale()] }}">Buy ticket</a>
							</div>
						@endif
					</div>
				@endif
			@endforeach
		</div>
	</div>
</div> 	
<div class="load-more text-center" >{{ __('website.trip_load_more') }}</div>

@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	@parent
	
	<script src="https://sc1-cdn.24ats.com/afp.min.js"></script>
@endsection

@section('javascript')
	<script type="text/javascript">

		$(function(){
			setCarousels();
			fourBullets();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();

		});

		$('.load-more').click(function(){
			$('.my-current-park-item').removeClass('active');
			$('.my-current-park-item').addClass('active');
			$('.load-more').hide();
		});
	</script>
@endsection


