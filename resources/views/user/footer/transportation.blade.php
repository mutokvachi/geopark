@extends('user.layouts.master')

@section('head')
	@parent
	<style type="text/css">
		#origin-input{
		  margin-top: 10px;
		  border: 1px solid transparent;
		  box-sizing: content-box;
		  height: 40px;
		  outline: none;
		  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		  background-color: #fff;
		  font-family: Roboto;
		  font-size: 15px;
		  font-weight: 300;
		  margin-left: 12px;
		  padding: 0 11px 0 13px;
		  text-overflow: ellipsis;
		  width: 300px;
		}
	</style>
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
@endsection

@section('content')
<div class="guide_cont drk-wht">
	<div class="web-container p-15-lr p-20-tb t-blck">
		<div class="row">
			<div class="col-md-8">
				<div class="guide m-40-b">
					<?php $lng = App::getLocale(); ?>
					<h2 class="verlagB t-up t-30-px m-20-b">{{ Config::get("setting.footer_$lng.bus.title") }}</h2>
					<p>{{ Config::get("setting.footer_$lng.bus.description") }}</p>
				</div>
			<h4 class="b-title t-20-px m-10-b">{{ __('website.footer_choose_park') }}</h4>
			<div class="_ex sortBtn">
				<div class="sort_label sort_placeholder sort-parks-adventure" data-sort="ლაშქრობა">{{ __('website.footer_choose_park') }}</div>
					<ul class="sort_list">
						@foreach($my_parks as $my_park)
							<li>
								<a href="{{ route('transportation', ['lang'=>App::getLocale(), 'park'=>$my_park->alias]) }}" style="color: #222;">{{ Stuff::trans($my_park, 'name') }}</a>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

@if(!$optional)
	<div class="memories_feed for_ev ar_me">
		<div class="web-container p-15-lr p-20-tb">
			<div class="row">
				@foreach($my_parks as $key => $my_park)
					@if($key < 4)
						<div class="col-md-6 my-current-park-item active">
					@else
						<div class="col-md-6 my-current-park-item">
					@endif
						<a href="{{ route('transportation', ['lang'=>App::getLocale(), 'park'=>$my_park->alias]) }}">
							<div class="m_cont">
								<div class="photo">
									<img src="{{ Stuff::imgUrl(json_decode($my_park->banner)[0], true) }}">
								</div>
								<div class="comment">
									<h4 class="w-title-ex t-30-px">{{ Stuff::trans($my_park, 'name') }}</h4>
									<div class="autor data">
										<div class="data_name verlagB t-blck t-up">
											<span class="block">
												{{ __('website.aroundMe_distance') }}:
											</span>
											{{-- <span class="block">
												{{ __('website.aroundMe_time') }}:
											</span> --}}
											<span class="block">
												{{ __('website.aroundMe_working_hours') }}:
											</span>
										</div>
										<div class="ev_data georgia t-blck">
											<span class="block">
												@if(intval($my_park->distance))
													<span class="day">{{ intval($my_park->distance)}}</span>
												@else
													<span class="day"></span>
												@endif
												<span class="month">
													{{ __('website.aroundMe_km') }}
												</span>
											</span>
											{{-- <span class="block">
												<span>
													@if(intval($my_park->distance))
													{{ number_format(Stuff::drivingTime(intval($my_park->distance)), 1) }} 	{{ __('website.aroundMe_driving') }}
													@else
														{{ __('website.aroundMe_driving') }}
													@endif
												</span>
											</span> --}}
											<span class="block">
												<span class="from">
													<?php $working_hours = 'operating_hours_'.$year_season; ?>
													{!! Stuff::trans($my_park, $working_hours) !!}
												</span>
											</span>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
				@endforeach
			</div>
		</div>
	</div>
	<div class="load-more text-center" >{{ __('website.trip_load_more') }}</div>
@else
	<div class="how_to_get_here_map">
		<div id="mapDir" class="map" data-lat="{{ $park->coordinate_lat }}" data-lng="{{ $park->coordinate_long }}"></div>
		<input id="origin-input" class="from_field controls" type="text" name="" placeholder="From Where?">
	</div>
	<div class="info_cont">
		@foreach($heres as $key => $here)
			@if($key % 2 == 0)
				<div class="info_section col-md-3 p-15-tb p-20-l-r">
			@else
				<div class="info_section col-md-3 drk-wht p-15-tb p-20-l-r">
			@endif
				<h3 class="grn-title">{{ Stuff::trans($here, 'name') }}</h3>
				<p class="georgia">{!! Stuff::trans($here, 'description') !!}</p>
			</div>
		@endforeach
	</div>
@endif
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&sensor=false&key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek"></script>
@endsection

@section('javascript')
	<script type="text/javascript">

		$(function(){
			setCarousels();
			fourBullets();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			sortDropdown();

			if(pathName.length > 4){
				initMap();
			    $('html, body').animate({
			        scrollTop: $("#mapDir").offset().top-50
			    }, 300);
			}
		});

		var pathName = window.location.pathname.split('/');
		if(pathName.length > 4){
				
			var lat = $('#mapDir').data('lat');
			var lng = $('#mapDir').data('lng');

			function initMap() {
			  var map = new google.maps.Map(document.getElementById('mapDir'), {
			    mapTypeControl: false,
			    center: {
			      lat: lat,
			      lng: lng
			    },
			    zoom: 13
			  });
			  var marker = new google.maps.Marker({
			    map: map,
			    icon: '/main/img/map-pin-by.png',
			    position: new google.maps.LatLng(lat, lng)
			  });
			  new AutocompleteDirectionsHandler(map);
			}

			/**
			 * @constructor
			 */
			function AutocompleteDirectionsHandler(map) {
			  this.map = map;
			  this.originPlaceId = null;
			  this.travelMode = 'DRIVING';
			  var originInput = document.getElementById('origin-input');
			  this.directionsService = new google.maps.DirectionsService;
			  this.directionsDisplay = new google.maps.DirectionsRenderer;
			  this.directionsDisplay.setMap(map);

			  var originAutocomplete = new google.maps.places.Autocomplete(
			    originInput, {
			      placeIdOnly: true
			    });

			  this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
			  this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
			}



			AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
			  var me = this;
			  autocomplete.bindTo('bounds', this.map);
			  autocomplete.addListener('place_changed', function() {
			    var place = autocomplete.getPlace();
			    if (!place.place_id) {
			      window.alert("Please select an option from the dropdown list.");
			      return;
			    }
			    me.originPlaceId = place.place_id;
			    me.route();
			  });
			};

			AutocompleteDirectionsHandler.prototype.route = function() {
			  if (!this.originPlaceId) {
			    return;
			  }
			  var me = this;
			  this.directionsService.route({
			    origin: {
			      'placeId': this.originPlaceId
			    },
			    destination: new google.maps.LatLng(lat, lng),
			    travelMode: this.travelMode
			  }, function(response, status) {
			    if (status === 'OK') {
			      me.directionsDisplay.setDirections(response);
			    } else {
			      window.alert('Directions request failed due to ' + status);
			    }
			  });
			};
		}


		$('.load-more').click(function(){
			$('.my-current-park-item').removeClass('active');
			$('.my-current-park-item').addClass('active');
			$('.load-more').hide();
		});

	</script>
@endsection


