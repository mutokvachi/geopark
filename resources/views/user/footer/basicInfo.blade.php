{{-- BASIC INFO --}}
@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
@endsection

@section('content')
<div class="trips basic_info">
	<div class="list_info">
		<div class="listBtn">
			<span class="t"></span>
			<span class="m"></span>
			<span class="b"></span>
		</div>
		<ul>
			@foreach($infos as $info)
				<li data-show="{{ $info->id }}">{{ Stuff::trans($info, 'name') }}</li>
			@endforeach
		</ul>
	</div>
	@foreach($infos as $info)
		<div class="info_container" data-show="{{ $info->id }}">
			<div class="clearfix"></div>
			<div class="txt georgia">
				<p>{!! Stuff::trans($info, 'description') !!}</p>
			</div>
		</div>
	@endforeach
</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">

		$(function(){
			setCarousels();
			fourBullets();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			basicInfoContainer();
		});


	</script>
@endsection


