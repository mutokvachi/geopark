@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarMain')
	@include('user.layouts.main.mainSlider')
@endsection

@section('content')
<div class="guide_cont drk-wht">
	<div class="web-container p-15-lr p-20-tb t-blck">
		<div class="row">
			<div class="col-md-8">
				<div class="guide m-40-b">
					<?php $lng = App::getLocale(); ?>
					<h2 class="verlagB t-up t-30-px m-20-b">{{ Config::get("setting.footer_$lng.guide.title") }}</h2>
					<p>{{ Config::get("setting.footer_$lng.guide.description") }}</p>
				</div>
			<h4 class="b-title t-20-px m-10-b">{{ __('website.footer_choose_park') }}</h4>
			<div class="_ex sortBtn">
				<div class="sort_label sort_placeholder sort-parks-adventure" data-sort="ლაშქრობა">{{ __('website.footer_choose_park') }}</div>
					<ul class="sort_list">
						@foreach($my_parks as $my_park)
							<li>
								<a href="{{ route('siteGuide', ['lang'=>App::getLocale(), 'park'=>$my_park->alias]) }}" style="color: #222;">{{ Stuff::trans($my_park, 'name') }}</a>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="memories_feed for_ev ar_me">
	<div class="web-container p-15-lr p-20-tb">
		<div class="row">
			@foreach($my_parks as $key => $my_park)
				@if($key < 4)
					<div class="col-md-6 my-current-park-item active">
				@else
					<div class="col-md-6 my-current-park-item">
				@endif
					<a href="{{ route('siteGuide', ['lang'=>App::getLocale(), 'park'=>$my_park->alias]) }}">

						{{-- guidis gadmosaceeri buttoni start, labex  --}}
						@if(isset($downloadables[$my_park->id]))
							<?php 
								$guide_link = json_decode($downloadables[$my_park->id]->file)[0]; ?>
							<a href="{{ asset("files/$guide_link") }}" class="downloadable">{{ __('website.download_guide') }}</a>
						@endif
						{{-- guidis gadmosaceeri buttoni end, labex  --}}

						<div class="m_cont">
							<div class="photo">
								<img src="{{ Stuff::imgUrl(json_decode($my_park->banner)[0], true) }}">
							</div>
							<div class="comment">
								<h4 class="w-title-ex t-30-px">{{ Stuff::trans($my_park, 'name') }}</h4>
								<div class="autor data">
									<div class="data_name verlagB t-blck t-up">
										<span class="block">
											{{ __('website.aroundMe_distance') }}:
										</span>
										{{-- <span class="block">
											{{ __('website.aroundMe_time') }}:
										</span> --}}
										<span class="block">
											{{ __('website.aroundMe_working_hours') }}:
										</span>
									</div>
									<div class="ev_data georgia t-blck">
										<span class="block">
											@if(intval($my_park->distance))
												<span class="day">{{ intval($my_park->distance)}}</span>
											@else
												<span class="day"></span>
											@endif
											<span class="month">
												{{ __('website.aroundMe_km') }}
											</span>
										</span>
										<span class="block">
											<span class="from">
												<?php $working_hours = 'operating_hours_'.$year_season; ?>
												{!! Stuff::trans($my_park, $working_hours) !!}

											</span>
										</span>
									</div>
								</div>								
							</div>
						</div>

					</a>
				</div>
			@endforeach
		</div>
	</div>
</div>
<div class="load-more text-center" >{{ __('website.trip_load_more') }}</div>
@endsection

@section('footer')
	@parent
@endsection

@section('js_sources')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">

		$(function(){
			setCarousels();
			fourBullets();
			setScrollBar();
			switchBtn();
			topSliderCounterBtn();
			resBtn();
			sortDropdown();
		});


		$('.load-more').click(function(){
			$('.my-current-park-item').removeClass('active');
			$('.my-current-park-item').addClass('active');
			$('.load-more').hide();
		});
	</script>
@endsection


