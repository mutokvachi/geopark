@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarProfile')
@endsection

@section('content')
	<div class="ui_container">
		<div class="reg_log">
			<h2 class="ui_ttl">{{ __('website.subscribe_unnewsletter') }}</h2>
			<div class="content">
				<div class="log_in">
					<form class="b_rght" method="post" action="{{ route('unSubscribe', ['lang'=>App::getLocale()]) }}">
						{{ csrf_field() }}
						<label class="ui_sttl block">{{ __('website.subscribe_email') }}:</label>
						<input type="text" name="email" class="ml">
						{{-- <div class="b-1-b" style="border-bottom: none;">
							<div class="ch_list">
							 	<input type="checkbox" name="events" id="evs">
						    	<label class="ui_sttl" for="evs">{{ __('website.subscribe_events') }}</label>
							</div>
							<div class="ch_list">
						    	<input type="checkbox" name="trips" id="trps">
						    	<label class="ui_sttl" for="trps">{{ __('website.subscribe_trips') }}</label>
						    </div>
							<div class="ch_list">
						    	<input type="checkbox" name="alerts" id="alrts">
						    	<label class="ui_sttl" for="alrts">{{ __('website.subscribe_alerts') }}</label>
							</div>
							<div class="ch_list">
						    	<input type="checkbox" name="services" id="servcs">
						    	<label class="ui_sttl" for="servcs">{{ __('website.subscribe_services') }}</label>
							</div>
						</div> --}}
						
						<div class="flex">
							<button type="submit" class="authBtn block">{{ __('website.subscribe_unsubscribe') }}</button>
						<a href='{{ route('subscribe', ['lang'=>App::getLocale()]) }}' class="subBtn">{{ __('website.subscribe_btn_subscribe') }}</a>
						</div>
						@if(Session::has('errors'))
							<div class="alert alert-danger">
								<ol>
								@foreach(Session::get('errors') as $err)
									<li>{{ $err }}</li>
								@endforeach
								</ol>
							</div>
						@endif

						@if(Session::has('success'))
							<div class="alert alert-success">
								{{ __('website.subscribe_unsuccess') }}
							</div>
						@endif
					</form>
				</div>
			</div>
			<div class="alternate_login">
				<p class="t-18-px t-blck devinne">{{ __('website.subscribe_social_connects') }}</p>
				<a href="{{ Config::get('setting.social.fb') }}" class="fb_log t-up">{{ __('website.subscribe_fb_join') }}</a>
				{{-- <a href="{{ Config::get('setting.social.twitter') }}" class="twitter_log t-up">{{ __('website.subscribe_twitter_join') }}</a> --}}
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	@parent
@endsection


