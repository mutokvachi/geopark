@extends('user.layouts.master')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/summernote.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/bootstrap-tagsinput.css') }}">
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarProfile')
@endsection

@section('content')
	<div class="ui_container">
		@include('user.layouts.profile.sideNav')
		<div class="content-md memory-delete" style="display: none;">
			<h2 class="ui_ttl">{{ __('website.memories_memories') }}</h2>
			<h4>{{ __('website.memories_reviewed') }}</h4>
			<button type="button" delete-id={{ route('memoryDeletor', ['lang'=> App::getLocale(), 'id'=>$memory->id]) }} class="delete-alert authBtn block m-20-t">{{ __('website.memories_delete') }}</button>
		</div>
		<div class="content-md editable-memory" style="display: none;">
			<h2 class="ui_ttl">{{ __('website.memories_memories') }}</h2>
			<form method="post" action="{{ route('memoriesEdit', ['lang' => App::getLocale(), 'id' => $memory->id]) }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="mem_opt">
					<label class="ui_sttl block">{{ __('website.memories_Park') }}:</label>
					<select class="day" name="park_id">
						@foreach($parks as $park)
							@if($memory->park_id == $park->id)
								<option value="{{ $park->id }}" selected>{{ Stuff::trans($park, 'name') }}</option>
							@else
								<option value="{{ $park->id }}">{{ Stuff::trans($park, 'name') }}</option>

							@endif
						@endforeach
					</select>
					<input type="hidden" class="redirect_hash" name="redirect_hash">
					<label class="ui_sttl block">{{ __('website.memories_title') }}:</label>
					<input type="text" name="title" value="{{ $memory->title }}" class="nm">
					{{-- <label class="ui_sttl block">{{ __('website.memories_Type') }}:</label> --}}
					
					{{-- <div class="rd_btns" style="visibility: hidden;">
						<label class="relative m-15-r active" data-show="bp">{{ __('website.memories_blog_post') }}
							<input type="radio" checked="checked" name="radio">
							<span class="bullet"></span>
						</label>
						<label class="relative m-15-r" data-show="pg">{{ __('website.memories_photo_gallery') }}
							<input type="radio" name="radio">
							<span class="bullet"></span>
						</label>
						<label class="relative m-15-r" data-show="vg">{{ __('website.memories_video_gallery') }}
							<input type="radio" name="radio">
							<span class="bullet"></span>
						</label>
					</div> --}}
					<?php $images = json_decode($memory->images); ?>
					<div class="postBox blog_post active edit_blog" style="display: none;" data-show="bp">
						<label class="ui_sttl block">{{ __('website.memories_Memory') }}:</label>
						<textarea class="wr_memory summernote" name="blog">{{ $memory->extra }}</textarea>
						<label class="ui_sttl block">{{ __('website.memories_Photo_view') }}:</label>
						@foreach($images as $img)
							<img src="{{ asset("/main/img/$img") }}" width="150" height="70" class="img-margin-bottom">
						@endforeach
						{{-- <div class="chs_file">
							<div class="img_placeholder"></div>
							<input type="file" data-upload="post-tumb" name="blog_photos[]" multiple accept="image/*">
							<a href="#" class="authBtn block" data-upload="post-tumb">{{ __('website.memories_choose') }}</a>
						</div> --}}
						<label class="ui_sttl block">{{ __('website.memories_Tags') }}:</label>
						<div class="tag_container">
							<input type="text" name="blog_tags" value="{{ $memory->tags }}" data-role="tagsinput">
						</div>
						<div class="flex">
							<button type="submit" name="blog_submitter" class="authBtn block m-20-t">{{ __('website.memories_edit') }}</button>
						</div>
					</div>

					<div class="postBox photo_gallery edit_image" style="display: none;" data-show="pg">
						<label class="ui_sttl block">{{ __('website.memories_multiple_photo_view') }}:</label>
						@foreach($images as $img)
							<img src="{{ asset("/main/img/$img") }}" width="150" height="70" class="img-margin-bottom">
						@endforeach
						{{-- <div class="chs_file">
							<div class="img_placeholder"></div>
							<input type="file" data-upload="photo-tumb" name="gallery_photos[]" multiple accept="image/*">
							<a href="#" class="authBtn block" data-upload="photo-tumb">{{ __('website.memories_choose') }}</a>
						</div> --}}
						<label class="ui_sttl block">{{ __('website.memories_Tags') }}:</label>
						<div class="tag_container">
							<input type="text" value="{{ $memory->tags }}" data-role="tagsinput" name="gallery_tags">
						</div>
						<div class="flex">
							<button type="submit" name="gallery_submitter" class="authBtn block m-20-t">{{ __('website.memories_edit') }}</button>
						</div>
					</div>

					<div class="postBox video_gallery clearfix edit_video" style="display: none;" data-show="vg">
						<label class="ui_sttl block">{{ __('website.memories_Link') }}:</label>
						<div class="add-video-links">
							@if(is_array(json_decode($memory->extra)))
								@foreach(json_decode($memory->extra) as $link)
									@if($link !== null)
									<input type="text" name="video_links[]" value="{{ $link }}" class="nm pull-left">
									@endif
								@endforeach
							@endif
						</div>
						<a href="#" class="authBtn block addVideo">{{ __('website.memories_more') }}</a>
						<div class="clearfix"></div>
						<label class="ui_sttl block">{{ __('website.memories_Photo_view') }}:</label>
						@foreach($images as $img)
							<img src="{{ asset("/main/img/$img") }}" width="150" height="70" class="img-margin-bottom">
						@endforeach
						{{-- <div class="chs_file">
							<div class="img_placeholder"></div>
							<input type="file" data-upload="video-tumb" name="video_photos[]" multiple accept="image/*">
							<a href="#" class="authBtn block" data-upload="video-tumb">{{ __('website.memories_choose') }}</a>
						</div> --}}
						<label class="ui_sttl block">{{ __('website.memories_Tags') }}:</label>
						<div class="tag_container">
							<input type="text" data-role="tagsinput" value="{{ $memory->tags }}" name="video_tags">
						</div>
						<div class="flex">
							<button type="submit" name="video_submitter" class="authBtn block m-20-t">{{ __('website.memories_edit') }}</button>
						</div>
					</div>
				</div>
				<button type="button" delete-id={{ route('memoryDeletor', ['lang'=> App::getLocale(), 'id'=>$memory->id]) }} class="delete-alert authBtn block m-20-t">{{ __('website.memories_delete') }}</button>
				<div class="clearfix"></div>
				@if(Session::has('errors'))
					<div class="alert alert-danger">
						<ol>
						@foreach(Session::get('errors') as $err)
							<li>{{ $err }}</li>
						@endforeach
						</ol>
					</div>
				@endif

				@if(Session::has('success'))
					<div class="alert alert-success">
						{{ Session::get('success') }}
					</div>
				@endif
			</form>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript" src="{{ asset('/main/js/summernote.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/main/js/bootstrap-tagsinput.js') }}"></script>
	<script type="text/javascript">

		function editable(){
			var status = '{{ $memory->status }}';
			if(status != 0){
				$('.editable-memory').remove();
				$('.memory-delete').show();
			}else
				$('.editable-memory').show();
		}
		editable();
		
		$(function(){
			addPost();
			summerNote();
			changePhoto();
			imgNamePlaeHolder();
			alertBeforeDelete();
			$('[data-activator=memories]').addClass('active');
		});

		var page = '{{ $memory->type }}';
		$('.edit_'+page).show();



	</script>
@endsection


