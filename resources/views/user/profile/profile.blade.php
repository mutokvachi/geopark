@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarProfile')
@endsection

@section('content')
	<div class="ui_container">
		@include('user.layouts.profile.sideNav')
		<div class="content-md">
			<h2 class="ui_ttl">{{ __('website.profile_profile') }}</h2>
			<form method="post" action="{{ route('profileEdit', ['lang' => App::getLocale()]) }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-lg-5 col-md-12">
						<label class="ui_sttl block">{{ __('website.profile_Name') }}:</label>
						<input type="text" name="name" class="nm" value="{{ $user->name }}">
						<div class="birth_d">
							<label class="ui_sttl block">{{ __('website.profile_Birth') }}</label>
							<div class="rw">
								<div class="wrpr">
									<select class="day my_birthday" data-picker="day">
										<option disabled selected>{{ __('website.profile_Day') }}</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
										<option value="21">21</option>
										<option value="22">22</option>
										<option value="23">23</option>
										<option value="24">24</option>
										<option value="25">25</option>
										<option value="26">26</option>
										<option value="27">27</option>
										<option value="28">28</option>
										<option value="29">29</option>
										<option value="30">30</option>
										<option value="31">31</option>
									</select>
								</div>
								<div class="wrpr">
									<select class="month my_birthday" data-picker="month">
										<option disabled selected>{{ __('website.profile_Month') }}</option>
										<option value="January">{{ __('website.profile_january') }}</option>
										<option value="February">{{ __('website.profile_february') }}</option>
										<option value="March">{{ __('website.profile_march') }}</option>
										<option value="April">{{ __('website.profile_april') }}</option>
										<option value="May">{{ __('website.profile_may') }}</option>
										<option value="June">{{ __('website.profile_june') }}</option>
										<option value="July">{{ __('website.profile_july') }}</option>
										<option value="August">{{ __('website.profile_august') }}</option>
										<option value="September">{{ __('website.profile_september') }}</option>
										<option value="October">{{ __('website.profile_october') }}</option>
										<option value="November">{{ __('website.profile_november') }}</option>
										<option value="December">{{ __('website.profile_december') }}</option>
									</select>
								</div>
								<div class="wrpr">
									<select class="year my_birthday" data-picker="year">
										<option disabled selected>{{ __('website.profile_Year') }}</option>
										<option value="2018">2018</option>
										<option value="2017">2017</option>
										<option value="2016">2016</option>
										<option value="2015">2015</option>
										<option value="2014">2014</option>
										<option value="2013">2013</option>
										<option value="2012">2012</option>
										<option value="2011">2011</option>
										<option value="2010">2010</option>
										<option value="2009">2009</option>
										<option value="2008">2008</option>
										<option value="2007">2007</option>
										<option value="2006">2006</option>
										<option value="2005">2005</option>
										<option value="2004">2004</option>
										<option value="2003">2003</option>
										<option value="2002">2002</option>
										<option value="2001">2001</option>
										<option value="2000">2000</option>
										<option value="1999">1999</option>
										<option value="1998">1998</option>
										<option value="1997">1997</option>
										<option value="1996">1996</option>
										<option value="1995">1995</option>
										<option value="1994">1994</option>
										<option value="1993">1993</option>
										<option value="1992">1992</option>
										<option value="1991">1991</option>
										<option value="1990">1990</option>
										<option value="1989">1989</option>
										<option value="1988">1988</option>
										<option value="1987">1987</option>
										<option value="1986">1986</option>
										<option value="1985">1985</option>
										<option value="1984">1984</option>
										<option value="1983">1983</option>
										<option value="1982">1982</option>
										<option value="1981">1981</option>
										<option value="1980">1980</option>
										<option value="1979">1979</option>
										<option value="1978">1978</option>
										<option value="1977">1977</option>
										<option value="1976">1976</option>
										<option value="1975">1975</option>
										<option value="1974">1974</option>
										<option value="1973">1973</option>
										<option value="1972">1972</option>
										<option value="1971">1971</option>
										<option value="1970">1970</option>
										<option value="1969">1969</option>
										<option value="1968">1968</option>
										<option value="1967">1967</option>
										<option value="1966">1966</option>
										<option value="1965">1965</option>
										<option value="1964">1964</option>
										<option value="1963">1963</option>
										<option value="1962">1962</option>
										<option value="1961">1961</option>
										<option value="1960">1960</option>
									</select>
								</div>
							</div>
						</div>
						<label class="ui_sttl block">{{ __('website.profile_id_Number') }}:</label>
						<input type="text" name="idnumber" class="id" value="{{ $user->IDnumber }}">
						<input type="hidden" name="birthday" class="birthday_picker" value="{{ $user->birthday }}">
					</div>
					<div class="col-lg-5 col-md-12">
						<label class="ui_sttl block">{{ __('website.profile_Mail') }}:</label>
						<input type="text" name="email" class="ml" value="{{ $user->email }}" disabled>
						<label class="ui_sttl block">{{ __('website.profile_number') }}:</label>
						<input type="text" name="mobile" class="phn" value="{{ $user->mobile }}">
						<div class="change_photo">
							<label class="ui_sttl block">{{ __('website.profile_Photo') }}:</label>
							<div class="photo">
								<img class="u-avatar" src="{{ asset("/main/img/$user->img") }}">
							</div>
							<input class="none up_photo" name="img" onchange="previewFile()" type="file" accept="image/*">
							<button class="authBtn block">{{ __('website.profile_photo') }}</button>
						</div>

					</div>
				</div>
				<div class="flex">

					<button type="submit" class="authBtn block m-20-t">{{ __('website.profile_save') }}</button>
				</div>
				@if(Session::has('errors'))
					<div class="alert alert-danger" style="width: 50%;">
						<ol>
						@foreach(Session::get('errors') as $err)
							<li>{{ $err }}</li>
						@endforeach
						</ol>
					</div>
				@endif

				@if(Session::has('success'))
					<div class="alert alert-success" style="width: 50%;">
						{{ __('website.profile_success') }}
					</div>
				@endif
			</form>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		$(function(){
			changePhoto();
			$('[data-activator=profile]').addClass('active');
		});

		
		var birthday = $('.birthday_picker').val();
		function showDate(){
			birthday = birthday.split('-');
			$('.my_birthday[data-picker=day] option[value='+birthday[0]+']').attr("selected", true);
			$('.my_birthday[data-picker=month] option[value='+birthday[1]+']').attr("selected", true);
			$('.my_birthday[data-picker=year] option[value='+birthday[2]+']').attr("selected", true);
		}
		showDate();


		function getDate(){
			$('.my_birthday').change(function(){
				$('.my_birthday').each(function(key,item){
					var val = $('.my_birthday').eq(key).val();
					var attr = $('.my_birthday').eq(key).attr('data-picker');
					if(val === null)
						return;
					
					if(attr == 'year')
						birthday[2] = val;
					else if(attr == 'month')
						birthday[1] = val;
					else if(attr == 'day')
						birthday[0] = val;

					$('[name=birthday]').val(birthday.join('-'));
				});
			});
		}
		getDate();
	</script>
@endsection


