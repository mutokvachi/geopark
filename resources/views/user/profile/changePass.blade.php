@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarProfile')
@endsection

@section('content')
	<div class="ui_container">
		@include('user.layouts.profile.sideNav')
		<div class="content-md">
			<h2 class="ui_ttl">{{ __('website.change_pass_title') }}</h2>
			<form method="post" action="{{ route('changePass', ['lang' => App::getLocale()]) }}">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-lg-5 col-md-12">
						<label class="ui_sttl block">{{ __('website.change_pass_current') }}:</label>
						<input type="password" name="password" class="pss">
						<label class="ui_sttl block">{{ __('website.change_pass_new') }}:</label>
						<input type="password" name="new_password" class="pss">
					</div>
					<div class="col-lg-5 col-md-12">
						<label class="ui_sttl block">{{ __('website.change_pass_confirm') }}:</label>
						<input type="password" name="new_password_confirmation" class="pss">
					</div>
				</div>
				<div class="flex">
					<button type="submit" class="authBtn block m-20-t">{{ __('website.change_pass_save') }}</button>
				</div>

				@if(Session::has('errors'))
					<div class="alert alert-danger" style="width: 50%;">
						<ol>
						@foreach(Session::get('errors') as $err)
							<li>{{ $err }}</li>
						@endforeach
						</ol>
					</div>
				@endif

				@if(Session::has('success'))
					<div class="alert alert-success" style="width: 50%;">
						{{ __('website.change_pass_success') }}
					</div>
				@endif

				@if(Session::has('fail'))
					<div class="alert alert-danger" style="width: 50%;">
						{{ __('website.change_pass_fail') }}
					</div>
				@endif
			</form>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		$(function(){
			$('[data-activator=password]').addClass('active');
		});
	</script>
@endsection


