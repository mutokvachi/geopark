@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarProfile')
@endsection

@section('content')
	<div class="ui_container">
		@include('user.layouts.profile.sideNav')
		<div class="content-md m_memory_page">
			<h2 class="ui_ttl">{{ __('website.memories_my_memory') }}</h2>
			<a href="{{ route("memoriesAdd", ['lang' => App::getLocale()]) }}" class="w_new_memory" style="width: auto;">{{ __('website.memories_new_memory') }}</a>
			<div class="clearfix"></div>
			<div class="memories_feed my_mem">
				<div class="row">
					@if(count($memories) == 0)
						<h4 class="text-danger">{{ __('website.memories_empty') }}</h4>
					@endif
					@foreach($memories as $memory)
					<div class="col-lg-6 col-md-12">
						<a href="{{ route('memoriesEditView', ['lang' => App::getLocale(), 'id' => $memory->id]) }}">
							<div class="m_cont">
								<div class="photo">
									<?php $main_img = json_decode($memory->images)[0]; ?>
									<img src="{{ asset("/main/img/$main_img") }}">
								</div>
								<div class="comment t-16-px">
									<h4 class="w-title-ex t-30-px">{{ $memory->title }}</h4>
									<div class="status m-5-b verlagB t-cp p-15-l">
										<span class="t-blck m-5-r">{{ __('website.memories_my_status_') }}:</span>
										@if($memory->status == 0)
											<span class="rejected"  style="color: darkorange;">{{ __('website.memories_pending') }}</span>
										@elseif($memory->status == 1)
											<span class="rejected" style="color: darkgreen;">{{ __('website.memories_active') }}</span>
										@else
											<span class="rejected">{{ __('website.memories_rejected') }}</span>
										@endif
									</div>
									<div class="published t-blck m-5-b verlagB p-15-l">
										<span class="m-5-r">{{ __('website.memories_publish') }}:</span>
										<?php 
											$birthday = explode('-', $memory->date);
											$month = strtolower($birthday[1]);
										?>
										<span>{{ $birthday[0].'-'.__("website.profile_".$month).'-'.$birthday[2] }}</span>
									</div>
								</div>
							</div>
						</a>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		
		$(function(){
			$('[data-activator=memories]').addClass('active');
		});
	</script>
@endsection


