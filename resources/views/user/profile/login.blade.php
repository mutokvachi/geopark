@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarProfile')
@endsection

@section('content')
	<div class="ui_container">
		<div class="termsBg"></div>
		<div class="terms_popup">
			<span class="cls">×</span>
			<p>ტესტ ტექსტი.</p>
		</div>
		<div class="reg_log">
			<div class="l_nav">
				<ul>
					<li><a href="#login" class="logiBtn active" data-show="{{ __('website.login_log_in') }}">{{ __('website.login_log_in') }}</a></li>
					<li><a href="#register" class="logiBtn" data-show="{{ __('website.login_register') }}">{{ __('website.login_register') }}</a></li>
				</ul>
			</div>
			<h2 class="ui_ttl">{{ __('website.login_login') }}</h2>
			<div class="content">
				<div class="loginDiv log_in" data-show="{{ __('website.login_log_in') }}">
					<form class="b_rght" method="post" action="{{ route('authenticate', ['lang' => App::getLocale()]) }}">
						{{ csrf_field() }}
						<label class="ui_sttl block">{{ __('website.login_email') }}:</label>
						<input type="text" name="email" class="ml">
						<label class="ui_sttl block">{{ __('website.login_password') }}:</label>
						<input type="password" name="password" class="pss">
						<div class="ch_list">
						 	{{-- <input type="checkbox" name="" id="ui-checked"> --}}
					    	{{-- <label class="ui_sttl" for="ui-checked">Save Password</label> --}}
							@if(Session::has('error'))
								<div class="alert alert-danger">
									<ul>
										<li>{{ Session::get('error') }}</li>
									</ul>
								</div>
							@endif
						</div>
						<div class="flex">
							<button type="submit" class="authBtn block">{{ __('website.login_log_in') }}</button>
							<div class="recover">
								<p class="ui_sttl m-5-b">{{ __('website.login_forgot') }}</p>
								<a href="#" class="rec_pass">{{ __('website.login_recover') }}</a>
							</div>
						</div>
					</form>
				</div>
				<div class="loginDiv reg_in" data-show="{{ __('website.login_register') }}">
					<form class="b_rght" method="post" action="{{ route('register',['lang' => App::getLocale()]) }}" >
						{{ csrf_field() }}
						<label class="ui_sttl block">{{ __('website.login_name') }}:</label>
						<input type="text" name="name" class="nm" required value="{{ old('name') }}">
						<label class="ui_sttl block">{{ __('website.login_email') }}:</label>
						<input type="text" name="email" class="ml" required value="{{ old('email') }}">
						<label class="ui_sttl block">{{ __('website.login_password') }}:</label>
						<input type="password" name="password" class="pss" required >
						<label class="ui_sttl block">{{ __('website.login_confirm') }}:</label>
						<input type="password" name="password_confirmation" class="pss" required>
						{{--<div class="ch_list">
						 	<input type="checkbox" name="terms" id="ui-agree">
					    	<label class="ui_sttl" for="ui-agree">{{ __('website.login_agree') }} <a href="#" class="termPopup t-blck t-u-line">{{ __('website.login_terms') }}</a></label>
						</div> --}}


						<input type="hidden" name="lang" value="{{ App::getLocale() }}">
						<div class="captcha">
						</div>
						@if(Session::has('errors'))
							<div class="alert alert-danger">
								<ol>
								@foreach(Session::get('errors') as $err)
									<li>{{ $err }}</li>
								@endforeach
								</ol>
							</div>
						@endif
						<div class="flex">	
							<button type="submit" class="authBtn block">{{ __('website.login_register') }}</button>
						</div>
					</form>
				</div>
			</div>
			<div class="alternate_login">
				<a href="{{ route('facebookAuth') }}" class="fb_log block">{{ __('website.login_fb_onnect') }}</a>
				<a href="{{ route('googleAuth') }}" class="googl_log block">{{ __('website.login_google_connect') }}</a>
				<a href='{{ route('subscribe', ['lang'=>App::getLocale()]) }}' class="subsBtn">{{ __('website.subscribe_btn_subscribe') }}</a>
			</div>
		</div>
	</div>

@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function(){
			userLeftNav();
			var hash = window.location.hash.substr(1);
			if(hash.length > 0)
				$('.l_nav li a[data-show='+hash+']').click();
		});
		termsPopup();
	</script>
@endsection


