@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarProfile')
@endsection

@section('content')
	<div class="ui_container">
		@include('user.layouts.profile.sideNav')
		<div class="content-lg">
			<h2 class="ui_ttl">{{ __('website.profile_view_profile') }}</h2>
			<div class="row">
				<div class="col-lg-3 col-sm-6 m-20-b">
					<h4 class="ui_sttl">{{ __('website.profile_view_name') }}:</h4>
					<p class="verlagB t-cp">{{ $user->name }}</p>
				</div>
				<div class="col-lg-3 col-sm-6 m-20-b">
					<h4 class="ui_sttl">{{ __('website.profile_view_email') }}:</h4>
					<p class="verlagB t-cp">{{ $user->email }}</p>
				</div>
				<div class="col-lg-3 col-sm-6 m-20-b">
					<h4 class="ui_sttl">{{ __('website.profile_view_number') }}:</h4>
					<p class="verlagB t-cp"><span class="call-index">{{ $user->mobile }}</p>
				</div>
				<div class="col-lg-3 col-sm-6 m-20-b">
					<h4 class="ui_sttl">{{ __('website.profile_view_date') }}:</h4>
					<p class="verlagB t-cp">
						<?php 
							$birthday = explode('-', $user->birthday);
							$month = strtolower($birthday[1]);
						?>
						@if($birthday[0] == 0 || $birthday[0] == 0 || $birthday[2] == 0)
							<span>{{ __('website.profile_view_empty') }}</span>
						@else
							<span>{{ $birthday[0].'-'.__("website.profile_".$month).'-'.$birthday[2] }}</span>
						@endif
					</p>
				</div>
				<div class="col-lg-3 col-sm-6 m-20-b">
					<h4 class="ui_sttl">{{ __('website.profile_view_IDnumber') }}:</h4>
					<p class="verlagB t-cp">{{ $user->IDnumber }}</p>
				</div>
			</div>
			<div class="flex">
				<a href="{{ route('profile', ['lang' => App::getLocale()]) }}" class="authBtn block m-20-t">{{ __('website.profile_view_edit_profile') }}</a>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		$(function(){
			$('[data-activator=profile]').addClass('active');
		});
	</script>
@endsection


