@extends('user.layouts.master')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/summernote.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/main/css/bootstrap-tagsinput.css') }}">
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarProfile')
@endsection

@section('content')
	<div class="ui_container">
		@include('user.layouts.profile.sideNav')
		<div class="content-md">
			<h2 class="ui_ttl">{{ __('website.memories_memories') }}</h2>
			<form method="post" action="{{ route('memoriesAdder', ['lang' => App::getLocale()]) }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="mem_opt">
					<label class="ui_sttl block">{{ __('website.memories_Park') }}:</label>
					<select class="day" name="park_id">
						@foreach($parks as $park)
							<option value="{{ $park->id }}">{{ Stuff::trans($park, 'name') }}</option>
						@endforeach
					</select>
					<input type="hidden" class="redirect_hash" name="redirect_hash">
					<label class="ui_sttl block">{{ __('website.memories_title') }}:</label>
					<input type="text" name="title" value="{{ old('title') }}" class="nm">
					<label class="ui_sttl block">{{ __('website.memories_Type') }}:</label>
					<div class="rd_btns">
						<label class="relative m-15-r active" data-show="bp">{{ __('website.memories_blog_post') }}
							<input type="radio" checked="checked" name="radio">
							<span class="bullet"></span>
						</label>
						<label class="relative m-15-r" data-show="pg">{{ __('website.memories_photo_gallery') }}
							<input type="radio" name="radio">
							<span class="bullet"></span>
						</label>
						<label class="relative m-15-r" data-show="vg">{{ __('website.memories_video_gallery') }}
							<input type="radio" name="radio">
							<span class="bullet"></span>
						</label>
					</div>

					<div class="postBox blog_post active" data-show="bp">
						<label class="ui_sttl block">{{ __('website.memories_Memory') }}:</label>
						<textarea class="wr_memory summernote" name="blog">{{ old('blog') }}</textarea>
						<label class="ui_sttl block">{{ __('website.memories_Photo') }}:</label>
						<div class="chs_file">
							<div class="img_placeholder"></div>
							<input type="file" data-upload="post-tumb" name="blog_photos[]" accept="image/*">
							<a href="#" class="authBtn block" data-upload="post-tumb">{{ __('website.memories_choose') }}</a>
						</div>
						<label class="ui_sttl block">{{ __('website.memories_Tags') }}:</label>
						<div class="tag_container">
							<input type="text" name="blog_tags" value="{{ old('blog_tags') }}" data-role="tagsinput">
						</div>
						<div class="flex">
							<button type="submit" name="blog_submitter" class="authBtn block m-20-t">{{ __('website.memories_post') }}</button>
						</div>
					</div>

					<div class="postBox photo_gallery" data-show="pg">
						<label class="ui_sttl block">{{ __('website.memories_multiple_photos') }}:</label>
						<div class="chs_file">
							<div class="img_placeholder"></div>
							<input type="file" data-upload="photo-tumb" name="gallery_photos[]" multiple accept="image/*">
							<a href="#" class="authBtn block" data-upload="photo-tumb">{{ __('website.memories_choose') }}</a>
						</div>
						<label class="ui_sttl block">{{ __('website.memories_Tags') }}:</label>
						<div class="tag_container">
							<input type="text" value="{{ old('gallery_tags') }}" data-role="tagsinput" name="gallery_tags">
						</div>
						<div class="flex">
							<button type="submit" name="gallery_submitter" class="authBtn block m-20-t">{{ __('website.memories_post') }}</button>
						</div>
					</div>

					<div class="postBox video_gallery clearfix" data-show="vg">
						<label class="ui_sttl block">{{ __('website.memories_Link') }}:</label>
						<div class="add-video-links">
							<input type="text" name="video_links[]" class="nm pull-left">
						</div>
						<a href="#" class="authBtn block addVideo">{{ __('website.memories_more') }}</a>
						<div class="clearfix"></div>
						<label class="ui_sttl block">{{ __('website.memories_Photo') }}:</label>
						<div class="chs_file">
							<div class="img_placeholder"></div>
							<input type="file" data-upload="video-tumb" name="video_photos[]" accept="image/*">
							<a href="#" class="authBtn block" data-upload="video-tumb">{{ __('website.memories_choose') }}</a>
						</div>
						<label class="ui_sttl block">{{ __('website.memories_Tags') }}:</label>
						<div class="tag_container">
							<input type="text" data-role="tagsinput" value="{{ old('video_tags') }}" name="video_tags">
						</div>
						<div class="flex">
							<button type="submit" name="video_submitter" class="authBtn block m-20-t">{{ __('website.memories_post') }}</button>
						</div>
					</div>
				</div>
				@if(Session::has('errors'))
					<div class="alert alert-danger">
						<ol>
						@foreach(Session::get('errors') as $err)
							<li>{{ $err }}</li>
						@endforeach
						</ol>
					</div>
				@endif

				@if(Session::has('success'))
					<div class="alert alert-success">
						{{ Session::get('success') }}
					</div>
				@endif
			</form>
		</div>
	</div>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript" src="{{ asset('/main/js/summernote.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/main/js/bootstrap-tagsinput.js') }}"></script>
	<script type="text/javascript">
		function hashChanger(){
			var hash = window.location.hash.substr(1);
			if(hash.length > 0){
				$('.rd_btns label[data-show='+hash+']').click();
				$('.redirect_hash').val(hash);
			}

			$('.rd_btns label').click(function(){
				var attr = $(this).attr('data-show');
				window.location.hash = attr;
				$('.redirect_hash').val(attr);
			});
		}

		$(function(){
			addPost();
			summerNote();
			changePhoto();
			imgNamePlaeHolder();
			$('[data-activator=memories]').addClass('active');
			hashChanger();
		});


	</script>
@endsection


