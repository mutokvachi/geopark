@extends('user.layouts.master')

@section('head')
	@parent
@endsection

@section('header')
	@parent
@endsection

@section('navbar')
	@include('user.layouts.navbar.navbarProfile')
@endsection

@section('content')
	<div class="ui_container">
		@include('user.layouts.profile.sideNav')
		<div class="content-md favorites_page">
			<h2 class="ui_ttl">{{ __('website.favourite_favorities') }}</h2>
			<ul class="f_sort">
				<li data-id="all-section">{{ __('website.favourite_all') }}</li>
				<li data-id="memories-section">{{ __('website.favourite_memories') }}</li>
				<li data-id="around-park-section">{{ __('website.favourite_around') }}</li>
				<li data-id="trips-section">{{ __('website.favourite_trips') }}</li>
				<li data-id="to-see">{{ __('website.favourite_toSee') }}</li>
			</ul>
			<div class="clearfix"></div>
			<div class="f_nav"></div>

			<div class="f_content" data-id="all-section">
				<h1>{{ __('website.favourite_all') }}</h1>
				<div class="trips p-0">
					@if(isset($my_favours['tripsInner']))
						@foreach($my_favours['tripsInner'] as $aroundTrip)
							
						<div class="row">
							<div class="col-lg-6 col-md-12 m-20-b">
								<div class="photo_map">
									<div class="photo">
										<img src="{{ Stuff::imgUrl(json_decode($aroundTrip->trip->image)[0], true) }}">
									</div>
									<div class="trip_maps">
										<div class="map" data-lat="{{ $aroundTrip->trip->coordinate_lat }}" data-lng="{{ $aroundTrip->trip->coordinate_lng }}"></div>
									</div>
								</div>
								<div class="info_box">
									<div class="name">
										<h2 class="w-title t-30-px">
											<a href="{{ route('siteTripsInner', ['lang'=>App::getLocale(), 'park'=>$aroundTrip->trip->park->alias, 'id'=>$aroundTrip->trip->id ]) }}">

												{{ Stuff::trans($aroundTrip->trip, 'name') }}
											</a>
										</h2>
									</div>
									<div class="details">
										<div class="duration">
											<span class="cuant block">{{ $aroundTrip->trip->trip_days }}</span>
											<span class="days block">{{ __('website.trip_days') }}</span>
										</div>
										<div class="events">
											<?php 
												$highlights = explode(',', Stuff::trans($aroundTrip->trip, 'highlights'));
											?>
											@foreach($highlights as $highlight)
												<span class="block">{{ $highlight }}</span>
											@endforeach
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					@endif

					@if(isset($my_favours['memoriesInner']))
						<ul>
							@foreach($my_favours['memoriesInner'] as $favour)
								<li>
									<a href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>$favour->memory->park->alias, 'id'=>$favour->memory->id ]) }}" style="color: #222;">
										{{ Stuff::trans($favour->memory, 'title') }}
									</a>
								</li>
							@endforeach
						</ul>
					@endif

					@if(isset($my_favours['aroundInner']))
						@foreach($my_favours['aroundInner'] as $aroundFavour)
							<li>
								<a href="{{ route('siteAroundInner', ['lang'=>App::getLocale(), 'park'=>$aroundFavour->aroundPark->park->alias, 'id'=>$aroundFavour->aroundPark->id ]) }}" style="color: #222;">
									{{ Stuff::trans($aroundFavour->aroundPark, 'name') }}
								</a>
							</li>
						@endforeach
					@endif

					@if(isset($my_favours['toSeeInner']))
						@foreach($my_favours['toSeeInner'] as $aroundFavour)
							<li>
								<a href="{{ route('siteToSeeInner', ['lang'=>App::getLocale(), 'park'=>$aroundFavour->toSee->park->alias, 'id'=>$aroundFavour->toSee->id ]) }}" style="color: #222;">
									{{ Stuff::trans($aroundFavour->toSee, 'name') }}
								</a>
							</li>
						@endforeach
					@endif	
				</div>
			</div>

			<div class="f_content" data-id="trips-section">
				<h1>{{ __('website.favourite_trips') }}</h1>
				<div class="trips p-0">
					@if(isset($my_favours['tripsInner']))
						@foreach($my_favours['tripsInner'] as $aroundTrip)
							
						<div class="row">
							<div class="col-lg-6 col-md-12 m-20-b">
								<div class="photo_map">
									<div class="photo">
										<img src="{{ Stuff::imgUrl(json_decode($aroundTrip->trip->image)[0], true) }}">
									</div>
									<div class="trip_maps">
										<div class="map" data-lat="{{ $aroundTrip->trip->coordinate_lat }}" data-lng="{{ $aroundTrip->trip->coordinate_lng }}"></div>
									</div>
								</div>
								<div class="info_box">
									<div class="name">
										<h2 class="w-title t-30-px">
											<a href="{{ route('siteTripsInner', ['lang'=>App::getLocale(), 'park'=>$aroundTrip->trip->park->alias, 'id'=>$aroundTrip->trip->id ]) }}">

												{{ Stuff::trans($aroundTrip->trip, 'name') }}
											</a>
										</h2>
									</div>
									<div class="details">
										<div class="duration">
											<span class="cuant block">{{ $aroundTrip->trip->trip_days }}</span>
											<span class="days block">{{ __('website.trip_days') }}</span>
										</div>
										<div class="events">
											<?php 
												$highlights = explode(',', Stuff::trans($aroundTrip->trip, 'highlights'));
											?>
											@foreach($highlights as $highlight)
												<span class="block">{{ $highlight }}</span>
											@endforeach
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					@else				
						<br>
						<h4>{{ __('website.favourite_empty') }}</h4>
					@endif
				</div>
			</div>
			<div class="f_content" data-id="memories-section">
				<h1>{{ __('website.favourite_memories') }}</h1>
				@if(isset($my_favours['memoriesInner']))
					<ul>
						@foreach($my_favours['memoriesInner'] as $favour)
							<li>
								<a href="{{ route('siteMemoriesInnerView', ['lang'=>App::getLocale(), 'park'=>$favour->memory->park->alias, 'id'=>$favour->memory->id ]) }}" style="color: #222;">
									{{ Stuff::trans($favour->memory, 'title') }}
								</a>
							</li>
						@endforeach
					</ul>
				@else
					<br>
					<h4>{{ __('website.favourite_empty') }}</h4>
				@endif
			</div>
			<div class="f_content" data-id="around-park-section">
				<h1>{{ __('website.favourite_around') }}</h1>
				@if(isset($my_favours['aroundInner']))
					@foreach($my_favours['aroundInner'] as $aroundFavour)
						<li>
							<a href="{{ route('siteAroundInner', ['lang'=>App::getLocale(), 'park'=>$aroundFavour->aroundPark->park->alias, 'id'=>$aroundFavour->aroundPark->id ]) }}" style="color: #222;">
								{{ Stuff::trans($aroundFavour->aroundPark, 'name') }}
							</a>
						</li>
					@endforeach
				@else				
					<br>
					<h4>{{ __('website.favourite_empty') }}</h4>
				@endif
			</div>

			<div class="f_content" data-id="to-see">
				<h1>{{ __('website.favourite_toSee') }}</h1>
				@if(isset($my_favours['toSeeInner']))
					@foreach($my_favours['toSeeInner'] as $aroundFavour)
						<li>
							<a href="{{ route('siteToSeeInner', ['lang'=>App::getLocale(), 'park'=>$aroundFavour->toSee->park->alias, 'id'=>$aroundFavour->toSee->id ]) }}" style="color: #222;">
								{{ Stuff::trans($aroundFavour->toSee, 'name') }}
							</a>
						</li>
					@endforeach
				@else				
					<br>
					<h4>{{ __('website.favourite_empty') }}</h4>
				@endif
			</div>
		</div>
	</div>
@endsection

@section('js_sources')
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGYLxQnrVG08qL8oYZ4H7dVOWDk18Q1Ek&callback=initMap">
    </script>
@endsection

@section('footer')
	@parent
@endsection

@section('javascript')
	<script type="text/javascript">
		$(function(){
			favoriteSort();
			$('[data-activator=favorities]').addClass('active');
			$('.f_sort li').eq(0).click();
		});

		function initMap() {
    		$('.map').gMap(function(element, lat, lng){
    			lat = JSON.parse(lat);
    			lng = JSON.parse(lng)
    			var position = {lat: lat, lng: lng};
    			var map = new google.maps.Map(element,{
    				zoom: 5,
			        center: position,
			        scrollwheel: false,
			        gestureHandling: 'greedy',
			        disableDefaultUI: true,
			        mapTypeId: 'satellite'
    			});
    			var marker = new google.maps.Marker({
    				position : position,
    				map:map,
    			});
    		});
		}
	</script>
@endsection


